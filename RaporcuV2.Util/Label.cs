﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaporcuV2.Util
{
    public static class Labels
    {
        #region Base
        public const string createdDate = "Ekleme Tarihi";
        public const string createdUser = "Ekleyen";
        public const string UpdatedDate = "Değiştirme Tarihi";
        public const string UpdatedUser = "Değiştiren";
        #endregion

        #region User
        public const string userUserName = "Kullanıcı Adı";
        public const string userPassword = "Şifre";
        public const string userFullName = "Tam Adı";
        public const string userEmail = "Email";

        #endregion
        
        #region Permission
        public const string permissionTag = "Tag No";
        public const string permissionName = "Yetki Tanımı";

        #endregion

        #region Custom Report

        public const string customReportName = "Özel Rapor Adı";
        public const string customReportSql = "Sql Sorgusu";
        public const string customReportTag = "Rapor Tag";

        #endregion

        public const string Ocak = "Ocak";
        public const string Subat = "Şubat";
        public const string Mart = "Mart";
        public const string Nisan = "Nisan";
        public const string Mayis = "Mayıs";
        public const string Haziran = "Haziran";
        public const string Temmuz = "Temmuz";
        public const string Agustos = "Ağustos";
        public const string Eylul = "Eylül";
        public const string Ekim = "Ekim";
        public const string Kasim = "Kasim";
        public const string Aralik = "Aralık";
        public const string Yil = "Yıl    ";
        public const string HizmetKodu = "Hizmet Kodu";
        public const string HizmetAciklamasi = "Hizmet Açıklaması";

        public const string Hizmet_CASHAMNT1 = "Ocak";
        public const string Hizmet_CASHAMNT2 = "Şubat";
        public const string Hizmet_CASHAMNT3 = "Mart";
        public const string Hizmet_CASHAMNT4 = "Nisan";
        public const string Hizmet_CASHAMNT5 = "Mayıs";
        public const string Hizmet_CASHAMNT6 = "Haziran";
        public const string Hizmet_CASHAMNT7 = "Temmuz";
        public const string Hizmet_CASHAMNT8 = "Ağustos";
        public const string Hizmet_CASHAMNT9 = "Eylül";
        public const string Hizmet_CASHAMNT10 = "Ekim";
        public const string Hizmet_CASHAMNT11 = "Kasım";
        public const string Hizmet_CASHAMNT12 = "Aralık";
        public const string KasaAdi = "KasaAdi";
        public const string KasaKodu = "KasaKodu";
        public const string CariHesapKodu = "Cari Hesap Kodu";
        public const string CariBanka = "Cari / Banka";
        public const string CariHesapUnvani = "Cari Hesap Ünvanı";
        public const string CariAlacak = "Cari Alacak";
        public const string Sehir = "Şehir";
        public const string Ilce = "İlçe";
        public const string CariBorc = "Cari Borç";
        public const string CariBakiye = "Cari Bakiye";
        public const string MuhasebeAlacak = "Muhasebe Alacak";
        public const string MuhasebeBorc =   "Muhasebe Borç";
        public const string MuhasebeBakiye = "Muhasebe Bakiye";
        public const string CariMuhasebeFark = "Fark";
        public const string EvrakTuru = "Evrak Türü";
        public const string BankaKodu = "Banka Kodu";
        public const string BankaUnvani = "Banka Ünvanı";
        public const string BankaHesapKodu = "Hesap Kodu";
        public const string BankaHesapUnvani = "Hesap Ünvanı";
        public const string Banka = "Banka";
        public const string StokKodu = "Stok Kodu";
        public const string StokAdi = "Stok Adı";
        public const string StokAdi2 = "Stok Adı2";
        public const string StokBirimKodu = "Birim Kodu";
        public const string StokBirimAdi = "Birim Adı";
        public const string StokMiktari = "Stok Miktarı";
        public const string StokOzelKod = "Stok Özel Kod";
        public const string TeminMiktari = "Temin Miktarı";
        public const string OzelKod = "Özel Kod";
        public const string CariOzelKod = "Cari Özel Kod";
        public const string FaturaOzelKod = "Fatura Özel Kod";
        public const string FaturaAciklama = "Fatura Açıklama";
        public const string OzelKod2 = "Özel Kod2";
        public const string HareketOzelKod2 = "Hareket Özel Kod2";
        public const string HareketOzelKod = "Hareket Özel Kod";
        public const string HizmetOzelKod = "Hizmet Özel Kod";
        public const string CariOzelKod2 = "Cari Özel Kod2";
        public const string YetkiKod = "Yetki Kodu";
        public const string FatYetkiKod = "Fatura Yetki Kodu";
        public const string CariYetkiKod = "Cari Yetki Kodu";
        public const string TelNo = "Tel No1";
        public const string CariIlgili = "İlgili";
        public const string Email = "E-Mail";
        public const string GrupKodu = "Grup Kodu";
        public const string GrupKoduAciklama="Grup Kodu Açıklama";
        public const string MarkaKodu = "Marka Kodu";
        public const string ProjeKodu = "Proje Kodu";
        public const string ProjeAdi = "Proje Adı";
        public const string SatisElemaniKodu = "Satış Elemanı Kodu";
        public const string SatisElemaniAdi = "Satış Elemanı Adı";

        public const string SiparisTarihi = "Sipariş Tarihi";
        public const string SiparisMiktar = "Sipariş Miktar";
        public const string SiparisBekleyenMiktar = "Sipariş Bekleyen Miktar";
        public const string SiparisFisNo = "Sipariş Fiş No";
        public const string Tarih = "Tarih";
        public const string FisTarihi = "Fiş Tarihi";
        public const string FisAyi = "Fiş Ayı";
        public const string FisHaftasi = "Fiş Haftası";
        public const string IslemTarihi = "İşlem Tarihi";
        public const string VadeAyi = "Vade Ayı";
        public const string VadeYili = "Vade Yılı";
        public const string VadeTarihi = "Vade Tarihi";
        public const string VadeGunu = "Vade Günü";
        public const string Gun = "Gün";
        public const string Donem = "Dönem";
        public const string Kur = "Kur";
        public const string FisNo = "Fiş No";
        public const string FisRef = "Fiş Ref";
        public const string BelgeNo = "Belge No";
        public const string FaturaNo = "Fatura No";
        public const string FaturaTarihi = "Fatura Tarihi";
        public const string OdemeTarihi = "Ödeme Tarihi";
        public const string FisTuru = "Fiş Türü";
        public const string IslemTuru = "İşlem Türü";
        public const string Aciklama = "Açıklama";
        public const string IslemAciklama = "İşlem Açıklaması";
        public const string KrediKodu = "Kredi Kodu";
        public const string KrediAciklamasi = "Kredi Açıklaması";
        public const string KrediAnaPara = "Anapara";
        public const string KrediFaiz = "Faiz";
        public const string KrediToplam = "Kredi Toplamı";
        public const string CekSenetSayi = "Ç/S Say.";
        public const string CekSayi = "Sayı";
        public const string TL = "TL";
        public const string USD = "USD";
        public const string EURO = "EURO";
        public const string DunDevir = "Dün.Devir";
        public const string YarinDevir = "Yarına Devir";
        public const string Aciklama1 = "Açıklama 1";
        public const string DovizTipi = "Döviz Tipi";
        public const string Miktar = "Miktar";
        public const string BirimFiyat = "Birim Fiyat";
        public const string Odenen = "Ödenen";
        public const string BorcDoviz = "Borç Döviz";
        public const string AlacakDoviz = "Alacak Döviz";
        public const string Borc = "Borç";
        public const string Alacak = "Alacak";
        public const string Bakiye = "Bakiye";
        public const string BorcAlacak = "Borç / Alacak";
        public const string BorcBakiye = "Borç Bakiye";
        public const string AlacakBakiye = "Alacak Bakiye";
        public const string Tutar = "Tutar";
        public const string ToplamSatisTutari = "Top.Sat.Tutarı";
        public const string SsKar = "S.S.Kar";
        public const string SonSKarOrani = "Son S.Kar Oranı";
        public const string OrtBirimFiyat = "Ort.B.Fiyat";
        public const string OrtalamaKar = "Ort.Kar";
        public const string OrtalamaKarOrani = "Ort.Kar Oranı";
        public const string SonSatinalmaBirimFiyat = "S.S.B.Fiyat";
        public const string NetSatirToplami = "Net Satır Toplamı";
        public const string Indirim = "İndirim";
        public const string Kdv = "Kdv";
        public const string KdvTutari = "Kdv Tutarı";
        public const string KdvsizTutar = "Kdv.siz Tutar";
        public const string KdvliTutarTl = "Kdv.li Tutar TL";
        public const string KdvliTutarId = "Kdv.li Tutar ID";
        public const string DovizTutari = "Döviz Tutarı";
        public const string Fiyati = "Fiyatı";
        public const string Devir = "Devir";
        public const string SatiTutari = "Satır Tutarı";
        public const string Masraflar = "Masraflar";
        public const string Iskonto = "İskonto";
        public const string KdvMatrahi = "Kdv Matrahı";
        public const string Status = "Status";
        public const string KapanmaDurumu = "Kapanma Durumu";
        public const string Muhasebelesme = "Muhasebelesme";
        public const string SeriNo = "Seri No";
        public const string Giren = "Giren";
        public const string Cikan = "Çıkan    ";
        public const string KimeVerildigi = "Kime Verildiği";
        public const string BaslangicTarihi = "Başlangıç Tarihi";
        public const string BitisTarihi = "Bitiş Tarihi";
        public const string Portfoy = "Portföy No";
        public const string Bolum = "B.";

        public const string SessionId = "Oturum Id";
        public const string HostName = "Bilgisayar Adı";
        public const string LoginTime = "Oturum Açma Zamanı";
        public const string LastActivityTime = "Son İşlem Zamanı";

    }

}
