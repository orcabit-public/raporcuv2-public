﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaporcuV2.Util
{
    public static class DbConstants
    {
        public const int codeLenght = 50;
        public const int nameLenght = 100;
        public const int descriptionLenght = 250;
        public const string dateFormat = "{0:MM/dd/yyyy}"; // [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]

    }
}
