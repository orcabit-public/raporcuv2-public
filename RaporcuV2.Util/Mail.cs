﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace RaporcuV2.Util
{
    public class Mail
    {
        private string result = "ok";
        public string SendMail(MemoryStream ms,string reportName)
        {
            string mailFileName = $"{reportName}_{DateTime.Now.ToString("yyyyMMyyyyHHssmm")}.pdf";
            var memStream = ms;
            memStream.Position = 0;
            var contentType = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Application.Pdf);
            var reportAttachment = new Attachment(memStream, contentType);
            reportAttachment.ContentDisposition.FileName = mailFileName;
            //mailMessage.Attachments.Add(reportAttachment);

            string htmlBody = File.ReadAllText(@"./EmailTemplate.html");
            string subject = string.Format("{1} Raporu - {0} ", DateTime.Now.ToString("dd/MM/yyyy HH:ss:mm"), reportName);
            string messageString = null;
            string speCode = "";

            string detailMessage = "Raporcu Mail Bilgilendirme Servisi otomatik olarak oluşturulmuştur!";
            htmlBody = htmlBody.Replace("{MessageNote}", detailMessage);

            byte[] lData = Encoding.GetEncoding(1254).GetBytes(" " + detailMessage);
            List<object> parameterList = new List<object>();
            parameterList.Add(lData);


            MailMessage mail = new MailMessage();
            mail.Attachments.Add(reportAttachment);
            mail.IsBodyHtml = true;
            mail.From = new MailAddress("raporcu@orcabit.com");

            htmlBody = htmlBody.Replace("{MessageHeader}", string.Format("Rapor Tanımı : {0}",reportName))
                    .Replace("{MessageDetail}", DateTime.Now.ToString("dd/MM/yyyy HH:ss:mm"));

            mail.To.Add("otopcu@gmail.com");
            mail.To.Add("alierdem.ertas@orcabit.com");
            mail.Subject = subject;

            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);

            mail.AlternateViews.Add(alternateView);

            SmtpClient smtpClient = new SmtpClient
            {
                UseDefaultCredentials = true,
                Host = "mail.hostingvadisi.com",
                Port = Convert.ToInt32(587),
                EnableSsl = false,
                Credentials = new System.Net.NetworkCredential("raporcu@orcabit.com", "R@p0rcuV2")
            };
            try
            {
                smtpClient.Send(mail);
            }
            catch (Exception ex)
            {
                result = (ex.Message);
            }

            return result;
        }
    }
}
