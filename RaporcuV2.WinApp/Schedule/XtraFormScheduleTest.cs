﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using RaporcuV2.DataAccess;
using RaporcuV2.WinApp.Forms.CariHesap;
using RaporcuV2.WinApp.Forms.Customs;
using RaporcuV2.WinApp.Forms.Main;

namespace RaporcuV2.WinApp.Schedule
{
    public partial class XtraFormScheduleTest : DevExpress.XtraEditors.XtraForm
    {
        public XtraFormScheduleTest()
        {
            InitializeComponent();
            AddEvents();
        }

        private void AddEvents()
        {
            simpleButton1_Click(this,null);
        }

        private void AfterLoading(object sender, EventArgs e)
        {
            ShowPrintPreview(sender);
        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            RaporcuV2.DataAccess.RaporcuDbContext cnt = new RaporcuDbContext();
            List<Model.Database.Schedule> schedules = cnt.Schedules.ToList();
            foreach (Model.Database.Schedule schedule in schedules)
            {
                var _formName = (from t in System.Reflection.Assembly.GetExecutingAssembly().GetTypes()
                    where t.Name.Equals(schedule.FormName)
                    select t.FullName).Single();
                var _form = (XtraFormBase)Activator.CreateInstance(Type.GetType(_formName));
                if (_form != null)
                {
                    _form.IsSchedule = true;
                    _form.SqlTextCustom = schedule.ReportParameter;
                    _form.FirmName = schedule.FirmName;
                    _form.Schedule = schedule;
                    _form.ShowDialog();
                }
            }   
        }

        private void ShowPrintPreview(object sender)
        {
            XtraFormRiskBakiyeKontrol form = sender as XtraFormRiskBakiyeKontrol;
            BarManager manager = form.barManagerControl1.barManagerForm;
            foreach (BarItem barItem in manager.Items)
            {
                string barItemName = barItem.Caption;
                if (barItemName.Equals("Önizleme"))
                {
                    BarButtonItem buton = (BarButtonItem) barItem;
                    buton.PerformClick();
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            XtraFormMain frm = new XtraFormMain();
            frm.ShowDialog();
        }
    }
}