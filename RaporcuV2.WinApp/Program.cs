﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using Microsoft.Win32;
using RaporcuV2.DataAccess;
using RaporcuV2.WinApp.Forms.Customs;
using RaporcuV2.WinApp.Forms.Dashboard;
using RaporcuV2.WinApp.Forms.Kasa;
using RaporcuV2.WinApp.Forms.Main;
using RaporcuV2.WinApp.Forms.Muhasebe;
using RaporcuV2.WinApp.Schedule;

namespace RaporcuV2.WinApp
{
    static class Program
    {
        //private static Mutex _mutex = null;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //string appName = AppDomain.CurrentDomain.DomainManager.EntryAssembly.GetName().Name;
            //bool createdNew;

            //_mutex = new Mutex(true, appName, out createdNew);

            //if (!createdNew)
            //{
            //    //Uygulama zaten çalışıyor  ....
            //    return; 
            //}

            WindowsFormsSettings.ForceDirectXPaint();

            Application.ApplicationExit += new EventHandler(OnApplicationExit);
            Application.ThreadException += new ThreadExceptionEventHandler(OnThreadException);
            //Application.SetUnhandledExceptionMode(UnhandledExceptionMode.ThrowException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(OnUnhandledException);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("tr");
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("tr");

            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.UserSkins.BonusSkins.Register();

            try
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"Software", true);
                if (rk != null)
                {
                    RegistryKey subKey = rk.OpenSubKey("Raporcu");
                    try
                    {
                        string skinName = (string)subKey.GetValue("Skin");
                        if (string.IsNullOrEmpty(skinName))
                            skinName = "Metropolis";
                        DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = skinName;
                        UserLookAndFeel.Default.SetSkinStyle(skinName);
                        UserLookAndFeel.Default.UseDefaultLookAndFeel = true;
                        UserLookAndFeel.Default.Style = LookAndFeelStyle.Skin;

                    }
                    catch (Exception) { }
                }
            }
            catch (Exception) { }
            finally
            {
                UserLookAndFeel.Default.SetSkinStyle("Metropolis");
            }


            using (XtraFormLogin fLogin = new XtraFormLogin() { StartPosition = FormStartPosition.CenterScreen })
            {
                if (fLogin.ShowDialog() == DialogResult.OK)
                {
                    UserInfo.RemoveTimeoutedUserSession();
                    if (UserInfo.CheckUserSession())
                    {
                         Application.Run(new XtraFormMain());
                        //Application.Run(new XtraFormDashBoardDesigner());
                        //Application.Run(new XtraFormMaliTablolar());
                    }
                    else
                    {
                        XtraMessageBox.Show("Kullanıcı sayısı limiti aşılmıştır!", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        Application.Exit();
                    }
                }
                else
                {
                    Application.Exit();
                }
            }
        }
           
        private static void OnApplicationExit(object sender, EventArgs e)
        {
            UserInfo.RemoveUserSession();
        }

        private static void OnThreadException(object sender, ThreadExceptionEventArgs e)
        {
            UserInfo.RemoveUserSession();
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            UserInfo.RemoveUserSession();
        }
    }
}
