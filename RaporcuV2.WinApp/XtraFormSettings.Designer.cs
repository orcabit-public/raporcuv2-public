﻿namespace RaporcuV2.WinApp
{
    partial class XtraFormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraFormSettings));
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.FontSizeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.ServerNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dbSettingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DbNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.UserNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.UserPassTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FirmaTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForServerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDbName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPass = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserName1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFontSize = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditSerialNr = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FontSizeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServerNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbSettingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DbNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPassTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirmaTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDbName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFontSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSerialNr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.textEditSerialNr);
            this.dataLayoutControl1.Controls.Add(this.FontSizeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.simpleButtonSave);
            this.dataLayoutControl1.Controls.Add(this.simpleButtonCancel);
            this.dataLayoutControl1.Controls.Add(this.ServerNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DbNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.UserNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPassTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FirmaTextEdit);
            this.dataLayoutControl1.DataSource = this.dbSettingBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(499, 202);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // FontSizeTextEdit
            // 
            this.FontSizeTextEdit.Location = new System.Drawing.Point(326, 60);
            this.FontSizeTextEdit.Name = "FontSizeTextEdit";
            this.FontSizeTextEdit.Size = new System.Drawing.Size(161, 20);
            this.FontSizeTextEdit.StyleController = this.dataLayoutControl1;
            this.FontSizeTextEdit.TabIndex = 10;
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonSave.ImageOptions.Image")));
            this.simpleButtonSave.Location = new System.Drawing.Point(369, 152);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(118, 38);
            this.simpleButtonSave.StyleController = this.dataLayoutControl1;
            this.simpleButtonSave.TabIndex = 9;
            this.simpleButtonSave.Text = "Kaydet";
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonCancel.ImageOptions.Image")));
            this.simpleButtonCancel.Location = new System.Drawing.Point(257, 152);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(108, 38);
            this.simpleButtonCancel.StyleController = this.dataLayoutControl1;
            this.simpleButtonCancel.TabIndex = 8;
            this.simpleButtonCancel.Text = "İptal";
            // 
            // ServerNameTextEdit
            // 
            this.ServerNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dbSettingBindingSource, "ServerName", true));
            this.ServerNameTextEdit.Location = new System.Drawing.Point(87, 12);
            this.ServerNameTextEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ServerNameTextEdit.Name = "ServerNameTextEdit";
            this.ServerNameTextEdit.Size = new System.Drawing.Size(160, 20);
            this.ServerNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ServerNameTextEdit.TabIndex = 4;
            // 
            // dbSettingBindingSource
            // 
            this.dbSettingBindingSource.DataSource = typeof(RaporcuV2.Model.DbSetting);
            // 
            // DbNameTextEdit
            // 
            this.DbNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dbSettingBindingSource, "DbName", true));
            this.DbNameTextEdit.Location = new System.Drawing.Point(326, 12);
            this.DbNameTextEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DbNameTextEdit.Name = "DbNameTextEdit";
            this.DbNameTextEdit.Size = new System.Drawing.Size(161, 20);
            this.DbNameTextEdit.StyleController = this.dataLayoutControl1;
            this.DbNameTextEdit.TabIndex = 5;
            // 
            // UserNameTextEdit
            // 
            this.UserNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dbSettingBindingSource, "UserName", true));
            this.UserNameTextEdit.Location = new System.Drawing.Point(87, 36);
            this.UserNameTextEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.UserNameTextEdit.Name = "UserNameTextEdit";
            this.UserNameTextEdit.Size = new System.Drawing.Size(160, 20);
            this.UserNameTextEdit.StyleController = this.dataLayoutControl1;
            this.UserNameTextEdit.TabIndex = 6;
            // 
            // UserPassTextEdit
            // 
            this.UserPassTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.dbSettingBindingSource, "UserPass", true));
            this.UserPassTextEdit.Location = new System.Drawing.Point(326, 36);
            this.UserPassTextEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.UserPassTextEdit.Name = "UserPassTextEdit";
            this.UserPassTextEdit.Properties.PasswordChar = '*';
            this.UserPassTextEdit.Size = new System.Drawing.Size(161, 20);
            this.UserPassTextEdit.StyleController = this.dataLayoutControl1;
            this.UserPassTextEdit.TabIndex = 7;
            // 
            // FirmaTextEdit
            // 
            this.FirmaTextEdit.Location = new System.Drawing.Point(87, 60);
            this.FirmaTextEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FirmaTextEdit.Name = "FirmaTextEdit";
            this.FirmaTextEdit.Size = new System.Drawing.Size(160, 20);
            this.FirmaTextEdit.StyleController = this.dataLayoutControl1;
            this.FirmaTextEdit.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(499, 202);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForServerName,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.ItemForUserName1,
            this.layoutControlItem3,
            this.ItemForUserName,
            this.ItemForUserPass,
            this.ItemForDbName,
            this.ItemForFontSize,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup2.Size = new System.Drawing.Size(479, 182);
            // 
            // ItemForServerName
            // 
            this.ItemForServerName.Control = this.ServerNameTextEdit;
            this.ItemForServerName.Location = new System.Drawing.Point(0, 0);
            this.ItemForServerName.Name = "ItemForServerName";
            this.ItemForServerName.Size = new System.Drawing.Size(239, 24);
            this.ItemForServerName.Text = "Sunucı Adı";
            this.ItemForServerName.TextSize = new System.Drawing.Size(71, 13);
            // 
            // ItemForDbName
            // 
            this.ItemForDbName.Control = this.DbNameTextEdit;
            this.ItemForDbName.Location = new System.Drawing.Point(239, 0);
            this.ItemForDbName.Name = "ItemForDbName";
            this.ItemForDbName.Size = new System.Drawing.Size(240, 24);
            this.ItemForDbName.Text = "Database Adı";
            this.ItemForDbName.TextSize = new System.Drawing.Size(71, 13);
            // 
            // ItemForUserName
            // 
            this.ItemForUserName.Control = this.UserNameTextEdit;
            this.ItemForUserName.Location = new System.Drawing.Point(0, 24);
            this.ItemForUserName.Name = "ItemForUserName";
            this.ItemForUserName.Size = new System.Drawing.Size(239, 24);
            this.ItemForUserName.Text = "Kullanıcı Adı";
            this.ItemForUserName.TextSize = new System.Drawing.Size(71, 13);
            // 
            // ItemForUserPass
            // 
            this.ItemForUserPass.Control = this.UserPassTextEdit;
            this.ItemForUserPass.Location = new System.Drawing.Point(239, 24);
            this.ItemForUserPass.Name = "ItemForUserPass";
            this.ItemForUserPass.Size = new System.Drawing.Size(240, 24);
            this.ItemForUserPass.Text = "Şifre";
            this.ItemForUserPass.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButtonCancel;
            this.layoutControlItem1.Location = new System.Drawing.Point(245, 140);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(112, 42);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonSave;
            this.layoutControlItem2.Location = new System.Drawing.Point(357, 140);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(122, 42);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // ItemForUserName1
            // 
            this.ItemForUserName1.Control = this.FirmaTextEdit;
            this.ItemForUserName1.CustomizationFormText = "Kullanıcı Adı";
            this.ItemForUserName1.Location = new System.Drawing.Point(0, 48);
            this.ItemForUserName1.Name = "ItemForUserName1";
            this.ItemForUserName1.Size = new System.Drawing.Size(239, 24);
            this.ItemForUserName1.Text = "Firma";
            this.ItemForUserName1.TextSize = new System.Drawing.Size(71, 13);
            // 
            // ItemForFontSize
            // 
            this.ItemForFontSize.Control = this.FontSizeTextEdit;
            this.ItemForFontSize.Location = new System.Drawing.Point(239, 48);
            this.ItemForFontSize.Name = "ItemForFontSize";
            this.ItemForFontSize.Size = new System.Drawing.Size(240, 24);
            this.ItemForFontSize.Text = "Yazı Büyüklüğü";
            this.ItemForFontSize.TextSize = new System.Drawing.Size(71, 13);
            // 
            // textEditSerialNr
            // 
            this.textEditSerialNr.Location = new System.Drawing.Point(12, 101);
            this.textEditSerialNr.Name = "textEditSerialNr";
            this.textEditSerialNr.Size = new System.Drawing.Size(475, 20);
            this.textEditSerialNr.StyleController = this.dataLayoutControl1;
            this.textEditSerialNr.TabIndex = 11;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEditSerialNr;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(479, 41);
            this.layoutControlItem3.Text = "Seri No";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(71, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 113);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(479, 27);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 140);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(245, 42);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // XtraFormSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 202);
            this.Controls.Add(this.dataLayoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "XtraFormSettings";
            this.Text = "Ayarlar";
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FontSizeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServerNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbSettingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DbNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPassTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirmaTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDbName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFontSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSerialNr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit ServerNameTextEdit;
        private System.Windows.Forms.BindingSource dbSettingBindingSource;
        private DevExpress.XtraEditors.TextEdit DbNameTextEdit;
        private DevExpress.XtraEditors.TextEdit UserNameTextEdit;
        private DevExpress.XtraEditors.TextEdit UserPassTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServerName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDbName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPass;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit FirmaTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserName1;
        private DevExpress.XtraEditors.TextEdit FontSizeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFontSize;
        private DevExpress.XtraEditors.TextEdit textEditSerialNr;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    }
}