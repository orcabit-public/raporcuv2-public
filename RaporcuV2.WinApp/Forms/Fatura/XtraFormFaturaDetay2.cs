﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.Fatura
{
    public partial class XtraFormFaturaDetay2 : XtraFormBase, IReport
    {
        public XtraFormFaturaDetay2()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                CreateFirmLookup();
                CreatePeriodLookup();
                CreateDivLookup();
                CreateRefreshButton();
                RefreshGridData();
                CreatePivotButton();
            };
        }
        #endregion




        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
          
            _reportTag = RaporcuV2.Helper.ReportTags.FaturaDetay2;
            RefreshGridData<Model.Fatura.FaturaDetay2>();
        }

        #endregion
    }
}