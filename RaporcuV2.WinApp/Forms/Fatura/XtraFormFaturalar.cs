﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.PLinq;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;
using DevExpress.XtraGrid.Views.Grid;

namespace RaporcuV2.WinApp.Forms.Fatura
{
    public partial class XtraFormFaturalar : XtraFormBase
    {
        public XtraFormFaturalar()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                CreateFirmLookupCustoms(barManager1);
                CreatePeriodLookupCustoms(barManager1);
                CreateDivLookupCustoms(barManager1);
                CreateRefreshButton();
                RefreshGridData();
                CreatePivotButton();
            };


            this.barManagerControl1.gridView1.FocusedRowChanged += (sender, arg)=>
            {
                GridView grid = sender as GridView;
                SelectedRowValue = grid.GetFocusedRow();
                Model.Fatura.Fatura fatura = SelectedRowValue as Model.Fatura.Fatura;

                #region Sql Sorgu çekme

                try
                {
                    string sqlText = string.Format(@"
                                            SELECT 
                                            CASE STRNS.LINETYPE 
                                            WHEN 0 THEN 'Malzeme Hareketi' WHEN 1 THEN 'Promosyon Hareketi' 
                                            WHEN 2 THEN 'İndirim Hareketi' WHEN 3 THEN 'Masraf Hareketi' WHEN
                                            4 THEN 'Hizmet Hareketi' WHEN 6 THEN 'Karma Koli Hareketi' 
                                            WHEN 8 THEN 'Sabit Kıymet Hareketi' WHEN 9 THEN 'Ek Malzeme Hareketi' WHEN 10 THEN 'Malzeme Sınıfı Hareketi'
                                            WHEN 11 THEN 'Fason Hareketi' END AS [IslemTuru],
                                            ITM.CODE ,
                                            ITM.NAME ,
                                            ISNULL(STRNS.AMOUNT, 0) * (CASE WHEN STRNS.TRCODE IN (3, 6) THEN - 1 ELSE 1 END) AS [Miktar],
                                            STRNS.PRICE AS [Fiyati],
                                            STRNS.TOTAL * (CASE WHEN STRNS.TRCODE IN (3, 6) THEN - 1 ELSE 1 END) AS [SatirTutari],
                                            STRNS.DISTEXP * (CASE WHEN STRNS.TRCODE IN (3, 6) THEN - 1 ELSE 1 END) AS [Masraflar],
                                            STRNS.DISTDISC * (CASE WHEN STRNS.TRCODE IN (3, 6) THEN - 1 ELSE 1 END) AS [Iskonto],
                                            STRNS.VATMATRAH * (CASE WHEN STRNS.TRCODE IN (3, 6) THEN - 1 ELSE 1 END) AS [KdvMatrahi], 
                                            STRNS.VATAMNT * (CASE WHEN STRNS.TRCODE IN (3, 6)
                                            THEN - 1 ELSE 1 END) AS [KdvTutari], 
                                            (STRNS.VATMATRAH + STRNS.VATAMNT) * (CASE WHEN STRNS.TRCODE IN (3, 6) THEN - 1 ELSE 1 END) AS [ToplamTutar],
                                            SVC.DEFINITION_ AS [HizmetAdi]
                                            FROM            
                                            {0}.dbo.LG_{1}_{2}_STLINE AS STRNS LEFT OUTER JOIN
                                            {0}.dbo.LG_{1}_{2}_INVOICE AS INV ON INV.LOGICALREF = STRNS.INVOICEREF AND ISNULL(INV.CANCELLED, 0) = 0 LEFT OUTER JOIN
                                            {0}.dbo.LG_{1}_CLCARD AS CLC ON CLC.LOGICALREF = STRNS.CLIENTREF LEFT OUTER JOIN
                                            {0}.dbo.LG_{1}_ITEMS AS ITM ON ITM.LOGICALREF = STRNS.STOCKREF AND ITM.ACTIVE=0 AND STRNS.LINETYPE IN (0, 1, 5, 6, 7, 8, 9, 10, 11) LEFT OUTER JOIN
                                            {0}.dbo.LG_{1}_SRVCARD AS SVC ON SVC.LOGICALREF = STRNS.STOCKREF AND STRNS.LINETYPE IN (4) 
                                            WHERE STRNS.INVOICEREF={3}
                                            ORDER BY STRNS.INVOICELNNO
                                                ",LogoDbName, FirmNrStr, PeriodNrStr, fatura.LOGICALREF);
                    RaporcuV2.DataAccess.RaporcuDbContext cnt = new DataAccess.RaporcuDbContext();
                    var list = cnt.Database.SqlQuery<Model.Fatura.FaturaSatirlari>(sqlText).ToList();
                    this.gCFaturaSatirlari.DataSource = list;
                    string sqlText1 = string.Format(@"
                                            Select 
                                            Case When Crossref=0 then 'Kapanmamış' 
                                            When Crossref<>0 then 'kapanmış' end as KapanmaDurumu,
                                            Case When py.TRCURR=0 then 'TL' 
                                            When py.TRCURR=1  then 'USD'
                                            When py.TRCURR=20  then 'EURO'
                                            When py.TRCURR=17  then 'GBP' end as DOVIZ,   
                                            C.CODE,C.DEFINITION_,PY.procDATE,PY.DATE_,PY.TOTAL,  PY.PAID AS Odenen,      
                                            CASE when 
                                            PY.TRCODE =8 and PY.MODULENR =4 then 'Toptan Satış Faturası'
                                            when PY.TRCODE =1 and PY.MODULENR =4 then 'Mal Alım Faturası'
                                            END AS 'TURU'
                                            FROM
                                            {0}.dbo.LG_{1}_{2}_PAYTRANS PY INNER JOIN 
                                            {0}.dbo.LG_{1}_CLCARD C ON PY.CARDREF=C.LOGICALREF 
                                            WHERE  TRCODE IN (1,3,6,8) AND MODULENR=4 AND PY.FICHEREF ={3}
                                            ORDER BY PY.FICHEREF
                                                ",LogoDbName, FirmNrStr, PeriodNrStr, fatura.LOGICALREF);
                    RaporcuV2.DataAccess.RaporcuDbContext cnt1 = new DataAccess.RaporcuDbContext();
                    var list1 = cnt1.Database.SqlQuery<Model.Fatura.FaturaOdemeHareketleri>(sqlText1).ToList();
                    this.gCOdeme.DataSource = list1;
                }
                catch (Exception)
                {

                   
                }
                #endregion
            };
        }
        #endregion


       

        #endregion

        #region Refresh Grid Data
        public void RefreshGridData()
        {
            //SqlText = SqlFatura.Faturalar;
            _reportTag = RaporcuV2.Helper.ReportTags.Faturalar;
            RefreshGridData<Model.Fatura.Fatura>();
        }

        #endregion
        
    }

}