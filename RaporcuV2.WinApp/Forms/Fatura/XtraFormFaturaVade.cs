﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.PLinq;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;


namespace RaporcuV2.WinApp.Forms.Fatura
{
    public partial class XtraFormFaturaVade : XtraFormBase, IReport
    {
        public XtraFormFaturaVade()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
               
                CreateFirmLookup();
                CreatePeriodLookup();
                CreateDivLookup();
               // CreateCurrency();
                CreateRefreshButton();
               // CreateCustomButton("Fatura Vade");
                RefreshGridData();
                CreatePivotButton();
            };
        }
        #endregion




        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
            //SqlText = SqlFatura.FaturaVade;
            _reportTag = RaporcuV2.Helper.ReportTags.FaturaVade;
            RefreshGridData<Model.Fatura.FaturaVade>();
        }

        #endregion

        #region Custom Refresh Grid Data

        public void CustomRefreshGridData()
        {
            //SqlText = SqlFatura.FaturaVadeToplam;
            _reportTag = RaporcuV2.Helper.ReportTags.FaturaVadeToplam;
            RefreshGridData<Model.Fatura.FaturaVadeToplam>();
            
        }

        #endregion

    }
}