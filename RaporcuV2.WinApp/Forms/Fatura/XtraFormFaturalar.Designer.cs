﻿namespace RaporcuV2.WinApp.Forms.Fatura
{
    partial class XtraFormFaturalar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gCFaturalar = new DevExpress.XtraGrid.GridControl();
            this.gVFaturalar = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gCOdeme = new DevExpress.XtraGrid.GridControl();
            this.gVOdeme = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gCFaturaSatirlari = new DevExpress.XtraGrid.GridControl();
            this.gVFaturaSatirlari = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.bar5 = new DevExpress.XtraBars.Bar();
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditFirst.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditLast.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrencyRadioGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CekTuruRadioGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FaturaRadioGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HizmetRadioGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditCardRadioGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gCFaturalar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVFaturalar)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gCOdeme)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVOdeme)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gCFaturaSatirlari)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVFaturaSatirlari)).BeginInit();
            this.SuspendLayout();
            // 
            // DateEditFirst
            // 
            // 
            // DateEditLast
            // 
            // 
            // barManagerControl1
            // 
            this.barManagerControl1.Size = new System.Drawing.Size(1171, 321);
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.Text = "Tools";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar4});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 0;
            this.barManager1.StatusBar = this.bar4;
            // 
            // bar4
            // 
            this.bar4.BarName = "Status bar";
            this.bar4.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar4.OptionsBar.AllowQuickCustomization = false;
            this.bar4.OptionsBar.DrawDragBorder = false;
            this.bar4.OptionsBar.UseWholeRow = true;
            this.bar4.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1171, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 509);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1171, 22);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 509);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1171, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 509);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gCFaturalar);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1171, 509);
            this.panelControl1.TabIndex = 4;
            // 
            // gCFaturalar
            // 
            this.gCFaturalar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gCFaturalar.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gCFaturalar.Location = new System.Drawing.Point(2, 2);
            this.gCFaturalar.MainView = this.gVFaturalar;
            this.gCFaturalar.Name = "gCFaturalar";
            this.gCFaturalar.Size = new System.Drawing.Size(1167, 505);
            this.gCFaturalar.TabIndex = 1;
            this.gCFaturalar.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gVFaturalar});
            // 
            // gVFaturalar
            // 
            this.gVFaturalar.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn28,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39});
            this.gVFaturalar.GridControl = this.gCFaturalar;
            this.gVFaturalar.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NETTOTAL", this.gridColumn7, "")});
            this.gVFaturalar.Name = "gVFaturalar";
            this.gVFaturalar.OptionsBehavior.Editable = false;
            this.gVFaturalar.OptionsSelection.InvertSelection = true;
            this.gVFaturalar.OptionsView.ShowAutoFilterRow = true;
            this.gVFaturalar.OptionsView.ShowFooter = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Türü";
            this.gridColumn1.FieldName = "durum";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tarih";
            this.gridColumn2.FieldName = "DATE_";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Fiş No";
            this.gridColumn3.FieldName = "FICHENO";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Belge No";
            this.gridColumn4.FieldName = "DOCODE";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Cari Hesap";
            this.gridColumn5.FieldName = "DEFINITION_";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 6;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Kdv.siz Tutar";
            this.gridColumn6.DisplayFormat.FormatString = "N2";
            this.gridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn6.FieldName = "KDVSIZTUTAR";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "KDVSIZTUTAR", "{0:N2}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 16;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Kdv.li Tutar İD";
            this.gridColumn7.DisplayFormat.FormatString = "N2";
            this.gridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn7.FieldName = "KDVLIDOVIZTUTAR";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "KDVLIDOVIZTUTAR", "{0:N2}")});
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 18;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Açıklama";
            this.gridColumn8.FieldName = "GENEXP1";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 7;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Özel Kod";
            this.gridColumn28.FieldName = "SPECODE";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 13;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Proje Adı";
            this.gridColumn30.FieldName = "NAME";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 14;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Cari Hesap Kodu";
            this.gridColumn31.FieldName = "CODE";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 5;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Satış Elemanı Kodu";
            this.gridColumn32.FieldName = "SATISKODU";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 10;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Satış Elemanı Adı";
            this.gridColumn33.FieldName = "SATISADI";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 11;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Yetki Kodu";
            this.gridColumn34.FieldName = "CYPHCODE";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 12;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Cari Özel Kod";
            this.gridColumn35.FieldName = "CSPECODE";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 8;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Cari Özel Kod2";
            this.gridColumn36.FieldName = "CSPECODE2";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 9;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Döviz Tipi";
            this.gridColumn37.FieldName = "DOVIZTIPI";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 15;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Kdv.li Tutar TL";
            this.gridColumn38.DisplayFormat.FormatString = "N2";
            this.gridColumn38.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn38.FieldName = "KDVLITL";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "KDVLITL", "{0:N2}")});
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 17;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Düz. Tarihi";
            this.gridColumn39.FieldName = "DOCDATE";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panelControl3);
            this.panel1.Controls.Add(this.panelControl2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 321);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1171, 188);
            this.panel1.TabIndex = 5;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.groupBox2);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(567, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(604, 188);
            this.panelControl3.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.gCOdeme);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(600, 184);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Fatura Ödeme Hareketleri";
            // 
            // gCOdeme
            // 
            this.gCOdeme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gCOdeme.Location = new System.Drawing.Point(3, 17);
            this.gCOdeme.MainView = this.gVOdeme;
            this.gCOdeme.Name = "gCOdeme";
            this.gCOdeme.Size = new System.Drawing.Size(594, 164);
            this.gCOdeme.TabIndex = 0;
            this.gCOdeme.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gVOdeme});
            // 
            // gVOdeme
            // 
            this.gVOdeme.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn29});
            this.gVOdeme.GridControl = this.gCOdeme;
            this.gVOdeme.Name = "gVOdeme";
            this.gVOdeme.OptionsBehavior.Editable = false;
            this.gVOdeme.OptionsView.ColumnAutoWidth = false;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Durum";
            this.gridColumn20.FieldName = "KapanmaDurumu";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            this.gridColumn20.Width = 114;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Cari Kodu";
            this.gridColumn21.FieldName = "CODE";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            this.gridColumn21.Width = 130;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Cari Ünvanı";
            this.gridColumn22.FieldName = "DEFINITION_";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            this.gridColumn22.Width = 183;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "İşlem Tarihi";
            this.gridColumn23.FieldName = "procDATE";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            this.gridColumn23.Width = 99;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Ödeme Tarihi";
            this.gridColumn24.FieldName = "DATE_";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            this.gridColumn24.Width = 98;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Ödeme Tutarı";
            this.gridColumn25.DisplayFormat.FormatString = "N2";
            this.gridColumn25.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn25.FieldName = "TOTAL";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            this.gridColumn25.Width = 95;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Ödenen Tutar";
            this.gridColumn26.DisplayFormat.FormatString = "N2";
            this.gridColumn26.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn26.FieldName = "ÖDENEN";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            this.gridColumn26.Width = 91;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Döviz";
            this.gridColumn29.FieldName = "DOVIZ";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 7;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.groupBox1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(567, 188);
            this.panelControl2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gCFaturaSatirlari);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(563, 184);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Fatura Satır Detayları";
            // 
            // gCFaturaSatirlari
            // 
            this.gCFaturaSatirlari.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gCFaturaSatirlari.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gCFaturaSatirlari.Location = new System.Drawing.Point(3, 17);
            this.gCFaturaSatirlari.MainView = this.gVFaturaSatirlari;
            this.gCFaturaSatirlari.Name = "gCFaturaSatirlari";
            this.gCFaturaSatirlari.Size = new System.Drawing.Size(557, 164);
            this.gCFaturaSatirlari.TabIndex = 0;
            this.gCFaturaSatirlari.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gVFaturaSatirlari});
            // 
            // gVFaturaSatirlari
            // 
            this.gVFaturaSatirlari.GridControl = this.gCFaturaSatirlari;
            this.gVFaturaSatirlari.Name = "gVFaturaSatirlari";
            this.gVFaturaSatirlari.OptionsBehavior.Editable = false;
            this.gVFaturaSatirlari.OptionsView.ColumnAutoWidth = false;
            // 
            // bar5
            // 
            this.bar5.BarName = "Tools";
            this.bar5.DockCol = 0;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar5.Offset = 3;
            this.bar5.Text = "Tools";
            // 
            // XtraFormFaturalar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1171, 531);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "XtraFormFaturalar";
            this.Text = "Faturalar";
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.barManagerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditFirst.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditLast.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrencyRadioGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CekTuruRadioGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FaturaRadioGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HizmetRadioGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditCardRadioGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gCFaturalar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVFaturalar)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gCOdeme)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVOdeme)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gCFaturaSatirlari)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gVFaturaSatirlari)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraGrid.GridControl gCOdeme;
        private DevExpress.XtraGrid.Views.Grid.GridView gVOdeme;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraGrid.GridControl gCFaturaSatirlari;
        private DevExpress.XtraGrid.Views.Grid.GridView gVFaturaSatirlari;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gCFaturalar;
        private DevExpress.XtraGrid.Views.Grid.GridView gVFaturalar;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraBars.Bar bar5;
    }
}