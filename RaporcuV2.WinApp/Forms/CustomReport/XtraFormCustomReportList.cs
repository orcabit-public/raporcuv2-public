﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RaporcuV2.Model.Database;
using RaporcuV2.WinApp.Forms.Kullanici;

namespace RaporcuV2.WinApp.Forms.CustomReport
{
    public partial class XtraFormCustomReportList : DevExpress.XtraEditors.XtraForm
    {
        public XtraFormCustomReportList()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Events
        private void AddEvents()
        {
            RefreshData();

            #region New

            barButtonItemNew.ItemClick += (sender, events) =>
            {
                OpenForm(0);
            };

            #endregion

            #region Edit

            barButtonItemEdit.ItemClick += (sender, events) =>
            {
                Model.Database.CustomReport customReport = (Model.Database.CustomReport)gridViewList.GetFocusedRow();
                OpenForm(customReport.CustomReportId);
            };

            #endregion

            #region Delete

            barButtonItemDelete.ItemClick += (sender, events) =>
            {
                Model.Database.CustomReport customReport = (Model.Database.CustomReport) gridViewList.GetFocusedRow();

                RaporcuV2.DataAccess.RaporcuDbContext cnt = new DataAccess.RaporcuDbContext();

                Model.Database.CustomReport deleted = cnt.CustomReports.Find(customReport.CustomReportId);
                cnt.CustomReports.Remove(deleted);
                cnt.SaveChanges();

                RefreshData();
            };
            #endregion

            #region refresh

            barButtonItemRefresh.ItemClick += (sender, events) =>
            {
                RefreshData();
            };

            #endregion

            #region Close

            barButtonItemClose.ItemClick += (sender, events) =>
            {
                this.Close();
            };

            #endregion
        }
        #endregion

        #region Methots
        #region Refresh Data
        private void RefreshData()
        {
            RaporcuV2.DataAccess.RaporcuDbContext cnt = new DataAccess.RaporcuDbContext();
            gridControlList.DataSource = cnt.CustomReports.ToList();
        }
        #endregion

        #region Open Edit Form
        private void OpenForm(int reportId)
        {
            var frm = new XtraFormAddCustomReport(reportId)
            {
                FormBorderStyle = FormBorderStyle.FixedToolWindow,
                StartPosition = FormStartPosition.CenterScreen,
                KeyPreview = true,
                ShowIcon = false,
                ShowInTaskbar = true
            };
            var dialogResult = frm.ShowDialog();
            RefreshData();
        }
        #endregion

        #endregion
    }
}