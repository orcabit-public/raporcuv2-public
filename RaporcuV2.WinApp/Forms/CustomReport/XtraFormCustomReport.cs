﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RaporcuV2.DataAccess;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.WinApp.Forms.Customs;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;

namespace RaporcuV2.WinApp.Forms.CustomReport
{
    public partial class XtraFormCustomReport : XtraFormBase, IReport
    {
        private readonly int _reportTag;
        RaporcuDbContext cnt = new RaporcuDbContext();
        private Model.Database.CustomReport _report;

        public XtraFormCustomReport(int reportTag)
        {
            _reportTag = reportTag;
            InitializeComponent();
            AddEvents();
        }

        #region Grid Save
        public void GridLoad()
        {
            this.Shown += (sender, args) =>
            {

                XtraForm parentForm = GetParentForm(this);
                string parentFormName = parentForm.Name;
                string name = this.barManagerControl1.gridView1.Name;
                GridView gridView = this.barManagerControl1.gridView1;
                GridControl gridControl = this.barManagerControl1.gridControl1;
                string regKey = string.Format("Software\\Raporcu\\{0}.{1}", parentFormName, gridControl);

                try
                {
                    gridView.RestoreLayoutFromRegistry(regKey);
                }
                catch (Exception e)
                {

                }
            };
        }
        public void GridSave()
        {

            this.Shown += (sender, args) =>
            {

                XtraForm parentForm = GetParentForm(this);
                string parentFormName = parentForm.Name;
                string name = this.barManagerControl1.gridView1.Name;
                GridView gridView = this.barManagerControl1.gridView1;
                GridControl gridControl = this.barManagerControl1.gridControl1;
                string regKey = string.Format("Software\\Raporcu\\{0}.{1}", parentFormName, gridControl);

                try
                {
                    gridView.RestoreLayoutFromRegistry(regKey);
                }
                catch (Exception e)
                {

                }
            };
            this.Closed += (sender, args) => {
                XtraForm parentForm = GetParentForm(this);
                string parentFormName = parentForm.Name;
                string name = this.barManagerControl1.gridView1.Name;
                GridView gridView = this.barManagerControl1.gridView1;
                GridControl gridControl = this.barManagerControl1.gridControl1;
                string regKey = string.Format("Software\\Raporcu\\{0}.{1}", parentFormName, gridControl);

                try
                {
                    gridView.SaveLayoutToRegistry(regKey);
                }
                catch (Exception e)
                {

                }
            };
        }

        #endregion

        #region Add Events
        public void AddEvents()
        {
            this.Tag = _reportTag;
            _report = cnt.CustomReports.FirstOrDefault(a => a.CustomReportTag == _reportTag);
            this.Text = _report.CustomReportName;
            this.Load += (sender, args) =>
            {
                CreateFirmLookup();
                CreatePeriodLookup();
                CreateDivLookup();
                CreateRefreshButton();
                RefreshGridData();
                CreateLastDate();
                CreatePivotButton();
            };
        }
        #endregion

        #region Refresh Grid Data
        
        public void RefreshGridData()
        {
            RefreshGridDataCustom(_report.CustomReportSql);
            /*
             GridSave();
            string connectionString = SingleConnection.ConString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                DataTable dt = new DataTable();
                SqlTextCustom = _report.CustomReportSql;
                SqlCommand cmd = new SqlCommand(SqlTextCustom, conn);
                
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                // this will query your database and return the result to your datatable
                da.Fill(dt);
                barManagerControl1.gridControl1.DataSource = dt;
                barManagerControl1.gridView1.BestFitColumns();
            }
            */
        }

        #endregion
    }
}