﻿namespace RaporcuV2.WinApp.Forms.CustomReport
{
    partial class XtraFormAddCustomReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.CustomReportNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CustomReportTagTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.CustomReportSqlTextEdit = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCustomReportTag = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomReportName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCustomReportSql = new DevExpress.XtraLayout.LayoutControlItem();
            this.customReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustomReportNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomReportTagTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomReportSqlTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomReportTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomReportName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomReportSql)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customReportBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.CustomReportNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CustomReportTagTextEdit);
            this.dataLayoutControl1.Controls.Add(this.simpleButtonSave);
            this.dataLayoutControl1.Controls.Add(this.simpleButtonCancel);
            this.dataLayoutControl1.Controls.Add(this.CustomReportSqlTextEdit);
            this.dataLayoutControl1.DataSource = this.customReportBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1012, 181, 650, 400);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(996, 632);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // CustomReportNameTextEdit
            // 
            this.CustomReportNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customReportBindingSource, "CustomReportName", true));
            this.CustomReportNameTextEdit.Location = new System.Drawing.Point(249, 60);
            this.CustomReportNameTextEdit.Name = "CustomReportNameTextEdit";
            this.CustomReportNameTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.CustomReportNameTextEdit.Size = new System.Drawing.Size(723, 20);
            this.CustomReportNameTextEdit.StyleController = this.dataLayoutControl1;
            this.CustomReportNameTextEdit.TabIndex = 4;
            // 
            // CustomReportTagTextEdit
            // 
            this.CustomReportTagTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customReportBindingSource, "CustomReportTag", true));
            this.CustomReportTagTextEdit.Location = new System.Drawing.Point(24, 60);
            this.CustomReportTagTextEdit.Name = "CustomReportTagTextEdit";
            this.CustomReportTagTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.CustomReportTagTextEdit.Size = new System.Drawing.Size(221, 20);
            this.CustomReportTagTextEdit.StyleController = this.dataLayoutControl1;
            this.CustomReportTagTextEdit.TabIndex = 6;
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonSave.Location = new System.Drawing.Point(838, 597);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(66, 23);
            this.simpleButtonSave.StyleController = this.dataLayoutControl1;
            this.simpleButtonSave.TabIndex = 5;
            this.simpleButtonSave.Text = "Kaydet";
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(918, 597);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(66, 23);
            this.simpleButtonCancel.StyleController = this.dataLayoutControl1;
            this.simpleButtonCancel.TabIndex = 6;
            this.simpleButtonCancel.Text = "Vazgeç";
            // 
            // CustomReportSqlTextEdit
            // 
            this.CustomReportSqlTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.customReportBindingSource, "CustomReportSql", true));
            this.CustomReportSqlTextEdit.Location = new System.Drawing.Point(24, 128);
            this.CustomReportSqlTextEdit.Name = "CustomReportSqlTextEdit";
            this.CustomReportSqlTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.CustomReportSqlTextEdit.Size = new System.Drawing.Size(948, 453);
            this.CustomReportSqlTextEdit.StyleController = this.dataLayoutControl1;
            this.CustomReportSqlTextEdit.TabIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(996, 632);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(976, 612);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButtonSave;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(826, 585);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(70, 27);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(70, 27);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(70, 27);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonCancel;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(906, 585);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(70, 27);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(70, 27);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(70, 27);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(896, 585);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(10, 27);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 27);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(10, 27);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 585);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(826, 27);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCustomReportTag,
            this.ItemForCustomReportName});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(976, 84);
            this.layoutControlGroup3.Text = "Rapor Tanımı";
            // 
            // ItemForCustomReportTag
            // 
            this.ItemForCustomReportTag.Control = this.CustomReportTagTextEdit;
            this.ItemForCustomReportTag.Location = new System.Drawing.Point(0, 0);
            this.ItemForCustomReportTag.Name = "ItemForCustomReportTag";
            this.ItemForCustomReportTag.Size = new System.Drawing.Size(225, 40);
            this.ItemForCustomReportTag.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForCustomReportTag.TextSize = new System.Drawing.Size(71, 13);
            // 
            // ItemForCustomReportName
            // 
            this.ItemForCustomReportName.Control = this.CustomReportNameTextEdit;
            this.ItemForCustomReportName.Location = new System.Drawing.Point(225, 0);
            this.ItemForCustomReportName.Name = "ItemForCustomReportName";
            this.ItemForCustomReportName.Size = new System.Drawing.Size(727, 40);
            this.ItemForCustomReportName.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForCustomReportName.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCustomReportSql});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 84);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(976, 501);
            this.layoutControlGroup4.Text = "SQL Sorgusu";
            // 
            // ItemForCustomReportSql
            // 
            this.ItemForCustomReportSql.Control = this.CustomReportSqlTextEdit;
            this.ItemForCustomReportSql.Location = new System.Drawing.Point(0, 0);
            this.ItemForCustomReportSql.Name = "ItemForCustomReportSql";
            this.ItemForCustomReportSql.Size = new System.Drawing.Size(952, 457);
            this.ItemForCustomReportSql.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForCustomReportSql.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForCustomReportSql.TextVisible = false;
            // 
            // customReportBindingSource
            // 
            this.customReportBindingSource.DataSource = typeof(RaporcuV2.Model.Database.CustomReport);
            // 
            // XtraFormAddCustomReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 632);
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "XtraFormAddCustomReport";
            this.Text = "Tanımlı Rapor";
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CustomReportNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomReportTagTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustomReportSqlTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomReportTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomReportName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomReportSql)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customReportBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit CustomReportNameTextEdit;
        private System.Windows.Forms.BindingSource customReportBindingSource;
        private DevExpress.XtraEditors.TextEdit CustomReportTagTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomReportName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomReportSql;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomReportTag;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.MemoEdit CustomReportSqlTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
    }
}