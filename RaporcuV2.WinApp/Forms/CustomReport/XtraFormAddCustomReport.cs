﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using RaporcuV2.Model.Database;

namespace RaporcuV2.WinApp.Forms.CustomReport
{
    public partial class XtraFormAddCustomReport : DevExpress.XtraEditors.XtraForm
    {
        private readonly int _reportId;
        DataAccess.RaporcuDbContext cnt = new DataAccess.RaporcuDbContext();


        public XtraFormAddCustomReport(int reportId)
        {
            _reportId = reportId;
            InitializeComponent();
            AddEvents();
        }

        #region Add Events
        private void AddEvents()
        {
            this.Load += (sender, arg) =>
            {

                Model.Database.CustomReport customReport = cnt.CustomReports.Find(_reportId);
                if (customReport == null)
                {
                    customReport = new Model.Database.CustomReport();
                }
                this.customReportBindingSource.DataSource = customReport;
            };

            simpleButtonSave.Click += (sender, arg) =>
            {
                SaveData();

            };

            simpleButtonCancel.Click += (sender, arg) =>
            {
                this.DialogResult = DialogResult.Cancel;

            };
        }


        #endregion

        #region Methots
        public void SaveData()
        {
            if (!Validation())
                return;
            Model.Database.CustomReport customReport = (Model.Database.CustomReport)customReportBindingSource.Current;

            using (DataAccess.RaporcuDbContext context = new DataAccess.RaporcuDbContext())
            {
                context.Entry(customReport).State = customReport.CustomReportId == 0 ?
                    EntityState.Added :
                    EntityState.Modified;
                context.SaveChanges();
            }
            DialogResult = DialogResult.OK;
        }
        public bool Validation()
        {
            simpleButtonCancel.Focus();
            using (DataAccess.RaporcuDbContext cnt = new DataAccess.RaporcuDbContext())
            {
                Model.Database.CustomReport customReport = (Model.Database.CustomReport)customReportBindingSource.Current;

                bool isNew = _reportId == 0 ? true : false;
                Model.Database.CustomReport finded =
                    cnt.CustomReports.FirstOrDefault(a => a.CustomReportName == customReport.CustomReportName || a.CustomReportTag == customReport.CustomReportTag);
                bool exist = finded != null ? true : false;

                if ((isNew && exist) || ((!isNew && exist) && (finded.CustomReportId != customReport.CustomReportId)))
                {
                    XtraMessageBox.Show("Rapor Tanımı yada Tag Daha Önce Tanımlanmış", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}