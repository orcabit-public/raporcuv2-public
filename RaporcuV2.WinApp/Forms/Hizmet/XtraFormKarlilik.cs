﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Spreadsheet;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.Hizmet
{
    public partial class XtraFormKarlilik : XtraFormBase, IReport
    {
        //deneme
        public int SectionStart { get; set; } = 4;
        private string _s = "A";
        private string _e = "N";
        private int _spaceRow = 2;

        public XtraFormKarlilik()
        {
            InitializeComponent();
            this.barManagerControl1.gridControl1.Visible = false;
            AddEvents();
        }

        #region Methots

        #region Add Events

        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {

                CreateFirmLookup();
                CreatePeriodLookup();
                CreateFirstDate();
                foreach (BarItem barItem in barManagerControl1.barManagerForm.Items)
                {
                    if (barItem.Name == "firstDate")
                    {
                        ((DevExpress.XtraBars.BarEditItem) barItem).EditValue = DateTime.Today;
                        //DateEdit dateedit = ((BarEditItem)barItem) as DateEdit;
                        ((DevExpress.XtraEditors.Repository.RepositoryItemDateEdit)
                            ((DevExpress.XtraBars.BarEditItem) barItem).Edit).MinValue = DateTime.Today;

                    }
                }

                CreateRefreshButton();

                barManager1.BeginUpdate();

                var parentTools = this.barManagerControl1.barManagerForm.Bars[0];

                this.barManager1.Bars.Add(parentTools);

                barManager1.EndUpdate();

                commonBar1.Merge(parentTools);
            };

            //this.spreadsheetControl1.Dock = DockStyle.Fill;
        }

        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
            #region Sorgular

            _reportTag = RaporcuV2.Helper.ReportTags.AtaSmm;
            List<AtaSmm> ataSmm = GetPocoList<AtaSmm>();

            _reportTag = RaporcuV2.Helper.ReportTags.AtaKarZarar;
            List<AtaKarZarar> ataKarZarar = GetPocoList<AtaKarZarar>();

            #endregion

            // Kasa ve Kasa Detay Renkelnedirme
            BuiltInStyleId kasaBaslikStyle = BuiltInStyleId.Accent1;
            BuiltInTableStyleId kasaStyle = BuiltInTableStyleId.TableStyleLight2;

            // Banka ve Banka HAreketerli Renklendirme
            BuiltInStyleId bankaBaslikStyle = BuiltInStyleId.Accent2;
            BuiltInTableStyleId bankaStyle = BuiltInTableStyleId.TableStyleLight3;

            // Kredi Durumu 
            BuiltInStyleId cekBaslikStyle = BuiltInStyleId.Accent3;
            BuiltInTableStyleId cekStyle = BuiltInTableStyleId.TableStyleLight4;

            // Çek Durumu 
            BuiltInStyleId cariBaslikStyle = BuiltInStyleId.Accent4;
            BuiltInTableStyleId cariStyle = BuiltInTableStyleId.TableStyleLight5;


            spreadsheetControl1.BeginUpdate();




            // Rapor Grubu
            int i = 1;
            IWorkbook workbook = spreadsheetControl1.Document;
            Worksheet ws = workbook.Worksheets[0];
            ws.ActiveView.PaperKind = PaperKind.A4;
            ws.ActiveView.Orientation = PageOrientation.Landscape;
            ws.ActiveView.Margins.Left = 0;
            ws.ActiveView.Margins.Top = 0;
            ws.ActiveView.Margins.Right = 0;
            ws.ActiveView.Margins.Bottom = 0;

            ws.Clear(ws.GetUsedRange());

            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToWidth = 1;
            ws.PrintOptions.FitToHeight = 0;

            var range = ws.Range[$"A{i}:I{i}"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[BuiltInStyleId.Title];
            ws.Cells[$"A{i}"].Value = "Ata Karbon";
            i++;

            range = ws.Range[$"A{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = string.Format("{0} Ata Kar/Zarar", FirstDate.ToString("dd MMMM yyyy , dddd"));
            range.Style = workbook.Styles[BuiltInStyleId.Heading1];

            #region SMM

            i = ws.GetUsedRange().RowCount + 2;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "Kasalar";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            int dataCount = 0;

            #endregion

            bool isFirstRow = true;

            int dataRowCount = ataSmm.Count;
            dataCount = dataRowCount + i;
            Table tableBanka = ws.Tables.Add(ws[$"A{i}:T{dataCount}"], true);
            tableBanka.ShowTotals = true;
            tableBanka.AutoFilter.Disable();
            tableBanka.Style = workbook.TableStyles[kasaStyle];
            tableBanka.DataRange.NumberFormat = "#,##0.00";
            tableBanka.Columns[0].Name = $"Kodu";
            tableBanka.Columns[0].TotalRowLabel = $"Ata SMM Toplamı : ";
            //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
            tableBanka.Columns[1].Name = "Tanımı";
            tableBanka.Columns[2].Name = "Proje Kodu";
            tableBanka.Columns[3].Name = "Referans";
            tableBanka.Columns[4].Name = "Dönem Başı Miktar";
            tableBanka.Columns[5].Name = "Dönem Başı Tutar TL";
            tableBanka.Columns[6].Name = "Dönem Başı Tutar USD";
            tableBanka.Columns[7].Name = "Dönem İçi Alım Miktarı";
            tableBanka.Columns[8].Name = "Dönem İçi Alım Tutarı TL";
            tableBanka.Columns[9].Name = "Dönem İçi Alım Tutarı USD";
            tableBanka.Columns[10].Name = "Dönem İçi Satış Miktarı";
            tableBanka.Columns[11].Name = "Dönem İçi Satış Tutarı TL";
            tableBanka.Columns[12].Name = "Dönem İçi Satış Tutarı USD";
            tableBanka.Columns[13].Name = "Dönem Sonu Miktarı";
            tableBanka.Columns[14].Name = "Dönem Sonu Tuarı TL";
            tableBanka.Columns[15].Name = "Dönem Sonu Tutarı USD";
            tableBanka.Columns[16].Name = "DS Birim Fiyat TL";
            tableBanka.Columns[17].Name = "DS Birim Fiyat USD";
            tableBanka.Columns[18].Name = "SMM TL";
            tableBanka.Columns[19].Name = "SMM USD";

            foreach (var item in ataSmm)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                //if (!isFirstRow)
                //{
                //    i++;
                //}

                i++;
                ws.Cells[$"A{i}"].Value = item.CODE;
                ws.Cells[$"B{i}"].Value = item.NAME;
                ws.Cells[$"C{i}"].Value = item.PROJECT;
                ws.Cells[$"D{i}"].Value = item.LOGICALREF;
                ws.Cells[$"E{i}"].Value = item.DB_MIKTAR;
                ws.Cells[$"F{i}"].Value = item.DB_TUTARI_TL;
                ws.Cells[$"G{i}"].Value = item.DB_TUTARI_USD;
                ws.Cells[$"H{i}"].Value = item.DIA_MIKTAR;
                ws.Cells[$"I{i}"].Value = item.DIA_TUTARI_TL;
                ws.Cells[$"J{i}"].Value = item.DIA_TUTARI_USD;
                ws.Cells[$"K{i}"].Value = item.DIS_MIKTAR;
                ws.Cells[$"L{i}"].Value = item.DIS_TUTARI_TL;
                ws.Cells[$"M{i}"].Value = item.DIS_TUTARI_USD;
                ws.Cells[$"N{i}"].Value = item.DS_MIKTAR;
                ws.Cells[$"O{i}"].Value = item.DS_TUTARI_TL;
                ws.Cells[$"P{i}"].Value = item.DS_TUTARI_USD;
                ws.Cells[$"Q{i}"].Value = item.DS_TUTARI_TL_PR;
                ws.Cells[$"R{i}"].Value = item.DS_TUTARI_USD_PR;
                ws.Cells[$"S{i}"].Value = item.SMM_TL;
                ws.Cells[$"T{i}"].Value = item.SMM_USD;



                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #region Banka Durumu 

            i = ws.GetUsedRange().RowCount + 2;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:G{i}"];
            ws.MergeCells(range);
            range.Value = "Gelir / Gider";
            range.Style = workbook.Styles[bankaBaslikStyle];
            //i++;


            #endregion

            //isFirstRow = true;
            var groups = ataKarZarar.GroupBy(a => a.GRUP).Select(s => new { pb = s.Key });

            foreach (var group in groups)
            {
                i = ws.GetUsedRange().RowCount + 5;

                if (!isFirstRow)
                {
                    i++;
                }
                isFirstRow = false;
                dataRowCount = ataKarZarar.Count(a => a.GRUP == group.pb);
                dataCount = dataRowCount + i;
                var table = ws.Tables.Add(ws[$"A{i}:G{dataCount}"], true);
                table.ShowTotals = true;
                table.AutoFilter.Disable();
                table.Style = workbook.TableStyles[bankaStyle];
                table.DataRange.NumberFormat = "#,##0.00";
                table.Columns[0].Name = $"Tür";
                table.Columns[0].TotalRowLabel = $"{group.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                table.Columns[1].Name = "Filtre";
                table.Columns[2].Name = "Grup";
                table.Columns[3].Name = "Proje";
                table.Columns[4].Name = "Miktar";
                table.Columns[5].Name = "TL";
                table.Columns[6].Name = "USD";
                
                foreach (var item in ataKarZarar.Where(a => a.GRUP == group.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.TUR;
                    ws.Cells[$"B{i}"].Value = item.FILTRE;
                    ws.Cells[$"C{i}"].Value = item.GRUP;
                    ws.Cells[$"D{i}"].Value = item.PROJE;
                    ws.Cells[$"E{i}"].Value = item.MIKTAR;
                    ws.Cells[$"F{i}"].Value = item.TL;
                    ws.Cells[$"G{i}"].Value = item.USD;

                }

                table.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in table.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #endregion

            var allCell = ws.GetUsedRange();

            allCell.Alignment.WrapText = true;

            spreadsheetControl1.EndUpdate();

            #endregion
        }
    }
}