﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.PLinq;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.Data;
namespace RaporcuV2.WinApp.Forms.Hizmet
{
    public partial class XtraFormHizmetToplamlari : XtraFormBase, IReport
    {
        public XtraFormHizmetToplamlari()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                CreateFirmLookup();
                CreatePeriodLookup();
              //  CreateDivLookup();

               // CreateHizmetTuru();

                RefreshGridData();
                CreateRefreshButton();

            };
        }
        #endregion




        #endregion

        #region Refresh Grid Data


        public void RefreshGridData()
        {
            //SqlText = SqlHizmet.HizmetToplamlari;
            _reportTag = RaporcuV2.Helper.ReportTags.HizmetToplamlari;

            RefreshGridData<Model.Hizmet.HizmetToplamlari>();
           
        }

        #endregion
    }
}