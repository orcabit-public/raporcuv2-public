﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RaporcuV2.Model.Database;
using System.Data.Entity;
using DevExpress.XtraGrid.Views.Grid;

namespace RaporcuV2.WinApp.Forms.Kullanici
{
    public partial class XtraFormKullaniciEkle : DevExpress.XtraEditors.XtraForm
    {
        DataAccess.RaporcuDbContext cnt = new DataAccess.RaporcuDbContext();
        private readonly int _userId;

        public XtraFormKullaniciEkle(int userId)
        {
            InitializeComponent();
            this._userId = userId;
            AddEvents();
        }

        #region Add Events
        private void AddEvents()
        {
            this.Load += (sender, arg) =>
            {
                if(_userId != 0)
                {
                    User user = cnt.Users.Find(_userId);
                    this.userBindingSource.DataSource = user;
                    if (user.UserName.Equals("admin"))
                    {
                        this.UserNameTextEdit.ReadOnly = true;
                        gridControlYetki.Enabled = false;
                    }
                    List<UserPermission> userPermissions = cnt.UserPermissions.Include("Permission").Where(a => a.UserId == user.UserId).AsNoTracking().ToList();
                    this.userPermissionBindingSource.DataSource = userPermissions;
                }
                else
                {
                    List<UserPermission> userPermissions = new List<UserPermission>();

                    User appUser = new User();
                    this.userBindingSource.DataSource = appUser;
                    

                    #region Create Permissions

                    #region New Permission

                    List<Permission> permissionList = cnt.Permissions.ToList();

                        #region Add User Permission

                        foreach (var item in permissionList)
                        {
                            if (!cnt.UserPermissions.Any(a => a.UserId == appUser.UserId && a.PermissionId == item.PermissionId))
                            {
                                UserPermission uperm = new UserPermission()
                                {
                                    PermissionStatus = true,
                                    PermissionId = item.PermissionId,
                                    UserId = appUser.UserId,
                                    Permission = item
                                };
                                userPermissions.Add(uperm);
                            }}

                    #endregion

                    this.userPermissionBindingSource.DataSource = userPermissions;


                    #endregion

                    #endregion
                }
            };

            simpleButtonCancel.Click += (sender, arg) =>
            {
                this.DialogResult = DialogResult.Cancel;
            };

            simpleButtonSave.Click += (sender, arg) =>
            {
                SaveData();
            };
            gridViewYetki.DoubleClick += GridViewYetki_DoubleClick;
        }

        private void GridViewYetki_DoubleClick(object sender, EventArgs e)
        {
            GridView view = sender as GridView;
            UserPermission userPermission = view.GetFocusedRow() as UserPermission;

            bool status = userPermission.PermissionStatus;
            userPermission.PermissionStatus = !status;
            view.RefreshData();
        }
        #endregion

        #region Methots
        public void SaveData()
        {
            if (!Validation())
                return;
            User user = (User)userBindingSource.Current;

            using (DataAccess.RaporcuDbContext context = new DataAccess.RaporcuDbContext())
            {
                context.Entry(user).State = user.UserId == 0 ?
                    EntityState.Added :
                    EntityState.Modified;
                context.SaveChanges();
            }

            List<UserPermission> list = userPermissionBindingSource.DataSource as List<UserPermission>;
            foreach (UserPermission permission in list)
            {
                permission.UserId = user.UserId;
                permission.Permission = null;
                using (DataAccess.RaporcuDbContext context = new DataAccess.RaporcuDbContext())
                {
                    context.Entry(permission).State = permission.UserPermissionId == 0 ?
                        EntityState.Added :
                        EntityState.Modified;
                    context.SaveChanges();
                }
            }
            DialogResult = DialogResult.OK;
        }
        public bool Validation()
        {
            simpleButtonSave.Focus();
            using (DataAccess.RaporcuDbContext cnt = new DataAccess.RaporcuDbContext())
            {
                User user = (User)userBindingSource.Current;

                bool isNew = _userId == 0 ? true : false;
                User finded = cnt.Users.FirstOrDefault(a => a.UserName == user.UserName);
                bool exist = finded != null ? true : false;

                if ((isNew && exist) || ((!isNew && exist) && (finded.UserId != user.UserId)))
                {
                    XtraMessageBox.Show("Kullanıcı Daha Önce Tanımlanmış","Hata", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}