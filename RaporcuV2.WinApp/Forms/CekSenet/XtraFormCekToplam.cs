﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.PLinq;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;
using DevExpress.XtraGrid.Views.Grid;

namespace RaporcuV2.WinApp.Forms.CekSenet
{
    public partial class XtraFormCekToplam : XtraFormBase
    {
        public XtraFormCekToplam()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                CreateFirmLookupCustoms(barManager1);
                CreatePeriodLookupCustoms(barManager1);
                CreateDivLookup();
                CreateCekTuruCustom(barManager1);
                CreateCurrencyCustom(barManager1);
                CreateLastDate();
                //CreateDivLookupCustoms(barManager1);
                CreateRefreshButton();
                RefreshGridData();
                CreateCustomButton("Çek Giriş");
            };

            
            this.barManagerControl1.gridView1.DataSourceChanged += (sender, arg) =>
            {
                GridView grid = sender as GridView;
                SelectedRowValue = grid.GetFocusedRow();
                Model.CekSenet.CekGunlukToplam CekToplamm = SelectedRowValue as Model.CekSenet.CekGunlukToplam;

                #region Sql Sorgu çekme

                // MessageBox.Show(fatura.LOGICALREF.ToString());
                //string sqlText = SqlReports.CekSenetGunlukToplam;

                //TODO: Buraya Bakılacak Tekrar

                //_reportTag = RaporcuV2.Helper.ReportTags.CekGunlukToplam;

                //RaporcuV2.DataAccess.RaporcuDbContext cnt = new DataAccess.RaporcuDbContext();
                //var list = cnt.Database.SqlQuery<Model.CekSenet.CekGunlukToplam>(sqlText).ToList();
                //this.gridControl1.DataSource = list;

            };
        }
        #endregion

        #endregion

        #region Refresh Grid Data
        public void RefreshGridData()
        {
            //SqlText = SqlCekSenet.CekToplam;
            _reportTag = RaporcuV2.Helper.ReportTags.CekToplam;
            RefreshGridData<Model.CekSenet.CekToplam>();
        }

        #endregion

        #region Custom Refresh Grid Data

        public void CustomRefreshGridData()
        {
            //SqlText = SqlCekSenet.CekGirisToplam;
            _reportTag = RaporcuV2.Helper.ReportTags.CekGirisToplam;
            RefreshGridData<Model.CekSenet.CekToplamGiris>();
        }

        #endregion
        #endregion

    }
}