﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RaporcuV2.WinApp.Forms.Customs;
using RaporcuV2.Helper;

namespace RaporcuV2.WinApp.Forms.CariHesap
{
    public partial class XtraFormCariSiparisListesi : XtraFormBase, IReport
    {
        public XtraFormCariSiparisListesi()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                CreateFirmLookup();
                CreatePeriodLookup();
                CreateRefreshButton();
                RefreshGridData();
                CreatePivotButton();
            };

        }
        #endregion

        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {

            _reportTag = ReportTags.CariSiparisListesi;
            RefreshGridData<Model.CariHesap.CariSiparisListesi>();

        }

        #endregion
    }
}