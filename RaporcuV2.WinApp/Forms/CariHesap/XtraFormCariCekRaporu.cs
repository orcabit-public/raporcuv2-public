﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RaporcuV2.Helper;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.CariHesap
{
    public partial class XtraFormCariCekRaporu : XtraFormBase, IReport
    {
        public XtraFormCariCekRaporu()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {

                CreateFirmLookup();
                CreatePeriodLookup();


                //CreateFirstDate();
                //CreateLastDate();

                CreateRefreshButton();
                RefreshGridData();

                CreatePivotButton();

            };
        }
        #endregion




        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
            //SqlText = SqlCariHesap.CariCekRaporu;
            base._reportTag = ReportTags.CariCek;
            RefreshGridData<Model.CariHesap.CariCekRaporu>();
        }

        #endregion
    }
}