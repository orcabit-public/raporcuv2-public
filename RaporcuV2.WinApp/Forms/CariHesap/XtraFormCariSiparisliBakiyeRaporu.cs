﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RaporcuV2.WinApp.Forms.Customs;
using RaporcuV2.Helper;

namespace RaporcuV2.WinApp.Forms.CariHesap
{
    public partial class XtraFormCariSiparisliBakiyeRaporu : XtraFormBase, IReport
    {
        public XtraFormCariSiparisliBakiyeRaporu()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {

                CreateFirmLookup();
                CreatePeriodLookup();

                CreateFirstDate();
                CreateLastDate();

                CreateRefreshButton();
                RefreshGridData();

                CreatePivotButton();


            };

        }
        #endregion

        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
            
            _reportTag = ReportTags.SiparisliBakiye;
            RefreshGridData<Model.CariHesap.CariSiparisliBakiye>();

        }

        #endregion
    }
}