﻿namespace RaporcuV2.WinApp.Forms.CariHesap
{
    partial class XtraFormBorcAlacakDurumRaporu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditFirst.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditLast.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrencyRadioGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditCardRadioGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // DateEditFirst
            // 
            // 
            // DateEditLast
            // 
            // 
            // XtraFormBorcAlacakDurumRaporu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 436);
            this.Name = "XtraFormBorcAlacakDurumRaporu";
            this.Text = "Borç Alacak Durum Raporu";
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditFirst.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditLast.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrencyRadioGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditCardRadioGroup)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}