﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.PLinq;
using DevExpress.XtraEditors;
using RaporcuV2.Helper;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.CariHesap
{
    public partial class XtraFormCariAcikBakiye : XtraFormBase, IReport
    {
        public XtraFormCariAcikBakiye()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {

                CreateFirmLookup();
                CreatePeriodLookup();
                CreateDivLookup();

                //CreateFirstDate();
                //CreateLastDate();

                CreateRefreshButton();
                RefreshGridData();


                //CreateFirstDate();
                //CreateLastDate();
                //CreateCurrency();
                //CreateCreditCard();
                CreatePivotButton();

            };
        }
        #endregion




        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {

            _reportTag = ReportTags.AcikBakiye;
            base.RefreshGridData<RaporcuV2.Model.CariHesap.AcikBakiye>();
        }

        #endregion
    }
}