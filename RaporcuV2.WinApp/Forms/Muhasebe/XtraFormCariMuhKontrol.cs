﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Spreadsheet;
using DevExpress.XtraBars;
using RaporcuV2.WinApp.Forms.Customs;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.Model.Reports;
using RaporcuV2.Util;
using RaporcuV2.WinApp.Forms.Customs;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace RaporcuV2.WinApp.Forms.Muhasebe
{
    public partial class XtraFormCariMuhKontrol : XtraFormBase,IReport
    {
        public int SectionStart { get; set; } = 4;
        public XtraFormCariMuhKontrol()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Add Events
        public void AddEvents()
        {

            this.Load += (sender, args) =>
            {
                this.barManagerControl1.gridControl1.Visible = false;
                CreateFirmLookup();
                CreatePeriodLookup();
                CreateFirstDate();
                CreateLastDate();
                CreateRefreshButton();
                RefreshGridData();

                foreach (BarItem barItem in barManagerControl1.barManagerForm.Items)
                {
                    //if (barItem.Name == "firstDate")//{
                    //    ((DevExpress.XtraBars.BarEditItem)barItem).EditValue = DateTime.Today;
                    //}
                }

                barManager1.BeginUpdate();
                var parentTools = this.barManagerControl1.barManagerForm.Bars[0];
                this.barManager1.Bars.Add(parentTools);
                barManager1.EndUpdate();
                commonBar2.Merge(parentTools);
            };
        }
        #endregion

        public void RefreshGridData()
        {
            _reportTag = RaporcuV2.Helper.ReportTags.CariMuhasebeKontrol;
            string title = string.Format("{0} - {1} Muhasebe Cari Kontrol", FirstDate.ToString("dd MMMM yyyy , dddd"), LastDate.ToString("dd MMMM yyyy , dddd"));
            FillExcel<CariMuhasebeKontrol>(spreadsheetControl1,title);
        }
    }
}