﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.PLinq;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.Muhasebe
{
    public partial class XtraFormMizan : XtraFormBase, IReport
    {
        public XtraFormMizan()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                CreateFirmLookup();
                CreatePeriodLookup();
                CreateFirstDate();
                CreateLastDate();
                CreateRefreshButton();
                RefreshGridData();
                CreatePivotButton();

            };
        }
        #endregion




        #endregion

        #region Refresh Grid Data
        public void RefreshGridData()
        {
            //SqlText = SqlMuhasebe.Mizan;
            _reportTag = RaporcuV2.Helper.ReportTags.Mizan;
            RefreshGridData<Mizan>();
        }

        #endregion
    }
}