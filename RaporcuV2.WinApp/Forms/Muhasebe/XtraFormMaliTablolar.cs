﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.PivotGrid.Criteria;
using DevExpress.Spreadsheet;
using DevExpress.Utils.Extensions;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraSpreadsheet.Model;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;
using Worksheet = DevExpress.Spreadsheet.Worksheet;

namespace RaporcuV2.WinApp.Forms.Muhasebe
{
    public partial class XtraFormMaliTablolar : XtraFormBase, IReport
    {
        public int SectionStart { get; set; } = 4;
        private string _s = "A";
        private string _e = "N";
        private int _spaceRow = 2;

        public XtraFormMaliTablolar()
        {
            InitializeComponent();
            this.barManagerControl1.gridControl1.Visible = false;
            AddEvents();
        }

        #region Methots

        #region Add Events

        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {

                CreateFirmLookup();
                CreatePeriodLookup();
                CreateFirstDate();
                

                CreateRefreshButton();

                barManager1.BeginUpdate();

                var parentTools = this.barManagerControl1.barManagerForm.Bars[0];

                this.barManager1.Bars.Add(parentTools);

                barManager1.EndUpdate();

                commonBar1.Merge(parentTools);
            };

            //this.spreadsheetControl1.Dock = DockStyle.Fill;
        }

        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
            #region Sorgular

            _reportTag = RaporcuV2.Helper.ReportTags.Bilanco;
            List<Bilanco> bilancoList = GetPocoList<Bilanco>().Where(a => Math.Round(a.BALANCE, 2) != 0)
                .OrderBy(a => (a.ACODE)).ThenBy(a => a.BCODE)
                .ThenBy(a => a.CODE).ToList();

            _reportTag = RaporcuV2.Helper.ReportTags.GelirTablosu;
            List<GelirTablosu> gelirTablosuList = GetPocoList<GelirTablosu>()
                .OrderBy(a => (a.GRPCODE)).ThenBy(a => a.CODE).ToList();

            #endregion

            BuiltInStyleId headerStyle = BuiltInStyleId.Accent1;

            BuiltInTableStyleId tableStyle = BuiltInTableStyleId.TableStyleLight2;

            spreadsheetControl1.BeginUpdate();

            IWorkbook workbook = spreadsheetControl1.Document;

            if(workbook.Worksheets.Count <2)
            workbook.Worksheets.Add();


            #region Page1

            // Rapor Grubu
            int i = 1;

            Worksheet ws = workbook.Worksheets[0];
            ws.Name = "BİLANÇO";
            ws.ActiveView.PaperKind = PaperKind.A4;
            ws.ActiveView.Orientation = PageOrientation.Landscape;
            ws.ActiveView.Margins.Left = 0;
            ws.ActiveView.Margins.Top = 0;
            ws.ActiveView.Margins.Right = 0;
            ws.ActiveView.Margins.Bottom = 0;

            ws.Clear(ws.GetUsedRange());

            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToWidth = 1;
            ws.PrintOptions.FitToHeight = 0;

            var range = ws.Range[$"A{i}:M{i}"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[BuiltInStyleId.Title];
            ws.Cells[$"A{i}"].Value = FirmName;
            i++;

            range = ws.Range[$"A{i}:M{i}"];
            ws.MergeCells(range);
            range.Value = string.Format("{0} Bilanço", FirstDate.ToString("dd MMMM yyyy , dddd"));
            range.Style = workbook.Styles[BuiltInStyleId.Heading1];


            #region Bilanco

            i = ws.GetUsedRange().RowCount + 2;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            var typeNr = bilancoList.GroupBy(a => a.TYPENR).OrderBy(o => o.Key);

            foreach (var nr in typeNr)
            {
                int aktifrow = i;

                if (nr.Key == 0)
                {
                    range = ws.Range[$"B{i}:F{i}"];
                    range.Value = "AKTİF HESAPLAR";
                    range.Font.Bold = true;

                    #region Bilanco Aktifleri

                    var aktifler = bilancoList.Where(a => a.TYPENR == nr.Key)
                        .GroupBy(g => new {g.ACODE, g.ADEFINITION_});
                    aktifrow++;
                    foreach (var aktif in aktifler)
                    {
                        double aTotal = 0;
                        var aktifRange = ws.Range[$"C{aktifrow}:E{aktifrow}"];
                        ws.MergeCells(aktifRange);
                        aktifRange.Value = aktif.Key.ADEFINITION_;
                        aktifRange.ColumnWidthInCharacters = aktif.Key.ADEFINITION_.Length;
                        range.Font.Bold = true;

                        var aktifTotal = ws.Range[$"F{aktifrow}:F{aktifrow}"];
                        aktifTotal.Font.Bold = true;
                        aktifTotal.Value = bilancoList.Where(a => a.ACODE == aktif.Key.ACODE).Sum(s => s.BALANCE);

                        #region 2. kırılım

                        aktifrow++;

                        var bList = bilancoList.Where(a => a.ACODE == aktif.Key.ACODE)
                            .GroupBy(g => new {g.BCODE, g.BDEFINITION});
                        foreach (var b in bList)
                        {
                            double bTotal = 0;
                            var bRange = ws.Range[$"D{aktifrow}:E{aktifrow}"];
                            ws.MergeCells(bRange);
                            bRange.Value = b.Key.BDEFINITION;
                            bRange.Font.Bold = true;
                            bRange.ColumnWidthInCharacters = b.Key.BDEFINITION.Length;

                            var bTotalRange = ws.Range[$"F{aktifrow}:F{aktifrow}"];
                            bTotalRange.Font.Bold = true;
                            bTotalRange.Value = bilancoList.Where(a => a.BCODE == b.Key.BCODE).Sum(s => s.BALANCE);

                            aktifrow++;

                            #region 3. Kırılım

                            var kebirList = bilancoList.Where(a => a.BCODE == b.Key.BCODE);
                            foreach (var kebir in kebirList)
                            {
                                var kebirRange = ws.Range[$"E{aktifrow}:E{aktifrow}"];
                                ws.MergeCells(kebirRange);
                                kebirRange.Value = $"{kebir.CODE} {kebir.DEFINITION_}";
                                kebirRange.ColumnWidthInCharacters = 50;

                                var kebirTotalRange = ws.Range[$"F{aktifrow}:F{aktifrow}"];
                                ws.MergeCells(kebirTotalRange);
                                kebirTotalRange.Value = (Math.Round(kebir.BALANCE, 2));
                                kebirTotalRange.ColumnWidthInCharacters = 20;
                                kebirTotalRange.Name = $"H_{kebir.CODE}";
                                //if (aktifrow % 2 != 0)
                                //{
                                //    kebirRange.Style = workbook.Styles[BuiltInStyleId.Accent1_20percent];
                                //    kebirTotalRange.Style = workbook.Styles[BuiltInStyleId.Accent1_20percent];
                                //}

                                aktifrow++;
                            }


                            #endregion
                        }

                        #endregion

                        //aktifrow++;
                    }

                    #endregion

                }
                else if (nr.Key == 1)
                {
                    range = ws.Range[$"H{i}:L{i}"];
                    range.Value = "PASİF HESAPLAR";

                    #region Bilanco Aktifleri

                    var aktifler = bilancoList.Where(a => a.TYPENR == nr.Key)
                        .GroupBy(g => new {g.ACODE, g.ADEFINITION_});
                    aktifrow++;
                    foreach (var aktif in aktifler)
                    {

                        var aktifRange = ws.Range[$"I{aktifrow}:L{aktifrow}"];
                        ws.MergeCells(aktifRange);
                        aktifRange.Value = aktif.Key.ADEFINITION_;
                        aktifRange.ColumnWidthInCharacters = aktif.Key.ADEFINITION_.Length;
                        range.Font.Bold = true;

                        #region 2. kırılım

                        aktifrow++;

                        var bList = bilancoList.Where(a => a.ACODE == aktif.Key.ACODE)
                            .GroupBy(g => new {g.BCODE, g.BDEFINITION});
                        foreach (var b in bList)
                        {
                            var bRange = ws.Range[$"J{aktifrow}:L{aktifrow}"];
                            ws.MergeCells(bRange);
                            bRange.Value = b.Key.BDEFINITION;
                            bRange.Font.Bold = true;
                            bRange.ColumnWidthInCharacters = b.Key.BDEFINITION.Length;
                            aktifrow++;

                            #region 3. Kırılım

                            var kebirList = bilancoList.Where(a => a.BCODE == b.Key.BCODE);
                            foreach (var kebir in kebirList)
                            {
                                var kebirRange = ws.Range[$"K{aktifrow}:K{aktifrow}"];
                                ws.MergeCells(kebirRange);
                                kebirRange.Value = $"{kebir.CODE} {kebir.DEFINITION_}";
                                kebirRange.ColumnWidthInCharacters = 50;

                                var kebirTotalRange = ws.Range[$"L{aktifrow}:L{aktifrow}"];
                                ws.MergeCells(kebirTotalRange);
                                kebirTotalRange.Value = Math.Round(kebir.BALANCE, 2);
                                kebirTotalRange.ColumnWidthInCharacters = 20;
                                kebirTotalRange.Name = $"H_{kebir.CODE}";
                                //if (aktifrow % 2 != 0)
                                //{
                                //    kebirRange.Style = workbook.Styles[BuiltInStyleId.Accent1_20percent];
                                //    kebirTotalRange.Style = workbook.Styles[BuiltInStyleId.Accent1_20percent];
                                //}
                                aktifrow++;
                            }


                            #endregion
                        }

                        #endregion

                        //aktifrow++;
                    }

                    #endregion
                }

                ws.MergeCells(range);
                range.Style = workbook.Styles[headerStyle];
            }

            i++;

            int gTotalRow = ws.GetUsedRange().RowCount + 1;
            range = ws.Range[$"B{gTotalRow}:E{gTotalRow}"];
            range.Value = "AKTİF HESAPLAR TOPLAMI";
            ws.MergeCells(range);
            range.Style = workbook.Styles[BuiltInStyleId.Accent1];
            range = ws.Range[$"F{gTotalRow}:F{gTotalRow}"];
            range.Value = bilancoList.Where(a => a.TYPENR == 0).Sum(s => s.BALANCE);
            range.Style = workbook.Styles[BuiltInStyleId.Accent1];

            range = ws.Range[$"H{gTotalRow}:K{gTotalRow}"];
            range.Value = "PASİF HESAPLAR TOPLAMI";
            ws.MergeCells(range);
            range.Style = workbook.Styles[BuiltInStyleId.Accent1];
            range = ws.Range[$"L{gTotalRow}:L{gTotalRow}"];
            range.Value = bilancoList.Where(a => a.TYPENR == 1).Sum(s => s.BALANCE);
            range.Style = workbook.Styles[BuiltInStyleId.Accent1];

            //range = ws.Cells.FirstOrDefault(a => a.Name.Equals("H_590"));
            //range.Formula = $"='GELİR TABLOSU'!H_692";
            //range.NumberFormat = "#,##0.00";


            int dataCount = 0;

            #endregion

            var allCell = ws.GetUsedRange();

            allCell.Alignment.WrapText = false;

            for (int j = 0; j < 4; j++)
            {
                ws.Columns[j].Width = ws.Rows[3].RowHeight;
                ws.Columns[j].Font.Bold = true;
                ws.Columns[j].AutoFitColumns();
            }

            for (int j = 6; j < 10; j++)
            {
                ws.Columns[j].Width = ws.Rows[3].RowHeight;
                ws.Columns[j].Font.Bold = true;
                ws.Columns[j].AutoFitColumns();
            }

            ws.Columns[5].NumberFormat = "#,##0.00";
            ws.Columns[11].NumberFormat = "#,##0.00";
            ws.Columns[12].Width = ws.Rows[3].RowHeight;

            #endregion


            #endregion

            #region Gelir Tablosu

            i = 1;

            ws = workbook.Worksheets[1];
            ws.Name = "GELİR TABLOSU";
            ws.ActiveView.PaperKind = PaperKind.A4;
            ws.ActiveView.Orientation = PageOrientation.Landscape;
            ws.ActiveView.Margins.Left = 0;
            ws.ActiveView.Margins.Top = 0;
            ws.ActiveView.Margins.Right = 0;
            ws.ActiveView.Margins.Bottom = 0;

            ws.Clear(ws.GetUsedRange());

            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToWidth = 1;
            ws.PrintOptions.FitToHeight = 0;

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[BuiltInStyleId.Title];
            ws.Cells[$"A{i}"].Value = FirmName;
            i++;

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = string.Format("{0} Gelir Tablosu", FirstDate.ToString("dd MMMM yyyy , dddd"));
            range.Style = workbook.Styles[BuiltInStyleId.Heading1];

            i = ws.GetUsedRange().RowCount + 2;
            SectionStart = i;

            int aktifrow1 = i;


            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "GELİR TABLOSU";
            range.Font.Bold = true;
            range.Style = workbook.Styles[BuiltInStyleId.Accent1];

            #region 2. kırılım

            var aktifler1 = gelirTablosuList.GroupBy(a => new {a.GRPCODE, a.GRPDEFINITION_});
            aktifrow1++;
            foreach (var aktif in aktifler1)
            {
                double aTotal = 0;
                var aktifRange = ws.Range[$"B{aktifrow1}:C{aktifrow1}"];
                ws.MergeCells(aktifRange);
                aktifRange.Value = aktif.Key.GRPDEFINITION_;
                aktifRange.ColumnWidthInCharacters = aktif.Key.GRPDEFINITION_.Length;
                range.Font.Bold = true;

                var aktifTotal = ws.Range[$"D{aktifrow1}:D{aktifrow1}"];
                aktifTotal.Font.Bold = true;
                aktifTotal.Value = gelirTablosuList.Where(a => a.GRPCODE == aktif.Key.GRPCODE).Sum(s => s.BALANCE);
                

                #region 2. kırılım

                aktifrow1++;

                var bList = gelirTablosuList.Where(a => a.GRPCODE== aktif.Key.GRPCODE);
                foreach (var b in bList)
                {
                    double bTotal = 0;
                    var bRange = ws.Range[$"C{aktifrow1}:C{aktifrow1}"];
                    ws.MergeCells(bRange);
                    bRange.Value = $"{b.CODE} {b.DEFINITION_}";
                    //bRange.Font.Bold = true;
                    bRange.ColumnWidthInCharacters = 80;

                    var bTotalRange = ws.Range[$"D{aktifrow1}:D{aktifrow1}"];
                    bTotalRange.Name = $"H_{b.CODE}";
                    bTotalRange.Value = gelirTablosuList.Where(a => a.CODE == b.CODE).Sum(s => s.BALANCE);
                    bTotalRange.ColumnWidthInCharacters = 25;
                    aktifrow1++;
                }

                #endregion

                //aktifrow++;
            }

            range = ws.Cells.FirstOrDefault(a => a.Name.Equals("H_692"));
            range.Formula = $"H_690-H_691";
            range.NumberFormat = "#,##0.00";


            i++;

            allCell = ws.GetUsedRange();

            allCell.Alignment.WrapText = false;

            for (int j = 0; j < 2; j++)
            {
                ws.Columns[j].Width = ws.Rows[3].RowHeight;
                ws.Columns[j].Font.Bold = true;
                ws.Columns[j].AutoFitColumns();
            }

            i = aktifrow1++;
            range = ws.Range[$"A{i}:D{i+1}"];
            //range.Value = "PASİF HESAPLAR TOPLAMI";
            ws.MergeCells(range);
            range.Style = workbook.Styles[BuiltInStyleId.Accent1];
           
            ws.Columns[3].NumberFormat = "#,##0.00";

            #endregion

            #endregion


            #endregion

            ws = workbook.Worksheets[0];
            range = ws.Cells.FirstOrDefault(a => a.Name.Equals("H_590"));
            range.Formula = $"'GELİR TABLOSU'!H_692";
            range.NumberFormat = "#,##0.00";

            spreadsheetControl1.EndUpdate();

            #endregion
        }
    }
}