﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.PLinq;
using DevExpress.XtraEditors;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;
using RaporcuV2.Helper.Sorgular;

namespace RaporcuV2.WinApp.Forms.Muhasebe
{
    public partial class XtraFormBaBs : XtraFormBase, IReport
    {
        public XtraFormBaBs()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                CreateFirmLookup();
                CreatePeriodLookup();
                CreateRefreshButton();
                RefreshGridData();

            };
        }
        #endregion




        #endregion

        #region Refresh Grid Data
        
        public void RefreshGridData()
        {
            //SqlText = SqlMuhasebe.FormBaBs;
            _reportTag = RaporcuV2.Helper.ReportTags.BABS;
            RefreshGridData<FormBaBs>();

        }

        #endregion
    }
}