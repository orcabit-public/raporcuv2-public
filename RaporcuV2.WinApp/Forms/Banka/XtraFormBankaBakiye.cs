﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.PLinq;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.Banka
{
    public partial class XtraFormBankaBakiye : XtraFormBase, IReport
    {
        public XtraFormBankaBakiye()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                CreateFirmLookup();
                CreatePeriodLookup();
                CreateDivLookup();
                //CreateCurrency();
                CreateFirstDate();
                CreateLastDate();
                RefreshGridData();
                CreateRefreshButton();
                CreatePivotButton();

            };
        }
        #endregion

        #endregion

        #region Refresh Grid Data
        public void RefreshGridData()
        {
            //SqlText = SqlBanka.BankaBakiye;
            _reportTag = RaporcuV2.Helper.ReportTags.BankaBakiye;
            RefreshGridDataCustom(null);

        }

        #endregion
    }
}