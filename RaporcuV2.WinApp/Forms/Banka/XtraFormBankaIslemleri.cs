﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.PLinq;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.Banka
{
    public partial class XtraFormBankaIslemleri : XtraFormBase, IReport
    {
        public XtraFormBankaIslemleri()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {

                CreateFirmLookup();
                CreatePeriodLookup();
                CreateDivLookup();

                CreateRefreshButton();
                RefreshGridData();
                CreatePivotButton();

            };
        }
        #endregion




        #endregion

        #region Refresh Grid Data
        // sql Poco Class doluyor, Model ve Helper Projelerinin içerisindeler
        public void RefreshGridData()
        {
            //SqlText = SqlBanka.BankaIslemleri;
            _reportTag = RaporcuV2.Helper.ReportTags.Bankaislemleri;
            RefreshGridData<Model.Banka.BankaIslemleri>();
        }

        #endregion
    }
}