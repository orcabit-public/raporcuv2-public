﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Spreadsheet;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model.Reports;
using RaporcuV2.Util;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.Kasa
{
    public partial class XtraFormNakitAkis : XtraFormBase, IReport
    {
        public int SectionStart { get; set; } = 4;
        private string _s = "A";
        private string _e = "N";
        private int _spaceRow = 2;

        public XtraFormNakitAkis()
        {
            InitializeComponent();
            this.barManagerControl1.gridControl1.Visible = false;
            AddEvents();
        }

        #region Methots

        #region Add Events

        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                IWorkbook workbook = spreadsheetControl1.Document;
                Worksheet ws = workbook.Worksheets[0];
                ws.Name = "Nakit Akış Detay";


                CreateFirmLookup();
                CreatePeriodLookup();
                CreateFirstDate();
                foreach (BarItem barItem in barManagerControl1.barManagerForm.Items)
                {
                    if (barItem.Name == "firstDate")
                    {
                        ((DevExpress.XtraBars.BarEditItem)barItem).EditValue = DateTime.Today;
                        //DateEdit dateedit = ((BarEditItem)barItem) as DateEdit;
                        ((DevExpress.XtraEditors.Repository.RepositoryItemDateEdit)
                            ((DevExpress.XtraBars.BarEditItem)barItem).Edit).MinValue = DateTime.Today;

                    }
                }

                CreateRefreshButton();

                barManager1.BeginUpdate();

                var parentTools = this.barManagerControl1.barManagerForm.Bars[0];

                this.barManager1.Bars.Add(parentTools);

                barManager1.EndUpdate();

                commonBar1.Merge(parentTools);
            };

            //this.spreadsheetControl1.Dock = DockStyle.Fill;
        }

        #endregion

        #endregion
        #region Refresh Grid Data

        public void RefreshGridData()
        {
            #region Sorgular

            _reportTag = RaporcuV2.Helper.ReportTags.NakitAkisVarliklar;
            List<NakitAkisOzet> varliklar = GetPocoList<NakitAkisOzet>();

            _reportTag = RaporcuV2.Helper.ReportTags.NakitAkisBorclarKredi;
            List<NakitAkisOzet> borclarKredi = GetPocoList<NakitAkisOzet>();

            _reportTag = RaporcuV2.Helper.ReportTags.NakitAkisBorclarCeklerimiz;
            List<NakitAkisOzet> borclarCeklerimiz = GetPocoList<NakitAkisOzet>();
            #endregion

            #region Başlık
            // Varlıklar
            BuiltInStyleId kasaBaslikStyle = BuiltInStyleId.Accent1;
            BuiltInTableStyleId kasaStyle = BuiltInTableStyleId.TableStyleLight2;

            // Borçlar Kredi
            BuiltInStyleId bankaBaslikStyle = BuiltInStyleId.Accent2;
            BuiltInTableStyleId bankaStyle = BuiltInTableStyleId.TableStyleLight3;

            // Borçlar  Çeklerimiz
            BuiltInStyleId cekBaslikStyle = BuiltInStyleId.Accent3;
            BuiltInTableStyleId cekStyle = BuiltInTableStyleId.TableStyleLight4;

            BuiltInStyleId kurEuroBaslikStyle = BuiltInStyleId.Accent6;
            BuiltInTableStyleId kurEuroStyle = BuiltInTableStyleId.TableStyleMedium7;

            BuiltInStyleId kurDolarBaslikStyle = BuiltInStyleId.Accent3;
            BuiltInTableStyleId kurDolarStyle = BuiltInTableStyleId.TableStyleMedium4;
            spreadsheetControl1.BeginUpdate();

            // Rapor Grubu
            int i = 1;
            IWorkbook workbook = spreadsheetControl1.Document;
            Worksheet ws = workbook.Worksheets[0];

            
            ws.ActiveView.PaperKind = PaperKind.A4;
            ws.ActiveView.Orientation = PageOrientation.Landscape;
            ws.ActiveView.Margins.Left = 0;
            ws.ActiveView.Margins.Top = 0;
            ws.ActiveView.Margins.Right = 0;
            ws.ActiveView.Margins.Bottom = 0;

            ws.Clear(ws.GetUsedRange());
            

            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToWidth = 1;
            ws.PrintOptions.FitToHeight = 0;

            var range = ws.Range[$"A{i}:I{i}"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[BuiltInStyleId.Title];
            ws.Cells[$"A{i}"].Value = FirmName;
            i++;

            range = ws.Range[$"A{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = string.Format("{0} Nakit Akışı", FirstDate.ToString("dd MMMM yyyy , dddd"));
            range.Style = workbook.Styles[BuiltInStyleId.Heading1];

            #region KurEuro
            TrCurr = 20;
            //SqlText = SqlReports.Kur;
            _reportTag = RaporcuV2.Helper.ReportTags.KurBuGun;
            List<Kur> Kur = GetPocoList<Kur>();

            double kur = 0;

            if (Kur.Count > 0)
            {
                kur = (double)Kur.FirstOrDefault().RATES1;
            }
            range = ws.Range["B3"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[kurEuroBaslikStyle];
            ws.Cells["B3"].Value = kur;
            ws.Cells["B3"].Style.NumberFormat = "€#,##0.00000";


            #endregion

            #region KurDolar
            TrCurr = 1;
            //SqlText = SqlReports.Kur;
            _reportTag = RaporcuV2.Helper.ReportTags.KurBuGun;
            List<Kur> Kur1 = GetPocoList<Kur>();

            double kur1 = 0;

            if (Kur1.Count > 0)
            {
                kur1 = (double)Kur1.FirstOrDefault().RATES1;
            }

            range = ws.Range["B4"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[kurDolarBaslikStyle];
            ws.Cells["B4"].Value = kur1;
            ws.Cells["B4"].Style.NumberFormat = "$#,##0.00000";


            #endregion

            #endregion

            #region Toplamlar Doviz Turu

            Dictionary<string, double> dovizList = new Dictionary<string, double>();
            Dictionary<string, double> dovizTotals = new Dictionary<string, double>();

            #region Doviz List 

            #endregion

            #endregion

            #region Varliklar  

            i = ws.GetUsedRange().RowCount + dovizList.Count + 1;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:N{i}"];
            ws.MergeCells(range);
            range.Value = "Varlıklar";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            int dataCount = 0;

            #endregion

            var currencyList = varliklar;
            bool isFirstRow = true;

                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = varliklar.Count(a => a.YIL == a.YIL);
                dataCount = dataRowCount + i;
                Table tableVarliklar = ws.Tables.Add(ws[$"A{i}:N{dataCount}"], true);
                tableVarliklar.Name = $"A_Varliklar";
                tableVarliklar.ShowTotals = true;
                tableVarliklar.AutoFilter.Disable();
                tableVarliklar.Style = workbook.TableStyles[kasaStyle];
                //
                tableVarliklar.Columns[0].Name = $"YIL";
                tableVarliklar.Columns[0].TotalRowLabel = "Toplam : ";
                tableVarliklar.Columns[1].Name = "AY";
                tableVarliklar.DataRange.NumberFormat = "#,##0.00";
                tableVarliklar.Columns[2].Name = "TL VARLIKLAR";
                tableVarliklar.Columns[3].Name = "USD VARLIKLAR"; 
                tableVarliklar.Columns[4].Name = "EUR VARLIKLAR";
                tableVarliklar.Columns[5].Name = "VARLIKLAR TOPLAM TL";
                tableVarliklar.Columns[6].Name = "TL BORCLAR";
                tableVarliklar.Columns[7].Name = "USD BORCLAR";
                tableVarliklar.Columns[8].Name = "EURO BORCLAR";
                tableVarliklar.Columns[9].Name = "BORCLAR TOPLAM TL";
                tableVarliklar.Columns[10].Name = "TL FARK";
                tableVarliklar.Columns[11].Name = "USD FARK";
                tableVarliklar.Columns[12].Name = "EURO FARK";
                tableVarliklar.Columns[13].Name = "FARK TOPLAM TL";
            foreach (var item in varliklar)
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.YIL;
                    ws.Cells[$"A{i}"].NumberFormat = "#,##0";
                ws.Cells[$"B{i}"].Value = item.AY;
                    ws.Cells[$"B{i}"].NumberFormat = "#,##0";
                    ws.Cells[$"C{i}"].Value = item.TL_VARLIKLAR;
                    ws.Cells[$"D{i}"].Value = item.USD_VARLIKLAR;
                    ws.Cells[$"E{i}"].Value = item.EUR_VARLIKLAR;
                    ws.Cells[$"F{i}"].Formula = $"C{i} + (D{i}* B4)+ (E{i}*B3)";

                    ws.Cells[$"G{i}"].Value = item.TL_BORCLAR;
                    ws.Cells[$"H{i}"].Value = item.USD_BORCLAR;
                    ws.Cells[$"I{i}"].Value = item.EUR_BORCLAR;
                    ws.Cells[$"J{i}"].Formula = $"G{i} + (H{i}* B4)+ (I{i}*B3)";

                    ws.Cells[$"K{i}"].Formula = $"C{i}-G{i}";
                    ws.Cells[$"L{i}"].Formula = $"D{i}-H{i}";
                    ws.Cells[$"M{i}"].Formula = $"E{i}-I{i}";
                    ws.Cells[$"N{i}"].Formula = $"K{i} + (L{i}* B4)+ (M{i}*B3)";

            }

                tableVarliklar.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableVarliklar.Columns)
                {
                    if (column.Index != 0 ) if  (column.Index != 1)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            

            #endregion



            var allCell1 = ws.GetUsedRange();
            allCell1.Alignment.WrapText = true;
            spreadsheetControl1.EndUpdate();
        }


        #endregion

    }
}