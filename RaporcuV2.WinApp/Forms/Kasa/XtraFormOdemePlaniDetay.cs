﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Spreadsheet;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model.Reports;
using RaporcuV2.Util;
using RaporcuV2.WinApp.Forms.Customs;


namespace RaporcuV2.WinApp.Forms.Kasa
{
    public partial class XtraFormOdemePlaniDetay : XtraFormBase, IReport
    {
        public int SectionStart { get; set; } = 4;
        private string _s = "A";
        private string _e = "N";
        private int _spaceRow = 2;

        public XtraFormOdemePlaniDetay()
        {
            InitializeComponent();
            this.barManagerControl1.gridControl1.Visible = false;
            AddEvents();
        }

        #region Methots

        #region Add Events

        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                IWorkbook workbook = spreadsheetControl1.Document;
                Worksheet ws = workbook.Worksheets[0];
                ws.Name = "Ödeme Planı Detay";
                workbook.Worksheets.Add("Özet");

                CreateFirmLookup();
                CreatePeriodLookup();
                CreateFirstDate();
                foreach (BarItem barItem in barManagerControl1.barManagerForm.Items)
                {
                    if (barItem.Name == "firstDate")
                    {
                        ((DevExpress.XtraBars.BarEditItem) barItem).EditValue = DateTime.Today;
                        //DateEdit dateedit = ((BarEditItem)barItem) as DateEdit;
                        ((DevExpress.XtraEditors.Repository.RepositoryItemDateEdit)
                            ((DevExpress.XtraBars.BarEditItem) barItem).Edit).MinValue = DateTime.Today;

                    }
                }

                CreateRefreshButton();

                barManager1.BeginUpdate();

                var parentTools = this.barManagerControl1.barManagerForm.Bars[0];

                this.barManager1.Bars.Add(parentTools);

                barManager1.EndUpdate();

                commonBar1.Merge(parentTools);
            };

            //this.spreadsheetControl1.Dock = DockStyle.Fill;
        }

        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
            #region Sorgular

            //SqlText = SqlNakitAkis.AlacakKasalar;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniAlacakKasalar;
            List<NakitAkis> alacakKasalar = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.AlacakBankalar;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniAlacakBankalar;
            List<NakitAkis> alacakBankalar = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.AlacakCekSenet;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniAlacakCekSenet;
            List<NakitAkis> alacakCekSenet = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.AlacakCariHesap;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniAlacakCariHesap;
            List<NakitAkis> alacakCariHesap = GetPocoList<NakitAkis>();

            //SqlText = SqlNakitAkis.BorcKrediler;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniBorcKredilerDetay;
            List<NakitAkis> borcBorcKrediler = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.BorcCekler;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniBorcCekler;
            List<NakitAkis> borcBorcKCekler = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.BorcKrediKarti;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniBorcKrediKartiDetay;
            List<NakitAkis> borcBorcKrediKarti = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.BorcCariHesap;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniBorcCariHesap;
            List<NakitAkis> borcCariHesap = GetPocoList<NakitAkis>();



            #region Diğer Borçlar

            List<NakitAkis> digerBorclar = new List<NakitAkis>();
            digerBorclar.Add(new NakitAkis()
            {
                TYPENR = 100,
                TYPE = "Diğer Borçlar",
                CURCODE = "TL",
                CURTYPE = 0,
                DUEDATE = DateTime.Today,
                EXP = "",
                NAME = "KDV",
                TOTAL = 0
            });
            digerBorclar.Add(new NakitAkis()
            {
                TYPENR = 100,
                TYPE = "Diğer Borçlar",
                CURCODE = "TL",
                CURTYPE = 0,
                DUEDATE = DateTime.Today,
                EXP = "",
                NAME = "KDV2",
                TOTAL = 0
            });
            digerBorclar.Add(new NakitAkis()
            {
                TYPENR = 100,
                TYPE = "Diğer Borçlar",
                CURCODE = "TL",
                CURTYPE = 0,
                DUEDATE = DateTime.Today,
                EXP = "",
                NAME = "Muhtasar",
                TOTAL = 0
            });
            digerBorclar.Add(new NakitAkis()
            {
                TYPENR = 100,
                TYPE = "Diğer Borçlar",
                CURCODE = "TL",
                CURTYPE = 0,
                DUEDATE = DateTime.Today,
                EXP = "",
                NAME = "Kurumlar / Gelir Vergisi",
                TOTAL = 0
            });
            digerBorclar.Add(new NakitAkis()
            {
                TYPENR = 100,
                TYPE = "Diğer Borçlar",
                CURCODE = "TL",
                CURTYPE = 0,
                DUEDATE = DateTime.Today,
                EXP = "",
                NAME = "SGK",
                TOTAL = 0
            });
            digerBorclar.Add(new NakitAkis()
            {
                TYPENR = 100,
                TYPE = "Diğer Borçlar",
                CURCODE = "TL",
                CURTYPE = 0,
                DUEDATE = DateTime.Today,
                EXP = "",
                NAME = "Bağkur",
                TOTAL = 0
            });




            #endregion

            #endregion

            #region Başlık
            // Kasa ve Kasa Detay Renkelnedirme
            BuiltInStyleId kasaBaslikStyle = BuiltInStyleId.Accent1;
            BuiltInTableStyleId kasaStyle = BuiltInTableStyleId.TableStyleLight2;

            // Banka ve Banka HAreketerli Renklendirme
            BuiltInStyleId bankaBaslikStyle = BuiltInStyleId.Accent2;
            BuiltInTableStyleId bankaStyle = BuiltInTableStyleId.TableStyleLight3;

            // Kredi Durumu 
            BuiltInStyleId cekBaslikStyle = BuiltInStyleId.Accent3;
            BuiltInTableStyleId cekStyle = BuiltInTableStyleId.TableStyleLight4;

            // Çek Durumu 
            BuiltInStyleId cariBaslikStyle = BuiltInStyleId.Accent4;
            BuiltInTableStyleId cariStyle = BuiltInTableStyleId.TableStyleLight5;

            BuiltInStyleId kurEuroBaslikStyle = BuiltInStyleId.Accent6;
            BuiltInTableStyleId kurEuroStyle = BuiltInTableStyleId.TableStyleMedium7;

            BuiltInStyleId kurDolarBaslikStyle = BuiltInStyleId.Accent3;
            BuiltInTableStyleId kurDolarStyle = BuiltInTableStyleId.TableStyleMedium4;
            spreadsheetControl1.BeginUpdate();


            // Rapor Grubu
            int i = 1;
            IWorkbook workbook = spreadsheetControl1.Document;
            Worksheet ws = workbook.Worksheets[0];

            Worksheet ws1 = workbook.Worksheets[1];
            ws.ActiveView.PaperKind = PaperKind.A4;
            ws.ActiveView.Orientation = PageOrientation.Landscape;
            ws.ActiveView.Margins.Left = 0;
            ws.ActiveView.Margins.Top = 0;
            ws.ActiveView.Margins.Right = 0;
            ws.ActiveView.Margins.Bottom = 0;

            ws.Clear(ws.GetUsedRange());
            ws1.Clear(ws1.GetUsedRange());

            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToWidth = 1;
            ws.PrintOptions.FitToHeight = 0;

            var range = ws.Range[$"A{i}:I{i}"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[BuiltInStyleId.Title];
            ws.Cells[$"A{i}"].Value = FirmName;
            i++;

            range = ws.Range[$"A{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = string.Format("{0} Ödeme Planı", FirstDate.ToString("dd MMMM yyyy , dddd"));
            range.Style = workbook.Styles[BuiltInStyleId.Heading1];
            #endregion

            #region Toplamlar Doviz Turu

            Dictionary<string, double> dovizList = new Dictionary<string, double>();
            Dictionary<string, double> dovizTotals = new Dictionary<string, double>();

            #region Doviz List 

            var exchange = alacakKasalar.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }

            exchange = alacakBankalar.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }

            exchange = alacakCekSenet.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }

            exchange = alacakCariHesap.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }

            exchange = borcBorcKrediler.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }

            exchange = borcBorcKCekler.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }

            exchange = borcBorcKrediKarti.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }

            exchange = borcCariHesap.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }

            #endregion

            i = ws.GetUsedRange().RowCount + 1;
            foreach (var exc in dovizList)
            {
                var pb = exc.Key;
                double sum = alacakKasalar.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);
                sum += alacakBankalar.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);
                sum += alacakCekSenet.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);
                sum += alacakCariHesap.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);

                ws.Cells[$"C{i}"].Value = pb;
                ws.Cells[$"D{i}"].Name = $"A{pb}";
                ws.Cells[$"D{i}"].NumberFormat = "#,##0.00";



                sum = 0;
                sum = borcBorcKrediler.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);
                sum += borcBorcKCekler.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);
                sum += borcBorcKrediKarti.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);
                sum += borcCariHesap.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);

                ws.Cells[$"H{i}"].Value = pb;
                ws.Cells[$"I{i}"].Name = $"B{pb}";
                ws.Cells[$"I{i}"].NumberFormat = "#,##0.00";

                digerBorclar.Add(new NakitAkis()
                {
                    TYPENR = 100,
                    TYPE = "Diğer Borçlar",
                    CURCODE = exc.Key,
                    CURTYPE = 0, DUEDATE = DateTime.Today,
                    EXP = "",
                    NAME = $"Diğer {exc.Key} Borçlar",
                    TOTAL = 0
                });

                i++;
            }

            i = ws.GetUsedRange().RowCount + 2;
            int condStart = i;
            foreach (var exc in exchange)
            {
                var pb = exc.pb;
                ws.Cells[$"C{i}"].Value = $"Fark {pb}";
                ws.Cells[$"C{i}"].Font.Bold = true;
                ws.Cells[$"D{i}"].Name = $"Fark_{pb}";
                ws.Cells[$"D{i}"].Font.Bold = true;
                ws.Cells[$"D{i}"].Formula = $"A{pb}-B{pb}";
                ws.Cells[$"D{i}"].NumberFormat = "#,##0.00";
                i++;
            }

            #region Conditional Formatting

            var enumerable = ws.Cells.Where(a => a.Name.StartsWith("Fark"));
            string val = $"$D${condStart}:$D${i}";
            var cfRange = ws[val];
            ExpressionConditionalFormatting cfRule =
                ws.ConditionalFormattings.AddExpressionConditionalFormatting(cfRange,
                    ConditionalFormattingExpressionCondition.LessThan, "=0");

            cfRule.Formatting.Font.Color = Color.Red;

            #endregion

            #endregion

            #region Kasa Durumu 

            i = ws.GetUsedRange().RowCount + dovizList.Count + 1;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "Kasalar";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            int dataCount = 0;

            #endregion

            var currencyList = alacakKasalar.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            bool isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = alacakKasalar.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"A{i}:D{dataCount}"], true);
                tableBanka.Name = $"A_Kasa_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in alacakKasalar.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.TYPE;
                    ws.Cells[$"B{i}"].Value = item.NAME;
                    ws.Cells[$"C{i}"].Value = item.EXP;
                    ws.Cells[$"D{i}"].Value = item.TOTAL;

                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #region Banka Durumu 

            i = ws.GetUsedRange().RowCount + 2;
            //SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "Bankalar";
            range.Style = workbook.Styles[bankaBaslikStyle];
            //i++;


            #endregion

            isFirstRow = true;
            currencyList = alacakBankalar.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});

            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = alacakBankalar.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"A{i}:D{dataCount}"], true);
                tableBanka.Name = $"A_Banka_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[bankaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in alacakBankalar.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.TYPE;
                    ws.Cells[$"B{i}"].Value = item.NAME;
                    ws.Cells[$"C{i}"].Value = item.EXP;
                    ws.Cells[$"D{i}"].Value = item.TOTAL;

                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #region Çek Senet

            i = ws.GetUsedRange().RowCount + 2;
            //SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "Müşteri Çek Senetleri";
            range.Style = workbook.Styles[cekBaslikStyle];
            //i++;


            #endregion

            isFirstRow = true;
            currencyList = alacakCekSenet.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});

            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = alacakCekSenet.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"A{i}:D{dataCount}"], true);
                tableBanka.Name = $"A_CekSenet_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[cekStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in alacakCekSenet.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.TYPE;
                    ws.Cells[$"B{i}"].Value = item.NAME;
                    ws.Cells[$"C{i}"].Value = item.EXP;
                    ws.Cells[$"D{i}"].Value = item.TOTAL;

                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #region Cari Hesap

            i = ws.GetUsedRange().RowCount + 2;
            //SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "Müşterilerden Alacaklar";
            range.Style = workbook.Styles[cariBaslikStyle];
            //i++;


            #endregion

            isFirstRow = true;
            currencyList = alacakCariHesap.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});

            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = alacakCariHesap.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"A{i}:D{dataCount}"], true);
                tableBanka.Name = $"A_Cari_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[cariStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in alacakCariHesap.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.TYPE;
                    ws.Cells[$"B{i}"].Value = item.NAME;
                    ws.Cells[$"C{i}"].Value = item.EXP;
                    ws.Cells[$"D{i}"].Value = item.TOTAL;

                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #region Krediler

            dataCount = 0;
            i = SectionStart;
            //SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Krediler";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = borcBorcKrediler.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = borcBorcKrediler.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.Name = $"B_Kredi_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in borcBorcKrediler.Where(a => a.CURCODE == curCode.pb).OrderBy(o => o.DUEDATE))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = string.IsNullOrEmpty(item.TYPE) ? "Krediler" : item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.DUEDATE.ToString("d");
                    ws.Cells[$"H{i}"].Value = item.EXP;
                    ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableBanka.TotalRowRange.BottomRowIndex) + _spaceRow;
            }

            #endregion

            #region Kendi Çeklerimiz

            #region Grup Başlığı Renklendir

            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Çeklerimiz";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = borcBorcKCekler.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = borcBorcKCekler.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.Name = $"B_Ceklerimiz_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in borcBorcKCekler.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.NAME;
                    ws.Cells[$"H{i}"].Value = item.EXP;
                    ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableBanka.TotalRowRange.BottomRowIndex) + _spaceRow;
            }

            #endregion

            #region Firma Kredi Kartı


            #region Grup Başlığı Renklendir


            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Kredi Kartı";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = borcBorcKrediKarti.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = borcBorcKrediKarti.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.Name = $"B_KK_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in borcBorcKrediKarti.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.DUEDATE.ToString("");
                    ws.Cells[$"H{i}"].Value = item.NAME;
                    ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableBanka.TotalRowRange.BottomRowIndex + 2) + _spaceRow;
            }

            #endregion

            #region Borclu Cari Hesap


            #region Grup Başlığı Renklendir

            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Cari Hesap";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = borcCariHesap.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = borcCariHesap.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.Name = $"B_Cari_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in borcCariHesap.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.NAME;
                    ws.Cells[$"H{i}"].Value = item.EXP;
                    ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableBanka.TotalRowRange.BottomRowIndex) + _spaceRow;
            }

            #endregion

            #region Diğer Borçlar


            #region Grup Başlığı Renklendir

            i += _spaceRow;
            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Diğer Borçlar";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = digerBorclar.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = digerBorclar.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.Name = $"B_Diger_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in digerBorclar.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.NAME;
                    ws.Cells[$"H{i}"].Value = item.EXP;
                    ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableBanka.TotalRowRange.BottomRowIndex) + _spaceRow;
            }

            #endregion

            #endregion

        #region Ust Toplam

            var allCell = ws.GetUsedRange();


            foreach (var d in dovizList)
            {
                string formula_ATL = "";
                string formula_BTL = "";

                foreach (Table table in ws.Tables.Where(a => a.Name.EndsWith(d.Key)))
                {
                    if (table.Name.StartsWith("A_"))
                    {
                        formula_ATL += $"+{table.Name}[[#Totals];[Toplam]]";
                    }

                    if (table.Name.StartsWith("B_"))
                    {
                        formula_BTL += $"+{table.Name}[[#Totals];[Toplam]]";
                    }

                }


                Cell cell = ws.Cells.FirstOrDefault(a => a.Name == $"A{d.Key}");
                cell.Formula = formula_ATL;
                cell = ws.Cells.FirstOrDefault(a => a.Name == $"B{d.Key}");
                cell.Formula = formula_BTL;
            }

            #endregion

        #region Özet 

        range = ws1.Range[$"A1"];


        ws1.MergeCells(range);
        range.Value = string.Format("{0} Özet", FirstDate.ToString("dd MMMM yyyy , dddd"));
        range.Style = workbook.Styles[BuiltInStyleId.Heading1];

        #region KurEuro
            TrCurr = 20;
            //SqlText = SqlReports.Kur;
            _reportTag = RaporcuV2.Helper.ReportTags.KurBuGun;
            List<Kur> Kur = GetPocoList<Kur>();

            double kur = 0;

            if (Kur.Count > 0)
            {
            kur = (double)Kur.FirstOrDefault().RATES1;
            }
            range = ws1.Range["B3"];
            ws1.MergeCells(range);
            range.Style = workbook.Styles[kurEuroBaslikStyle];
            ws1.Cells["B3"].Value = kur;
            ws1.Cells["B3"].Style.NumberFormat = "€#,##0.00000";


            #endregion

        #region KurDolar
            TrCurr = 1;
            //SqlText = SqlReports.Kur;
            _reportTag = RaporcuV2.Helper.ReportTags.KurBuGun;
            List<Kur> Kur1 = GetPocoList<Kur>();

            double kur1 = 0;

            if (Kur1.Count > 0)
            {
            kur1 = (double)Kur1.FirstOrDefault().RATES1;
            }

            range = ws1.Range["B4"];
            ws1.MergeCells(range);
            range.Style = workbook.Styles[kurDolarBaslikStyle];
            ws1.Cells["B4"].Value = kur1;
            ws1.Cells["B4"].Style.NumberFormat = "$#,##0.00000";


            #endregion

        #region Toplam Varlıkların TL Karşılığı

            range = ws1.Range[$"A5:B5"];
            ws1.MergeCells(range);
            range.Value = "TOPLAM VARLIKLARIN TL KARŞILIĞI";
            range.Style = workbook.Styles[kasaBaslikStyle];
            Table tableVarlik = ws1.Tables.Add(ws1[$"A6:B16"], true);
            tableVarlik.ShowTotals = true;
            tableVarlik.AutoFilter.Disable();
            tableVarlik.Style = workbook.TableStyles[kasaStyle];
            tableVarlik.DataRange.NumberFormat = "#,##0.00";
            tableVarlik.Columns[0].Name = $"Açıklama";
            tableVarlik.Columns[1].Name = $"Tutar";

            ws1.Cells[$"A7"].Value = "Kasalar";
            ws1.Cells[$"A8"].Value = "Bankalar";
            ws1.Cells[$"A9"].Value = "Müşteri Çek Senetleri";
            ws1.Cells[$"A10"].Value = "Müşteriden Alacaklar";

            ws1.Cells[$"B7"].Formula = $"E7+(H7*B4)+(K7*B3)";
            ws1.Cells[$"B8"].Formula = $"E8+(H8*B4)+(K8*B3)";
            ws1.Cells[$"B9"].Formula = $"E9+(H9*B4)+(K9*B3)";
            ws1.Cells[$"B10"].Formula = $"E10+(H10*B4)+(K10*B3)";
            ws1.Cells[$"B11"].Formula = $"E11+(H11*B4)+(K11*B3)";
            ws1.Cells[$"B12"].Formula = $"E12+(H12*B4)+(K12*B3)";
            ws1.Cells[$"B13"].Formula = $"E13+(H13*B4)+(K13*B3)";
            ws1.Cells[$"B14"].Formula = $"E14+(H14*B4)+(K14*B3)";
            ws1.Cells[$"B15"].Formula = $"E15+(H15*B4)+(K15*B3)";
            ws1.Cells[$"B16"].Formula = $"E16+(H16*B4)+(K16*B3)";
            ws1.Cells[$"A17"].Value = "TL KARŞILIĞI TOPLAM";
            ws1.Cells[$"B17"].Formula = $"SUM(B7:B16)";
            ws1.Cells[$"B17"].NumberFormat = "#,##0.00";


            #endregion

        #region Tl Varlıklar
            range = ws1.Range[$"D5:E5"];
            ws1.MergeCells(range);
            range.Value = "TL VARLIKLAR";
            range.Style = workbook.Styles[cariBaslikStyle];
            Table tableTlVarliklar = ws1.Tables.Add(ws1[$"D6:E16"], true);
            tableTlVarliklar.ShowTotals = true;
            tableTlVarliklar.AutoFilter.Disable();
            tableTlVarliklar.Style = workbook.TableStyles[cariStyle];
            tableTlVarliklar.DataRange.NumberFormat = "#,##0.00";
            tableTlVarliklar.Columns[0].Name = $"Açıklama";
            tableTlVarliklar.Columns[1].Name = $"Tutar";

            ws1.Cells[$"D7"].Value = "Kasalar";
            ws1.Cells[$"E7"].Name = $"ATL";

            ws1.Cells[$"D8"].Value = "Bankalar";
            ws1.Cells[$"E8"].Name = $"Banka_ATL";

            ws1.Cells[$"D9"].Value = "Müşteri Çek Senetleri";
            ws1.Cells[$"E9"].Name = $"MusteriCek_ATL";

            ws1.Cells[$"D10"].Value = "Müşteriden Alacaklar";
            ws1.Cells[$"E10"].Name = $"Cari_ATL";


            ws1.Cells[$"D17"].Value = "TL TOPLAM";
            ws1.Cells[$"E17"].Formula = $"SUM(E7:E16)";
            ws1.Cells[$"E17"].NumberFormat = "#,##0.00";
            #endregion

        #region USD Varlıklar
            range = ws1.Range[$"G5:H5"];
            ws1.MergeCells(range);
            range.Value = "USD VARLIKLAR";
            range.Style = workbook.Styles[kurDolarBaslikStyle];
            Table tableUSDVarliklar = ws1.Tables.Add(ws1[$"G6:H16"], true);
            tableUSDVarliklar.ShowTotals = true;
            tableUSDVarliklar.AutoFilter.Disable();
            tableUSDVarliklar.Style = workbook.TableStyles[kurDolarStyle];
            tableUSDVarliklar.DataRange.NumberFormat = "#,##0.00";
            tableUSDVarliklar.Columns[0].Name = $"Açıklama";
            tableUSDVarliklar.Columns[1].Name = $"Tutar";
            ws1.Cells[$"G7"].Value = "Kasalar";
            ws1.Cells[$"H7"].Name = $"AUSD";

            ws1.Cells[$"G8"].Value = "Bankalar";
            ws1.Cells[$"H8"].Name = $"Banka_AUSD";
            ws1.Cells[$"G9"].Value = "Müşteri Çek Senetleri";
            ws1.Cells[$"H9"].Name = $"MusteriCek_AUSD";
            ws1.Cells[$"G10"].Value = "Müşteriden Alacaklar";
            ws1.Cells[$"H10"].Name = $"Cari_AUSD";
            ws1.Cells[$"G17"].Value = "USD TOPLAM";
            ws1.Cells[$"H17"].Formula = $"SUM(H7:H16)";
            ws1.Cells[$"H17"].NumberFormat = "#,##0.00";

            #endregion

        #region EUR Varlıklar
            range = ws1.Range[$"J5:K5"];
            ws1.MergeCells(range);
            range.Value = "EUR VARLIKLAR";
            range.Style = workbook.Styles[bankaBaslikStyle];
            Table tableEURVarliklar = ws1.Tables.Add(ws1[$"J6:K16"], true);
            tableEURVarliklar.ShowTotals = true;
            tableEURVarliklar.AutoFilter.Disable();
            tableEURVarliklar.Style = workbook.TableStyles[bankaStyle];
            tableEURVarliklar.DataRange.NumberFormat = "#,##0.00";
            tableEURVarliklar.Columns[0].Name = $"Açıklama";
            tableEURVarliklar.Columns[1].Name = $"Tutar";
            ws1.Cells[$"J7"].Value = "Kasalar";
            ws1.Cells[$"K7"].Name = $"AEUR";
            ws1.Cells[$"J8"].Value = "Bankalar";
            ws1.Cells[$"K8"].Name = $"Banka_AEUR";
            ws1.Cells[$"J9"].Value = "Müşteri Çek Senetleri";
            ws1.Cells[$"K9"].Name = $"MusteriCek_AEUR";
            ws1.Cells[$"J10"].Value = "Müşteriden Alacaklar";
            ws1.Cells[$"K10"].Name = $"Cari_AEUR";
            ws1.Cells[$"J17"].Value = "EUR TOPLAM";
            ws1.Cells[$"K17"].Formula = $"SUM(K7:K16)";
            ws1.Cells[$"K17"].NumberFormat = "#,##0.00";
            #endregion

        #region TOPLAM BORÇLARIN TL KARŞILIĞI
            range = ws1.Range[$"A19:B19"];
            ws1.MergeCells(range);
            range.Value = "TOPLAM BORÇLARIN TL KARŞILIĞI";
            range.Style = workbook.Styles[kasaBaslikStyle];
            Table tableBorc = ws1.Tables.Add(ws1[$"A20:B30"], true);
            tableBorc.ShowTotals = true;
            tableBorc.AutoFilter.Disable();
            tableBorc.Style = workbook.TableStyles[kasaStyle];
            tableBorc.DataRange.NumberFormat = "#,##0.00";
            tableBorc.Columns[0].Name = $"Açıklama";
            tableBorc.Columns[1].Name = $"Tutar";
            ws1.Cells[$"A21"].Value = "Krediler";

            ws1.Cells[$"A22"].Value = "Çeklerimiz";
            ws1.Cells[$"A23"].Value = "Kredi Kartı";
            ws1.Cells[$"A24"].Value = "Cari Hesap";
            ws1.Cells[$"A25"].Value = "Diğer Borçlar";

            ws1.Cells[$"B21"].Formula = $"E21+(H21*B4)+(K21*B3)";
            ws1.Cells[$"B22"].Formula = $"E22+(H22*B4)+(K22*B3)";
            ws1.Cells[$"B23"].Formula = $"E23+(H23*B4)+(K23*B3)";
            ws1.Cells[$"B24"].Formula = $"E24+(H24*B4)+(K24*B3)";
            ws1.Cells[$"B25"].Formula = $"E25+(H25*B4)+(K25*B3)";
            ws1.Cells[$"B26"].Formula = $"E26+(H26*B4)+(K26*B3)";
            ws1.Cells[$"B27"].Formula = $"E27+(H27*B4)+(K27*B3)";
            ws1.Cells[$"B28"].Formula = $"E28+(H28*B4)+(K28*B3)";
            ws1.Cells[$"B29"].Formula = $"E29+(H29*B4)+(K29*B3)";
            ws1.Cells[$"B30"].Formula = $"E30+(H30*B4)+(K30*B3)";

            ws1.Cells[$"A31"].Value = "TL KARŞILIĞI TOPLAM";
            ws1.Cells[$"B31"].NumberFormat = "#,##0.00";
            ws1.Cells[$"B31"].Formula = $"SUM(B21:B30)";

            ws1.Cells[$"A33"].Value = "TL KARŞILIĞI FARK";
            ws1.Cells[$"B33"].NumberFormat = "#,##0.00";
            ws1.Cells[$"B33"].Formula = $"B17-B31";

            #endregion

        #region Tl Borçlar
            range = ws1.Range[$"D19:E19"];
            ws1.MergeCells(range);
            range.Value = "TL BORÇLAR";
            range.Style = workbook.Styles[cariBaslikStyle];
            Table tableTlBorclar = ws1.Tables.Add(ws1[$"D20:E30"], true);
            tableTlBorclar.ShowTotals = true;
            tableTlBorclar.AutoFilter.Disable();
            tableTlBorclar.Style = workbook.TableStyles[cariStyle];
            tableTlBorclar.DataRange.NumberFormat = "#,##0.00";
            tableTlBorclar.Columns[0].Name = $"Açıklama";
            tableTlBorclar.Columns[1].Name = $"Tutar";
            ws1.Cells[$"D21"].Value = "Krediler";
            ws1.Cells[$"D22"].Value = "Çeklerimiz";
            ws1.Cells[$"D23"].Value = "Kredi Kartı";
            ws1.Cells[$"D24"].Value = "Cari Hesap";
            ws1.Cells[$"D25"].Value = "Diğer Borçlar";

            ws1.Cells[$"D31"].Value = "TL TOPLAM";
            ws1.Cells[$"E31"].NumberFormat = "#,##0.00";
            ws1.Cells[$"E31"].Formula = $"SUM(E21:E30)";

            ws1.Cells[$"D33"].Value = "TL FARK";
            ws1.Cells[$"E33"].NumberFormat = "#,##0.00";
            ws1.Cells[$"E33"].Formula = $"E17-E31";
            #endregion

        #region USD Borçlar
            range = ws1.Range[$"G19:H19"];
            ws1.MergeCells(range);
            range.Value = "USD BORÇLAR";
            range.Style = workbook.Styles[kurDolarBaslikStyle];
            Table tableUsdBorclar = ws1.Tables.Add(ws1[$"G20:H30"], true);
            tableUsdBorclar.ShowTotals = true;
            tableUsdBorclar.AutoFilter.Disable();
            tableUsdBorclar.Style = workbook.TableStyles[kurDolarStyle];
            tableUsdBorclar.DataRange.NumberFormat = "#,##0.00";
            tableUsdBorclar.Columns[0].Name = $"Açıklama";
            tableUsdBorclar.Columns[1].Name = $"Tutar";

            ws1.Cells[$"G21"].Value = "Krediler";
            ws1.Cells[$"G22"].Value = "Çeklerimiz";
            ws1.Cells[$"G23"].Value = "Kredi Kartı";
            ws1.Cells[$"G24"].Value = "Cari Hesap";
            ws1.Cells[$"G25"].Value = "Diğer Borçlar";

            ws1.Cells[$"G31"].Value = "USD TOPLAM";
            ws1.Cells[$"H31"].NumberFormat = "#,##0.00";
            ws1.Cells[$"H31"].Formula = $"SUM(H21:H30)";
            ws1.Cells[$"G33"].Value = "USD FARK";
            ws1.Cells[$"H33"].NumberFormat = "#,##0.00";
            ws1.Cells[$"H33"].Formula = $"H17-H31";

            #endregion

        #region EUR Borçlar
            range = ws1.Range[$"J19:K19"];
            ws1.MergeCells(range);
            range.Value = "EUR BORÇLAR";
            range.Style = workbook.Styles[bankaBaslikStyle];
            Table tableEurBorclar = ws1.Tables.Add(ws1[$"J20:K30"], true);
            tableEurBorclar.ShowTotals = true;
            tableEurBorclar.AutoFilter.Disable();
            tableEurBorclar.Style = workbook.TableStyles[bankaStyle];
            tableEurBorclar.DataRange.NumberFormat = "#,##0.00";
            tableEurBorclar.Columns[0].Name = $"Açıklama";
            tableEurBorclar.Columns[1].Name = $"Tutar";

            ws1.Cells[$"J21"].Value = "Krediler";
            ws1.Cells[$"J22"].Value = "Çeklerimiz";
            ws1.Cells[$"J23"].Value = "Kredi Kartı";
            ws1.Cells[$"J24"].Value = "Cari Hesap";
            ws1.Cells[$"J25"].Value = "Diğer Borçlar";

            ws1.Cells[$"J31"].Value = "EUR TOPLAM";ws1.Cells[$"K31"].NumberFormat = "#,##0.00";
            ws1.Cells[$"K31"].Formula = $"SUM(K21:K30)";

            ws1.Cells[$"J33"].Value = "EUR FARK";
            ws1.Cells[$"K33"].NumberFormat = "#,##0.00";
            ws1.Cells[$"K33"].Formula = $"K17-K31";
            #endregion

        foreach (var d in dovizList)
        {
        string formula_ATL1 = "";
        string formula_BUSD1 = "";
        string formula_CEURO1 = "";
        string formula_Banka_ATL1 = "";
        string formula_Banka_BUSD1 = "";
        string formula_Banka_CEURO1 = "";
        string formula_MusteriCek_ATL1 = "";
        string formula_MusteriCek_BUSD1 = "";
        string formula_MusteriCek_CEURO1 = "";
        string formula_MusteriAlacak_ATL1 = "";
        string formula_MusteriAlacak_BUSD1 = "";
        string formula_MusteriAlacak_CEURO1 = "";
        string formula_Krediler_ATL1 = "";
        string formula_Krediler_BUSD1 = "";
        string formula_Krediler_CEURO1 = "";
        string formula_Ceklerimiz_ATL1 = "";
        string formula_Ceklerimiz_BUSD1 = "";
        string formula_Ceklerimiz_CEURO1 = "";
        string formula_KrediKarti_ATL1 = "";
        string formula_KrediKarti_BUSD1 = "";
        string formula_KrediKarti_CEURO1 = "";
        string formula_Cariler_ATL1 = "";
        string formula_Cariler_BUSD1 = "";
        string formula_Cariler_CEURO1 = "";
        string formula_DigerBorclar_ATL1 = "";
        string formula_DigerBorclar_BUSD1 = "";
        string formula_DigerBorclar_CEURO1 = "";

        foreach (Table table in ws.Tables.Where(a => a.Name.EndsWith(d.Key)))
        {
            if (table.Name.StartsWith("A_Kasa_TL"))
            {
                formula_ATL1 += $"+{table.Name}[[#Totals];[Toplam]]";
                Cell cell1 = ws1.Cells.FirstOrDefault(a => a.Name == $"ATL");
                cell1.Formula = formula_ATL1;
            }
            else if (table.Name.StartsWith("A_Banka_TL"))
            {
                formula_Banka_ATL1 += $"+{table.Name}[[#Totals];[Toplam]]";
                var cell1 = ws1.Cells.FirstOrDefault(a => a.Name == $"Banka_ATL");
                cell1.Formula = formula_Banka_ATL1;
            }
            else if (table.Name.StartsWith("A_CekSenet_TL"))
            {
                formula_MusteriCek_ATL1 += $"+{table.Name}[[#Totals];[Toplam]]";
                var cell1 = ws1.Cells.FirstOrDefault(a => a.Name == $"MusteriCek_ATL");
                cell1.Formula = formula_MusteriCek_ATL1;

            }
            else if (table.Name.StartsWith("A_Cari_TL"))
            {
                formula_MusteriAlacak_ATL1 += $"+{table.Name}[[#Totals];[Toplam]]";
                var cell1 = ws1.Cells.FirstOrDefault(a => a.Name == $"Cari_ATL");
                cell1.Formula = formula_MusteriAlacak_ATL1;

            }
            else if (table.Name.StartsWith("B_Kredi_TL"))
            {
                formula_Krediler_ATL1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"E21"].Formula = formula_Krediler_ATL1;

            }
            else if (table.Name.StartsWith("B_Ceklerimiz_TL"))
            {
                formula_Ceklerimiz_ATL1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"E22"].Formula = formula_Ceklerimiz_ATL1;

            }
            else if (table.Name.StartsWith("B_KK_TL"))
            {
                formula_KrediKarti_ATL1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"E23"].Formula = formula_KrediKarti_ATL1;

            }
            else if (table.Name.StartsWith("B_Cari_TL"))
            {
                formula_Cariler_ATL1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"E24"].Formula = formula_Cariler_ATL1;

            }
            else if (table.Name.StartsWith("B_Diger_TL"))
            {
                formula_DigerBorclar_ATL1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"E25"].Formula = formula_DigerBorclar_ATL1;
            }

            else if (table.Name.StartsWith("A_Kasa_USD"))
            {
                formula_BUSD1 += $"+{table.Name}[[#Totals];[Toplam]]";
                var cell1 = ws1.Cells.FirstOrDefault(a => a.Name == $"AUSD");
                cell1.Formula = formula_BUSD1;
            }
            else if (table.Name.StartsWith("A_Banka_USD"))
            {
                formula_Banka_BUSD1 += $"+{table.Name}[[#Totals];[Toplam]]";
                var cell1 = ws1.Cells.FirstOrDefault(a => a.Name == $"Banka_AUSD");
                cell1.Formula = formula_Banka_BUSD1;
            }
            else if (table.Name.StartsWith("A_CekSenet_USD"))
            {
                formula_MusteriCek_BUSD1 += $"+{table.Name}[[#Totals];[Toplam]]";
                var cell1 = ws1.Cells.FirstOrDefault(a => a.Name == $"MusteriCek_AUSD");
                cell1.Formula = formula_MusteriCek_BUSD1;
            }
            else if (table.Name.StartsWith("A_Cari_USD"))
            {
                formula_MusteriAlacak_BUSD1 += $"+{table.Name}[[#Totals];[Toplam]]";
                var cell1 = ws1.Cells.FirstOrDefault(a => a.Name == $"Cari_AUSD");
                cell1.Formula = formula_MusteriAlacak_BUSD1;

            }
            else if (table.Name.StartsWith("B_Kredi_USD"))
            {
                formula_Krediler_BUSD1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"H21"].Formula = formula_Krediler_BUSD1;

            }

            else if (table.Name.StartsWith("B_Ceklerimiz_USD"))
            {
                formula_Ceklerimiz_BUSD1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"H22"].Formula = formula_Ceklerimiz_BUSD1;

            }
            else if (table.Name.StartsWith("B_KK_USD"))
            {
                formula_KrediKarti_BUSD1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"H23"].Formula = formula_KrediKarti_BUSD1;

            }
            else if (table.Name.StartsWith("B_Cari_USD"))
            {
                formula_Cariler_BUSD1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"H24"].Formula = formula_Cariler_BUSD1;

            }
            else if (table.Name.StartsWith("B_Diger_USD"))
            {
                formula_DigerBorclar_BUSD1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"H25"].Formula = formula_DigerBorclar_BUSD1;

            }
            else if (table.Name.StartsWith("A_Kasa_EUR"))
            {
                formula_CEURO1 += $"+{table.Name}[[#Totals];[Toplam]]";
                var cell1 = ws1.Cells.FirstOrDefault(a => a.Name == $"AEUR");
                cell1.Formula = formula_CEURO1;
            }
            else if (table.Name.StartsWith("A_Banka_EUR"))
            {
                formula_Banka_CEURO1 += $"+{table.Name}[[#Totals];[Toplam]]";
                var cell1 = ws1.Cells.FirstOrDefault(a => a.Name == $"Banka_AEUR");
                cell1.Formula = formula_Banka_CEURO1;
            }
            else if (table.Name.StartsWith("A_CekSenet_EUR"))
            {
                formula_MusteriCek_CEURO1 += $"+{table.Name}[[#Totals];[Toplam]]";
                var cell1 = ws1.Cells.FirstOrDefault(a => a.Name == $"MusteriCek_AEUR");
                cell1.Formula = formula_MusteriCek_CEURO1;
            }

            else if (table.Name.StartsWith("A_Cari_EUR"))
            {
                formula_MusteriAlacak_CEURO1 += $"+{table.Name}[[#Totals];[Toplam]]";
                var cell1 = ws1.Cells.FirstOrDefault(a => a.Name == $"Cari_AEUR");
                cell1.Formula = formula_MusteriAlacak_CEURO1;

            }
            else if (table.Name.StartsWith("B_Kredi_EUR"))
            {
                formula_Krediler_CEURO1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"K21"].Formula = formula_Krediler_CEURO1;
            }
            else if (table.Name.StartsWith("B_Ceklerimiz_EUR"))
            {
                formula_Ceklerimiz_CEURO1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"K22"].Formula = formula_Ceklerimiz_CEURO1;
            }
            else if (table.Name.StartsWith("B_KK_EUR"))
            {
                formula_KrediKarti_CEURO1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"K23"].Formula = formula_KrediKarti_CEURO1;
            }
            else if (table.Name.StartsWith("B_Cari_EUR"))
            {
                formula_Cariler_CEURO1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"K24"].Formula = formula_Cariler_CEURO1;
            }
            else if (table.Name.StartsWith("B_Diger_EUR"))
            {
                formula_DigerBorclar_CEURO1 += $"+{table.Name}[[#Totals];[Toplam]]";
                ws1.Cells[$"K25"].Formula = formula_DigerBorclar_CEURO1;
            }

        }

        }

        #endregion  

        var allCell1 = ws1.GetUsedRange();
        allCell1.Alignment.WrapText = true;
        spreadsheetControl1.EndUpdate();
        #endregion

        }
    }
}