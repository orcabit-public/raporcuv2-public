﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.PLinq;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;
using DevExpress.XtraGrid.Views.Grid;

namespace RaporcuV2.WinApp.Forms.Kasa
{
    public partial class XtraFormKasa : XtraFormBase
    {
        public XtraFormKasa()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                CreateFirmLookup();
                CreatePeriodLookup();
                CreateRefreshButton();
                RefreshGridData();
                CreatePivotButton();

            };


        }
        #endregion




        #endregion

        #region Refresh Grid Data
        public void RefreshGridData()
        {
            //SqlText = SqlKasa.KasaHareketler;
            _reportTag = RaporcuV2.Helper.ReportTags.KasaHareket;
            RefreshGridData<Model.Kasa.KasaAyrinti>();
        }

        #endregion

    }
}