﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.DataAccess.Native.ExpressionEditor;
using DevExpress.Spreadsheet;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.Kasa
{
    public partial class XtraFormExcelTest : XtraFormBase, IReport
    {

        public int SectionStart { get; set; } = 10;
        private string _s = "A";
        private string _e = "N";
        private int _spaceRow = 2;


        public XtraFormExcelTest()
        {
            InitializeComponent();
            this.barManagerControl1.gridControl1.Visible = false;
            AddEvents();
        }

        #region Methots

        #region Add Events

        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {

                CreateFirmLookup();
                CreatePeriodLookup();
                CreateFirstDate();
                foreach (BarItem barItem in barManagerControl1.barManagerForm.Items)
                {
                    if (barItem.Name == "firstDate")
                    {
                        ((DevExpress.XtraBars.BarEditItem)barItem).EditValue = DateTime.Today;
                        //DateEdit dateedit = ((BarEditItem)barItem) as DateEdit;
                        ((DevExpress.XtraEditors.Repository.RepositoryItemDateEdit)
                            ((DevExpress.XtraBars.BarEditItem)barItem).Edit).MinValue = DateTime.Today;

                    }
                }

                CreateRefreshButton();

                barManager1.BeginUpdate();

                var parentTools = this.barManagerControl1.barManagerForm.Bars[0];

                this.barManager1.Bars.Add(parentTools);

                barManager1.EndUpdate();

                commonBar1.Merge(parentTools);
            };

            //this.spreadsheetControl1.Dock = DockStyle.Fill;
        }

        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
            #region Sorgular

            //SqlText = SqlNakitAkis.AlacakKasalar;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniAlacakKasalar;
            List<NakitAkis> alacakKasalar = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.AlacakBankalar;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniAlacakBankalar;
            List<NakitAkis> alacakBankalar = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.AlacakCekSenet;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniAlacakCekSenet;
            List<NakitAkis> alacakCekSenet = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.AlacakCariHesap;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniAlacakCariHesap;
            List<NakitAkis> alacakCariHesap = GetPocoList<NakitAkis>();

            //SqlText = SqlNakitAkis.BorcKrediler;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniBorcKredilerDetay;
            List<NakitAkis> borcBorcKrediler = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.BorcCekler;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniBorcCekler;
            List<NakitAkis> borcBorcKCekler = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.BorcKrediKarti;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniBorcKrediKartiDetay;
            List<NakitAkis> borcBorcKrediKarti = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.BorcCariHesap;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniBorcCariHesap;
            List<NakitAkis> borcCariHesap = GetPocoList<NakitAkis>();

            #region Diğer Borçlar

            List<NakitAkis> digerBorclar = new List<NakitAkis>();
            digerBorclar.Add(new NakitAkis()
            {
                TYPENR = 100,
                TYPE = "Diğer Borçlar",
                CURCODE = "TL",
                CURTYPE = 0,
                DUEDATE = DateTime.Today,
                EXP = "",
                NAME = "KDV",
                TOTAL = 0
            });
            digerBorclar.Add(new NakitAkis()
            {
                TYPENR = 100,
                TYPE = "Diğer Borçlar",
                CURCODE = "TL",
                CURTYPE = 0,
                DUEDATE = DateTime.Today,
                EXP = "",
                NAME = "KDV2",
                TOTAL = 0
            });
            digerBorclar.Add(new NakitAkis()
            {
                TYPENR = 100,
                TYPE = "Diğer Borçlar",
                CURCODE = "TL",
                CURTYPE = 0,
                DUEDATE = DateTime.Today,
                EXP = "",
                NAME = "Muhtasar",
                TOTAL = 0
            });
            digerBorclar.Add(new NakitAkis()
            {
                TYPENR = 100,
                TYPE = "Diğer Borçlar",
                CURCODE = "TL",
                CURTYPE = 0,
                DUEDATE = DateTime.Today,
                EXP = "",
                NAME = "Kurumlar / Gelir Vergisi",
                TOTAL = 0
            });
            digerBorclar.Add(new NakitAkis()
            {
                TYPENR = 100,
                TYPE = "Diğer Borçlar",
                CURCODE = "TL",
                CURTYPE = 0,
                DUEDATE = DateTime.Today,
                EXP = "",
                NAME = "SGK",
                TOTAL = 0
            });
            digerBorclar.Add(new NakitAkis()
            {
                TYPENR = 100,
                TYPE = "Diğer Borçlar",
                CURCODE = "TL",
                CURTYPE = 0,
                DUEDATE = DateTime.Today,
                EXP = "",
                NAME = "Bağkur",
                TOTAL = 0
            });


            
            
            #endregion

            #endregion

            // Kasa ve Kasa Detay Renkelnedirme
            BuiltInStyleId kasaBaslikStyle = BuiltInStyleId.Accent1;
            BuiltInTableStyleId kasaStyle = BuiltInTableStyleId.TableStyleLight2;

            // Banka ve Banka HAreketerli Renklendirme
            BuiltInStyleId bankaBaslikStyle = BuiltInStyleId.Accent2;
            BuiltInTableStyleId bankaStyle = BuiltInTableStyleId.TableStyleLight3;

            // Kredi Durumu 
            BuiltInStyleId cekBaslikStyle = BuiltInStyleId.Accent3;
            BuiltInTableStyleId cekStyle = BuiltInTableStyleId.TableStyleLight4;

            // Çek Durumu 
            BuiltInStyleId cariBaslikStyle = BuiltInStyleId.Accent4;
            BuiltInTableStyleId cariStyle = BuiltInTableStyleId.TableStyleLight5;


            spreadsheetControl1.BeginUpdate();


            // Rapor Grubu
            int i = 1;
            IWorkbook workbook = spreadsheetControl1.Document;
            Worksheet ws = workbook.Worksheets[0];
            ws.ActiveView.PaperKind = PaperKind.A4;
            ws.ActiveView.Orientation = PageOrientation.Landscape;
            ws.ActiveView.Margins.Left = 0;
            ws.ActiveView.Margins.Top = 0;
            ws.ActiveView.Margins.Right = 0;
            ws.ActiveView.Margins.Bottom = 0;

            ws.Clear(ws.GetUsedRange());

            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToWidth = 1;
            ws.PrintOptions.FitToHeight = 0;

            var range = ws.Range[$"A{i}:I{i}"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[BuiltInStyleId.Title];
            ws.Cells[$"A{i}"].Value = FirmName;
            i++;

            range = ws.Range[$"A{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = string.Format("{0} Ödeme Planı", FirstDate.ToString("dd MMMM yyyy , dddd"));
            range.Style = workbook.Styles[BuiltInStyleId.Heading1];


            


            #region Toplamlar Doviz Turu

            Dictionary<string,double> dovizList = new Dictionary<string, double>();
            Dictionary<string,double> dovizTotals = new Dictionary<string, double>();

            #region Doviz List 
            var exchange = alacakKasalar.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }
            exchange = alacakBankalar.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }
            exchange = alacakCekSenet.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }
            exchange = alacakCariHesap.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }
            exchange = borcBorcKrediler.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }
            exchange = borcBorcKCekler.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }
            exchange = borcBorcKrediKarti.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }
            exchange = borcCariHesap.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            foreach (var item in exchange)
            {
                bool any = dovizList.Any(a => a.Key == item.pb);
                if (!any)
                    dovizList.Add(item.pb, 0);
            }

            #endregion

            i = ws.GetUsedRange().RowCount + 1;
            foreach (var exc in exchange)
            {
                var pb = exc.pb;
                double sum = alacakKasalar.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);
                sum += alacakBankalar.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);
                sum += alacakCekSenet.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);
                sum += alacakCariHesap.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);

                ws.Cells[$"C{i}"].Value = pb;
                ws.Cells[$"D{i}"].Name = $"A{pb}";
                ws.Cells[$"D{i}"].NumberFormat= "#,##0.00";
                sum = 0;
                sum = borcBorcKrediler.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);
                sum += borcBorcKCekler.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);
                sum += borcBorcKrediKarti.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);
                sum += borcCariHesap.Where(a => a.CURCODE == pb).Sum(s => s.TOTAL);

                ws.Cells[$"H{i}"].Value = pb;
                ws.Cells[$"I{i}"].Name = $"B{pb}";
                ws.Cells[$"I{i}"].NumberFormat = "#,##0.00";

                digerBorclar.Add(new NakitAkis()
                {
                    TYPENR = 100,
                    TYPE = "Diğer Borçlar",
                    CURCODE = exc.pb,
                    CURTYPE = 0,
                    DUEDATE = DateTime.Today,
                    EXP = "",
                    NAME = $"Diğer {exc.pb} Borçlar",
                    TOTAL = 0
                });

                i++;
            }

            i = ws.GetUsedRange().RowCount + 2;
            int condStart = i;
            foreach (var exc in exchange)
            {
                var pb = exc.pb;

                ws.Cells[$"C{i}"].Value = $"Fark {pb}";
                ws.Cells[$"C{i}"].Font.Bold = true;
                ws.Cells[$"D{i}"].Name= $"Fark_{pb}";
                ws.Cells[$"D{i}"].Font.Bold = true;
                ws.Cells[$"D{i}"].Formula = $"A{pb}-B{pb}";
                ws.Cells[$"D{i}"].NumberFormat = "#,##0.00";
                i++;
            }

            #region Conditional Formatting

            var enumerable = ws.Cells.Where(a => a.Name.StartsWith("Fark"));
            string val = $"$D${condStart}:$D${i}";
            var cfRange = ws[val];
            ExpressionConditionalFormatting cfRule =
                ws.ConditionalFormattings.AddExpressionConditionalFormatting(cfRange, ConditionalFormattingExpressionCondition.LessThan, "=0");
            // Specify formatting options to be applied to cells if the condition is true. 
            // Set the background color to yellow. 
            //cfRule.Formatting.Fill.BackgroundColor = Color.FromArgb(255, 0xFA, 0xF7, 0xAA);
            // Set the font color to red. 
            cfRule.Formatting.Font.Color = Color.Red;

            #endregion

            #endregion

            #region Kasa Durumu 

            i = ws.GetUsedRange().RowCount + dovizList.Count+1;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "Kasalar";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            int dataCount = 0;

            #endregion

            var currencyList = alacakKasalar.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            bool isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = alacakKasalar.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"A{i}:D{dataCount}"], true);
                tableBanka.Name = $"A_Kasa_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in alacakKasalar.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.TYPE;
                    ws.Cells[$"B{i}"].Value = item.NAME;
                    ws.Cells[$"C{i}"].Value = item.EXP;
                    ws.Cells[$"D{i}"].Value = item.TOTAL;

                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #region Banka Durumu 

            i = ws.GetUsedRange().RowCount + 2;
            //SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "Bankalar";
            range.Style = workbook.Styles[bankaBaslikStyle];
            //i++;


            #endregion

            isFirstRow = true;
            currencyList = alacakBankalar.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });

            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = alacakBankalar.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"A{i}:D{dataCount}"], true);
                tableBanka.Name = $"A_Banka_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[bankaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in alacakBankalar.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.TYPE;
                    ws.Cells[$"B{i}"].Value = item.NAME;
                    ws.Cells[$"C{i}"].Value = item.EXP;
                    ws.Cells[$"D{i}"].Value = item.TOTAL;

                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #region Çek Senet

            i = ws.GetUsedRange().RowCount + 2;
            //SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "Müşteri Çek Senetleri";
            range.Style = workbook.Styles[cekBaslikStyle];
            //i++;


            #endregion

            isFirstRow = true;
            currencyList = alacakCekSenet.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });

            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = alacakCekSenet.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"A{i}:D{dataCount}"], true);
                tableBanka.Name = $"A_CekSenet_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[cekStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in alacakCekSenet.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.TYPE;
                    ws.Cells[$"B{i}"].Value = item.NAME;
                    ws.Cells[$"C{i}"].Value = item.EXP;
                    ws.Cells[$"D{i}"].Value = item.TOTAL;

                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #region Cari Hesap

            i = ws.GetUsedRange().RowCount + 2;
            //SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "Müşterilerden Alacaklar";
            range.Style = workbook.Styles[cariBaslikStyle];
            //i++;


            #endregion

            isFirstRow = true;
            currencyList = alacakCariHesap.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });

            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = alacakCariHesap.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"A{i}:D{dataCount}"], true);
                tableBanka.Name = $"A_Cari_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[cariStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in alacakCariHesap.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.TYPE;
                    ws.Cells[$"B{i}"].Value = item.NAME;
                    ws.Cells[$"C{i}"].Value = item.EXP;
                    ws.Cells[$"D{i}"].Value = item.TOTAL;

                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #region Krediler

            dataCount = 0;
            i = SectionStart;
            //SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Krediler";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = borcBorcKrediler.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = borcBorcKrediler.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.Name = $"B_Kredi_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in borcBorcKrediler.Where(a => a.CURCODE == curCode.pb).OrderBy(o => o.DUEDATE))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = string.IsNullOrEmpty(item.TYPE) ? "Krediler" : item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.DUEDATE.ToString("d");
                    ws.Cells[$"H{i}"].Value = item.EXP; ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableBanka.TotalRowRange.BottomRowIndex) + _spaceRow;
            }

            #endregion

            #region Kendi Çeklerimiz

            #region Grup Başlığı Renklendir

            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Çeklerimiz";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = borcBorcKCekler.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = borcBorcKCekler.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.Name = $"B_Ceklerimiz_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in borcBorcKCekler.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.NAME;
                    ws.Cells[$"H{i}"].Value = item.EXP;
                    ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableBanka.TotalRowRange.BottomRowIndex) + _spaceRow;
            }

            #endregion

            #region Firma Kredi Kartı


            #region Grup Başlığı Renklendir


            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Kredi Kartı";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = borcBorcKrediKarti.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = borcBorcKrediKarti.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.Name = $"B_KK_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in borcBorcKrediKarti.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.DUEDATE.ToString("");
                    ws.Cells[$"H{i}"].Value = item.NAME;
                    ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableBanka.TotalRowRange.BottomRowIndex + 2) + _spaceRow;
            }

            #endregion

            #region Borclu Cari Hesap


            #region Grup Başlığı Renklendir

            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Cari Hesap";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = borcCariHesap.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = borcCariHesap.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.Name = $"B_Cari_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in borcCariHesap.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.NAME;
                    ws.Cells[$"H{i}"].Value = item.EXP;
                    ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableBanka.TotalRowRange.BottomRowIndex) + _spaceRow;
            }

            #endregion

            #region Diğer Borçlar


            #region Grup Başlığı Renklendir

            i += _spaceRow;
            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Diğer Borçlar";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = digerBorclar.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = digerBorclar.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.Name = $"B_Diger_{curCode.pb}";
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in digerBorclar.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.NAME;
                    ws.Cells[$"H{i}"].Value = item.EXP;
                    ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableBanka.TotalRowRange.BottomRowIndex) + _spaceRow;
            }

            #endregion

            #endregion

            var allCell = ws.GetUsedRange();

            allCell.Alignment.WrapText = true;

            foreach (var d in dovizList)
            {
                string formula_ATL = "";
                string formula_BTL = "";

                foreach (Table table in ws.Tables.Where(a=> a.Name.EndsWith(d.Key)))
                {
                    if (table.Name.StartsWith("A_"))
                    {
                        formula_ATL += $"+{table.Name}[[#Totals];[Toplam]]";
                    }
                    if (table.Name.StartsWith("B_"))
                    {
                        formula_BTL += $"+{table.Name}[[#Totals];[Toplam]]";
                    }
                }
                Cell cell = ws.Cells.FirstOrDefault(a => a.Name == $"A{d.Key}");
                cell.Formula = formula_ATL;
                cell = ws.Cells.FirstOrDefault(a => a.Name == $"B{d.Key}");
                cell.Formula = formula_BTL;
            }



            spreadsheetControl1.EndUpdate();

            #endregion
        }
    }
}