﻿namespace RaporcuV2.WinApp.Forms.Kasa
{
    partial class XtraFormNakitAkisDetay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.commonBar1 = new DevExpress.XtraSpreadsheet.UI.CommonBar();
            this.spreadsheetControl1 = new DevExpress.XtraSpreadsheet.SpreadsheetControl();
            this.spreadsheetCommandBarButtonItem1 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem2 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem3 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem4 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem5 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem6 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem7 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem8 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem9 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem10 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem11 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.statusBar1 = new DevExpress.XtraSpreadsheet.UI.StatusBar();
            this.averageInfoStaticItem1 = new DevExpress.XtraSpreadsheet.UI.AverageInfoStaticItem();
            this.countInfoStaticItem1 = new DevExpress.XtraSpreadsheet.UI.CountInfoStaticItem();
            this.numericalCountInfoStaticItem1 = new DevExpress.XtraSpreadsheet.UI.NumericalCountInfoStaticItem();
            this.minInfoStaticItem1 = new DevExpress.XtraSpreadsheet.UI.MinInfoStaticItem();
            this.maxInfoStaticItem1 = new DevExpress.XtraSpreadsheet.UI.MaxInfoStaticItem();
            this.sumInfoStaticItem1 = new DevExpress.XtraSpreadsheet.UI.SumInfoStaticItem();
            this.zoomEditItem1 = new DevExpress.XtraSpreadsheet.UI.ZoomEditItem();
            this.repositoryItemZoomTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.showZoomButtonItem1 = new DevExpress.XtraSpreadsheet.UI.ShowZoomButtonItem();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.spreadsheetBarController1 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetBarController(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditFirst.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditLast.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrencyRadioGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CekTuruRadioGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FaturaRadioGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HizmetRadioGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditCardRadioGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spreadsheetBarController1)).BeginInit();
            this.SuspendLayout();
            // 
            // DateEditFirst
            // 
            // 
            // DateEditLast
            // 
            // 
            // barManagerControl1
            // 
            this.barManagerControl1.Location = new System.Drawing.Point(0, 24);
            this.barManagerControl1.Size = new System.Drawing.Size(930, 443);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.commonBar1,
            this.statusBar1});
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.spreadsheetCommandBarButtonItem1,
            this.spreadsheetCommandBarButtonItem2,
            this.spreadsheetCommandBarButtonItem3,
            this.spreadsheetCommandBarButtonItem4,
            this.spreadsheetCommandBarButtonItem5,
            this.spreadsheetCommandBarButtonItem6,
            this.spreadsheetCommandBarButtonItem7,
            this.spreadsheetCommandBarButtonItem8,
            this.spreadsheetCommandBarButtonItem9,
            this.spreadsheetCommandBarButtonItem10,
            this.spreadsheetCommandBarButtonItem11,
            this.averageInfoStaticItem1,
            this.countInfoStaticItem1,
            this.numericalCountInfoStaticItem1,
            this.minInfoStaticItem1,
            this.maxInfoStaticItem1,
            this.sumInfoStaticItem1,
            this.zoomEditItem1,
            this.showZoomButtonItem1});
            this.barManager1.MaxItemId = 30;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemZoomTrackBar1});
            this.barManager1.StatusBar = this.statusBar1;
            // 
            // commonBar1
            // 
            this.commonBar1.Control = this.spreadsheetControl1;
            this.commonBar1.DockCol = 0;
            this.commonBar1.DockRow = 0;
            this.commonBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.commonBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem7),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem8),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem9),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem10),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem11)});
            // 
            // spreadsheetControl1
            // 
            this.spreadsheetControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spreadsheetControl1.Location = new System.Drawing.Point(0, 24);
            this.spreadsheetControl1.MenuManager = this.barManager1;
            this.spreadsheetControl1.Name = "spreadsheetControl1";
            this.spreadsheetControl1.Size = new System.Drawing.Size(930, 443);
            this.spreadsheetControl1.TabIndex = 5;
            this.spreadsheetControl1.Text = "spreadsheetControl1";
            // 
            // spreadsheetCommandBarButtonItem1
            // 
            this.spreadsheetCommandBarButtonItem1.CommandName = "FileNew";
            this.spreadsheetCommandBarButtonItem1.Id = 11;
            this.spreadsheetCommandBarButtonItem1.Name = "spreadsheetCommandBarButtonItem1";
            // 
            // spreadsheetCommandBarButtonItem2
            // 
            this.spreadsheetCommandBarButtonItem2.CommandName = "FileOpen";
            this.spreadsheetCommandBarButtonItem2.Id = 12;
            this.spreadsheetCommandBarButtonItem2.Name = "spreadsheetCommandBarButtonItem2";
            // 
            // spreadsheetCommandBarButtonItem3
            // 
            this.spreadsheetCommandBarButtonItem3.CommandName = "FileSave";
            this.spreadsheetCommandBarButtonItem3.Id = 13;
            this.spreadsheetCommandBarButtonItem3.Name = "spreadsheetCommandBarButtonItem3";
            // 
            // spreadsheetCommandBarButtonItem4
            // 
            this.spreadsheetCommandBarButtonItem4.CommandName = "FileSaveAs";
            this.spreadsheetCommandBarButtonItem4.Id = 14;
            this.spreadsheetCommandBarButtonItem4.Name = "spreadsheetCommandBarButtonItem4";
            // 
            // spreadsheetCommandBarButtonItem5
            // 
            this.spreadsheetCommandBarButtonItem5.CommandName = "FileQuickPrint";
            this.spreadsheetCommandBarButtonItem5.Id = 15;
            this.spreadsheetCommandBarButtonItem5.Name = "spreadsheetCommandBarButtonItem5";
            // 
            // spreadsheetCommandBarButtonItem6
            // 
            this.spreadsheetCommandBarButtonItem6.CommandName = "FilePrint";
            this.spreadsheetCommandBarButtonItem6.Id = 16;
            this.spreadsheetCommandBarButtonItem6.Name = "spreadsheetCommandBarButtonItem6";
            // 
            // spreadsheetCommandBarButtonItem7
            // 
            this.spreadsheetCommandBarButtonItem7.CommandName = "FilePrintPreview";
            this.spreadsheetCommandBarButtonItem7.Id = 17;
            this.spreadsheetCommandBarButtonItem7.Name = "spreadsheetCommandBarButtonItem7";
            // 
            // spreadsheetCommandBarButtonItem8
            // 
            this.spreadsheetCommandBarButtonItem8.CommandName = "FileUndo";
            this.spreadsheetCommandBarButtonItem8.Id = 18;
            this.spreadsheetCommandBarButtonItem8.Name = "spreadsheetCommandBarButtonItem8";
            // 
            // spreadsheetCommandBarButtonItem9
            // 
            this.spreadsheetCommandBarButtonItem9.CommandName = "FileRedo";
            this.spreadsheetCommandBarButtonItem9.Id = 19;
            this.spreadsheetCommandBarButtonItem9.Name = "spreadsheetCommandBarButtonItem9";
            // 
            // spreadsheetCommandBarButtonItem10
            // 
            this.spreadsheetCommandBarButtonItem10.CommandName = "FileEncrypt";
            this.spreadsheetCommandBarButtonItem10.Id = 20;
            this.spreadsheetCommandBarButtonItem10.Name = "spreadsheetCommandBarButtonItem10";
            // 
            // spreadsheetCommandBarButtonItem11
            // 
            this.spreadsheetCommandBarButtonItem11.CommandName = "FileShowDocumentProperties";
            this.spreadsheetCommandBarButtonItem11.Id = 21;
            this.spreadsheetCommandBarButtonItem11.Name = "spreadsheetCommandBarButtonItem11";
            // 
            // statusBar1
            // 
            this.statusBar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.statusBar1.Control = this.spreadsheetControl1;
            this.statusBar1.DockCol = 0;
            this.statusBar1.DockRow = 0;
            this.statusBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.statusBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.averageInfoStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.countInfoStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.numericalCountInfoStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.minInfoStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.maxInfoStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.sumInfoStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.zoomEditItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.showZoomButtonItem1)});
            this.statusBar1.OptionsBar.AllowQuickCustomization = false;
            this.statusBar1.OptionsBar.DrawDragBorder = false;
            this.statusBar1.OptionsBar.UseWholeRow = true;
            // 
            // averageInfoStaticItem1
            // 
            this.averageInfoStaticItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.averageInfoStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.averageInfoStaticItem1.Id = 22;
            this.averageInfoStaticItem1.Name = "averageInfoStaticItem1";
            toolTipItem9.Text = "Average of selected cells";
            superToolTip9.Items.Add(toolTipItem9);
            this.averageInfoStaticItem1.SuperTip = superToolTip9;
            this.averageInfoStaticItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // countInfoStaticItem1
            // 
            this.countInfoStaticItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.countInfoStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.countInfoStaticItem1.Id = 23;
            this.countInfoStaticItem1.Name = "countInfoStaticItem1";
            toolTipItem10.Text = "Number of selected cells that contain data";
            superToolTip10.Items.Add(toolTipItem10);
            this.countInfoStaticItem1.SuperTip = superToolTip10;
            this.countInfoStaticItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // numericalCountInfoStaticItem1
            // 
            this.numericalCountInfoStaticItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.numericalCountInfoStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.numericalCountInfoStaticItem1.Id = 24;
            this.numericalCountInfoStaticItem1.Name = "numericalCountInfoStaticItem1";
            toolTipItem11.Text = "Number of selected cells that contain numerical data";
            superToolTip11.Items.Add(toolTipItem11);
            this.numericalCountInfoStaticItem1.SuperTip = superToolTip11;
            this.numericalCountInfoStaticItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // minInfoStaticItem1
            // 
            this.minInfoStaticItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.minInfoStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.minInfoStaticItem1.Id = 25;
            this.minInfoStaticItem1.Name = "minInfoStaticItem1";
            toolTipItem12.Text = "Minimum value in selection";
            superToolTip12.Items.Add(toolTipItem12);
            this.minInfoStaticItem1.SuperTip = superToolTip12;
            this.minInfoStaticItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // maxInfoStaticItem1
            // 
            this.maxInfoStaticItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.maxInfoStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.maxInfoStaticItem1.Id = 26;
            this.maxInfoStaticItem1.Name = "maxInfoStaticItem1";
            toolTipItem13.Text = "Maximum value in selection";
            superToolTip13.Items.Add(toolTipItem13);
            this.maxInfoStaticItem1.SuperTip = superToolTip13;
            this.maxInfoStaticItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // sumInfoStaticItem1
            // 
            this.sumInfoStaticItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.sumInfoStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.sumInfoStaticItem1.Id = 27;
            this.sumInfoStaticItem1.Name = "sumInfoStaticItem1";
            toolTipItem14.Text = "Sum of selected cells";
            superToolTip14.Items.Add(toolTipItem14);
            this.sumInfoStaticItem1.SuperTip = superToolTip14;
            this.sumInfoStaticItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // zoomEditItem1
            // 
            this.zoomEditItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.zoomEditItem1.Edit = this.repositoryItemZoomTrackBar1;
            this.zoomEditItem1.Id = 28;
            this.zoomEditItem1.Name = "zoomEditItem1";
            toolTipItem15.Text = "Zoom";
            superToolTip15.Items.Add(toolTipItem15);
            this.zoomEditItem1.SuperTip = superToolTip15;
            // 
            // repositoryItemZoomTrackBar1
            // 
            this.repositoryItemZoomTrackBar1.AllowUseMiddleValue = true;
            this.repositoryItemZoomTrackBar1.LargeChange = 10;
            this.repositoryItemZoomTrackBar1.Maximum = 400;
            this.repositoryItemZoomTrackBar1.Middle = 100;
            this.repositoryItemZoomTrackBar1.Minimum = 10;
            this.repositoryItemZoomTrackBar1.Name = "repositoryItemZoomTrackBar1";
            this.repositoryItemZoomTrackBar1.SmallChange = 10;
            this.repositoryItemZoomTrackBar1.SnapToMiddle = 5;
            // 
            // showZoomButtonItem1
            // 
            this.showZoomButtonItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.showZoomButtonItem1.Id = 29;
            this.showZoomButtonItem1.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.showZoomButtonItem1.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.showZoomButtonItem1.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.showZoomButtonItem1.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.showZoomButtonItem1.Name = "showZoomButtonItem1";
            this.showZoomButtonItem1.Size = new System.Drawing.Size(45, 0);
            this.showZoomButtonItem1.SmallWithTextWidth = 45;
            toolTipItem7.Text = "Zoom level. Click to open the Zoom dialog box.";
            superToolTip7.Items.Add(toolTipItem7);
            this.showZoomButtonItem1.SuperTip = superToolTip7;
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl2.Location = new System.Drawing.Point(0, 0);
            this.barDockControl2.Manager = this.barManager1;
            this.barDockControl2.Size = new System.Drawing.Size(930, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 467);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(930, 22);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 443);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(930, 24);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 443);
            // 
            // spreadsheetBarController1
            // 
            this.spreadsheetBarController1.BarItems.Add(this.spreadsheetCommandBarButtonItem1);
            this.spreadsheetBarController1.BarItems.Add(this.spreadsheetCommandBarButtonItem2);
            this.spreadsheetBarController1.BarItems.Add(this.spreadsheetCommandBarButtonItem3);
            this.spreadsheetBarController1.BarItems.Add(this.spreadsheetCommandBarButtonItem4);
            this.spreadsheetBarController1.BarItems.Add(this.spreadsheetCommandBarButtonItem5);
            this.spreadsheetBarController1.BarItems.Add(this.spreadsheetCommandBarButtonItem6);
            this.spreadsheetBarController1.BarItems.Add(this.spreadsheetCommandBarButtonItem7);
            this.spreadsheetBarController1.BarItems.Add(this.spreadsheetCommandBarButtonItem8);
            this.spreadsheetBarController1.BarItems.Add(this.spreadsheetCommandBarButtonItem9);
            this.spreadsheetBarController1.BarItems.Add(this.spreadsheetCommandBarButtonItem10);
            this.spreadsheetBarController1.BarItems.Add(this.spreadsheetCommandBarButtonItem11);
            this.spreadsheetBarController1.BarItems.Add(this.averageInfoStaticItem1);
            this.spreadsheetBarController1.BarItems.Add(this.countInfoStaticItem1);
            this.spreadsheetBarController1.BarItems.Add(this.numericalCountInfoStaticItem1);
            this.spreadsheetBarController1.BarItems.Add(this.minInfoStaticItem1);
            this.spreadsheetBarController1.BarItems.Add(this.maxInfoStaticItem1);
            this.spreadsheetBarController1.BarItems.Add(this.sumInfoStaticItem1);
            this.spreadsheetBarController1.BarItems.Add(this.zoomEditItem1);
            this.spreadsheetBarController1.BarItems.Add(this.showZoomButtonItem1);
            this.spreadsheetBarController1.Control = this.spreadsheetControl1;
            // 
            // XtraFormNakitAkisDetay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 489);
            this.Controls.Add(this.spreadsheetControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControl2);
            this.Name = "XtraFormNakitAkisDetay";
            this.Text = "Nakit Akış Detay";
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.barManagerControl1, 0);
            this.Controls.SetChildIndex(this.spreadsheetControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditFirst.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditLast.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrencyRadioGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CekTuruRadioGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FaturaRadioGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HizmetRadioGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditCardRadioGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spreadsheetBarController1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraSpreadsheet.UI.CommonBar commonBar1;
        private DevExpress.XtraSpreadsheet.SpreadsheetControl spreadsheetControl1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem2;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem3;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem4;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem5;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem6;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem7;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem8;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem9;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem10;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem11;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetBarController spreadsheetBarController1;
        private DevExpress.XtraSpreadsheet.UI.StatusBar statusBar1;
        private DevExpress.XtraSpreadsheet.UI.AverageInfoStaticItem averageInfoStaticItem1;
        private DevExpress.XtraSpreadsheet.UI.CountInfoStaticItem countInfoStaticItem1;
        private DevExpress.XtraSpreadsheet.UI.NumericalCountInfoStaticItem numericalCountInfoStaticItem1;
        private DevExpress.XtraSpreadsheet.UI.MinInfoStaticItem minInfoStaticItem1;
        private DevExpress.XtraSpreadsheet.UI.MaxInfoStaticItem maxInfoStaticItem1;
        private DevExpress.XtraSpreadsheet.UI.SumInfoStaticItem sumInfoStaticItem1;
        private DevExpress.XtraSpreadsheet.UI.ZoomEditItem zoomEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar1;
        private DevExpress.XtraSpreadsheet.UI.ShowZoomButtonItem showZoomButtonItem1;
    }
}