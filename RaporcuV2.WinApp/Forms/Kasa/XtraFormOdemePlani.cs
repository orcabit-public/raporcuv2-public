﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Spreadsheet;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model.Reports;
using RaporcuV2.Util;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.Kasa
{
    public partial class XtraFormOdemePlani : XtraFormBase, IReport
    {
        public int SectionStart { get; set; } = 4;
        private string _s = "A";
        private string _e = "N";
        private int _spaceRow=2;

        public XtraFormOdemePlani()
        {
            InitializeComponent();
            this.barManagerControl1.gridControl1.Visible = false;
            AddEvents();
        }

        #region Methots

        #region Add Events

        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {

                CreateFirmLookup();
                CreatePeriodLookup();
                CreateFirstDate();
                foreach (BarItem barItem in barManagerControl1.barManagerForm.Items)
                {
                    if (barItem.Name == "firstDate")
                    {
                        ((DevExpress.XtraBars.BarEditItem) barItem).EditValue = DateTime.Today;
                        //DateEdit dateedit = ((BarEditItem)barItem) as DateEdit;
                        ((DevExpress.XtraEditors.Repository.RepositoryItemDateEdit)
                            ((DevExpress.XtraBars.BarEditItem) barItem).Edit).MinValue = DateTime.Today;

                    }
                }

                CreateRefreshButton();

                barManager1.BeginUpdate();

                var parentTools = this.barManagerControl1.barManagerForm.Bars[0];

                this.barManager1.Bars.Add(parentTools);

                barManager1.EndUpdate();

                commonBar1.Merge(parentTools);
            };

            //this.spreadsheetControl1.Dock = DockStyle.Fill;
        }

        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
            #region Sorgular

            //SqlText = SqlNakitAkis.AlacakKasalar;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniAlacakKasalar;
            List<NakitAkis> alacakKasalar = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.AlacakBankalar;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniAlacakBankalar;List<NakitAkis> alacakBankalar = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.AlacakCekSenet;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniAlacakCekSenet;
            List<NakitAkis> alacakCekSenet = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.AlacakCariHesap;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniAlacakCariHesap;
            List<NakitAkis> alacakCariHesap = GetPocoList<NakitAkis>();

            //SqlText = SqlNakitAkis.BorcKrediler;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniBorcKrediler;
            List<NakitAkis> borcBorcKrediler = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.BorcCekler;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniBorcCekler;
            List<NakitAkis> borcBorcKCekler = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.BorcKrediKarti;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniBorcKrediKarti;
            List<NakitAkis> borcBorcKrediKarti = GetPocoList<NakitAkis>();
            //SqlText = SqlNakitAkis.BorcCariHesap;
            _reportTag = RaporcuV2.Helper.ReportTags.OdemePlaniBorcCariHesap;
            List<NakitAkis> borcCariHesap = GetPocoList<NakitAkis>();

            #endregion

            // Kasa ve Kasa Detay Renkelnedirme
            BuiltInStyleId kasaBaslikStyle = BuiltInStyleId.Accent1;
            BuiltInTableStyleId kasaStyle = BuiltInTableStyleId.TableStyleLight2;

            // Banka ve Banka HAreketerli Renklendirme
            BuiltInStyleId bankaBaslikStyle = BuiltInStyleId.Accent2;
            BuiltInTableStyleId bankaStyle = BuiltInTableStyleId.TableStyleLight3;

            // Kredi Durumu 
            BuiltInStyleId cekBaslikStyle = BuiltInStyleId.Accent3;
            BuiltInTableStyleId cekStyle = BuiltInTableStyleId.TableStyleLight4;

            // Çek Durumu 
            BuiltInStyleId cariBaslikStyle = BuiltInStyleId.Accent4;
            BuiltInTableStyleId cariStyle = BuiltInTableStyleId.TableStyleLight5;

            
            spreadsheetControl1.BeginUpdate();




            // Rapor Grubu
            int i = 1;
            IWorkbook workbook = spreadsheetControl1.Document;
            Worksheet ws = workbook.Worksheets[0];
            ws.ActiveView.PaperKind = PaperKind.A4;
            ws.ActiveView.Orientation = PageOrientation.Landscape;
            ws.ActiveView.Margins.Left = 0;
            ws.ActiveView.Margins.Top = 0;
            ws.ActiveView.Margins.Right = 0;
            ws.ActiveView.Margins.Bottom = 0;

            ws.Clear(ws.GetUsedRange());

            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToWidth = 1;
            ws.PrintOptions.FitToHeight = 0;

            var range = ws.Range[$"A{i}:I{i}"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[BuiltInStyleId.Title];
            ws.Cells[$"A{i}"].Value = FirmName;
            i++;

            range = ws.Range[$"A{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = string.Format("{0} Ödeme Planı", FirstDate.ToString("dd MMMM yyyy , dddd"));
            range.Style = workbook.Styles[BuiltInStyleId.Heading1];

            #region Kasa Durumu 

            i = ws.GetUsedRange().RowCount + 2;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "Kasalar";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            int dataCount = 0;
            #endregion

            var currencyList = alacakKasalar.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            bool isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }
                isFirstRow = false;
                int dataRowCount = alacakKasalar.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"A{i}:D{dataCount}"], true);
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in alacakKasalar.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.TYPE;
                    ws.Cells[$"B{i}"].Value = item.NAME;
                    ws.Cells[$"C{i}"].Value = item.EXP;
                    ws.Cells[$"D{i}"].Value = item.TOTAL;

                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }
            #endregion

            #region Banka Durumu 
            
            i = ws.GetUsedRange().RowCount+2;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "Bankalar";
            range.Style = workbook.Styles[bankaBaslikStyle];
            //i++;


            #endregion

            isFirstRow = true;
            currencyList = alacakBankalar.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});

            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }
                isFirstRow = false;
                int dataRowCount = alacakBankalar.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;Table tableBanka = ws.Tables.Add(ws[$"A{i}:D{dataCount}"], true);
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[bankaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in alacakBankalar.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.TYPE;
                    ws.Cells[$"B{i}"].Value = item.NAME;
                    ws.Cells[$"C{i}"].Value = item.EXP;
                    ws.Cells[$"D{i}"].Value = item.TOTAL;

                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #region Çek Senet

            i = ws.GetUsedRange().RowCount + 2;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "Müşteri Çek Senetleri";
            range.Style = workbook.Styles[cekBaslikStyle];
            //i++;


            #endregion

            isFirstRow = true;
            currencyList = alacakCekSenet.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });

            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }
                isFirstRow = false;
                int dataRowCount = alacakCekSenet.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"A{i}:D{dataCount}"], true);
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[cekStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in alacakCekSenet.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.TYPE;
                    ws.Cells[$"B{i}"].Value = item.NAME;
                    ws.Cells[$"C{i}"].Value = item.EXP;
                    ws.Cells[$"D{i}"].Value = item.TOTAL;

                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #region Cari Hesap

            i = ws.GetUsedRange().RowCount + 2;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:D{i}"];
            ws.MergeCells(range);
            range.Value = "Müşterilerden Alacaklar";
            range.Style = workbook.Styles[cariBaslikStyle];
            //i++;


            #endregion

            isFirstRow = true;
            currencyList = alacakCariHesap.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });

            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }
                isFirstRow = false;
                int dataRowCount = alacakCariHesap.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"A{i}:D{dataCount}"], true);
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[cariStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in alacakCariHesap.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.TYPE;
                    ws.Cells[$"B{i}"].Value = item.NAME;
                    ws.Cells[$"C{i}"].Value = item.EXP;
                    ws.Cells[$"D{i}"].Value = item.TOTAL;

                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #region Krediler

            dataCount = 0;
            i = 4;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Krediler";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;
            #endregion

            currencyList = borcBorcKrediler.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = borcBorcKrediler.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in borcBorcKrediler.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.NAME;
                    ws.Cells[$"H{i}"].Value = item.EXP;
                    ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableBanka.TotalRowRange.BottomRowIndex) + _spaceRow;
            }

            #endregion

            #region Kendi Çeklerimiz
            
            #region Grup Başlığı Renklendir

            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Çeklerimiz";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;
            #endregion

            currencyList = borcBorcKCekler.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }
                isFirstRow = false;
                int dataRowCount = borcBorcKCekler.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in borcBorcKCekler.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.NAME;
                    ws.Cells[$"H{i}"].Value = item.EXP;
                    ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;}

                i = (tableBanka.TotalRowRange.BottomRowIndex) + _spaceRow;
            }

            #endregion

            #region Firma Kredi Kartı


            #region Grup Başlığı Renklendir


            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Kredi Kartı";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;
            #endregion

            currencyList = borcBorcKrediKarti.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }
                isFirstRow = false;
                int dataRowCount = borcBorcKrediKarti.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in borcBorcKrediKarti.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.NAME;
                    ws.Cells[$"H{i}"].Value = item.EXP;
                    ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }
                i = (tableBanka.TotalRowRange.BottomRowIndex + 2) + _spaceRow;
            }

            #endregion

            #region Borclu Cari Hesap

 
            #region Grup Başlığı Renklendir

            range = ws.Range[$"F{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = "Cari Hesap";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;
            #endregion

            currencyList = borcCariHesap.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {

                if (!isFirstRow)
                {
                    i++;
                }
                isFirstRow = false;
                int dataRowCount = borcCariHesap.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"F{i}:I{dataCount}"], true);
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[kasaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;
                tableBanka.Columns[1].Name = "Kaynak";
                tableBanka.Columns[2].Name = "Açıklama";
                tableBanka.Columns[3].Name = "Toplam";
                foreach (var item in borcCariHesap.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"F{i}"].Value = item.TYPE;
                    ws.Cells[$"G{i}"].Value = item.NAME;
                    ws.Cells[$"H{i}"].Value = item.EXP;
                    ws.Cells[$"I{i}"].Value = item.TOTAL;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableBanka.TotalRowRange.BottomRowIndex) + _spaceRow;}

            #endregion

            #endregion

            var allCell = ws.GetUsedRange();

            allCell.Alignment.WrapText = true;

            spreadsheetControl1.EndUpdate();

            #endregion

        }
    }
}