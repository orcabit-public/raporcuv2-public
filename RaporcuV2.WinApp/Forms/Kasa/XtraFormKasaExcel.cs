﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RaporcuV2.WinApp.Forms.Customs;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model.Reports;
using DevExpress.Spreadsheet;
using DevExpress.XtraBars;
using RaporcuV2.Util;

namespace RaporcuV2.WinApp.Forms.Kasa
{
    public partial class XtraFormKasaExcel : XtraFormBase, IReport
    {
        public int SectionStart { get; set; } = 4;
        private string _s = "A";
        private string _e = "N";
        public XtraFormKasaExcel()
        {
            InitializeComponent();
            this.barManagerControl1.gridControl1.Visible = false;

            AddEvents();
        }

        #region Methots

        #region Add Events

        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {

                CreateFirmLookup();
                CreatePeriodLookup();
                CreateFirstDate();
                foreach (BarItem barItem in barManagerControl1.barManagerForm.Items)
                {
                    if (barItem.Name == "firstDate")
                    {
                        ((DevExpress.XtraBars.BarEditItem) barItem).EditValue = DateTime.Today;
                    }
                }

                CreateRefreshButton();
               
                barManager1.BeginUpdate();

                var parentTools = this.barManagerControl1.barManagerForm.Bars[0];

                this.barManager1.Bars.Add(parentTools);

                barManager1.EndUpdate();

                commonBar1.Merge(parentTools);
            };

            //this.spreadsheetControl1.Dock = DockStyle.Fill;
        }

        #endregion

        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
            // Kasa ve Kasa Detay Renkelnedirme
            BuiltInStyleId kasaBaslikStyle = BuiltInStyleId.Accent1;
            BuiltInTableStyleId kasaStyle = BuiltInTableStyleId.TableStyleMedium2;

            // Banka ve Banka HAreketerli Renklendirme
            BuiltInStyleId bankaBaslikStyle = BuiltInStyleId.Accent2;
            BuiltInTableStyleId bankaStyle = BuiltInTableStyleId.TableStyleMedium3;

            // Kredi Durumu 
            BuiltInStyleId krediBaslikStyle = BuiltInStyleId.Accent3;
            BuiltInTableStyleId krediStyle = BuiltInTableStyleId.TableStyleMedium4;

            // Çek Durumu 
            BuiltInStyleId cekBaslikStyle = BuiltInStyleId.Accent4;
            BuiltInTableStyleId cekStyle = BuiltInTableStyleId.TableStyleMedium5;

            // Çek Durumu 
            BuiltInStyleId cekDurumBaslikStyle = BuiltInStyleId.Accent6;
            BuiltInTableStyleId cekDurumStyle = BuiltInTableStyleId.TableStyleMedium7;

            spreadsheetControl1.BeginUpdate();

            // Rapor Grubu
            int i = 1;
            IWorkbook workbook = spreadsheetControl1.Document;
            Worksheet ws = workbook.Worksheets[0];
            ws.ActiveView.PaperKind = PaperKind.A4;
            ws.ActiveView.Orientation = PageOrientation.Landscape;
            ws.ActiveView.Margins.Left = 0;
            ws.ActiveView.Margins.Top = 0;
            ws.ActiveView.Margins.Right = 0;
            ws.ActiveView.Margins.Bottom = 0;

            ws.Clear(ws.GetUsedRange());

            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToWidth = 1;
            ws.PrintOptions.FitToHeight = 0;

            var range = ws.Range[$"A{i}:M{i}"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[BuiltInStyleId.Title];
            ws.Cells[$"A{i}"].Value = FirmName;
            i++;

            range = ws.Range[$"A{i}:M{i}"];
            ws.MergeCells(range);
            range.Value = string.Format("{0} Kasa Durum Raporu", FirstDate.ToString("dd MMMM yyyy , dddd"));
            range.Style = workbook.Styles[BuiltInStyleId.Heading1];


            #region Kasa Toplamları

            #region Grup Başlığı Renklendir
            i = 4;
            range = ws.Range[$"A{i}:F{i}"];
            ws.MergeCells(range);
            range.Value = "Kasa Durumu";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            #endregion


            //SqlText = SqlReports.AtaKasa;
            _reportTag = RaporcuV2.Helper.ReportTags.AtaKasa;

            List<AtaKasa> kasa = GetPocoList<AtaKasa>();

            #region Add Data with Table

            int dataCount = kasa.Count+i;
            Table table = ws.Tables.Add(ws[$"A{i}:F{dataCount}"], true);
            table.AutoFilter.Disable();
            table.Style = workbook.TableStyles[kasaStyle];

            table.Columns[0].Name = Util.Labels.Aciklama;
            table.Columns[1].Name = Util.Labels.DunDevir;
            table.Columns[1].DataRange.NumberFormat = "#,##0.00";

            table.Columns[2].Name = Util.Labels.Borc;
            table.Columns[2].DataRange.NumberFormat = "#,##0.00";

            table.Columns[3].Name = Util.Labels.Alacak;
            table.Columns[3].DataRange.NumberFormat = "#,##0.00";

            table.Columns[4].Name = Util.Labels.Bakiye;
            table.Columns[4].DataRange.NumberFormat = "#,##0.00";
            table.Columns[5].Name = Util.Labels.YarinDevir;
            table.Columns[5].DataRange.NumberFormat = "#,##0.00";


            //table.ShowTotals = true;
            table.HeaderRowRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            table.Range.ColumnWidthInCharacters = 15;
            table.Columns[0].Range.ColumnWidthInCharacters = 30;

            #endregion

            foreach (var item in kasa)
            {
                i++;
                ws.Cells[$"A{i}"].Value = item.ACIKLAMA;
                ws.Cells[$"B{i}"].Value = item.DEVIR;
                ws.Cells[$"C{i}"].Value = item.BORC;
                ws.Cells[$"D{i}"].Value = item.ALACAK;
                ws.Cells[$"E{i}"].Value = item.BAKIYE;
                ws.Cells[$"F{i}"].Value = item.YARIN;
            }

            #endregion

            #region Kasa Hareketleri TL

            #region Grup Başlığı Renklendir
            i = 4;
            range = ws.Range[$"H{i}:M{i}"];
            ws.MergeCells(range);
            range.Value = "Kasa Hareketleri";range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            #endregion


            TrCurr = 0;
            //SqlText = SqlReports.AtaKasaHareketleri;
            _reportTag = RaporcuV2.Helper.ReportTags.AtaKasaHareketleri;
            List<AtaKasaHareketleri> kasaHareketleri = GetPocoList<AtaKasaHareketleri>();

            #region Add Data with Table

            dataCount = kasaHareketleri.Count +i;
            var rangeKhTl = ws[$"H{i}:M{dataCount}"];
            Table tableKhTl = ws.Tables.Add(rangeKhTl, true);
            tableKhTl.AutoFilter.Disable();
            tableKhTl.ShowTotals = true;
            tableKhTl.Style = workbook.TableStyles[kasaStyle];

            TableColumn colKhTl1 = tableKhTl.Columns[0];
            colKhTl1.Name = Util.Labels.KasaAdi;

            TableColumn colKhTl2 = tableKhTl.Columns[1];
            colKhTl2.Name = Util.Labels.Aciklama;

            TableColumn colKhTl3 = tableKhTl.Columns[2];
            colKhTl3.Name = Util.Labels.IslemAciklama;

            TableColumn colKhTl4 = tableKhTl.Columns[3];
            colKhTl4.Name = Util.Labels.Borc;
            colKhTl4.DataRange.NumberFormat = "#,##0.00";
            colKhTl4.TotalRowFunction = TotalRowFunction.Sum;
            colKhTl1.TotalRowLabel = "Toplam:";
            colKhTl4.Total.NumberFormat = "#,##0.00";

            TableColumn colKhTl5 = tableKhTl.Columns[4];
            colKhTl5.Name = Util.Labels.Alacak;
            colKhTl5.DataRange.NumberFormat = "#,##0.00";
            colKhTl5.TotalRowFunction = TotalRowFunction.Sum;
            colKhTl5.Total.NumberFormat = "#,##0.00";

            TableColumn colKhTl6 = tableKhTl.Columns[5];
            colKhTl6.Name = Util.Labels.Bolum;
            
            //table.ShowTotals = true;
            tableKhTl.HeaderRowRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            tableKhTl.Range.ColumnWidthInCharacters = 15;
            colKhTl1.Range.ColumnWidthInCharacters = 30;
            colKhTl2.Range.ColumnWidthInCharacters = 30;
            colKhTl3.Range.ColumnWidthInCharacters = 30;
            colKhTl5.Range.ColumnWidthInCharacters = 3;

            #endregion

            foreach (var item in kasaHareketleri)
            {
                i++;
                ws.Cells[$"H{i}"].Value = item.NAME;
                ws.Cells[$"I{i}"].Value = item.CUSTTITLE;
                ws.Cells[$"J{i}"].Value = item.LINEEXP;
                ws.Cells[$"K{i}"].Value = item.BORC;
                ws.Cells[$"L{i}"].Value = item.ALACAK;
                ws.Cells[$"M{i}"].Value = item.DEPARTMENT;
            }

            #endregion

            #region Kasa Hareketleri USD

            i = i + 4;
            TrCurr = 1;
            //SqlText = SqlReports.AtaKasaHareketleri;

            _reportTag = RaporcuV2.Helper.ReportTags.AtaKasaHareketleri;
            List<AtaKasaHareketleri> kasaHareketleriUsd = GetPocoList<AtaKasaHareketleri>();

            #region Add Data with Table

            dataCount = kasaHareketleriUsd.Count + i;
            var rangeKhUsd = ws[$"H{i}:M{dataCount}"];
            Table tableKhUsd = ws.Tables.Add(rangeKhUsd, true);
            tableKhUsd.AutoFilter.Disable();
            tableKhUsd.ShowTotals = true;
            tableKhUsd.Style = workbook.TableStyles[kasaStyle];

            TableColumn colKhUsd1 = tableKhUsd.Columns[0];
            colKhUsd1.Name = Util.Labels.KasaAdi;

            TableColumn colKhUsd2 = tableKhUsd.Columns[1];
            colKhUsd2.Name = Util.Labels.Aciklama;

            TableColumn colKhUsd3 = tableKhUsd.Columns[2];
            colKhUsd3.Name = Util.Labels.IslemAciklama;

            TableColumn colKhUsd4 = tableKhUsd.Columns[3];
            colKhUsd4.Name = Util.Labels.Borc;
            colKhUsd4.DataRange.NumberFormat = "#,##0.00";
            colKhUsd4.TotalRowFunction = TotalRowFunction.Sum;
            colKhUsd4.Total.NumberFormat = "#,##0.00";
            colKhUsd1.TotalRowLabel = "Toplam:";

            TableColumn colKhUsd5 = tableKhUsd.Columns[4];
            colKhUsd5.Name = Util.Labels.Alacak;
            colKhUsd5.DataRange.NumberFormat = "#,##0.00";
            colKhUsd5.TotalRowFunction = TotalRowFunction.Sum;
            colKhUsd5.Total.NumberFormat = "#,##0.00";

            TableColumn colKhUsd6 = tableKhUsd.Columns[5];
            colKhUsd6.Name = Util.Labels.Bolum;

            //table.ShowTotals = true;
            tableKhUsd.HeaderRowRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            tableKhUsd.Range.ColumnWidthInCharacters = 15;
            colKhUsd1.Range.ColumnWidthInCharacters = 30;
            colKhUsd2.Range.ColumnWidthInCharacters = 30;
            colKhUsd3.Range.ColumnWidthInCharacters = 30;

            #endregion

            foreach (var item in kasaHareketleriUsd)
            {
                i++;
                ws.Cells[$"H{i}"].Value = item.NAME;
                ws.Cells[$"I{i}"].Value = item.CUSTTITLE;
                ws.Cells[$"J{i}"].Value = item.LINEEXP;
                ws.Cells[$"K{i}"].Value = item.BORC;
                ws.Cells[$"L{i}"].Value = item.ALACAK;
                ws.Cells[$"M{i}"].Value = item.DEPARTMENT;

            }


            #endregion

            #region Kasa Hareketleri EURO

            i = i + 4;
            TrCurr = 20;
            //SqlText = SqlReports.AtaKasaHareketleri;

            _reportTag = RaporcuV2.Helper.ReportTags.AtaKasaHareketleri;
            List<AtaKasaHareketleri> kasaHareketleriEuro = GetPocoList<AtaKasaHareketleri>();

            #region Add Data with Table

            dataCount = kasaHareketleriEuro.Count + i;
            var rangeKhEuro = ws[$"H{i}:M{dataCount}"];
            Table tableKhEuro = ws.Tables.Add(rangeKhEuro, true);
            tableKhEuro.AutoFilter.Disable();
            tableKhEuro.ShowTotals = true;
            tableKhEuro.Style = workbook.TableStyles[kasaStyle];

            TableColumn colKhEuro1 = tableKhEuro.Columns[0];
            colKhEuro1.Name = Util.Labels.KasaAdi;

            TableColumn colKhEuro2 = tableKhEuro.Columns[1];
            colKhEuro2.Name = Util.Labels.Aciklama;

            TableColumn colKhEuro3 = tableKhEuro.Columns[2];
            colKhEuro3.Name = Util.Labels.IslemAciklama;

            TableColumn colKhEuro4 = tableKhEuro.Columns[3];
            colKhEuro4.Name = Util.Labels.Borc;
            colKhEuro4.DataRange.NumberFormat = "#,##0.00";
            colKhEuro4.TotalRowFunction = TotalRowFunction.Sum;
            colKhEuro4.Total.NumberFormat = "#,##0.00";
            colKhEuro1.TotalRowLabel = "Toplam:";

            TableColumn colKhEuro5 = tableKhEuro.Columns[4];
            colKhEuro5.Name = Util.Labels.Alacak;
            colKhEuro5.DataRange.NumberFormat = "#,##0.00";
            colKhEuro5.TotalRowFunction = TotalRowFunction.Sum;
            colKhEuro5.Total.NumberFormat = "#,##0.00";

            TableColumn colKhEuro6 = tableKhEuro.Columns[5];
            colKhEuro6.Name = Util.Labels.Bolum;

            //table.ShowTotals = true;
            tableKhEuro.HeaderRowRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            tableKhEuro.Range.ColumnWidthInCharacters = 15;
            colKhEuro1.Range.ColumnWidthInCharacters = 30;
            colKhEuro2.Range.ColumnWidthInCharacters = 30;
            colKhEuro3.Range.ColumnWidthInCharacters = 30;

            #endregion

            foreach (var item in kasaHareketleriEuro)
            {
                i++;
                ws.Cells[$"H{i}"].Value = item.NAME;
                ws.Cells[$"I{i}"].Value = item.CUSTTITLE;
                ws.Cells[$"J{i}"].Value = item.LINEEXP;
                ws.Cells[$"K{i}"].Value = item.BORC;
                ws.Cells[$"L{i}"].Value = item.ALACAK;
                ws.Cells[$"M{i}"].Value = item.DEPARTMENT;

            }


            #endregion

            #region Kasa Hareketleri GBP

            i = i + 4;
            TrCurr = 17;
            //SqlText = SqlReports.AtaKasaHareketleri;

            _reportTag = RaporcuV2.Helper.ReportTags.AtaKasaHareketleri;
            List<AtaKasaHareketleri> kasaHareketleriGbp = GetPocoList<AtaKasaHareketleri>();

            #region Add Data with Table

            dataCount = kasaHareketleriGbp.Count + i;
            var rangeKhGbp = ws[$"H{i}:M{dataCount}"];
            Table tableKhGbp = ws.Tables.Add(rangeKhGbp, true);
            tableKhGbp.AutoFilter.Disable();
            tableKhGbp.ShowTotals = true;
            tableKhGbp.Style = workbook.TableStyles[kasaStyle];

            TableColumn colKhGbp1 = tableKhGbp.Columns[0];
            colKhGbp1.Name = Util.Labels.KasaAdi;

            TableColumn colKhGbp2 = tableKhGbp.Columns[1];
            colKhGbp2.Name = Util.Labels.Aciklama;

            TableColumn colKhGbp3 = tableKhGbp.Columns[2];
            colKhGbp3.Name = Util.Labels.IslemAciklama;

            TableColumn colKhGbp4 = tableKhGbp.Columns[3];
            colKhGbp4.Name = Util.Labels.Borc;
            colKhGbp4.DataRange.NumberFormat = "#,##0.00";
            colKhGbp4.TotalRowFunction = TotalRowFunction.Sum;
            colKhGbp4.Total.NumberFormat = "#,##0.00";
            colKhGbp1.TotalRowLabel = "Toplam:";

            TableColumn colKhGbp5 = tableKhGbp.Columns[4];
            colKhGbp5.Name = Util.Labels.Alacak;
            colKhGbp5.DataRange.NumberFormat = "#,##0.00";
            colKhGbp5.TotalRowFunction = TotalRowFunction.Sum;
            colKhGbp5.Total.NumberFormat = "#,##0.00";

            TableColumn colKhGbp6 = tableKhGbp.Columns[5];
            colKhGbp6.Name = Util.Labels.Bolum;

            //table.ShowTotals = true;
            tableKhGbp.HeaderRowRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            tableKhGbp.Range.ColumnWidthInCharacters = 15;
            colKhGbp1.Range.ColumnWidthInCharacters = 30;
            colKhGbp2.Range.ColumnWidthInCharacters = 30;
            colKhGbp3.Range.ColumnWidthInCharacters = 30;

            #endregion

            foreach (var item in kasaHareketleriGbp)
            {
                i++;
                ws.Cells[$"H{i}"].Value = item.NAME;
                ws.Cells[$"I{i}"].Value = item.CUSTTITLE;
                ws.Cells[$"J{i}"].Value = item.LINEEXP;
                ws.Cells[$"K{i}"].Value = item.BORC;
                ws.Cells[$"L{i}"].Value = item.ALACAK;
                ws.Cells[$"M{i}"].Value = item.DEPARTMENT;

            }


            #endregion

            #region Banka Durumu 

            i = ws.GetUsedRange().RowCount + 4;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:F{i}"];
            ws.MergeCells(range);
            range.Value = "Banka Durumu";
            range.Style = workbook.Styles[bankaBaslikStyle];
            i++;

            #endregion

            //SqlText = SqlReports.BankaDurumu;
            _reportTag = RaporcuV2.Helper.ReportTags.AtaBankaDurumu;

            List<AtaBanka> bankaDurum = GetPocoList<AtaBanka>();

            var currencyList = bankaDurum.GroupBy(a => a.CURCODE).Select(s=> new { pb = s.Key});
            bool isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;

                int dataRowCount = bankaDurum.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"A{i}:F{dataCount}"], true);
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[bankaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";
                tableBanka.Columns[0].Name = $"{curCode.pb} {Labels.Banka}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Banka Toplamı :";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;

                tableBanka.Columns[1].Name = Labels.DunDevir;
                tableBanka.Columns[2].Name = Labels.Borc;
                tableBanka.Columns[3].Name = Labels.Alacak;
                tableBanka.Columns[4].Name = Labels.Bakiye;
                tableBanka.Columns[5].Name = Labels.YarinDevir;

                foreach (var item in bankaDurum.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.ACIKLAMA;
                    ws.Cells[$"B{i}"].Value = item.DEVIR;
                    ws.Cells[$"C{i}"].Value = item.BORC;
                    ws.Cells[$"D{i}"].Value = item.ALACAK;
                    ws.Cells[$"E{i}"].Value = item.BAKIYE;
                    ws.Cells[$"F{i}"].Value = item.YARIN;
                }

                tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }
                
            }

            #region TL Banka



            #endregion

            #endregion

            #region Banka Hareketleri 

            i = SectionStart;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"H{i}:L{i}"];
            ws.MergeCells(range);
            range.Value = "Banka Hareketleri";
            range.Style = workbook.Styles[bankaBaslikStyle];
            i++;

            #endregion

            //SqlText = SqlReports.AtaBankaHareketleri;
            _reportTag = RaporcuV2.Helper.ReportTags.AtaBankaHareketleri;
            List<AtaBankaHareketleri> bankaHareketleri = GetPocoList<AtaBankaHareketleri>();
            if(bankaHareketleri != null)
            {
                currencyList = bankaHareketleri.GroupBy(a => a.DVZCINS).Select(s => new { pb = s.Key });
                isFirstRow = true;
               // i++;
                foreach (var curCode in currencyList)
                {
                   if (!isFirstRow)
                   {
                       i = i + 3;
                   }

                    isFirstRow = false;

                    int dataRowCount = bankaHareketleri.Count(a => a.DVZCINS == curCode.pb);
                    dataCount = dataRowCount + i;
                    Table tableBanka = ws.Tables.Add(ws[$"H{i}:L{dataCount}"], true);
                    tableBanka.ShowTotals = true;
                    tableBanka.AutoFilter.Disable();
                    tableBanka.Style = workbook.TableStyles[bankaStyle];
                    tableBanka.DataRange.NumberFormat = "#,##0.00";
                    tableBanka.Columns[0].Name = $"{curCode.pb} {Labels.Banka}";
                    tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Banka Toplamı :";
                    //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;

                    tableBanka.Columns[1].Name = Labels.CariHesapUnvani;
                    tableBanka.Columns[2].Name = Labels.IslemAciklama;
                    tableBanka.Columns[3].Name = Labels.Borc;
                    tableBanka.Columns[4].Name = Labels.Alacak;


                    foreach (var item in bankaHareketleri.Where(a => a.DVZCINS == curCode.pb))
                    {
                        i++;
                        ws.Cells[$"H{i}"].Value = item.DEFINITION_;
                        ws.Cells[$"I{i}"].Value = item.CARI;
                        ws.Cells[$"J{i}"].Value = item.LINEEXP;

                        if (item.TRCURR == 0)
                        {
                            ws.Cells[$"K{i}"].Value = item.TLALACAK;
                            ws.Cells[$"L{i}"].Value = item.TLBORC;
                        }
                        else 
                        {
                            ws.Cells[$"K{i}"].Value = item.DVZALACAK;
                            ws.Cells[$"L{i}"].Value = item.DVZBORC;
                        }

                    }

                    tableBanka.TotalRowRange.NumberFormat = "#,##0.00";
                    foreach (TableColumn column in tableBanka.Columns)
                    {
                        if (column.Index != 0)
                            column.TotalRowFunction = TotalRowFunction.Sum;
                    }

                }
            }

            #region TL Banka



            #endregion

            #endregion

            #region Kredi Durumu 

            i = ws.GetUsedRange().RowCount + 4;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:F{i}"];
            ws.MergeCells(range);
            range.Value = "Kredi Durumu";
            range.Style = workbook.Styles[krediBaslikStyle];
            i++;

            #endregion

            //SqlText = SqlReports.KrediDurumu;
            _reportTag = RaporcuV2.Helper.ReportTags.AtaKrediDurumu;
            List<AtaKredi> krediDrum = GetPocoList<AtaKredi>();

            currencyList = krediDrum.GroupBy(a => a.DOVIZ).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;

                int dataRowCount = krediDrum.Count(a => a.DOVIZ == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableKredi = ws.Tables.Add(ws[$"A{i}:F{dataCount}"], true);
                tableKredi.ShowTotals = true;tableKredi.AutoFilter.Disable();
                tableKredi.Style = workbook.TableStyles[krediStyle];
                tableKredi.DataRange.NumberFormat = "#,##0.00";
                tableKredi.Columns[0].Name = $"{curCode.pb} {Labels.KrediKodu}";
                tableKredi.Columns[0].TotalRowLabel = $"{curCode.pb} Kredi Toplamı :";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;

                tableKredi.Columns[1].Name = Labels.KrediAciklamasi;
                tableKredi.Columns[2].Name = Labels.DovizTipi;
                tableKredi.Columns[3].Name = Labels.KrediAnaPara;
                tableKredi.Columns[4].Name = Labels.KrediFaiz;
                tableKredi.Columns[5].Name = Labels.KrediToplam;

                foreach (var item in krediDrum.Where(a => a.DOVIZ == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.CODE;
                    ws.Cells[$"B{i}"].Value = item.DEFINITION_;
                    ws.Cells[$"C{i}"].Value = item.DOVIZ;
                    ws.Cells[$"D{i}"].Value = item.TRTOTAL;
                    ws.Cells[$"E{i}"].Value = item.INTTOTAL;
                    ws.Cells[$"F{i}"].Value = item.BAKIYE;
                }

                tableKredi.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableKredi.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }
            #endregion

            #region Emanet Hesapları

            i = SectionStart;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"H{i}:L{i}"];
            ws.MergeCells(range);
            range.Value = "Emanet Hesaplar Durumu";
            range.Style = workbook.Styles[krediBaslikStyle];
            i++;

            #endregion


            #endregion

            #region Portfoy Cek Senet Cari

            i = ws.GetUsedRange().RowCount + 4;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:F{i}"];
            ws.MergeCells(range);
            range.Value = "Portföy Çek Senet Müşteri Toplamı";
            range.Style = workbook.Styles[cekBaslikStyle];
            i++;

            #endregion

            //SqlText = SqlReports.PortfoyCekSenetCari;

            _reportTag = RaporcuV2.Helper.ReportTags.AtaPortfoyCekSenetCari;

            List<AtaPortfoyCekSenetCari> cekSenetDurumu = GetPocoList<AtaPortfoyCekSenetCari>();

            // new List<string>(new[] {"TL"}); //krediDrum.GroupBy(a => a.DOVIZ).Select(s => new { pb = s.Key });
            isFirstRow = true;
                i = ws.GetUsedRange().RowCount + 1;
            if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;

                int rowCount = cekSenetDurumu.Count;
                dataCount = rowCount + i;
                Table tableCek = ws.Tables.Add(ws[$"A{i}:F{dataCount}"], true);
                tableCek.ShowTotals = true;
                tableCek.AutoFilter.Disable();
                tableCek.Style = workbook.TableStyles[cekStyle];

                tableCek.DataRange.NumberFormat = "#,##0.00";

                tableCek.Columns[0].Name = $"{Labels.CariHesapUnvani}";
                tableCek.Columns[0].TotalRowLabel = $"Çek Toplamı :";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;

                tableCek.Columns[1].Name = " ";
                tableCek.Columns[2].Name = Labels.CekSayi;
                tableCek.Columns[3].Name = Labels.TL;
                tableCek.Columns[4].Name = Labels.USD;tableCek.Columns[5].Name = Labels.EURO;

                foreach (var item in cekSenetDurumu)
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.CARIISIM;
                    ws.Cells[$"C{i}"].Value = item.SAYI;
                    ws.Cells[$"D{i}"].Value = item.TL;
                    ws.Cells[$"E{i}"].Value = item.DOLAR;
                    ws.Cells[$"F{i}"].Value = item.EURO;
                }

                tableCek.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableCek.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;}


            #endregion

            #region Portföy Çek Senet Toplamı

            i = SectionStart;
            #region Grup Başlığı Renklendir

            range = ws.Range[$"H{i}:L{i}"];
            ws.MergeCells(range);
            range.Value = "Portföy Çek Senet Toplamı";
            range.Style = workbook.Styles[cekBaslikStyle];
            i++;

            #endregion

            //SqlText = SqlReports.MusteriCekSenetToplam;
            _reportTag = RaporcuV2.Helper.ReportTags.AtaMusteriCekSenetToplam;
            List<AtaMusteriCekSenetToplami> musteriCekSenetDurumu = GetPocoList<AtaMusteriCekSenetToplami>();

            if (musteriCekSenetDurumu != null)
            {
                currencyList = musteriCekSenetDurumu.GroupBy(a => a.DOVIZ).Select(s => new { pb = s.Key });
                isFirstRow = true;
                // i++;
                foreach (var curCode in currencyList)
                {
                    if (!isFirstRow)
                    {
                        i = i + 3;
                    }

                    isFirstRow = false;

                    int dataRowCount = musteriCekSenetDurumu.Count(a => a.DOVIZ == curCode.pb);
                    dataCount = dataRowCount + i;
                    Table tableMc = ws.Tables.Add(ws[$"H{i}:L{dataCount}"], true);
                    tableMc.ShowTotals = true;
                    tableMc.AutoFilter.Disable();
                    tableMc.Style = workbook.TableStyles[cekStyle];
                    tableMc.DataRange.NumberFormat = "#,##0.00";
                    tableMc.Columns[0].Name = $"{curCode.pb} {Labels.EvrakTuru}";
                    tableMc.Columns[0].TotalRowLabel = $"{curCode.pb} Evrak Toplamı :";
                    //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;

                    tableMc.Columns[1].Name = " ";
                    tableMc.Columns[2].Name = " ";
                    tableMc.Columns[3].Name = Labels.Tutar;
                    tableMc.Columns[4].Name = Labels.DovizTipi;


                    foreach (var item in musteriCekSenetDurumu.Where(a => a.DOVIZ == curCode.pb))
                    {
                        i++;
                        ws.Cells[$"H{i}"].Value = item.TUR;
                        //ws.Cells[$"I{i}"].Value = item.DOVIZ;
                        //ws.Cells[$"J{i}"].Value = item.TUTAR;

                        ws.Cells[$"K{i}"].Value = item.TUTAR;
                        ws.Cells[$"L{i}"].Value = item.DOVIZ;
                    }

                    tableMc.TotalRowRange.NumberFormat = "#,##0.00";
                    foreach (TableColumn column in tableMc.Columns)
                    {
                        if (column.Index != 0)
                            column.TotalRowFunction = TotalRowFunction.Sum;
                    }

                }
            }

            #endregion

            #region Çek Durumu 

            i = ws.GetUsedRange().RowCount + 4;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:F{i}"];
            ws.MergeCells(range);
            range.Value = "Çek Durumu";
            range.Style = workbook.Styles[cekDurumBaslikStyle];
            i++;

            #endregion

            //SqlText = SqlReports.CekDurumu;
            _reportTag = RaporcuV2.Helper.ReportTags.AtaCekDurumu;
            List<AtaCekDurumu> cekDurumu = GetPocoList<AtaCekDurumu>();

            currencyList = cekDurumu.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;

                int dataRowCount = cekDurumu.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableCekDurumu = ws.Tables.Add(ws[$"A{i}:F{dataCount}"], true);
                tableCekDurumu.ShowTotals = true;
                tableCekDurumu.AutoFilter.Disable();
                tableCekDurumu.Style = workbook.TableStyles[cekDurumStyle];
                tableCekDurumu.DataRange.NumberFormat = "#,##0.00";
                tableCekDurumu.Columns[0].Name = $"{curCode.pb} Çekler - {Labels.CariHesapUnvani}";
                tableCekDurumu.Columns[0].TotalRowLabel = $"{curCode.pb} Çek Toplamı :";
                //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;

                tableCekDurumu.Columns[1].Name = " ";
                tableCekDurumu.Columns[2].Name = " ";
                tableCekDurumu.Columns[3].Name = Labels.CekSayi;
                tableCekDurumu.Columns[4].Name = Labels.Tutar; // .DovizTipi;
                tableCekDurumu.Columns[5].Name = Labels.DovizTipi;


                foreach (var item in cekDurumu.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.DEFINITION_;
                    ws.Cells[$"B{i}"].Value = string.Empty;
                    ws.Cells[$"C{i}"].Value = string.Empty;
                    ws.Cells[$"D{i}"].Value = item.SAYI;
                    ws.Cells[$"E{i}"].Value = item.TUTAR;
                    ws.Cells[$"F{i}"].Value = item.CURCODE;
                }

                tableCekDurumu.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableCekDurumu.Columns)
                {
                    if (column.Index != 0 )
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }


            #endregion

            #region Portfoy ve Takas Çekleri
            i = SectionStart;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"H{i}:L{i}"];
            ws.MergeCells(range);
            range.Value = "Portfoy ve Takas Çekleri";
            range.Style = workbook.Styles[cekDurumBaslikStyle];
            i++;

            #endregion

            //SqlText = SqlReports.PortfoyVeTakasCekleri;
            _reportTag = RaporcuV2.Helper.ReportTags.AtaPortfoyVeTakasCekleri;
            List<AtaPortfoyVeTakasCekleri> portfoyVeTakasCekleri = GetPocoList<AtaPortfoyVeTakasCekleri>();

            if (portfoyVeTakasCekleri != null)
            {
                currencyList = portfoyVeTakasCekleri.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
                isFirstRow = true;
                // i++;
                foreach (var curCode in currencyList)
                {
                    if (!isFirstRow)
                    {
                        i = i + 3;
                    }

                    isFirstRow = false;

                    int dataRowCount = portfoyVeTakasCekleri.Count(a => a.CURCODE == curCode.pb);
                    dataCount = dataRowCount + i;
                    Table tableMc = ws.Tables.Add(ws[$"H{i}:L{dataCount}"], true);
                    tableMc.ShowTotals = true;
                    tableMc.AutoFilter.Disable();
                    tableMc.Style = workbook.TableStyles[cekDurumStyle];
                    tableMc.DataRange.NumberFormat = "#,##0.00";
                    tableMc.Columns[0].Name = $"{curCode.pb} {Labels.CariBanka}";
                    tableMc.Columns[0].TotalRowLabel = $"{curCode.pb} Evrak Toplamı :";
                    //tableBanka.Columns[0].Range.ColumnWidthInCharacters = 50;

                    tableMc.Columns[1].Name = Labels.VadeTarihi;
                    tableMc.Columns[1].DataRange.NumberFormat = "dd/mm/yyyy";

                    tableMc.Columns[2].Name = " ";
                    tableMc.Columns[3].Name = Labels.Tutar;
                    tableMc.Columns[4].Name = Labels.DovizTipi;


                    foreach (var item in portfoyVeTakasCekleri.Where(a => a.CURCODE == curCode.pb))
                    {
                        i++;
                        ws.Cells[$"H{i}"].Value = item.DEFINITION_;
                        ws.Cells[$"I{i}"].Value = item.DUEDATE;
                        //ws.Cells[$"J{i}"].Value = item.TUTAR;

                        ws.Cells[$"K{i}"].Value = item.TRNET;
                        ws.Cells[$"L{i}"].Value = item.CURCODE;
                    }

                    tableMc.TotalRowRange.NumberFormat = "#,##0.00";
                    foreach (TableColumn column in tableMc.Columns)
                    {
                        if (column.Index != 0)
                            column.TotalRowFunction = TotalRowFunction.Sum;
                    }

                }
            }

            #endregion

            #region Teminatlar

            i = ws.GetUsedRange().RowCount + 4;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:F{i}"];
            ws.MergeCells(range);
            range.Value = "Teminat Mektubu Detayı";
            range.Style = workbook.Styles[cekBaslikStyle];
            i++;

            #endregion

            //SqlText = SqlReports.TeminatMektuplari;
            _reportTag = RaporcuV2.Helper.ReportTags.AtaTeminatMektuplari;
            List<AtaTeminatMektuplari> teminatDurumu = GetPocoList<AtaTeminatMektuplari>();

            // new List<string>(new[] {"TL"}); //krediDrum.GroupBy(a => a.DOVIZ).Select(s => new { pb = s.Key });
            isFirstRow = true;
            i = ws.GetUsedRange().RowCount + 1;
            if (!isFirstRow)
            {
                i++;}

            isFirstRow = false;

            rowCount = teminatDurumu.Count;
            dataCount = rowCount + i;
            Table tableTeminat = ws.Tables.Add(ws[$"A{i}:F{dataCount}"], true);
            tableTeminat.ShowTotals = true;
            tableTeminat.AutoFilter.Disable();
            tableTeminat.Style = workbook.TableStyles[cekStyle];

            tableTeminat.DataRange.NumberFormat = "#,##0.00";
            tableTeminat.Columns[0].Name = $"{Labels.BankaHesapUnvani}";
            tableTeminat.Columns[0].TotalRowLabel = $"Teminat Toplamı :";
            //tableTeminat.Columns[0].Range.ColumnWidthInCharacters = 50;

            tableTeminat.Columns[1].Name = Labels.KimeVerildigi;
            //tableTeminat.Columns[2].Name = Labels.BelgeNo;
            tableTeminat.Columns[2].Name = Labels.Tutar;
            tableTeminat.Columns[3].Name = Labels.DovizTipi;
            tableTeminat.Columns[4].Name = Labels.BaslangicTarihi;
            tableTeminat.Columns[4].DataRange.NumberFormat = "dd/mm/yyyy";
            tableTeminat.Columns[5].Name = Labels.BitisTarihi;
            tableTeminat.Columns[5].DataRange.NumberFormat = "dd/mm/yyyy";
            foreach (var item in teminatDurumu)
            {
                i++;
                ws.Cells[$"A{i}"].Value = item.BANKAADI;
                ws.Cells[$"B{i}"].Value = $"{item.OWING} - {item.TMEKTUKNO}";
                ws.Cells[$"C{i}"].Value = item.TRNET;
                ws.Cells[$"D{i}"].Value = item.PB;
                ws.Cells[$"E{i}"].Value = item.BEGDATE;
                ws.Cells[$"F{i}"].Value = item.ENDDATE;
            }

            tableCek.TotalRowRange.NumberFormat = "#,##0.00";
            foreach (TableColumn column in tableCek.Columns)
            {
                if (column.Index != 0)
                    column.TotalRowFunction = TotalRowFunction.Sum;
            }


            #endregion

            #endregion

            int lastRow = ws.Rows.LastUsedIndex+5;

            range = ws.Range[$"B{lastRow}:C{lastRow+2}"];
            ws.MergeCells(range);
            range.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
            range.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            range.Alignment.Vertical = SpreadsheetVerticalAlignment.Top;


            ws.Cells[$"B{lastRow}"].Value =  "FİNANS";

            range = ws.Range[$"D{lastRow}:E{lastRow+2}"];
            ws.MergeCells(range);
            range.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
            range.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            range.Alignment.Vertical = SpreadsheetVerticalAlignment.Top;
            ws.Cells[$"D{lastRow}"].Value = "MUHASEBE";

            range = ws.Range[$"F{lastRow}:G{lastRow+2}"];
            ws.MergeCells(range);
            range.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
            range.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            range.Alignment.Vertical = SpreadsheetVerticalAlignment.Top;
            ws.Cells[$"F{lastRow}"].Value = "GENEL MÜDÜR";


            var allCell = ws.GetUsedRange();


            allCell.Alignment.WrapText = true;

            spreadsheetControl1.EndUpdate();

        }
    }
}