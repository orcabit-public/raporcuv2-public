﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Spreadsheet;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model.Reports;
using RaporcuV2.Util;
using RaporcuV2.WinApp.Forms.Customs;
using DevExpress.XtraSpreadsheet;

namespace RaporcuV2.WinApp.Forms.Kasa
{
    public partial class XtraFormNakitAkisDetay : XtraFormBase, IReport
    {
        public int SectionStart { get; set; } = 4;
        private string _s = "A";
        private string _e = "N";
        private int _spaceRow = 2;

        public XtraFormNakitAkisDetay()
        {
            InitializeComponent();
            this.barManagerControl1.gridControl1.Visible = false;
            AddEvents();
        }

        #region Methots

        #region Add Events

        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                IWorkbook workbook = spreadsheetControl1.Document;
                Worksheet ws = workbook.Worksheets[0];
                ws.Name = "Nakit Akış Detay";


                CreateFirmLookup();
                CreatePeriodLookup();
                //CreateFirstDate();
                foreach (BarItem barItem in barManagerControl1.barManagerForm.Items)
                {
                    if (barItem.Name == "firstDate")
                    {
                        ((DevExpress.XtraBars.BarEditItem)barItem).EditValue = DateTime.Today;
                        //DateEdit dateedit = ((BarEditItem)barItem) as DateEdit;
                        ((DevExpress.XtraEditors.Repository.RepositoryItemDateEdit)
                            ((DevExpress.XtraBars.BarEditItem)barItem).Edit).MinValue = DateTime.Today;

                    }
                }

                CreateRefreshButton();

                barManager1.BeginUpdate();

                var parentTools = this.barManagerControl1.barManagerForm.Bars[0];

                this.barManager1.Bars.Add(parentTools);

                barManager1.EndUpdate();

                commonBar1.Merge(parentTools);
            };

            this.spreadsheetControl1.Dock = DockStyle.Fill;
        }

        #endregion

        #endregion
        #region Refresh Grid Data

        public void RefreshGridData()
        {
            #region Sorgular

            _reportTag = RaporcuV2.Helper.ReportTags.NakitAkisDetay;
            List<NakitAkisDetay> detailData = GetPocoList<NakitAkisDetay>();

            #endregion

            #region SpreatSheet

            int i = 1;

            spreadsheetControl1.BeginUpdate();

            IWorkbook workbook = spreadsheetControl1.Document;
            bool any = workbook.Worksheets.Any(a => a.Name == "Detay");
            if (!any)
            {
                workbook.Worksheets.Add("Detay");
            }
            
            any = workbook.Worksheets.Any(a => a.Name == "Bütçe");
            if (!any)
            {
                workbook.Worksheets.Add("Bütçe");
            }

            Worksheet ws = workbook.Worksheets[0];
            Worksheet wsDetail = workbook.Worksheets[1];
            Worksheet wsBudget  = workbook.Worksheets[2];

            foreach (var item in workbook.Worksheets)
            {

                item.ActiveView.PaperKind = PaperKind.A4;
                item.ActiveView.Orientation = PageOrientation.Landscape;
                item.ActiveView.Margins.Left = 0;
                item.ActiveView.Margins.Top = 0;
                item.ActiveView.Margins.Right = 0;
                item.ActiveView.Margins.Bottom = 0;
                item.UnMergeCells(item.GetUsedRange());
                item.Clear(item.GetUsedRange());
                item.PrintOptions.FitToPage = true;
                item.PrintOptions.FitToPage = true;
                item.PrintOptions.FitToWidth = 1;
                item.PrintOptions.FitToHeight = 0;
            }

            var currencies = detailData.Select(a => new { a.CURRENCYTYPE, a.CURRENCY }).GroupBy(a => new { a.CURRENCYTYPE, a.CURRENCY }).OrderBy(o => o.Key.CURRENCYTYPE);
            var currencyTypes = detailData.Select(a => new { a.CURRENCYTYPE, a.CURRENCY}).GroupBy(a => new { a.CURRENCYTYPE, a.CURRENCY }).OrderBy(a => a.Key.CURRENCYTYPE);
            int currencyCount = currencies.Count();

            var range = ws.Range.FromLTRB(0, 0, 2+ ((currencyCount+1)*3), 0); // ws.Range[$"A{i}:O{i}"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[BuiltInStyleId.Title];
            ws.Cells[$"A{i}"].Value = FirmName;
            i++;

            range = ws.Range.FromLTRB(0, 1, 2 + ((currencyCount + 1) * 3),1); //[$"A{i}:O{i}"];
            ws.MergeCells(range);
            range.Value = string.Format("{0} Nakit Akış Detay Raporu", DateTime.Today.ToString("dd MMMM yyyy , dddd"));
            range.Style = workbook.Styles[BuiltInStyleId.Heading1];

            int ReportFirstRow = 4;
            ws.Cells[ReportFirstRow, 0].Value = "YIL";
            ws.Cells[ReportFirstRow, 0].Font.Bold = true;
            ws.Cells[ReportFirstRow, 1].Value = "AY";
            ws.Cells[ReportFirstRow, 1].Font.Bold = true;
            ws.Cells[ReportFirstRow, 2].Value = "DEVIR";
            ws.Cells[ReportFirstRow, 2].Font.Bold = true;

            // BURADAN MERGE İŞLEMİNİ YAP
            //var varliklarRange = ws.Range["R3C4:R7C10"];
            //varliklarRange.Merge();

            int leftIndex = 3;
            int bottomIndex = 3;
            int topIndex = 3;
            int headerSize = 2 + currencyCount;

            CellRange rangeA1D3 = ws.Range.FromLTRB(leftIndex, topIndex, headerSize, bottomIndex);
            rangeA1D3.FillColor = Color.LightGreen;
            rangeA1D3.Merge(MergeCellsMode.IgnoreIntersections);
            rangeA1D3.Value = "VARLIKLAR";
            rangeA1D3.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            rangeA1D3.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            rangeA1D3.Font.Bold = true;


            CellRange rangeG3G4 = ws.Range.FromLTRB(headerSize + 1, topIndex, headerSize + 1, bottomIndex + 1);
            rangeG3G4.Merge(MergeCellsMode.IgnoreIntersections);
            rangeG3G4.Value = "TOPLAM";
            rangeG3G4.FillColor = Color.Orange;
            rangeG3G4.Font.Bold = true;

            int colNr = leftIndex;
            foreach (var item in currencies)
            {
                Cell cell = ws.Cells[4, colNr];
                cell.Value = item.Key.CURRENCY;
                cell.Font.Bold = true;
                cell.FillColor = Color.LightGreen;
                colNr++;
            }


            #region Borçlar

            headerSize += 2 + currencyCount - 1;
            // soldan başlaker ilk üç sütün döviz adedi ve toplam şekilnde olmalı
            leftIndex = 3 + currencies.Count() + 1;
            CellRange rangeH3J3 = ws.Range.FromLTRB(leftIndex, topIndex, headerSize, bottomIndex);
            rangeH3J3.FillColor = Color.LightPink;
            rangeH3J3.Merge(MergeCellsMode.IgnoreIntersections);
            rangeH3J3.Value = "BORÇLAR";
            rangeH3J3.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            rangeH3J3.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            rangeH3J3.Font.Bold = true;

            CellRange rangeK3K4 = ws.Range.FromLTRB(headerSize + 1, topIndex, headerSize + 1, bottomIndex + 1);
            rangeK3K4.Merge(MergeCellsMode.IgnoreIntersections);
            rangeK3K4.Value = "TOPLAM";
            rangeK3K4.FillColor = Color.Orange;
            rangeK3K4.Font.Bold = true;

            colNr = leftIndex;
            foreach (var item in currencies)
            {
                Cell cell = ws.Cells[4, colNr];
                cell.Value = item.Key.CURRENCY;
                cell.Font.Bold = true;
                cell.FillColor = Color.LightPink;
                colNr++;
            }


            #endregion

            #region Fark

            headerSize += 2 + currencyCount - 1;
            // Döviz sayısı + toplamın iki katı olacak 
            leftIndex = 3 + ((currencies.Count() + 1) * 2);
            CellRange rangeL3N3 = ws.Range.FromLTRB(leftIndex, topIndex, headerSize, bottomIndex);
            rangeL3N3.FillColor = Color.LightSkyBlue;
            rangeL3N3.Merge(MergeCellsMode.IgnoreIntersections);
            rangeL3N3.Value = "FARK";
            rangeL3N3.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            rangeL3N3.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            rangeL3N3.Font.Bold = true;

            CellRange rangeO3O4 = ws.Range.FromLTRB(headerSize + 1, topIndex, headerSize + 1, bottomIndex + 1);
            rangeO3O4.Merge(MergeCellsMode.IgnoreIntersections);
            rangeO3O4.Value = "TOPLAM";
            rangeO3O4.Name = "FARKTOPLAM";
            rangeO3O4.FillColor = Color.Orange;
            rangeO3O4.Font.Bold = true;

            colNr = leftIndex;
            foreach (var item in currencies)
            {
                Cell cell = ws.Cells[4, colNr];
                cell.Value = item.Key.CURRENCY;
                cell.Font.Bold = true;
                cell.FillColor = Color.LightSkyBlue;
                colNr++;
            }
            #endregion

            workbook.DocumentSettings.R1C1ReferenceStyle = true;

            #region Currency Rates
            int currencyColumn = 0;
            foreach (var item in currencyTypes)
            {
                if (item.Key.CURRENCYTYPE == 0)
                    continue;

                TrCurr = item.Key.CURRENCYTYPE;
                _reportTag = RaporcuV2.Helper.ReportTags.KurBuGun;
                List<Kur> Kur1 = GetPocoList<Kur>();
                double kur1 = 0;
                if (Kur1.Count > 0)
                {
                    kur1 = (double)Kur1.FirstOrDefault().RATES1;
                }

                string currencyIcon = item.Key.CURRENCY; //TrCurr == 0 ? "₺" : TrCurr == 1 ? "$" : TrCurr == 20 ? "€" : "";
                string name = $"{item.Key.CURRENCY}_KUR";
                Cell cellCurrency = ws.Cells[2, currencyColumn];
                cellCurrency.Value = kur1;
                //cellCurrency.NumberFormat = $"{currencyIcon}#,##0.00000";
                cellCurrency.NumberFormat = $"[${currencyIcon}] #.##0,00";
                cellCurrency.Font.Bold = true;
                //cellCurrency.Name = name;
                currencyColumn++;
            }

            #endregion

            #region Yıl Dönem


            var period = detailData.Select(a => a.PERIODYEAR).GroupBy(g => g).OrderBy(o => o.Key).ToList();
            int yearRow = 5;
            int formulaRow = 1;
            foreach (var item in period)
            {
                int currentMonth = DateTime.Today.Month;
                int currentYear = DateTime.Today.Year;

                ws.Cells[yearRow, 0].Value = item.Key;

                for (int idx = 1; idx < 13; idx++)
                {
                    if (item.Key == currentYear && idx < currentMonth)
                        continue;

                    ws.Cells[yearRow, 0].Value = item.Key;
                    ws.Cells[yearRow, 0].NumberFormat = "#####";

                    ws.Cells[yearRow, 1].Value = idx;
                    ws.Cells[yearRow, 1].NumberFormat = "#####";

                    ws.Cells[yearRow, 2].Formula = "=+R[-1]C[12]";
                    ws.Cells[yearRow, 2].NumberFormat = "#,##0";

                    #region Varlıklar

                    int formulaCol = 3;
                    foreach (var curr in currencies)
                    {
                        //VARLIKLAR
                        ws.Cells[yearRow, formulaCol].Formula = "=SUMIFS(Detay!C7;Detay!C1;RC1;Detay!C2;RC2;Detay!C5;R4C4;Detay!C6;R5C)";
                        ws.Cells[yearRow, formulaCol].NumberFormat = "#,##0";
                        ws.Cells[yearRow, formulaCol].FillColor = Color.LightGreen;
                        formulaCol++;
                    }

                    string formula = $"=RC[-{currencyCount}]";
                    currencyColumn = 0;
                    int j = 1;
                    foreach (var totalItem in currencies)
                    {
                        if (totalItem.Key.CURRENCYTYPE == 0)
                        {
                            continue;
                        }

                        formula += $"+(RC[-{currencyCount - j}]*R3C{j})";
                        j++;
                    }

                    ws.Cells[yearRow, formulaCol].Formula = formula; // "=RC[-3]+(RC[-2]*R3C1)+(RC[-1]*R3C2)";
                    ws.Cells[yearRow, formulaCol].NumberFormat = "#,##0";
                    ws.Cells[yearRow, formulaCol].FillColor = Color.Orange;

                    #endregion

                    #region Borçlar


                    formulaCol++;
                    int headerCol = formulaCol +1;
                    foreach (var curr in currencies)
                    {
                        // BORÇLAR
                        ws.Cells[yearRow, formulaCol].Formula = $"=SUMIFS(Detay!C7;Detay!C1;RC1;Detay!C2;RC2;Detay!C5;R4C{headerCol};Detay!C6;R5C)";
                        ws.Cells[yearRow, formulaCol].NumberFormat = "#,##0";
                        ws.Cells[yearRow, formulaCol].FillColor = Color.LightPink;
                        formulaCol++;
                    }


                    //TOPLAM
                    formula = $"=RC[-{currencyCount}]";
                    currencyColumn = 0;
                    j = 1;
                    foreach (var totalItem in currencies)
                    {
                        if (totalItem.Key.CURRENCYTYPE == 0)
                        {
                            continue;
                        }

                        formula += $"+(RC[-{currencyCount - j}]*R3C{j})";
                        j++;
                    }

                    ws.Cells[yearRow, formulaCol].Formula = formula; //"=RC[-3]+(RC[-2]*R3C1)+(RC[-1]*R3C2)";
                    ws.Cells[yearRow, formulaCol].NumberFormat = "#,##0";
                    ws.Cells[yearRow, formulaCol].FillColor = Color.Orange;

                    #endregion

                    #region Fark

                    formulaCol++;
                    int debitFirstColumn = 2 + (currencyCount * 2);
                    foreach (var curr in currencies)
                    {
                        // BORÇLAR
                        ws.Cells[yearRow, formulaCol].Formula = $"=RC[-{debitFirstColumn}]-RC[-{debitFirstColumn / 2}]";
                        ws.Cells[yearRow, formulaCol].NumberFormat = "#,##0";
                        ws.Cells[yearRow, formulaCol].FillColor = Color.LightSkyBlue;
                        formulaCol++;
                    }

                    formula = $"=RC[-{currencyCount}]";
                    currencyColumn = 0;
                    j = 1;
                    foreach (var totalItem in currencies)
                    {
                        if (totalItem.Key.CURRENCYTYPE == 0)
                        {
                            continue;
                        }

                        formula += $"+(RC[-{currencyCount - j}]*R3C{j})";
                        j++;
                    }

                    ws.Cells[yearRow, formulaCol].Formula = formula; //"=RC[-12]+RC[-3]+(RC[-2]*R3C1)+(RC[-1]*R3C2)";
                    ws.Cells[yearRow, formulaCol].NumberFormat = "#,##0";
                    ws.Cells[yearRow, formulaCol].FillColor = Color.Orange;

                    #endregion

                    yearRow++;
                }
            }

            #endregion


            BuiltInStyleId detailHeaderStyle = BuiltInStyleId.Accent1;
            BuiltInTableStyleId detailStyle = BuiltInTableStyleId.TableStyleLight2;


            Table tableDetail = wsDetail.Tables.Add(wsDetail[$"A{1}:J{detailData.Count() + 1}"], true); // [$"A{i}:N{dataCount}"], true);
            tableDetail.Name = $"DetayListesi";
            tableDetail.ShowTotals = false;
            //tableDetail.AutoFilter.Disable();
            tableDetail.Style = workbook.TableStyles[detailStyle];

            #region Set Column Name

            tableDetail.Columns[0].Name = $"Yıl";
            tableDetail.Columns[1].Name = "Ay";
            tableDetail.Columns[2].Name = "Modül";
            tableDetail.Columns[3].Name = "Detay";
            tableDetail.Columns[4].Name = "Tür";
            tableDetail.Columns[5].Name = "Döviz";

            tableDetail.Columns[6].Name = "Toplam";
            tableDetail.Columns[6].Range.NumberFormat = "#,##0.00";

            tableDetail.Columns[7].Name = "İşlem Yılı";
            tableDetail.Columns[8].Name = "İşlem Ayı";
            tableDetail.Columns[9].Name = "Tur Int";

            //tableDetail.Columns[10].Name = "TL FARK";
            //tableDetail.Columns[11].Name = "USD FARK";
            //tableDetail.Columns[12].Name = "EURO FARK";
            //tableDetail.Columns[13].Name = "FARK TOPLAM TL";

            #endregion

            wsDetail.DataBindings.Clear();
            var options = new ExternalDataSourceOptions { ImportHeaders = false };
            var dataBinding = wsDetail.DataBindings.BindToDataSource(detailData, wsDetail.Tables[0].DataRange, options);

            #endregion

            #region Budget

            leftIndex = 0;
            int rowIndex = 0;
            bottomIndex = 0;
            int budgetHeaderLenght = 2 + currencyCount -1;

            #region General Header

            CellRange budgetRange = wsBudget.Range.FromLTRB(leftIndex, rowIndex, budgetHeaderLenght, bottomIndex);
            budgetRange.FillColor = Color.LightGreen;
            budgetRange.Merge(MergeCellsMode.IgnoreIntersections);
            budgetRange.Value = "VARLIK BÜTÇELEME";
            budgetRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            budgetRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            budgetRange.Font.Bold = true;

            leftIndex = budgetHeaderLenght + 1;
            budgetHeaderLenght = ((2 + currencyCount) * 5) - 1;
            budgetRange = wsBudget.Range.FromLTRB(leftIndex, rowIndex, budgetHeaderLenght, bottomIndex);
            budgetRange.FillColor = Color.LightBlue;
            budgetRange.Merge(MergeCellsMode.IgnoreIntersections);
            budgetRange.Value = "BORÇ BÜTÇELEME";
            budgetRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            budgetRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            budgetRange.Font.Bold = true;
            #endregion

            #region Group Header
            string[] groupHeders = new string[] { "Satış Bütçesi", "Satınalma Bütçesi", "Gider Bütçesi", "Vergi Bütçesi", "Yatırım Bütçesi" };

            leftIndex = 0;
            rowIndex = 0;
            bottomIndex = 0;
            budgetHeaderLenght = 2 + currencyCount -1;
            int colorNr = 1;
            for (int i1 = 0; i1 < groupHeders.Length; i1++)
            {
                string item = groupHeders[i1];
                budgetRange = wsBudget.Range.FromLTRB(leftIndex, 1, budgetHeaderLenght , 1);
                budgetRange.FillColor = colorNr == 1 ? Color.LightGreen : colorNr == 2 ? Color.LightBlue : colorNr == 3 ? Color.LightPink: colorNr == 4 ? Color.LightGray  : colorNr == 5 ? Color.LightYellow : Color.LightGreen;
                budgetRange.Merge(MergeCellsMode.IgnoreIntersections);
                budgetRange.Value = item;
                budgetRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                budgetRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                budgetRange.Font.Bold = true;

                colorNr++;
                leftIndex += budgetHeaderLenght * i1;
            }

            #endregion
            
            #endregion

            #region Budget Old
            /*
            #region Sales Budget

            int budgetHeader = 1 + currencyCount;

            CellRange budgetRange = wsBudget.Range.FromLTRB(0, 0, budgetHeader, 0);
            budgetRange.FillColor = Color.LightGreen;
            budgetRange.Merge(MergeCellsMode.IgnoreIntersections);
            budgetRange.Value = "VARLIK BÜTÇELEME";
            budgetRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            budgetRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            budgetRange.Font.Bold = true;

            budgetRange = wsBudget.Range.FromLTRB(0, 1, budgetHeader, 1);
            budgetRange.FillColor = Color.LightGreen;
            budgetRange.Merge(MergeCellsMode.IgnoreIntersections);
            budgetRange.Value = "Satış Bütçesi";
            budgetRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            budgetRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            budgetRange.Font.Bold = true;

            #region Period

            Cell cellHeader = wsBudget.Cells[2, 0];
            cellHeader.Value = "Yıl";
            cellHeader.Font.Bold = true;
            cellHeader.FillColor = Color.LightGreen;

            cellHeader = wsBudget.Cells[2, 1];
            cellHeader.Value = "Ay";
            cellHeader.Font.Bold = true;
            cellHeader.FillColor = Color.LightGreen;

            colNr = 2;
            foreach (var item in currencies)
            {
                Cell cell = wsBudget.Cells[2, colNr];
                cell.Value = item.Key.CURRENCY;
                cell.Font.Bold = true;
                cell.FillColor = Color.LightGreen;
                colNr++;
            }

            DateTime date = DateTime.Now;
            int year = date.Year;
            int month = date.Month;
            int budgetPeriodRow = 3;

            for (int k = 1; k < 13; k++)
            {
                year = date.Year;
                month = date.Month;
                Cell cell = wsBudget.Cells[budgetPeriodRow, 0];
                cell.Value = year;
                cell.Font.Bold = true;
                
                cell = wsBudget.Cells[budgetPeriodRow, 1];
                cell.Value = month;
                cell.Font.Bold = true;

                date = date.AddMonths(1);
                budgetPeriodRow++;
            }



            #endregion

            #endregion

            #region Purchase Budget
            int leftColumnIndex = 2+currencyCount;
            budgetHeader = ((2 + currencyCount)*5)-1;
            budgetRange = wsBudget.Range.FromLTRB(leftColumnIndex, 0, budgetHeader, 0);
            budgetRange.FillColor = Color.LightBlue;
            budgetRange.Merge(MergeCellsMode.IgnoreIntersections);
            budgetRange.Value = "BORÇ BÜTÇELEME";
            budgetRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            budgetRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            budgetRange.Font.Bold = true;
            
            // Purchase
            budgetHeader = (1 + currencyCount);
            budgetRange = wsBudget.Range.FromLTRB(leftColumnIndex, 1, leftColumnIndex+ budgetHeader, 1);
            budgetRange.FillColor = Color.LightBlue;
            budgetRange.Merge(MergeCellsMode.IgnoreIntersections);
            budgetRange.Value = "Satınalma Bütçesi";
            budgetRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            budgetRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            budgetRange.Font.Bold = true;

            #region Purchase Period

            cellHeader = wsBudget.Cells[2, budgetHeader + 1];
            cellHeader.Value = "Yıl";
            cellHeader.Font.Bold = true;
            cellHeader.FillColor = Color.LightBlue;

            cellHeader = wsBudget.Cells[2, budgetHeader + 2];
            cellHeader.Value = "Ay";
            cellHeader.Font.Bold = true;
            cellHeader.FillColor = Color.LightBlue;

            colNr = 2+2+currencyCount;
            foreach (var item in currencies)
            {
                Cell cell = wsBudget.Cells[2, colNr];
                cell.Value = item.Key.CURRENCY;
                cell.Font.Bold = true;
                cell.FillColor = Color.LightBlue;
                colNr++;
            }

            date = DateTime.Now;
            year = date.Year;
            month = date.Month;
            budgetPeriodRow = 3;

            for (int k = 1; k < 13; k++)
            {
                year = date.Year;
                month = date.Month;
                Cell cell = wsBudget.Cells[budgetPeriodRow, budgetHeader + 1];
                cell.Value = year;
                cell.Font.Bold = true;

                cell = wsBudget.Cells[budgetPeriodRow, budgetHeader + 2];
                cell.Value = month;
                cell.Font.Bold = true;

                date = date.AddMonths(1);
                budgetPeriodRow++;
            }



            #endregion



            #endregion

            #region Expense
            // Expense
            leftColumnIndex = (2 + currencyCount) * 2;
            budgetHeader = (1 + currencyCount);
            budgetRange = wsBudget.Range.FromLTRB(leftColumnIndex, 1, leftColumnIndex + budgetHeader, 1);
            budgetRange.FillColor = Color.LightPink;
            budgetRange.Merge(MergeCellsMode.IgnoreIntersections);
            budgetRange.Value = "Gider Bütçesi";
            budgetRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            budgetRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            budgetRange.Font.Bold = true;

            #endregion

            #region Expense Period

            cellHeader = wsBudget.Cells[2, leftColumnIndex];
            cellHeader.Value = "Yıl";
            cellHeader.Font.Bold = true;
            cellHeader.FillColor = Color.LightPink;

            cellHeader = wsBudget.Cells[2, leftColumnIndex + 1];
            cellHeader.Value = "Ay";
            cellHeader.Font.Bold = true;
            cellHeader.FillColor = Color.LightPink;

            colNr = leftColumnIndex +2;
            foreach (var item in currencies)
            {
                Cell cell = wsBudget.Cells[2, colNr];
                cell.Value = item.Key.CURRENCY;
                cell.Font.Bold = true;
                cell.FillColor = Color.LightPink;
                colNr++;
            }

            date = DateTime.Now;
            year = date.Year;
            month = date.Month;
            budgetPeriodRow = 3;

            for (int k = 1; k < 13; k++)
            {
                year = date.Year;
                month = date.Month;
                Cell cell = wsBudget.Cells[budgetPeriodRow, leftColumnIndex];
                cell.Value = year;
                cell.Font.Bold = true;

                cell = wsBudget.Cells[budgetPeriodRow, leftColumnIndex +1];
                cell.Value = month;
                cell.Font.Bold = true;

                date = date.AddMonths(1);
                budgetPeriodRow++;
            }



            #endregion

            */
            #endregion

            var allCell1 = ws.GetUsedRange();
            allCell1.Alignment.WrapText = true;
            var allCellDetail = wsDetail.GetUsedRange();
            allCellDetail.Alignment.WrapText = false;

            var oRng = wsDetail.GetUsedRange(); // .Columns[iColPos];
            oRng.AutoFitColumns();

            var oRng1 = ws.GetUsedRange();
            oRng1.AutoFitColumns();

            workbook.Worksheets.ActiveWorksheet = workbook.Worksheets[0]; // ["Sheet2"];
            
            spreadsheetControl1.EndUpdate();
        }


        #endregion
    }
}