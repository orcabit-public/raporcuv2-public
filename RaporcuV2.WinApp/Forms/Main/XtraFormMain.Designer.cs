﻿namespace RaporcuV2.WinApp.Forms.Main
{
    partial class XtraFormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraFormMain));
            this.barManagerMain = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItemMuhasebe = new DevExpress.XtraBars.BarSubItem();
            this.barBtnCariMuhKontrol = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnMizan = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnKdvRaporu = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnBaBs = new DevExpress.XtraBars.BarButtonItem();
            this.btnMaliTablolar = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemCariHesap = new DevExpress.XtraBars.BarSubItem();
            this.barBtnAcikBakiye = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnRiskBakiyeKontrol = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnCariBorcAlacakRaporu = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnHesapIslemleri = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnCariVadeRaporu = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCariCekRaporu = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCariSiparisliBakiye = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCariSiparisListesi = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSiparisliCariHesapEkstresi = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemBanka = new DevExpress.XtraBars.BarSubItem();
            this.barBtnBankaIslemleri = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnBankaBakiye = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnKrediKarti = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnKrediKartiCiroDetay = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemStok = new DevExpress.XtraBars.BarSubItem();
            this.barBtnStokHareketleri = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnSiparisUretimSatis = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnDonemselStokAlisSatis = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnAlisSatisFiyatAnaliz = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnAmbarlarDurumRaporu = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnGunlukSatis = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnSeriTakip = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonSevkiyatPlani = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonIrsaliyeDetay = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemKasa = new DevExpress.XtraBars.BarSubItem();
            this.barBtnKasa = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnAtaKasa = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnOdemePlani = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnOdemePlaniDetay = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnNakitAkis = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemNakitAkisDetay = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemCekSenet = new DevExpress.XtraBars.BarSubItem();
            this.barBtnCekSenet = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnCekSenetAyrinti = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnCekToplam = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemFatura = new DevExpress.XtraBars.BarSubItem();
            this.barBtnFatura = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnFaturaDetay = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnFaturaKarliligi = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnFaturaVade = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnSatisElemaniRaporu = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemHizmet = new DevExpress.XtraBars.BarSubItem();
            this.barBtnHizmetHareketleri = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonHizmetToplamlari = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAtaKarlilikRaporu = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemCustomReport = new DevExpress.XtraBars.BarSubItem();
            this.barSubItemSettings = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItemConnectionString = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemKullaniciListesi = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCustomReportList = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDashboard = new DevExpress.XtraBars.BarButtonItem();
            this.skinBarSubItemTheme = new DevExpress.XtraBars.SkinBarSubItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barStaticItemUserName = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemClock = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barBtnCariMuhBakiye = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnGenelDurum = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.skinBarSubItem1 = new DevExpress.XtraBars.SkinBarSubItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnBorcAlacakToplamlari = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnOdemeAlacakTakip = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnAlisSatisOrani = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnAlisSatisCariyeOrani = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnCariHesapHareketSayisi = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCustomReportTest = new DevExpress.XtraBars.BarButtonItem();
            this.xtraTabbedMdiManagerMenu = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.defaultLookAndFeelMain = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.timerClock = new System.Windows.Forms.Timer(this.components);
            this.timerRefreshSession = new System.Windows.Forms.Timer(this.components);
            this.barButtonItemFaturaDetay2 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManagerMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // barManagerMain
            // 
            this.barManagerMain.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar1});
            this.barManagerMain.DockControls.Add(this.barDockControlTop);
            this.barManagerMain.DockControls.Add(this.barDockControlBottom);
            this.barManagerMain.DockControls.Add(this.barDockControlLeft);
            this.barManagerMain.DockControls.Add(this.barDockControlRight);
            this.barManagerMain.Form = this;
            this.barManagerMain.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItemMuhasebe,
            this.barBtnCariMuhKontrol,
            this.barSubItemSettings,
            this.barButtonItemConnectionString,
            this.skinBarSubItemTheme,
            this.barBtnCariMuhBakiye,
            this.barBtnMizan,
            this.barBtnGenelDurum,
            this.barButtonItem1,
            this.skinBarSubItem1,
            this.barSubItemCariHesap,
            this.barButtonItem2,
            this.barBtnCariBorcAlacakRaporu,
            this.barBtnBorcAlacakToplamlari,
            this.barSubItemCekSenet,
            this.barBtnCekSenet,
            this.barBtnRiskBakiyeKontrol,
            this.barSubItemFatura,
            this.barBtnFatura,
            this.barBtnKdvRaporu,
            this.barBtnBaBs,
            this.barBtnAcikBakiye,
            this.barBtnOdemeAlacakTakip,
            this.barBtnAlisSatisOrani,
            this.barBtnAlisSatisCariyeOrani,
            this.barBtnHesapIslemleri,
            this.barBtnCariHesapHareketSayisi,
            this.barSubItemBanka,
            this.barBtnBankaIslemleri,
            this.barBtnBankaBakiye,
            this.barBtnKrediKarti,
            this.barBtnKrediKartiCiroDetay,
            this.barSubItemStok,
            this.barBtnStokHareketleri,
            this.barBtnSiparisUretimSatis,
            this.barBtnDonemselStokAlisSatis,
            this.barBtnAlisSatisFiyatAnaliz,
            this.barBtnAmbarlarDurumRaporu,
            this.barBtnGunlukSatis,
            this.barBtnSeriTakip,
            this.barBtnFaturaKarliligi,
            this.barBtnFaturaVade,
            this.barBtnCekSenetAyrinti,
            this.barBtnCekToplam,
            this.barSubItemKasa,
            this.barBtnKasa,
            this.barSubItemHizmet,
            this.barBtnHizmetHareketleri,
            this.barBtnSatisElemaniRaporu,
            this.barButtonHizmetToplamlari,
            this.barButtonItem3,
            this.barButtonItemKullaniciListesi,
            this.barSubItemCustomReport,
            this.barButtonItemCustomReportTest,
            this.barButtonItemCustomReportList,
            this.barBtnCariVadeRaporu,
            this.barBtnAtaKasa,
            this.barStaticItemUserName,
            this.barStaticItemClock,
            this.barBtnOdemePlani,
            this.barButtonItemCariCekRaporu,
            this.barBtnOdemePlaniDetay,
            this.barButtonItemAtaKarlilikRaporu,
            this.btnMaliTablolar,
            this.barButtonItemDashboard,
            this.barBtnFaturaDetay,
            this.barButtonItemCariSiparisliBakiye,
            this.barButtonItemCariSiparisListesi,
            this.barButtonItemSiparisliCariHesapEkstresi,
            this.barButtonSevkiyatPlani,
            this.barButtonIrsaliyeDetay,
            this.barBtnNakitAkis,
            this.barButtonItemNakitAkisDetay,
            this.barButtonItemFaturaDetay2});
            this.barManagerMain.MainMenu = this.bar2;
            this.barManagerMain.MaxItemId = 85;
            this.barManagerMain.MdiMenuMergeStyle = DevExpress.XtraBars.BarMdiMenuMergeStyle.Never;
            this.barManagerMain.StatusBar = this.bar1;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItemMuhasebe, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItemCariHesap, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItemBanka, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItemStok, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItemKasa, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItemCekSenet, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItemFatura, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItemHizmet, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItemCustomReport, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItemSettings, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.skinBarSubItemTheme, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barSubItemMuhasebe
            // 
            this.barSubItemMuhasebe.Caption = "Muhasebe";
            this.barSubItemMuhasebe.Id = 0;
            this.barSubItemMuhasebe.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItemMuhasebe.ImageOptions.Image")));
            this.barSubItemMuhasebe.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barBtnCariMuhKontrol, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnMizan),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnKdvRaporu),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnBaBs),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnMaliTablolar)});
            this.barSubItemMuhasebe.Name = "barSubItemMuhasebe";
            // 
            // barBtnCariMuhKontrol
            // 
            this.barBtnCariMuhKontrol.Caption = "Cari Muhasebe Kontrol";
            this.barBtnCariMuhKontrol.Id = 1;
            this.barBtnCariMuhKontrol.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnCariMuhKontrol.ImageOptions.Image")));
            this.barBtnCariMuhKontrol.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnCariMuhKontrol.ImageOptions.LargeImage")));
            this.barBtnCariMuhKontrol.Name = "barBtnCariMuhKontrol";
            this.barBtnCariMuhKontrol.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barBtnMizan
            // 
            this.barBtnMizan.Caption = "Mizan";
            this.barBtnMizan.Id = 6;
            this.barBtnMizan.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnMizan.ImageOptions.Image")));
            this.barBtnMizan.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnMizan.ImageOptions.LargeImage")));
            this.barBtnMizan.Name = "barBtnMizan";
            this.barBtnMizan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnMizan_ItemClick);
            // 
            // barBtnKdvRaporu
            // 
            this.barBtnKdvRaporu.Caption = "Kdv Raporu";
            this.barBtnKdvRaporu.Id = 22;
            this.barBtnKdvRaporu.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnKdvRaporu.ImageOptions.Image")));
            this.barBtnKdvRaporu.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnKdvRaporu.ImageOptions.LargeImage")));
            this.barBtnKdvRaporu.Name = "barBtnKdvRaporu";
            this.barBtnKdvRaporu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnKdvRaporu_ItemClick);
            // 
            // barBtnBaBs
            // 
            this.barBtnBaBs.Caption = "Ba-Bs";
            this.barBtnBaBs.Id = 23;
            this.barBtnBaBs.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnBaBs.ImageOptions.Image")));
            this.barBtnBaBs.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnBaBs.ImageOptions.LargeImage")));
            this.barBtnBaBs.Name = "barBtnBaBs";
            this.barBtnBaBs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnBaBs_ItemClick);
            // 
            // btnMaliTablolar
            // 
            this.btnMaliTablolar.Caption = "Mali Tablolar";
            this.btnMaliTablolar.Id = 72;
            this.btnMaliTablolar.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnMaliTablolar.ImageOptions.LargeImage")));
            this.btnMaliTablolar.Name = "btnMaliTablolar";
            this.btnMaliTablolar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnMaliTablolar_ItemClick);
            // 
            // barSubItemCariHesap
            // 
            this.barSubItemCariHesap.Caption = "Cari Hesap";
            this.barSubItemCariHesap.Id = 11;
            this.barSubItemCariHesap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItemCariHesap.ImageOptions.Image")));
            this.barSubItemCariHesap.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnAcikBakiye),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnRiskBakiyeKontrol),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barBtnCariBorcAlacakRaporu, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnHesapIslemleri),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnCariVadeRaporu),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemCariCekRaporu),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemCariSiparisliBakiye),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemCariSiparisListesi),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemSiparisliCariHesapEkstresi)});
            this.barSubItemCariHesap.Name = "barSubItemCariHesap";
            // 
            // barBtnAcikBakiye
            // 
            this.barBtnAcikBakiye.Caption = "Açık Bakiye";
            this.barBtnAcikBakiye.Id = 24;
            this.barBtnAcikBakiye.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnAcikBakiye.ImageOptions.Image")));
            this.barBtnAcikBakiye.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnAcikBakiye.ImageOptions.LargeImage")));
            this.barBtnAcikBakiye.Name = "barBtnAcikBakiye";
            this.barBtnAcikBakiye.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnAcikBakiye_ItemClick);
            // 
            // barBtnRiskBakiyeKontrol
            // 
            this.barBtnRiskBakiyeKontrol.Caption = "Risk-Bakiye Kontrol";
            this.barBtnRiskBakiyeKontrol.Id = 19;
            this.barBtnRiskBakiyeKontrol.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnRiskBakiyeKontrol.ImageOptions.Image")));
            this.barBtnRiskBakiyeKontrol.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnRiskBakiyeKontrol.ImageOptions.LargeImage")));
            this.barBtnRiskBakiyeKontrol.Name = "barBtnRiskBakiyeKontrol";
            this.barBtnRiskBakiyeKontrol.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnRiskBakiyeKontrol_ItemClick);
            // 
            // barBtnCariBorcAlacakRaporu
            // 
            this.barBtnCariBorcAlacakRaporu.Caption = "Cari Borç Alacak Raporu";
            this.barBtnCariBorcAlacakRaporu.Id = 15;
            this.barBtnCariBorcAlacakRaporu.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnCariBorcAlacakRaporu.ImageOptions.Image")));
            this.barBtnCariBorcAlacakRaporu.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnCariBorcAlacakRaporu.ImageOptions.LargeImage")));
            this.barBtnCariBorcAlacakRaporu.Name = "barBtnCariBorcAlacakRaporu";
            this.barBtnCariBorcAlacakRaporu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnCariBorcAlacakRaporu_ItemClick);
            // 
            // barBtnHesapIslemleri
            // 
            this.barBtnHesapIslemleri.Caption = "Hesap İşlemleri";
            this.barBtnHesapIslemleri.Id = 29;
            this.barBtnHesapIslemleri.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnHesapIslemleri.ImageOptions.Image")));
            this.barBtnHesapIslemleri.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnHesapIslemleri.ImageOptions.LargeImage")));
            this.barBtnHesapIslemleri.Name = "barBtnHesapIslemleri";
            this.barBtnHesapIslemleri.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnHesapIslemleri_ItemClick);
            // 
            // barBtnCariVadeRaporu
            // 
            this.barBtnCariVadeRaporu.Caption = "Cari Vade Raporu";
            this.barBtnCariVadeRaporu.Id = 60;
            this.barBtnCariVadeRaporu.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnCariVadeRaporu.ImageOptions.Image")));
            this.barBtnCariVadeRaporu.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnCariVadeRaporu.ImageOptions.LargeImage")));
            this.barBtnCariVadeRaporu.Name = "barBtnCariVadeRaporu";
            this.barBtnCariVadeRaporu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnCariVadeRaporu_ItemClick);
            // 
            // barButtonItemCariCekRaporu
            // 
            this.barButtonItemCariCekRaporu.Caption = "Cari Çek Raporu";
            this.barButtonItemCariCekRaporu.Id = 65;
            this.barButtonItemCariCekRaporu.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemCariCekRaporu.ImageOptions.Image")));
            this.barButtonItemCariCekRaporu.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemCariCekRaporu.ImageOptions.LargeImage")));
            this.barButtonItemCariCekRaporu.Name = "barButtonItemCariCekRaporu";
            this.barButtonItemCariCekRaporu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCariCekRaporu_ItemClick);
            // 
            // barButtonItemCariSiparisliBakiye
            // 
            this.barButtonItemCariSiparisliBakiye.Caption = "Siparişli Bakiye Raporu";
            this.barButtonItemCariSiparisliBakiye.Id = 77;
            this.barButtonItemCariSiparisliBakiye.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemCariSiparisliBakiye.ImageOptions.Image")));
            this.barButtonItemCariSiparisliBakiye.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemCariSiparisliBakiye.ImageOptions.LargeImage")));
            this.barButtonItemCariSiparisliBakiye.Name = "barButtonItemCariSiparisliBakiye";
            this.barButtonItemCariSiparisliBakiye.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCariSiparisliBakiye_ItemClick);
            // 
            // barButtonItemCariSiparisListesi
            // 
            this.barButtonItemCariSiparisListesi.Caption = "Cari Sipariş Listesi";
            this.barButtonItemCariSiparisListesi.Id = 78;
            this.barButtonItemCariSiparisListesi.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemCariSiparisListesi.ImageOptions.Image")));
            this.barButtonItemCariSiparisListesi.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemCariSiparisListesi.ImageOptions.LargeImage")));
            this.barButtonItemCariSiparisListesi.Name = "barButtonItemCariSiparisListesi";
            this.barButtonItemCariSiparisListesi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCariSiparisListesi_ItemClick);
            // 
            // barButtonItemSiparisliCariHesapEkstresi
            // 
            this.barButtonItemSiparisliCariHesapEkstresi.Caption = "Siparişli Cari Hesap Ekstresi";
            this.barButtonItemSiparisliCariHesapEkstresi.Id = 79;
            this.barButtonItemSiparisliCariHesapEkstresi.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemSiparisliCariHesapEkstresi.ImageOptions.Image")));
            this.barButtonItemSiparisliCariHesapEkstresi.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemSiparisliCariHesapEkstresi.ImageOptions.LargeImage")));
            this.barButtonItemSiparisliCariHesapEkstresi.Name = "barButtonItemSiparisliCariHesapEkstresi";
            this.barButtonItemSiparisliCariHesapEkstresi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSiparisliCariHesapEkstresi_ItemClick);
            // 
            // barSubItemBanka
            // 
            this.barSubItemBanka.Caption = "Banka";
            this.barSubItemBanka.Id = 31;
            this.barSubItemBanka.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItemBanka.ImageOptions.Image")));
            this.barSubItemBanka.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnBankaIslemleri),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnBankaBakiye),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, this.barBtnKrediKarti, false),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnKrediKartiCiroDetay)});
            this.barSubItemBanka.Name = "barSubItemBanka";
            // 
            // barBtnBankaIslemleri
            // 
            this.barBtnBankaIslemleri.Caption = "Banka İşlemleri";
            this.barBtnBankaIslemleri.Id = 32;
            this.barBtnBankaIslemleri.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnBankaIslemleri.ImageOptions.Image")));
            this.barBtnBankaIslemleri.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnBankaIslemleri.ImageOptions.LargeImage")));
            this.barBtnBankaIslemleri.Name = "barBtnBankaIslemleri";
            this.barBtnBankaIslemleri.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnBankaIslemleri_ItemClick);
            // 
            // barBtnBankaBakiye
            // 
            this.barBtnBankaBakiye.Caption = "Banka Bakiye";
            this.barBtnBankaBakiye.Id = 33;
            this.barBtnBankaBakiye.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnBankaBakiye.ImageOptions.Image")));
            this.barBtnBankaBakiye.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnBankaBakiye.ImageOptions.LargeImage")));
            this.barBtnBankaBakiye.Name = "barBtnBankaBakiye";
            this.barBtnBankaBakiye.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnBankaBakiye_ItemClick);
            // 
            // barBtnKrediKarti
            // 
            this.barBtnKrediKarti.Caption = "Kredi Kartı";
            this.barBtnKrediKarti.Id = 34;
            this.barBtnKrediKarti.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnKrediKarti.ImageOptions.Image")));
            this.barBtnKrediKarti.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnKrediKarti.ImageOptions.LargeImage")));
            this.barBtnKrediKarti.Name = "barBtnKrediKarti";
            // 
            // barBtnKrediKartiCiroDetay
            // 
            this.barBtnKrediKartiCiroDetay.Caption = "Kredi Kartı Ciro Detay";
            this.barBtnKrediKartiCiroDetay.Id = 35;
            this.barBtnKrediKartiCiroDetay.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnKrediKartiCiroDetay.ImageOptions.Image")));
            this.barBtnKrediKartiCiroDetay.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnKrediKartiCiroDetay.ImageOptions.LargeImage")));
            this.barBtnKrediKartiCiroDetay.Name = "barBtnKrediKartiCiroDetay";
            this.barBtnKrediKartiCiroDetay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnKrediKartiCiroDetay_ItemClick);
            // 
            // barSubItemStok
            // 
            this.barSubItemStok.Caption = "Stok";
            this.barSubItemStok.Id = 36;
            this.barSubItemStok.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItemStok.ImageOptions.Image")));
            this.barSubItemStok.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnStokHareketleri),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnSiparisUretimSatis),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnDonemselStokAlisSatis),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnAlisSatisFiyatAnaliz),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnAmbarlarDurumRaporu),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnGunlukSatis),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnSeriTakip),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonSevkiyatPlani, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonIrsaliyeDetay, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barSubItemStok.Name = "barSubItemStok";
            // 
            // barBtnStokHareketleri
            // 
            this.barBtnStokHareketleri.Caption = "Stok Hareketleri";
            this.barBtnStokHareketleri.Id = 37;
            this.barBtnStokHareketleri.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnStokHareketleri.ImageOptions.Image")));
            this.barBtnStokHareketleri.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnStokHareketleri.ImageOptions.LargeImage")));
            this.barBtnStokHareketleri.Name = "barBtnStokHareketleri";
            this.barBtnStokHareketleri.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnStokHareketleri_ItemClick);
            // 
            // barBtnSiparisUretimSatis
            // 
            this.barBtnSiparisUretimSatis.Caption = "Sipariş - Üretim - Satış";
            this.barBtnSiparisUretimSatis.Id = 38;
            this.barBtnSiparisUretimSatis.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnSiparisUretimSatis.ImageOptions.Image")));
            this.barBtnSiparisUretimSatis.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnSiparisUretimSatis.ImageOptions.LargeImage")));
            this.barBtnSiparisUretimSatis.Name = "barBtnSiparisUretimSatis";
            this.barBtnSiparisUretimSatis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnSiparisUretimSatis_ItemClick);
            // 
            // barBtnDonemselStokAlisSatis
            // 
            this.barBtnDonemselStokAlisSatis.Caption = "Dönemsel Stok Alış-Satış";
            this.barBtnDonemselStokAlisSatis.Id = 39;
            this.barBtnDonemselStokAlisSatis.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnDonemselStokAlisSatis.ImageOptions.Image")));
            this.barBtnDonemselStokAlisSatis.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnDonemselStokAlisSatis.ImageOptions.LargeImage")));
            this.barBtnDonemselStokAlisSatis.Name = "barBtnDonemselStokAlisSatis";
            this.barBtnDonemselStokAlisSatis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnDonemselStokAlisSatis_ItemClick);
            // 
            // barBtnAlisSatisFiyatAnaliz
            // 
            this.barBtnAlisSatisFiyatAnaliz.Caption = "Stok Alış Satış Fiyat Analiz";
            this.barBtnAlisSatisFiyatAnaliz.Id = 40;
            this.barBtnAlisSatisFiyatAnaliz.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnAlisSatisFiyatAnaliz.ImageOptions.Image")));
            this.barBtnAlisSatisFiyatAnaliz.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnAlisSatisFiyatAnaliz.ImageOptions.LargeImage")));
            this.barBtnAlisSatisFiyatAnaliz.Name = "barBtnAlisSatisFiyatAnaliz";
            this.barBtnAlisSatisFiyatAnaliz.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnAlisSatisFiyatAnaliz_ItemClick);
            // 
            // barBtnAmbarlarDurumRaporu
            // 
            this.barBtnAmbarlarDurumRaporu.Caption = "Ambarlar Durum Raporu";
            this.barBtnAmbarlarDurumRaporu.Id = 41;
            this.barBtnAmbarlarDurumRaporu.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnAmbarlarDurumRaporu.ImageOptions.Image")));
            this.barBtnAmbarlarDurumRaporu.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnAmbarlarDurumRaporu.ImageOptions.LargeImage")));
            this.barBtnAmbarlarDurumRaporu.Name = "barBtnAmbarlarDurumRaporu";
            this.barBtnAmbarlarDurumRaporu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnAmbarlarDurumRaporu_ItemClick);
            // 
            // barBtnGunlukSatis
            // 
            this.barBtnGunlukSatis.Caption = "Günlük Satış";
            this.barBtnGunlukSatis.Id = 42;
            this.barBtnGunlukSatis.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnGunlukSatis.ImageOptions.Image")));
            this.barBtnGunlukSatis.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnGunlukSatis.ImageOptions.LargeImage")));
            this.barBtnGunlukSatis.Name = "barBtnGunlukSatis";
            this.barBtnGunlukSatis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnGunlukSatis_ItemClick);
            // 
            // barBtnSeriTakip
            // 
            this.barBtnSeriTakip.Caption = "Seri Takip";
            this.barBtnSeriTakip.Id = 43;
            this.barBtnSeriTakip.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnSeriTakip.ImageOptions.Image")));
            this.barBtnSeriTakip.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnSeriTakip.ImageOptions.LargeImage")));
            this.barBtnSeriTakip.Name = "barBtnSeriTakip";
            this.barBtnSeriTakip.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnSeriTakip_ItemClick);
            // 
            // barButtonSevkiyatPlani
            // 
            this.barButtonSevkiyatPlani.Caption = "Sevkiyat Planı";
            this.barButtonSevkiyatPlani.Id = 80;
            this.barButtonSevkiyatPlani.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonSevkiyatPlani.ImageOptions.Image")));
            this.barButtonSevkiyatPlani.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonSevkiyatPlani.ImageOptions.LargeImage")));
            this.barButtonSevkiyatPlani.Name = "barButtonSevkiyatPlani";
            this.barButtonSevkiyatPlani.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonSevkiyatPlani_ItemClick);
            // 
            // barButtonIrsaliyeDetay
            // 
            this.barButtonIrsaliyeDetay.Caption = "İrsaliye Detay";
            this.barButtonIrsaliyeDetay.Id = 81;
            this.barButtonIrsaliyeDetay.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonIrsaliyeDetay.ImageOptions.Image")));
            this.barButtonIrsaliyeDetay.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonIrsaliyeDetay.ImageOptions.LargeImage")));
            this.barButtonIrsaliyeDetay.Name = "barButtonIrsaliyeDetay";
            this.barButtonIrsaliyeDetay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonIrsaliyeDetay_ItemClick);
            // 
            // barSubItemKasa
            // 
            this.barSubItemKasa.Caption = "Kasa";
            this.barSubItemKasa.Id = 48;
            this.barSubItemKasa.ImageOptions.Image = global::RaporcuV2.WinApp.Properties.Resources.safe_icon;
            this.barSubItemKasa.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnKasa),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnAtaKasa),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnOdemePlani),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnOdemePlaniDetay),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barBtnNakitAkis, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemNakitAkisDetay)});
            this.barSubItemKasa.Name = "barSubItemKasa";
            // 
            // barBtnKasa
            // 
            this.barBtnKasa.Caption = "Kasa";
            this.barBtnKasa.Id = 49;
            this.barBtnKasa.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnKasa.ImageOptions.Image")));
            this.barBtnKasa.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnKasa.ImageOptions.LargeImage")));
            this.barBtnKasa.Name = "barBtnKasa";
            this.barBtnKasa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnKasa_ItemClick);
            // 
            // barBtnAtaKasa
            // 
            this.barBtnAtaKasa.Caption = "Kasalar";
            this.barBtnAtaKasa.Id = 61;
            this.barBtnAtaKasa.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnAtaKasa.ImageOptions.Image")));
            this.barBtnAtaKasa.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnAtaKasa.ImageOptions.LargeImage")));
            this.barBtnAtaKasa.Name = "barBtnAtaKasa";
            this.barBtnAtaKasa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnAtaKasa_ItemClick);
            // 
            // barBtnOdemePlani
            // 
            this.barBtnOdemePlani.Caption = "Ödeme Planı";
            this.barBtnOdemePlani.Id = 64;
            this.barBtnOdemePlani.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnOdemePlani.ImageOptions.Image")));
            this.barBtnOdemePlani.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnOdemePlani.ImageOptions.LargeImage")));
            this.barBtnOdemePlani.Name = "barBtnOdemePlani";
            this.barBtnOdemePlani.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnOdemePlani_ItemClick);
            // 
            // barBtnOdemePlaniDetay
            // 
            this.barBtnOdemePlaniDetay.Caption = "Ödeme Planı Detay";
            this.barBtnOdemePlaniDetay.Id = 70;
            this.barBtnOdemePlaniDetay.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnOdemePlaniDetay.ImageOptions.LargeImage")));
            this.barBtnOdemePlaniDetay.Name = "barBtnOdemePlaniDetay";
            this.barBtnOdemePlaniDetay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnOdemePlaniDetay_ItemClick);
            // 
            // barBtnNakitAkis
            // 
            this.barBtnNakitAkis.Caption = "Nakit Akış";
            this.barBtnNakitAkis.Id = 82;
            this.barBtnNakitAkis.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnNakitAkis.ImageOptions.LargeImage")));
            this.barBtnNakitAkis.Name = "barBtnNakitAkis";
            this.barBtnNakitAkis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnNakitAkis_ItemClick);
            // 
            // barButtonItemNakitAkisDetay
            // 
            this.barButtonItemNakitAkisDetay.Caption = "Nakit Akış Detay";
            this.barButtonItemNakitAkisDetay.Id = 83;
            this.barButtonItemNakitAkisDetay.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemNakitAkisDetay.ImageOptions.LargeImage")));
            this.barButtonItemNakitAkisDetay.Name = "barButtonItemNakitAkisDetay";
            this.barButtonItemNakitAkisDetay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemNakitAkisDetay_ItemClick);
            // 
            // barSubItemCekSenet
            // 
            this.barSubItemCekSenet.Caption = "Çek Senet";
            this.barSubItemCekSenet.Id = 17;
            this.barSubItemCekSenet.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItemCekSenet.ImageOptions.Image")));
            this.barSubItemCekSenet.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barBtnCekSenet, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnCekSenetAyrinti),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnCekToplam)});
            this.barSubItemCekSenet.Name = "barSubItemCekSenet";
            // 
            // barBtnCekSenet
            // 
            this.barBtnCekSenet.Caption = "Cek Senet";
            this.barBtnCekSenet.Id = 18;
            this.barBtnCekSenet.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnCekSenet.ImageOptions.Image")));
            this.barBtnCekSenet.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnCekSenet.ImageOptions.LargeImage")));
            this.barBtnCekSenet.Name = "barBtnCekSenet";
            this.barBtnCekSenet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnCekSenet_ItemClick);
            // 
            // barBtnCekSenetAyrinti
            // 
            this.barBtnCekSenetAyrinti.Caption = "Çek Senet Ayrıntı";
            this.barBtnCekSenetAyrinti.Id = 46;
            this.barBtnCekSenetAyrinti.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnCekSenetAyrinti.ImageOptions.Image")));
            this.barBtnCekSenetAyrinti.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnCekSenetAyrinti.ImageOptions.LargeImage")));
            this.barBtnCekSenetAyrinti.Name = "barBtnCekSenetAyrinti";
            this.barBtnCekSenetAyrinti.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnCekSenetAyrinti_ItemClick);
            // 
            // barBtnCekToplam
            // 
            this.barBtnCekToplam.Caption = "Cek Toplam";
            this.barBtnCekToplam.Id = 47;
            this.barBtnCekToplam.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnCekToplam.ImageOptions.Image")));
            this.barBtnCekToplam.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnCekToplam.ImageOptions.LargeImage")));
            this.barBtnCekToplam.Name = "barBtnCekToplam";
            this.barBtnCekToplam.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnCekToplam_ItemClick);
            // 
            // barSubItemFatura
            // 
            this.barSubItemFatura.Caption = "Fatura";
            this.barSubItemFatura.Id = 20;
            this.barSubItemFatura.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItemFatura.ImageOptions.Image")));
            this.barSubItemFatura.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnFatura),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnFaturaDetay),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnFaturaKarliligi),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnFaturaVade),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnSatisElemaniRaporu),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemFaturaDetay2, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barSubItemFatura.Name = "barSubItemFatura";
            // 
            // barBtnFatura
            // 
            this.barBtnFatura.Caption = "Faturalar";
            this.barBtnFatura.Id = 21;
            this.barBtnFatura.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnFatura.ImageOptions.Image")));
            this.barBtnFatura.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnFatura.ImageOptions.LargeImage")));
            this.barBtnFatura.Name = "barBtnFatura";
            this.barBtnFatura.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnFatura_ItemClick);
            // 
            // barBtnFaturaDetay
            // 
            this.barBtnFaturaDetay.Caption = "Fatura Detay";
            this.barBtnFaturaDetay.Id = 76;
            this.barBtnFaturaDetay.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnFaturaDetay.ImageOptions.LargeImage")));
            this.barBtnFaturaDetay.Name = "barBtnFaturaDetay";
            this.barBtnFaturaDetay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnFaturaDetay_ItemClick);
            // 
            // barBtnFaturaKarliligi
            // 
            this.barBtnFaturaKarliligi.Caption = "Fatura Karlılığı";
            this.barBtnFaturaKarliligi.Id = 44;
            this.barBtnFaturaKarliligi.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnFaturaKarliligi.ImageOptions.Image")));
            this.barBtnFaturaKarliligi.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnFaturaKarliligi.ImageOptions.LargeImage")));
            this.barBtnFaturaKarliligi.Name = "barBtnFaturaKarliligi";
            this.barBtnFaturaKarliligi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnFaturaKarliligi_ItemClick);
            // 
            // barBtnFaturaVade
            // 
            this.barBtnFaturaVade.Caption = "Fatura Vade";
            this.barBtnFaturaVade.Id = 45;
            this.barBtnFaturaVade.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnFaturaVade.ImageOptions.Image")));
            this.barBtnFaturaVade.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnFaturaVade.ImageOptions.LargeImage")));
            this.barBtnFaturaVade.Name = "barBtnFaturaVade";
            this.barBtnFaturaVade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnFaturaVade_ItemClick);
            // 
            // barBtnSatisElemaniRaporu
            // 
            this.barBtnSatisElemaniRaporu.Caption = "Satış Elemanı Raporu";
            this.barBtnSatisElemaniRaporu.Id = 52;
            this.barBtnSatisElemaniRaporu.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnSatisElemaniRaporu.ImageOptions.Image")));
            this.barBtnSatisElemaniRaporu.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnSatisElemaniRaporu.ImageOptions.LargeImage")));
            this.barBtnSatisElemaniRaporu.Name = "barBtnSatisElemaniRaporu";
            this.barBtnSatisElemaniRaporu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnSatisElemaniRaporu_ItemClick);
            // 
            // barSubItemHizmet
            // 
            this.barSubItemHizmet.Caption = "Hizmet";
            this.barSubItemHizmet.Id = 50;
            this.barSubItemHizmet.ImageOptions.Image = global::RaporcuV2.WinApp.Properties.Resources.hizmet;
            this.barSubItemHizmet.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnHizmetHareketleri),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, this.barButtonHizmetToplamlari, "Hizmet Toplamları"),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemAtaKarlilikRaporu)});
            this.barSubItemHizmet.Name = "barSubItemHizmet";
            // 
            // barBtnHizmetHareketleri
            // 
            this.barBtnHizmetHareketleri.Caption = "Hizmet Hareketleri";
            this.barBtnHizmetHareketleri.Id = 51;
            this.barBtnHizmetHareketleri.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnHizmetHareketleri.ImageOptions.Image")));
            this.barBtnHizmetHareketleri.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barBtnHizmetHareketleri.ImageOptions.LargeImage")));
            this.barBtnHizmetHareketleri.Name = "barBtnHizmetHareketleri";
            this.barBtnHizmetHareketleri.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnHizmetHareketleri_ItemClick);
            // 
            // barButtonHizmetToplamlari
            // 
            this.barButtonHizmetToplamlari.Caption = "Hizmet Toplamları";
            this.barButtonHizmetToplamlari.Id = 54;
            this.barButtonHizmetToplamlari.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonHizmetToplamlari.ImageOptions.Image")));
            this.barButtonHizmetToplamlari.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonHizmetToplamlari.ImageOptions.LargeImage")));
            this.barButtonHizmetToplamlari.Name = "barButtonHizmetToplamlari";
            this.barButtonHizmetToplamlari.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonHizmetToplamlari_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Faaliyet Raporu";
            this.barButtonItem3.Id = 55;
            this.barButtonItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.Image")));
            this.barButtonItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.LargeImage")));
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barButtonItemAtaKarlilikRaporu
            // 
            this.barButtonItemAtaKarlilikRaporu.Caption = "Karlılık Raporu";
            this.barButtonItemAtaKarlilikRaporu.Id = 71;
            this.barButtonItemAtaKarlilikRaporu.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemAtaKarlilikRaporu.ImageOptions.LargeImage")));
            this.barButtonItemAtaKarlilikRaporu.Name = "barButtonItemAtaKarlilikRaporu";
            this.barButtonItemAtaKarlilikRaporu.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemAtaKarlilikRaporu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItemAtaKarlilikRaporu_ItemClick);
            // 
            // barSubItemCustomReport
            // 
            this.barSubItemCustomReport.Caption = "Tanımlı Raporlar";
            this.barSubItemCustomReport.Id = 57;
            this.barSubItemCustomReport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItemCustomReport.ImageOptions.Image")));
            this.barSubItemCustomReport.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItemCustomReport.ImageOptions.LargeImage")));
            this.barSubItemCustomReport.Name = "barSubItemCustomReport";
            // 
            // barSubItemSettings
            // 
            this.barSubItemSettings.Caption = "Ayarlar";
            this.barSubItemSettings.Id = 2;
            this.barSubItemSettings.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItemSettings.ImageOptions.Image")));
            this.barSubItemSettings.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConnectionString),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemKullaniciListesi, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItemCustomReportList, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemDashboard)});
            this.barSubItemSettings.Name = "barSubItemSettings";
            // 
            // barButtonItemConnectionString
            // 
            this.barButtonItemConnectionString.Caption = "Bağlantı Ayarları";
            this.barButtonItemConnectionString.Id = 3;
            this.barButtonItemConnectionString.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemConnectionString.ImageOptions.Image")));
            this.barButtonItemConnectionString.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemConnectionString.ImageOptions.LargeImage")));
            this.barButtonItemConnectionString.Name = "barButtonItemConnectionString";
            this.barButtonItemConnectionString.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConnectionString_ItemClick);
            // 
            // barButtonItemKullaniciListesi
            // 
            this.barButtonItemKullaniciListesi.Caption = "Kullanıcı Tanımları";
            this.barButtonItemKullaniciListesi.Id = 56;
            this.barButtonItemKullaniciListesi.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemKullaniciListesi.ImageOptions.Image")));
            this.barButtonItemKullaniciListesi.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemKullaniciListesi.ImageOptions.LargeImage")));
            this.barButtonItemKullaniciListesi.Name = "barButtonItemKullaniciListesi";
            this.barButtonItemKullaniciListesi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemKullaniciListesi_ItemClick);
            // 
            // barButtonItemCustomReportList
            // 
            this.barButtonItemCustomReportList.Caption = "Özel Rapor Tanımları";
            this.barButtonItemCustomReportList.Id = 59;
            this.barButtonItemCustomReportList.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemCustomReportList.ImageOptions.Image")));
            this.barButtonItemCustomReportList.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemCustomReportList.ImageOptions.LargeImage")));
            this.barButtonItemCustomReportList.Name = "barButtonItemCustomReportList";
            this.barButtonItemCustomReportList.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCustomReportList_ItemClick);
            // 
            // barButtonItemDashboard
            // 
            this.barButtonItemDashboard.Caption = "Dashboard Tasarımı";
            this.barButtonItemDashboard.Id = 73;
            this.barButtonItemDashboard.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemDashboard.ImageOptions.LargeImage")));
            this.barButtonItemDashboard.Name = "barButtonItemDashboard";
            this.barButtonItemDashboard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItemDashboard_ItemClick);
            // 
            // skinBarSubItemTheme
            // 
            this.skinBarSubItemTheme.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.skinBarSubItemTheme.Caption = "Tema";
            this.skinBarSubItemTheme.Id = 4;
            this.skinBarSubItemTheme.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("skinBarSubItemTheme.ImageOptions.Image")));
            this.skinBarSubItemTheme.Name = "skinBarSubItemTheme";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemUserName),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemClock)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // barStaticItemUserName
            // 
            this.barStaticItemUserName.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItemUserName.Caption = "UserName";
            this.barStaticItemUserName.Id = 62;
            this.barStaticItemUserName.Name = "barStaticItemUserName";
            // 
            // barStaticItemClock
            // 
            this.barStaticItemClock.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItemClock.Caption = "Clock";
            this.barStaticItemClock.Id = 63;
            this.barStaticItemClock.Name = "barStaticItemClock";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManagerMain;
            this.barDockControlTop.Size = new System.Drawing.Size(1265, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 571);
            this.barDockControlBottom.Manager = this.barManagerMain;
            this.barDockControlBottom.Size = new System.Drawing.Size(1265, 24);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Manager = this.barManagerMain;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 529);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1265, 42);
            this.barDockControlRight.Manager = this.barManagerMain;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 529);
            // 
            // barBtnCariMuhBakiye
            // 
            this.barBtnCariMuhBakiye.Id = 66;
            this.barBtnCariMuhBakiye.Name = "barBtnCariMuhBakiye";
            // 
            // barBtnGenelDurum
            // 
            this.barBtnGenelDurum.Id = 75;
            this.barBtnGenelDurum.Name = "barBtnGenelDurum";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Cari Hesap";
            this.barButtonItem1.Id = 9;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // skinBarSubItem1
            // 
            this.skinBarSubItem1.Caption = "Cari Hesap";
            this.skinBarSubItem1.Id = 10;
            this.skinBarSubItem1.Name = "skinBarSubItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Id = 14;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barBtnBorcAlacakToplamlari
            // 
            this.barBtnBorcAlacakToplamlari.Id = 67;
            this.barBtnBorcAlacakToplamlari.Name = "barBtnBorcAlacakToplamlari";
            // 
            // barBtnOdemeAlacakTakip
            // 
            this.barBtnOdemeAlacakTakip.Id = 68;
            this.barBtnOdemeAlacakTakip.Name = "barBtnOdemeAlacakTakip";
            // 
            // barBtnAlisSatisOrani
            // 
            this.barBtnAlisSatisOrani.Id = 27;
            this.barBtnAlisSatisOrani.Name = "barBtnAlisSatisOrani";
            // 
            // barBtnAlisSatisCariyeOrani
            // 
            this.barBtnAlisSatisCariyeOrani.Id = 74;
            this.barBtnAlisSatisCariyeOrani.Name = "barBtnAlisSatisCariyeOrani";
            // 
            // barBtnCariHesapHareketSayisi
            // 
            this.barBtnCariHesapHareketSayisi.Id = 69;
            this.barBtnCariHesapHareketSayisi.Name = "barBtnCariHesapHareketSayisi";
            // 
            // barButtonItemCustomReportTest
            // 
            this.barButtonItemCustomReportTest.Caption = "Test";
            this.barButtonItemCustomReportTest.Id = 58;
            this.barButtonItemCustomReportTest.Name = "barButtonItemCustomReportTest";
            this.barButtonItemCustomReportTest.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCustomReportTest_ItemClick);
            // 
            // xtraTabbedMdiManagerMenu
            // 
            this.xtraTabbedMdiManagerMenu.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPagesAndTabControlHeader;
            this.xtraTabbedMdiManagerMenu.MdiParent = this;
            // 
            // defaultLookAndFeelMain
            // 
            this.defaultLookAndFeelMain.LookAndFeel.SkinName = "Metropolis";
            // 
            // timerClock
            // 
            this.timerClock.Enabled = true;
            this.timerClock.Interval = 60000;
            this.timerClock.Tick += new System.EventHandler(this.timerClock_Tick);
            // 
            // timerRefreshSession
            // 
            this.timerRefreshSession.Enabled = true;
            this.timerRefreshSession.Interval = 300000;
            this.timerRefreshSession.Tick += new System.EventHandler(this.timerRefreshSession_Tick);
            // 
            // barButtonItemFaturaDetay2
            // 
            this.barButtonItemFaturaDetay2.Caption = "Fatura Detay 2";
            this.barButtonItemFaturaDetay2.Id = 84;
            this.barButtonItemFaturaDetay2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemFaturaDetay2.ImageOptions.Image")));
            this.barButtonItemFaturaDetay2.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemFaturaDetay2.ImageOptions.LargeImage")));
            this.barButtonItemFaturaDetay2.Name = "barButtonItemFaturaDetay2";
            this.barButtonItemFaturaDetay2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemFaturaDetay2_ItemClick);
            // 
            // XtraFormMain
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1265, 595);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("XtraFormMain.IconOptions.Icon")));
            this.IsMdiContainer = true;
            this.Name = "XtraFormMain";
            this.Text = "Raporcu V2";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.XtraFormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManagerMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManagerMenu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem barSubItemMuhasebe;
        private DevExpress.XtraBars.BarButtonItem barBtnCariMuhKontrol;
        private DevExpress.XtraBars.BarSubItem barSubItemSettings;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConnectionString;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManagerMenu;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItemTheme;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeelMain;
        private DevExpress.XtraBars.BarButtonItem barBtnCariMuhBakiye;
        private DevExpress.XtraBars.BarButtonItem barBtnMizan;
        private DevExpress.XtraBars.BarButtonItem barBtnGenelDurum;
        private DevExpress.XtraBars.BarSubItem barSubItemCariHesap;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem1;
        private DevExpress.XtraBars.BarButtonItem barBtnCariBorcAlacakRaporu;
        private DevExpress.XtraBars.BarButtonItem barBtnBorcAlacakToplamlari;
        private DevExpress.XtraBars.BarSubItem barSubItemCekSenet;
        private DevExpress.XtraBars.BarButtonItem barBtnCekSenet;
        private DevExpress.XtraBars.BarButtonItem barBtnRiskBakiyeKontrol;
        private DevExpress.XtraBars.BarSubItem barSubItemFatura;
        private DevExpress.XtraBars.BarButtonItem barBtnFatura;
        private DevExpress.XtraBars.BarButtonItem barBtnKdvRaporu;
        private DevExpress.XtraBars.BarButtonItem barBtnBaBs;
        private DevExpress.XtraBars.BarButtonItem barBtnAcikBakiye;
        private DevExpress.XtraBars.BarButtonItem barBtnOdemeAlacakTakip;
        private DevExpress.XtraBars.BarButtonItem barBtnAlisSatisOrani;
        private DevExpress.XtraBars.BarButtonItem barBtnAlisSatisCariyeOrani;
        private DevExpress.XtraBars.BarButtonItem barBtnHesapIslemleri;
        private DevExpress.XtraBars.BarButtonItem barBtnCariHesapHareketSayisi;
        private DevExpress.XtraBars.BarSubItem barSubItemBanka;
        private DevExpress.XtraBars.BarButtonItem barBtnBankaIslemleri;
        private DevExpress.XtraBars.BarButtonItem barBtnBankaBakiye;
        private DevExpress.XtraBars.BarButtonItem barBtnKrediKarti;
        private DevExpress.XtraBars.BarButtonItem barBtnKrediKartiCiroDetay;
        private DevExpress.XtraBars.BarSubItem barSubItemStok;
        private DevExpress.XtraBars.BarButtonItem barBtnStokHareketleri;
        private DevExpress.XtraBars.BarButtonItem barBtnSiparisUretimSatis;
        private DevExpress.XtraBars.BarButtonItem barBtnDonemselStokAlisSatis;
        private DevExpress.XtraBars.BarButtonItem barBtnAlisSatisFiyatAnaliz;
        private DevExpress.XtraBars.BarButtonItem barBtnAmbarlarDurumRaporu;
        private DevExpress.XtraBars.BarButtonItem barBtnGunlukSatis;
        private DevExpress.XtraBars.BarButtonItem barBtnSeriTakip;
        private DevExpress.XtraBars.BarButtonItem barBtnFaturaKarliligi;
        private DevExpress.XtraBars.BarButtonItem barBtnFaturaVade;
        private DevExpress.XtraBars.BarButtonItem barBtnCekSenetAyrinti;
        private DevExpress.XtraBars.BarButtonItem barBtnCekToplam;
        private DevExpress.XtraBars.BarSubItem barSubItemKasa;
        private DevExpress.XtraBars.BarButtonItem barBtnKasa;
        private DevExpress.XtraBars.BarSubItem barSubItemHizmet;
        private DevExpress.XtraBars.BarButtonItem barBtnHizmetHareketleri;
        private DevExpress.XtraBars.BarButtonItem barBtnSatisElemaniRaporu;
        private DevExpress.XtraBars.BarButtonItem barButtonHizmetToplamlari;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItemKullaniciListesi;
        public DevExpress.XtraBars.BarManager barManagerMain;
        private DevExpress.XtraBars.BarSubItem barSubItemCustomReport;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCustomReportTest;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCustomReportList;
        private DevExpress.XtraBars.BarButtonItem barBtnCariVadeRaporu;
        private DevExpress.XtraBars.BarButtonItem barBtnAtaKasa;
        private System.Windows.Forms.Timer timerClock;
        private System.Windows.Forms.Timer timerRefreshSession;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemClock;
        private DevExpress.XtraBars.BarStaticItem barStaticItemUserName;
        private DevExpress.XtraBars.BarButtonItem barBtnOdemePlani;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCariCekRaporu;
        private DevExpress.XtraBars.BarButtonItem barBtnOdemePlaniDetay;
        private DevExpress.XtraBars.BarButtonItem barButtonItemAtaKarlilikRaporu;
        private DevExpress.XtraBars.BarButtonItem btnMaliTablolar;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDashboard;
        private DevExpress.XtraBars.BarButtonItem barBtnFaturaDetay;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCariSiparisliBakiye;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCariSiparisListesi;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSiparisliCariHesapEkstresi;
        private DevExpress.XtraBars.BarButtonItem barButtonSevkiyatPlani;
        private DevExpress.XtraBars.BarButtonItem barButtonIrsaliyeDetay;
        private DevExpress.XtraBars.BarButtonItem barBtnNakitAkis;
        private DevExpress.XtraBars.BarButtonItem barButtonItemNakitAkisDetay;
        private DevExpress.XtraBars.BarButtonItem barButtonItemFaturaDetay2;
    }
}