﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Deployment.Application;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Security.AccessControl;
using System.Windows.Forms;
using DevExpress.DataAccess;
using DevExpress.LookAndFeel;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using Microsoft.Win32;
using RaporcuV2.DataAccess;
using RaporcuV2.Model.Database;
using RaporcuV2.WinApp.Forms.CustomReport;
using RaporcuV2.WinApp.Forms.Customs;
using RaporcuV2.WinApp.Forms.Dashboard;
using RaporcuV2.WinApp.Forms.Muhasebe;

namespace RaporcuV2.WinApp.Forms.Main
{
    public partial class XtraFormMain : DevExpress.XtraEditors.XtraForm
    {
        public XtraFormMain()
        {
            //dashboardViewer1.DataSourceOptions.ObjectDataSourceLoadingBehavior = DocumentLoadingBehavior.LoadSafely;

            InitializeComponent();
            AddEvents();
            CreateCustomReportMenuItems();
            SetVersion();
            LookAndFeel.StyleChanged += LookAndFeel_StyleChanged;
        }

        #region Methots

        #region Create Custom Report Menu Items

        private void CreateCustomReportMenuItems()
        {
            RaporcuV2.DataAccess.RaporcuDbContext cnt = new RaporcuDbContext();
            List<Model.Database.CustomReport> list = cnt.CustomReports.AsNoTracking().ToList();
            foreach (Model.Database.CustomReport report in list.OrderBy(a=> a.CustomReportTag))
            {
                BarButtonItem item = new BarButtonItem(barManagerMain, report.CustomReportName);
                item.Tag = report.CustomReportTag;
                item.Name = string.Format("CustomReportButton{0}", report.CustomReportTag);
                
                this.barSubItemCustomReport.AddItem(item);
                item.ItemClick += (sender, args) =>
                {
                    OpenCustomReportForm(typeof(CustomReport.XtraFormCustomReport), report.CustomReportTag);
                };
            }
        }

        #endregion

        #region Add Events

        public void AddEvents()
        {
            ApplyPermission();
            this.StartPosition = FormStartPosition.CenterScreen;
            RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"Software", true);
            if (rk != null)
            {
                RegistryKey subKey = rk.OpenSubKey("Raporcu");
                try
                {
                    string skinName = (string)subKey.GetValue("Skin");
                    if (string.IsNullOrEmpty(skinName))
                        skinName = "Metropolis";
                    DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = skinName;
                }
                catch (Exception)
                {

                }
            }
            OpenForm(ribbonType: typeof(XtraFormDashboardViewer));
        }

        #endregion

        #region Set Menu Tags

        private void SetMenuTags()
        {
            #region Muhasebe

            this.barSubItemMuhasebe.Tag = 100;
            this.barBtnCariMuhKontrol.Tag = 101;
            this.barBtnCariMuhBakiye.Tag = 102;
            this.barBtnMizan.Tag = 103;
            this.barBtnGenelDurum.Tag = 104;
            this.barBtnKdvRaporu.Tag = 105;
            this.barBtnBaBs.Tag = 106;


            #endregion
            #region Cari Hesap

            this.barSubItemCariHesap.Tag = 200;
            this.barBtnAcikBakiye.Tag = 201;
            this.barBtnOdemeAlacakTakip.Tag = 202;
            this.barBtnRiskBakiyeKontrol.Tag = 203;
            this.barBtnCariBorcAlacakRaporu.Tag = 204;
            this.barBtnBorcAlacakToplamlari.Tag = 205;
            this.barBtnAlisSatisCariyeOrani.Tag = 206;
            this.barBtnHesapIslemleri.Tag = 207;
            this.barBtnCariHesapHareketSayisi.Tag = 208;
            this.barBtnCariVadeRaporu.Tag = 209;
            this.barButtonItemCariCekRaporu.Tag = 210;
            this.barButtonItemCariSiparisListesi.Tag = 211;

            #endregion
            #region Banka

            this.barSubItemBanka.Tag = 300;
            this.barBtnBankaIslemleri.Tag = 301;
            this.barBtnBankaBakiye.Tag = 302;
            this.barBtnKrediKarti.Tag = 303;
            this.barBtnKrediKartiCiroDetay.Tag = 304;

            #endregion
            #region Stok

            this.barSubItemStok.Tag = 400;
            this.barBtnStokHareketleri.Tag = 401;
            this.barBtnSiparisUretimSatis.Tag = 402;
            this.barBtnDonemselStokAlisSatis.Tag = 403;
            this.barBtnAlisSatisFiyatAnaliz.Tag = 404;
            this.barBtnAmbarlarDurumRaporu.Tag = 405;
            this.barBtnGunlukSatis.Tag = 406;
            this.barBtnSeriTakip.Tag = 407;
            this.barButtonSevkiyatPlani.Tag = 408;

            #endregion
            #region Kasa

            this.barSubItemKasa.Tag = 500;
            this.barBtnKasa.Tag = 501;

            #endregion
            #region Çek Senet

            this.barSubItemCekSenet.Tag = 600;
            this.barBtnCekSenet.Tag = 601;
            this.barBtnCekSenetAyrinti.Tag = 602;
            this.barBtnCekToplam.Tag = 603;


            #endregion
            #region Fatura

            this.barSubItemFatura.Tag = 700;
            this.barBtnFatura.Tag = 701;
            this.barBtnFaturaKarliligi.Tag = 702;
            this.barBtnFaturaVade.Tag = 703;
            this.barBtnSatisElemaniRaporu.Tag = 704;

            #endregion
            #region Hizmet

            this.barSubItemHizmet.Tag = 800;
            this.barBtnHizmetHareketleri.Tag = 801;
            this.barButtonHizmetToplamlari.Tag = 802;
            this.barButtonItem3.Tag = 803;

            #endregion
            #region Ayarlar

            this.barSubItemSettings.Tag = 10000;
            this.barButtonItemConnectionString.Tag = 10001;
            this.barButtonItemKullaniciListesi.Tag = 10002;
            this.barButtonItemCustomReportList.Tag = 10003;

            #endregion
            #region Ayarlar

            this.barSubItemCustomReport.Tag = 1000000;
            this.skinBarSubItemTheme.Tag = 1000001;

            #endregion
        }

        #endregion


        #region Set Verison

        private void SetVersion()
        {
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                var myVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion;
                this.Text = this.Text + " - " + myVersion.ToString();
            }
        }

        #endregion

        #region Methots

        #region OpenForm

        Color[] TabColor = new Color[]{
            Color.FromArgb(35,83,194),
            Color.FromArgb(64,168,19),
            Color.FromArgb(245,121,10),
            Color.FromArgb(141,62,168),
            Color.FromArgb(70,155,183),
            Color.FromArgb(196,19,19)
        };
        private int _formCount;

        void ColourTab()
        {
            //if (bCheckItem.Checked)
            _formCount++;
            xtraTabbedMdiManagerMenu.Pages[xtraTabbedMdiManagerMenu.Pages.Count - 1].Appearance.Header.BackColor = TabColor[(_formCount - 1) % 6];
        }

        private void OpenForm(Type ribbonType)
        {
            try
            {
                XtraForm form = null;
                foreach (XtraForm f in MdiChildren)
                {
                    if (f.GetType() == ribbonType)
                    {
                        form = f as XtraForm;
                        break;
                    }
                }
                if (form == null)
                {
                    form = Activator.CreateInstance(ribbonType) as XtraForm;
                    form.MdiParent = this;
                    form.KeyPreview = true;
                    form.Show();
                    ColourTab();

                }
                else
                {
                    form.Activate();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void ReOpenForm(Type ribbonType)
        {
            try
            {
                XtraForm form = null;
                foreach (XtraForm f in MdiChildren)
                {
                    if (f.GetType() == ribbonType)
                    {
                        form = f as XtraForm;
                        break;
                    }
                }
                if (form == null)
                {

                    form = Activator.CreateInstance(ribbonType) as XtraForm;
                    form.MdiParent = this;
                    form.KeyPreview = true;
                    form.Show();
                    ColourTab();
                }
                else
                {

                    form.Dispose();

                    form = Activator.CreateInstance(ribbonType) as XtraForm;
                    form.MdiParent = this;
                    form.KeyPreview = true;
                    form.Show();
                    ColourTab();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void OpenCustomReportForm(Type ribbonType,int reportTag)
        {
            try
            {
                int tagNumber = 0;

                XtraForm form = null;
                foreach (XtraForm f in MdiChildren)
                {
                    if (f.GetType() == ribbonType)
                    {
                        form = f as XtraForm;

                        int.TryParse(form.Tag.ToString(), out tagNumber);
                        if (tagNumber == reportTag)
                        break;
                    }
                }

                if (tagNumber != reportTag || form == null)
                {
                    form = new XtraFormCustomReport(reportTag) {MdiParent = this, KeyPreview = true};
                    form.Show();
                    ColourTab();
                }
                else
                {
                    form.Activate();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        #endregion


        #region Look And Feel


        private void LookAndFeel_StyleChanged(object sender, EventArgs e)
        {
            string skinName = UserLookAndFeel.Default.ActiveSkinName;
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = skinName;
            try
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey("Software", true);
                RegistrySecurity rs = new RegistrySecurity();
                string user = Environment.UserDomainName + "\\" + Environment.UserName;

                rs.AddAccessRule(new RegistryAccessRule(user,
                    RegistryRights.WriteKey | RegistryRights.ReadKey | RegistryRights.Delete,
                    InheritanceFlags.None,
                    PropagationFlags.None,
                    AccessControlType.Allow));

                rk = rk.CreateSubKey("Raporcu", RegistryKeyPermissionCheck.Default, rs);
                // Write encrypted string, initialization vector and key to the registry
                rk.SetValue("Skin", skinName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        #endregion

        #region Apply Permissions

        void ApplyPermission()
        {
            SetMenuTags();
            User user = DataAccess.UserInfo.CreateInstance().User;
            DataAccess.RaporcuDbContext cnt = new DataAccess.RaporcuDbContext();


            List<UserPermission> roleUsers = cnt.UserPermissions.Include("Permission").Where(r => r.UserId == user.UserId).ToList();
            foreach (var roleUser in roleUsers)
            {
                int tag = 0;
                int permissionTag = roleUser.Permission.PermissionTag;
                Console.WriteLine(permissionTag);
                List<BarItem> managerItems = barManagerMain.MainMenu.Manager.Items.Where(a => a.Tag != null).ToList();
            }

            foreach (var x in barManagerMain.MainMenu.Manager.Items)
            {
                int tag = 0;

                if (x is BarSubItem && !(x is SkinBarSubItem) && (x as BarSubItem).Tag != null)
                {
                    int.TryParse(((BarSubItem)x).Tag.ToString(), out tag);
                    if (tag != 0)
                    {
                        var permission = roleUsers.FirstOrDefault(a => a.Permission.PermissionTag == tag);
                        if(permission != null)
                        (x as BarSubItem).Visibility = permission.PermissionStatus ? BarItemVisibility.Always : BarItemVisibility.Never;
                    }
                }

                if (x is BarButtonItem && !(x is SkinBarSubItem) && (x as BarButtonItem).Tag != null)
                {
                    int.TryParse(((BarButtonItem)x).Tag.ToString(), out tag);
                    if (tag != 0)
                    {
                        var permission = roleUsers.FirstOrDefault(a => a.Permission.PermissionTag == tag);
                        (x as BarButtonItem).Visibility = permission.PermissionStatus ? BarItemVisibility.Always : BarItemVisibility.Never;
                    }
                }
            }}

        #endregion

        #endregion

        #endregion


        private void barButtonItemConnectionString_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (var openForm = new XtraFormSettings())
            {
                openForm.StartPosition = FormStartPosition.CenterScreen;
                DialogResult result = openForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    MessageBox.Show("Ayarların Geçerli olması için , Uygulama Yeniden Başlatılacaktır.");
                    Application.Restart();
                }
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(XtraFormCariMuhKontrol));
        }

        private void barBtnMizan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(XtraFormMizan));
        }

        private void barBtnCariBorcAlacakRaporu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(CariHesap.XtraFormBorcAlacakDurumRaporu));
        }

        private void barBtnCekSenet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(CekSenet.XtraFormCekSenet));
        }

        private void barBtnRiskBakiyeKontrol_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(CariHesap.XtraFormRiskBakiyeKontrol));
        }

        public void barBtnFatura_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Fatura.XtraFormFaturalar));
        }

        private void barBtnKdvRaporu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Muhasebe.XtraFormKdvRaporu));
        }

        private void barBtnBaBs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Muhasebe.XtraFormBaBs));
        }

        private void barBtnAcikBakiye_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(CariHesap.XtraFormCariAcikBakiye));
        }



        private void barBtnHesapIslemleri_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(CariHesap.XtraFormHesapIslemleri));
        }

        private void barBtnBankaIslemleri_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Banka.XtraFormBankaIslemleri));
        }

        private void barBtnBankaBakiye_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Banka.XtraFormBankaBakiye));
        }

        private void barBtnKrediKartiCiroDetay_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Banka.XtraFormKrediKartiCiroDetay));
        }

        private void barBtnStokHareketleri_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Stok.XtraFormStokHareketleri));
        }

        private void barBtnSiparisUretimSatis_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Stok.XtraFormStokSiparisUretimSatis));
        }

        private void barBtnDonemselStokAlisSatis_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Stok.XtraFormStokDonemselAlisSatis));
        }

        private void barBtnAlisSatisFiyatAnaliz_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Stok.XtraFormStokAlisSatisFiyatAnaliz));
        }

        private void barBtnAmbarlarDurumRaporu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Stok.XtraFormAmbarlarDurumRaporu));
        }

        private void barBtnGunlukSatis_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Stok.XtraFormStokGunlukSatis));
        }

        private void barBtnSeriTakip_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Stok.XtraFormStokSeriTakip));
        }

        private void barBtnFaturaKarliligi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Fatura.XtraFormFaturaKarliligi));
        }

        private void barBtnFaturaVade_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Fatura.XtraFormFaturaVade));
        }

        private void barBtnCekSenetAyrinti_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(CekSenet.XtraFormCekSenetAyrinti));
        }

        private void barBtnCekToplam_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(CekSenet.XtraFormCekToplam));
        }

        private void barBtnKasa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Kasa.XtraFormKasa));
        }

        private void barBtnHizmetHareketleri_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Hizmet.XtraFormHizmetHareketleri));
        }

        private void barBtnSatisElemaniRaporu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Fatura.XtraFormSatisElemaniRaporu));
        }

        private void barButtonItemRapor_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
        }

        private void barButtonHizmetToplamlari_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Hizmet.XtraFormHizmetToplamlari));
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //OpenForm(ribbonType:typeof(Reports.XtraFormReports));
           // OpenForm(ribbonType:typeof(Reports.XtraFormFaaliyet));
            OpenForm(ribbonType: typeof(Reports.XtraFormFaaliyetRaporu));
        }
        private void barButtonItemKullaniciListesi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Kullanici.XtraFormKullaniciListesi));
        }
        private void barButtonItemCustomReportTest_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenCustomReportForm(typeof(CustomReport.XtraFormCustomReport),1);
        }

        private void barButtonItemCustomReportList_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(CustomReport.XtraFormCustomReportList));
        }

        private void barBtnCariVadeRaporu_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(CariHesap.XtraFormCariVadeRaporu));
        }

        private void barBtnAtaKasa_ItemClick(object sender, ItemClickEventArgs e)
        {
            //OpenForm(ribbonType: typeof(Reports.XtraFormAtaKasa));
            OpenForm(ribbonType: typeof(Kasa.XtraFormKasaExcel));
        }

        private void timerRefreshSession_Tick(object sender, EventArgs e)
        {
            UserInfo.RefreshUserSession();}

        private void timerClock_Tick(object sender, EventArgs e)
        {
            SetClock();
        }
        private void XtraFormMain_Load(object sender, EventArgs e)
        {
            SetClock();

            barStaticItemUserName.Caption = UserInfo.GetUserName();
        }

        private void SetClock()
        {
            barStaticItemClock.Caption = DateTime.Now.ToString("dd.MM.yyyy HH:mm");
        }

        private void barBtnOdemePlani_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Kasa.XtraFormOdemePlani));
        }

        private void barButtonItemCariCekRaporu_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(CariHesap.XtraFormCariCekRaporu));
        }

        private void barBtnOdemePlaniDetay_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Kasa.XtraFormOdemePlaniDetay));
        }

        private void BarButtonItemAtaKarlilikRaporu_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Hizmet.XtraFormKarlilik));

        }

        private void BtnMaliTablolar_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Muhasebe.XtraFormMaliTablolar));
        }

        private void BarButtonItemDashboard_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (var openForm = new XtraFormDashBoardDesigner())
            {
                openForm.StartPosition = FormStartPosition.CenterScreen;
                DialogResult result = openForm.ShowDialog();
                ReOpenForm(ribbonType: typeof(XtraFormDashboardViewer));

            }

        }

        private void barBtnFaturaDetay_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Fatura.XtraFormFaturaDetay));
        }

        private void barButtonItemCariSiparisliBakiye_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(CariHesap.XtraFormCariSiparisliBakiyeRaporu));
        }

        private void barButtonItemCariSiparisListesi_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(CariHesap.XtraFormCariSiparisListesi));
        }

        private void barButtonItemSiparisliCariHesapEkstresi_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(CariHesap.XtraFormSiparisliCariEkstre));
        }

        private void barButtonSevkiyatPlani_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Stok.XtraFormSevkiyatPlani));
        }

        private void barButtonIrsaliyeDetay_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Stok.XtraFormIrsaliyeDetay));
        }

        private void barBtnNakitAkis_ItemClick(object sender, ItemClickEventArgs e)
        {
           OpenForm(ribbonType: typeof(Kasa.XtraFormNakitAkis));
        }

        private void barButtonItemNakitAkisDetay_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Kasa.XtraFormNakitAkisDetay));
        }

        private void barButtonItemFaturaDetay2_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenForm(ribbonType: typeof(Fatura.XtraFormFaturaDetay2));
        }
    }
}