﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using DevExpress.XtraEditors;
using Microsoft.Win32;
using RaporcuV2.DataAccess;
using RaporcuV2.Lms;

namespace RaporcuV2.WinApp.Forms.Main
{
    public partial class XtraFormLogin : DevExpress.XtraEditors.XtraForm
    {
        public XtraFormLogin()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Add Events

        #region OnLoad

        private void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                if (SingleConnection.ServerName == null || SingleConnection.Serial == null || string.IsNullOrEmpty(SingleConnection.Serial))
                {
                    using (var openForm = new XtraFormSettings())
                    {
                        openForm.StartPosition = FormStartPosition.CenterScreen;
                        DialogResult result = openForm.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            MessageBox.Show("Ayarların Geçerli olması için , Uygulama Yeniden Başlatılacaktır.");
                            Application.Restart();
                        }
                        else if (result == DialogResult.Cancel)
                        {

                            MessageBox.Show("Ayarların Geçerli olması için , Uygulama Yeniden Başlatılacaktır.");
                            Application.Exit();
                        }
                    }
                }
                RaporcuV2.DataAccess.RaporcuDbContext cnt = new RaporcuDbContext();
                int count = cnt.Users.Count();
            };
            simpleButtonLogin.Click += SimpleButtonLogin_Click;


            this.Shown += (sender, args) =>
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"Software", true);
                if (rk != null)
                {
                    RegistryKey subKey = rk.OpenSubKey("Raporcu");
                    try
                    {
                        string userName = (string) subKey.GetValue("UserName");
                        UserNameTextEdit.Text = userName;
                        if (!string.IsNullOrEmpty(userName))
                            PasswordTextEdit.Focus();
                        string skinName = (string) subKey.GetValue("Skin");
                        if (string.IsNullOrEmpty(skinName))
                            skinName = "Metropolis";
                        DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = skinName;
                    }
                    catch (Exception)
                    {

                    }
                }
            };

            PasswordTextEdit.KeyDown += (sender, args) =>
            {
                if (args.KeyCode == Keys.Enter)
                    SimpleButtonLogin_Click(this, null);
            };
        }

        private void SimpleButtonLogin_Click(object sender, EventArgs e)
        {

            string userName = UserNameTextEdit.Text;
            string pass = PasswordTextEdit.Text;


            if (!DataAccess.UserInfo.SetUserInfo(userName, pass))
            {
                XtraMessageBox.Show("Giriş Bilgileri Hatalı!", "Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ExchageResult license = LmsService.CheckLicense();
            if (!license.Status)
            {
                XtraMessageBox.Show(license.Message, "Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            DataAccess.UserInfo.CreateInstance();
            this.DialogResult = DialogResult.OK;

        }

        #endregion

        #endregion
    }
}