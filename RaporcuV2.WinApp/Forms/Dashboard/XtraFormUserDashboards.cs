﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RaporcuV2.Model.Database;
using RaporcuV2.WinApp.Forms.Kullanici;

namespace RaporcuV2.WinApp.Forms.Dashboard
{
    public partial class XtraFormUserDashboards : DevExpress.XtraEditors.XtraForm
    {
        public XtraFormUserDashboards()
        {
            InitializeComponent();
        }


        #region Events
        private void AddEvents()
        {
            RefreshData();

            #region New

            barButtonItemNew.ItemClick += (sender, events) =>
            {
                OpenForm(0);
            };

            #endregion

            #region Edit

            barButtonItemEdit.ItemClick += (sender, events) =>
            {
                User user = (User)gridViewList.GetFocusedRow();
                OpenForm(user.UserId);
            };

            #endregion

            #region Delete

            barButtonItemDelete.ItemClick += (sender, events) =>
            {
                User user = (User)gridViewList.GetFocusedRow();
                if (user.UserId != 1 && user.UserName != "admin")
                {
                    RaporcuV2.DataAccess.RaporcuDbContext cnt = new DataAccess.RaporcuDbContext();

                    User deleted = cnt.Users.Find(user.UserId);
                    cnt.Users.Remove(deleted);
                    cnt.SaveChanges();
                }
                RefreshData();
            };

            #endregion

            #region refresh

            barButtonItemRefresh.ItemClick += (sender, events) =>
            {
                RefreshData();
            };

            #endregion

            #region Close

            barButtonItemClose.ItemClick += (sender, events) =>
            {
                this.Close();
            };

            #endregion
        }
        #endregion

        #region Methots
        #region Refresh Data
        private void RefreshData()
        {
            RaporcuV2.DataAccess.RaporcuDbContext cnt = new DataAccess.RaporcuDbContext();
            gridControlList.DataSource = cnt.Users.ToList();
        }
        #endregion

        #region Open Edit Form
        private void OpenForm(int userId)
        {
            if (userId == 0)
            {

                RaporcuV2.DataAccess.RaporcuDbContext cnt = new DataAccess.RaporcuDbContext();
                int count = cnt.Users.Count();
                if (count > 5)
                {
                    MessageBox.Show("Kullanıcı Sayısı Limiti aşılmıştır.");
                    return;
                }
            }
            var frm = new XtraFormKullaniciEkle(userId)
            {
                FormBorderStyle = FormBorderStyle.FixedToolWindow,
                StartPosition = FormStartPosition.CenterScreen,
                KeyPreview = true,
                ShowIcon = false,
                ShowInTaskbar = true
            };
            var dialogResult = frm.ShowDialog();
            RefreshData();
        }
        #endregion
        #endregion
    }
}