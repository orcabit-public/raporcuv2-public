﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace RaporcuV2.WinApp.Forms.Dashboard
{
    public partial class XtraFormDashboardViewer : DevExpress.XtraEditors.XtraForm
    {
        public XtraFormDashboardViewer()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots

        #region AddEvents

        void AddEvents()
        {
            this.dashboardViewer1.Dashboard.Title.Text = "";
        }

        #endregion

        #endregion
    }
}