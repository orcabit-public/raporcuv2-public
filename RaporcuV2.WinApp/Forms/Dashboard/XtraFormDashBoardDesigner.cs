﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.DashboardCommon;
using DevExpress.DataAccess;
using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.EntityFramework;
using DevExpress.DataAccess.Sql;
using DevExpress.XtraEditors;
using RaporcuV2.DataAccess;
using RaporcuV2.Model.Database;
using RaporcuV2.Model.Fatura;

namespace RaporcuV2.WinApp.Forms.Dashboard
{
    public partial class XtraFormDashBoardDesigner : DevExpress.XtraEditors.XtraForm
    {
        RaporcuDbContext cnt = new RaporcuDbContext();
        public XtraFormDashBoardDesigner()
        {
            InitializeComponent();
          
            AddEvents();

        }

        #region Methots

        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        #region Add Events

        private void AddEvents()
        {
            this.dashboardDesigner.DashboardSaving += (sender, args) =>
            {
                if (args.Command == DevExpress.DashboardWin.DashboardSaveCommand.Save)
                {
                    MemoryStream ms = new MemoryStream();
                    var stream = new System.IO.MemoryStream();
                    // Saves the dashboard to the specified XML file.
                    dashboardDesigner.Dashboard.SaveToXml(stream);

                    int userId = UserInfo.GetUserId();
                    UserDashboard userDashboard = cnt.UserDashboards.FirstOrDefault(a => a.UserId == userId);
                    if (userDashboard == null)
                    {
                        userDashboard = new UserDashboard()
                        {
                            UserId = userId,
                            Content = stream.ToArray(),
                        };
                    }
                    else
                    {
                        userDashboard.Content = stream.ToArray();
                        cnt.Entry(userDashboard).State = EntityState.Modified;

                    }

                    
                    cnt.UserDashboards.AddOrUpdate(userDashboard);
                    cnt.SaveChanges();

                    // Specifies that the dashboard has been saved and no default actions are required.
                    args.Handled = true;
                }
            };
            dashboardDesigner.DataSourceOptions.ObjectDataSourceLoadingBehavior = DocumentLoadingBehavior.LoadSafely;
            this.dashboardDesigner.Dashboard = new DashboardHelper();

            this.dashboardDesigner.SelectedDataSourceChanged += (sender, args) =>
            {
            };
        }

        #endregion

        #endregion
    }
}