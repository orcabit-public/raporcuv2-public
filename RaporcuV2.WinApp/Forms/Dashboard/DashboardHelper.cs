﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.DashboardCommon;
using DevExpress.Data;
using DevExpress.DataAccess;
using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.EntityFramework;
using DevExpress.DataAccess.ObjectBinding;
using DevExpress.DataAccess.Sql;
using DevExpress.DocumentServices.ServiceModel.ServiceOperations;
using RaporcuV2.DataAccess;
using RaporcuV2.Helper;
using RaporcuV2.Model;
using RaporcuV2.Model.Banka;
using RaporcuV2.Model.Database;
using RaporcuV2.Model.Fatura;
using RaporcuV2.Model.Kasa;
using RaporcuV2.Model.Stok;
using Parameter = DevExpress.DataAccess.ObjectBinding.Parameter;

namespace RaporcuV2.WinApp.Forms.Dashboard
{
    public partial class DashboardHelper : DevExpress.DashboardCommon.Dashboard
    {
        public DashboardHelper()
        {
            InitializeComponent();


            AddEvents();

        }

        #region CreateConnectionParameters


        private DataConnectionParametersBase CreateConnectionParameters(string providerName)
        {
            switch (providerName)
            {
                case "MSAccess":
                    return new Access97ConnectionParameters()
                    {
                        FileName = @"Data\nwind.mdb"
                    };
                case "MSSqlServer":
                    return new MsSqlConnectionParameters()
                    {
                        ServerName = SingleConnection.ServerName,
                        DatabaseName = SingleConnection.DbName,
                        UserName = SingleConnection.UserId,
                        Password = SingleConnection.Pass,
                        AuthorizationType = MsSqlAuthorizationType.SqlServer
                    };
                default:
                    return new XmlFileConnectionParameters()
                    {
                        FileName = @"Data\sales-person.xml"
                    };
            }
        }

        #endregion

        void CreateNewCustom()
        {

            DashboardSqlDataSource source = new DashboardSqlDataSource("Özel Raporlar", CreateConnectionParameters("MSSqlServer"));
            source.DataProcessingMode = DataProcessingMode.Server;
            
            foreach (var cntCustomReport in cnt.CustomReports)
            {
                CustomSqlQuery query = new CustomSqlQuery()
                {
                    Name = cntCustomReport.CustomReportName,
                    Sql = string.Format(cntCustomReport.CustomReportSql, SingleConnection.DbName, GetFirmParameter,
                        GetPeriodParameter, GetBranchParameter, GetFirstDateParameter, GetLastDateParameter),
                };
                
                source.Queries.Add(query);
               
            }


            var exist = this.DataSources.Any(a => a.Name == "Özel Raporlar");
            if (!exist)
            {
                this.DataSources.Add(source);
            }
            else
            {
                SqlDataSource dataSource = (SqlDataSource)this.DataSources.FirstOrDefault(a => a.Name == "Özel Raporlar");
                foreach (var item in cnt.CustomReports)
                {
                    CustomSqlQuery query = new CustomSqlQuery()
                    {
                        Name = item.CustomReportName,
                        Sql = string.Format(item.CustomReportSql, SingleConnection.DbName, GetFirmParameter,
                            GetPeriodParameter, GetBranchParameter, GetFirstDateParameter, GetLastDateParameter),
                    };
                    bool any = dataSource.Queries.Any(a => a.Name == item.CustomReportName);
                    if (!any)
                        dataSource.Queries.Add(query);
                }

            }
        }

        void CreateCustomRepors()
        {

            SqlDataConnection connection = new SqlDataConnection()
            {
                ConnectionString = new SqlConnectionStringBuilder()
                {

                    DataSource = SingleConnection.ServerName, // Server name
                    InitialCatalog = SingleConnection.DbName, //DbName,  //Database
                    UserID = SingleConnection.UserId,         //Username
                    Password = SingleConnection.Pass,  //Password
                    

                }.ConnectionString,
            };

            DashboardSqlDataSource source = new DashboardSqlDataSource("Özel Raporlar")
            {
                ConnectionParameters = new MsSqlConnectionParameters()
                {
                    ServerName = SingleConnection.ServerName, // Server name
                    DatabaseName = SingleConnection.DbName, //DbName,  //Database
                    UserName = SingleConnection.UserId,         //Username
                    Password = SingleConnection.Pass,  //Password
                    AuthorizationType = MsSqlAuthorizationType.SqlServer,
                },
                
            };

            List<CustomSqlQuery> queries = new List<CustomSqlQuery>();

            foreach (var cntCustomReport in cnt.CustomReports)
            {
                CustomSqlQuery query = new CustomSqlQuery()
                {
                    Name = cntCustomReport.CustomReportName,
                    Sql = string.Format(cntCustomReport.CustomReportSql, SingleConnection.DbName, GetFirmParameter,
                        GetPeriodParameter, GetBranchParameter, GetFirstDateParameter, GetLastDateParameter),
                };
                queries.Add(query);
            }

            source.Queries.AddRange(queries);

            //this.DataConnections.Add(connection);
            var exist = this.DataSources.Any(a => a.Name == "Özel Raporlar");
            if (!exist)
                this.DataSources.Add(source);
        }
        void CreateDataSource()
        {
            this.DataConnections.Add(new SqlDataConnection()
            {

                ConnectionString = new SqlConnectionStringBuilder()
                {

                    DataSource = SingleConnection.ServerName, // Server name
                    InitialCatalog = SingleConnection.DbName, //DbName,  //Database
                    UserID = SingleConnection.UserId,         //Username
                    Password = SingleConnection.Pass,         //Password

                }.ConnectionString,

            });

            #region Add Custom Query

            this.DataSources.Add(new DashboardSqlDataSource()
            {
                Name = "Orcabit",
                Connection =
                {
                    //StoreConnectionNameOnly = true,

                    
                },
                Queries =
                {
                    new CustomSqlQuery()
                    {
                        Name = "FIRMS",
                        Sql =
                            $"SELECT NR,convert(varchar,nr)+' - '+ NAME as NAME FROM {SingleConnection.DbName}.dbo.L_CAPIFIRM",
                    },
                    new CustomSqlQuery()
                    {
                        Name = "PERIODS",
                        Sql = $"select NR from {SingleConnection.DbName}.dbo.L_CAPIPERIOD where FIRMNR = @PARAMFIRMNR",
                        Parameters =
                        {
                            new QueryParameter("PARAMFIRMNR", typeof(Expression),
                                new Expression("[Parameters.FirmNr]")),
                        },

                    },
                    new CustomSqlQuery()
                    {
                        Name = "BRANCH",
                        Sql = $@"SELECT '%' as NR,'Tümü' as [NAME]
                                    UNION ALL
                                select CONVERT(VARCHAR,NR) AS NR,convert(varchar,nr)+' - '+ NAME as NAME 
                                    from {SingleConnection.DbName}.dbo.L_CAPIDIV where FIRMNR = @PARAMFIRMNR",
                        Parameters =
                        {
                            new QueryParameter("PARAMFIRMNR", typeof(Expression),
                                new Expression("[Parameters.FirmNr]")),
                        },

                    },

                },
                ConnectionOptions =
                {

                },
                ConnectionParameters = new MsSqlConnectionParameters()
                {
                    ServerName = SingleConnection.ServerName, // Server name
                    DatabaseName = SingleConnection.DbName, //DbName,  //Database
                    UserName = SingleConnection.UserId,         //Username
                    Password = SingleConnection.Pass,  //Password
                    AuthorizationType = MsSqlAuthorizationType.SqlServer
                }

            }
            );

            #endregion
        }

        

        private static RaporcuDbContext cnt = new RaporcuDbContext();

        #region Methots

        #region Create Parameters

        private void CreateParameters()
        {
            if (this.Parameters.All(a => a.Name != "FirmNr"))
                this.Parameters.Add(
                    new DashboardParameter()
                    {
                        Name = "FirmNr",
                        Type = typeof(string),
                        Value = SingleConnection.FirmaNo,
                        Description = "Firma No Tanımı",
                        Visible = true
                    });
            if (this.Parameters.All(a => a.Name != "PeriodNr"))
                this.Parameters.Add(
                    new DashboardParameter()
                    {
                        Name = "PeriodNr",
                        Type = typeof(string),
                        Value = "01",
                        Description = "Dönem Tanımı",
                        Visible = true
                    });
            if (this.Parameters.All(a => a.Name != "BranchNr"))
                this.Parameters.Add(
                    new DashboardParameter()
                    {
                        Name = "BranchNr",
                        Type = typeof(string),
                        Value = "%",
                        Description = "İşyeri Tanımı (% Tümü)",
                        Visible = true
                    });

            #region Old


            //if (this.Parameters.All(a => a.Name != "Firma"))
            //    this.Parameters.Add(
            //        new DashboardParameter()
            //        {
            //            Name = "FirmNr",
            //            Description = "Firma Tanımı",
            //            LookUpSettings = new DynamicListLookUpSettings()
            //            {
            //                DataSource = this.DataSources.FindFirst(a => a.Name == "Firma"),
            //                DataMember = "Firma",
            //                ValueMember = "NR",
            //                DisplayMember = "NAME",
            //                DataSourceName = "Firma",
            //            },
            //            Visible = true,
            //            Value = SingleConnection.FirmaNo,

            //        });

            //if (this.Parameters.All(a => a.Name != "PeriodNr"))
            //    this.Parameters.Add(
            //    new DashboardParameter()
            //    {
            //        Name = "PeriodNr",
            //        Description = "Dönem Tanımı",
            //        LookUpSettings = new DynamicListLookUpSettings()
            //        {
            //            DataSource = this.DataSources.FindFirst(a => a.Name == "Firma"),
            //            DataMember = "Period",
            //            ValueMember = "NR",
            //            DisplayMember = "NR",
            //            DataSourceName = "Period",
            //            SortByMember = "NR"
            //        },
            //        Visible = true,
            //        Value = 1,


            //    });

            //if (this.Parameters.All(a => a.Name != "BranchNr"))
            //    this.Parameters.Add(
            //    new DashboardParameter()
            //    {
            //        Name = "BranchNr",
            //        Description = "İş Yeri Tanımı",
            //        LookUpSettings = new DynamicListLookUpSettings()
            //        {
            //            DataSource = this.DataSources.FindFirst(a => a.Name == "Orcabit"),
            //            DataMember = "BRANCH",
            //            ValueMember = "NR",
            //            DisplayMember = "NAME",
            //            DataSourceName = "Orcabit",
            //        },
            //        Visible = true,
            //        Value = "%"
            //    });


            #endregion

            if (this.Parameters.All(a => a.Name != "FirstDate"))
                this.Parameters.Add(
                    new DashboardParameter()
                    {

                        Name = "FirstDate",
                        Description = "İlk Tarih",
                        Visible = true,
                        Value = new DateTime(DateTime.Now.Year, 1, 1),
                        Type = typeof(DateTime),

                    });

            if (this.Parameters.All(a => a.Name != "LastDate"))
                this.Parameters.Add(
                    new DashboardParameter()
                    {

                        Name = "LastDate",
                        Description = "Son Tarih",
                        Visible = true,
                        Value = new DateTime(DateTime.Now.Year, 12, 31),
                        Type = typeof(DateTime),
                    });

            foreach (DashboardParameter parameter in Parameters)
            {
                parameter.Visible = true; //LookUpSettings = new DynamicListLookUpSettings();

            }
        }

        #endregion
        
        #region Create

        void Create()
        {

            string  name = "FaturaKarlilik";

            this.DataSources.Add(
                new DashboardObjectDataSource()
                {

                    ComponentName = name,
                    Name = name,
                    Parameters =
                    {
                        new Parameter()
                        {
                            Name = "ReportId",
                            Type = typeof(int),
                            Value = (int) ReportTags.FaturaKarliligi //702
                        },
                    },
                }
            );

            name = "FaturaDetay";

            this.DataSources.Add(
                new DashboardObjectDataSource()
                {

                    ComponentName = name,
                    Name = name,
                    Parameters =
                    {
                        new Parameter()
                        {
                            Name = "ReportId",
                            Type = typeof(int),
                            Value = (int) ReportTags.FaturaDetay
                        },
                    },
                }
            );

            name = "FaturaDetay2";

            this.DataSources.Add(
                new DashboardObjectDataSource()
                {

                    ComponentName = name,
                    Name = name,
                    Parameters =
                    {
                        new Parameter()
                        {
                            Name = "ReportId",
                            Type = typeof(int),
                            Value = (int) ReportTags.FaturaDetay2
                        },
                    },
                }
            );
            name = "KasaHareket";

            this.DataSources.Add(
                new DashboardObjectDataSource()
                {

                    ComponentName = name,
                    Name = name,
                    Parameters =
                    {
                        new Parameter()
                        {
                            Name = "ReportId",
                            Type = typeof(int),
                            Value = (int) ReportTags.KasaHareket
                        },
                    },
                }
            );

            name = "AmbarlarDurumRaporu";

            this.DataSources.Add(
                new DashboardObjectDataSource()
                {

                    ComponentName = name,
                    Name = name,
                    Parameters =
                    {
                        new Parameter()
                        {
                            Name = "ReportId",
                            Type = typeof(int),
                            Value = (int) ReportTags.AmbarlarDurumRaporu
                        },
                    },
                }
            );

            try
            {
                int userId = UserInfo.GetUserId();
                cnt = new RaporcuDbContext();
                var userDashboards = cnt.UserDashboards.FirstOrDefault(a => a.UserId == userId);
                if (!(userDashboards is null))
                {
                    byte[] content = userDashboards.Content;
                    Stream stream = new MemoryStream(content);
                    this.LoadFromXml(stream);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


        }

        #endregion

        #region Get Dashboard PAramater

        public string GetFirmParameter
        {
            get
            {
                return Parameters.FirstOrDefault(a => a.Name == "FirmNr").Value.ToString().PadLeft(3, '0');
            }
        }

        public string GetPeriodParameter
        {
            get
            {
                return Parameters.FirstOrDefault(a => a.Name == "PeriodNr").Value.ToString().PadLeft(2, '0');
            }
        }

        public string GetBranchParameter
        {
            get
            {
                return Parameters.FirstOrDefault(a => a.Name == "BranchNr").Value.ToString();
            }
        }

        public string GetFirstDateParameter
        {
            get
            {
                return string.Format("{0:yyyy-MM-dd}", Parameters.FirstOrDefault(a => a.Name == "FirstDate").Value);
            }
        }
        public string GetLastDateParameter
        {
            get
            {
                return string.Format("{0:yyyy-MM-dd}", Parameters.FirstOrDefault(a => a.Name == "LastDate").Value);
            }
        }

        #endregion
        #region Get Datas

        #region Fatura Karliligi

        public IQueryable<FaturaKarliligi> FaturaKarliligiData()
        {
            string sqlText = $"exec OrcaReport..orca_uspExec {(int)ReportTags.FaturaKarliligi},'{SingleConnection.DbName};{GetFirmParameter};{GetPeriodParameter};{GetBranchParameter};{GetFirstDateParameter};{GetLastDateParameter};0;0;0;0;0'";
            var list = cnt.Database.SqlQuery<FaturaKarliligi>(sqlText).AsQueryable();

            return list;
        }

        #endregion

        #region Fatura Detay

        public IQueryable<FaturaDetay> FaturaDetayData()
        {
            string sqlText = $"exec OrcaReport..orca_uspExec {(int)ReportTags.FaturaDetay},'{SingleConnection.DbName};{GetFirmParameter};{GetPeriodParameter};{GetBranchParameter};{GetFirstDateParameter};{GetLastDateParameter};0;0;0;0;0'";
            return cnt.Database.SqlQuery<FaturaDetay>(sqlText).AsQueryable();
        }

        #endregion


        #region Fatura Detay2

        public IQueryable<FaturaDetay2> FaturaDetay2Data()
        {
            string sqlText = $"exec OrcaReport..orca_uspExec {(int)ReportTags.FaturaDetay2},'{SingleConnection.DbName};{GetFirmParameter};{GetPeriodParameter};{GetBranchParameter};{GetFirstDateParameter};{GetLastDateParameter};0;0;0;0;0'";
            return cnt.Database.SqlQuery<FaturaDetay2>(sqlText).AsQueryable();
        }

        #endregion

        #region Banka Bakiye

        public IQueryable<BankaBakiye> BankaBakiyeData()
        {
            string sqlText = $"exec OrcaReport..orca_uspExec {(int)ReportTags.BankaBakiye},'{SingleConnection.DbName};{GetFirmParameter};{GetPeriodParameter};{GetBranchParameter};{GetFirstDateParameter};{GetLastDateParameter};0;0;0;0;0'";
            return cnt.Database.SqlQuery<BankaBakiye>(sqlText).AsQueryable();
        }

        #endregion

        #region Kasa Toplam

        public IQueryable<KasaAyrinti> kasaAyrintiData()
        {
            string sqlText = $"exec OrcaReport..orca_uspExec {(int)ReportTags.KasaHareket},'{SingleConnection.DbName};{GetFirmParameter};{GetPeriodParameter};{GetBranchParameter};{GetFirstDateParameter};{GetLastDateParameter};0;0;0;0;0'";
            return cnt.Database.SqlQuery<KasaAyrinti>(sqlText).AsQueryable();
        }

        #endregion


        #region Ambarlar Durum Raporu

        public IQueryable<StokAmbarlarDurumRaporu> AmbarlarDurumRaporuData()
        {
            string sqlText = $"exec OrcaReport..orca_uspExec {(int)ReportTags.AmbarlarDurumRaporu},'{SingleConnection.DbName};{GetFirmParameter};{GetPeriodParameter};{GetBranchParameter};{GetFirstDateParameter};{GetLastDateParameter};0;0;0;0;0'";
            return cnt.Database.SqlQuery<StokAmbarlarDurumRaporu>(sqlText).AsQueryable();
        }

        #endregion

        #endregion

        #region Add Events

        void AddEvents()
        {
            //CreateDataSource();
            Create();
            CreateParameters();
            //CreateCustomRepors();
            CreateNewCustom();

            this.DataLoading += OnDataLoading;


            this.ParameterCollectionChanged += (sender, args) =>
            {
                int a = 1;

            };
            this.Parameters.CollectionChanged += (sender, args) =>
            {
                int a = 1;


            };


            this.CustomParameters += (sender, args) =>
            {
                foreach (var parameter in args.Parameters)
                {
                    if (parameter.Name == "FirmNr")
                    {
                        this.Parameters.FirstOrDefault(a => a.Name == "FirmNr").Value = parameter.Value;
                    }
                    else if(parameter.Name == "PeriodNr")
                    {
                        this.Parameters.FirstOrDefault(a => a.Name == "PeriodNr").Value = parameter.Value;

                    }
                    else if (parameter.Name == "BrachNr")
                    {
                        this.Parameters.FirstOrDefault(a => a.Name == "BrachNr").Value = parameter.Value;

                    }
                }

            };
        }

        private void OnDataLoading(object sender, DashboardDataLoadingEventArgs e)
        {

            int reportId = (int)((DevExpress.DataAccess.ObjectBinding.ObjectDataSource)e.DataSource).Parameters.FirstOrDefault(a => a.Name == "ReportId").Value;
            if (reportId == (int)ReportTags.FaturaKarliligi)
            {
                e.Data = FaturaKarliligiData();
            }
            else if (reportId == (int)ReportTags.FaturaDetay)
            {
                e.Data = FaturaDetayData();
            }
            else if (reportId == (int)ReportTags.FaturaDetay2)
            {
                e.Data = FaturaDetay2Data();
            }
            else if (reportId == (int)ReportTags.BankaBakiye)
            {
                e.Data = BankaBakiyeData();
            }
            else if (reportId == (int)ReportTags.KasaHareket)
            {
                e.Data = kasaAyrintiData();
            }
            else if (reportId == (int)ReportTags.AmbarlarDurumRaporu)
            {
                e.Data = AmbarlarDurumRaporuData();
            }
        }

        #endregion

        #endregion

    }
}
