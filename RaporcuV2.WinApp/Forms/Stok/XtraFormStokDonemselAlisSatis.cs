﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.PLinq;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.Stok
{
    public partial class XtraFormStokDonemselAlisSatis : XtraFormBase, IReport
    {
        public XtraFormStokDonemselAlisSatis()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {

                CreateFirmLookup();
                CreatePeriodLookup();
                CreateDivLookup();
                CreateFirstDate();
                CreateFaturaTuru();
                CreateRefreshButton();
                RefreshGridData();
                CreatePivotButton();

            };
        }
        #endregion




        #endregion

        #region Refresh Grid Data
        public void RefreshGridData()
        {
            //SqlText = SqlStok.DonemselStokAlisSatis;
            _reportTag = RaporcuV2.Helper.ReportTags.DonemselStokAlisSatis;
            RefreshGridData<Model.Stok.StokDonemselAlisSatis>();
        }

        #endregion
    }
}