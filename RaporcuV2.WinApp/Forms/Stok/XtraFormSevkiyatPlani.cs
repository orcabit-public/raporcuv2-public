﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.Stok
{
    public partial class XtraFormSevkiyatPlani : XtraFormBase, IReport
    {
        public XtraFormSevkiyatPlani()
        {
            InitializeComponent();
            AddEvents();
        }
        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {

                CreateFirmLookup();
                CreatePeriodLookup();
                CreateDivLookup();
                CreateFirstDate();
                CreateLastDate();
                CreateRefreshButton();
                RefreshGridData();
                CreatePivotButton();

            };
        }
        #endregion
        #endregion

        #region Refresh Grid Data
        
        public void RefreshGridData()
        {
            
            _reportTag = RaporcuV2.Helper.ReportTags.SevkiyatPlani;
            RefreshGridData<Model.Stok.SevkiyatTakip>();
        }

        #endregion
    }
}