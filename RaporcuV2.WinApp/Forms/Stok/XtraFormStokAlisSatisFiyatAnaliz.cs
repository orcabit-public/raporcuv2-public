﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.PLinq;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.Stok
{
    public partial class XtraFormStokAlisSatisFiyatAnaliz : XtraFormBase, IReport
    {
        public XtraFormStokAlisSatisFiyatAnaliz()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                CreateFirmLookup();
                CreatePeriodLookup();
                CreateDivLookup();
                CreateFaturaTuru();
                CreateRefreshButton();
                RefreshGridData();
                CreatePivotButton();

            };
        }
        #endregion




        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
            //SqlText = SqlStok.StokAlisSatisFiyatAnaliz;
            _reportTag = RaporcuV2.Helper.ReportTags.StokAlisSatisFiyatAnaliz;
            RefreshGridData<Model.Stok.StokAlisSatisFiyatAnaliz>();
        }

        #endregion
    }
}