﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Spreadsheet;
using DevExpress.XtraBars;
using RaporcuV2.WinApp.Forms.Customs;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model.Reports;
using RaporcuV2.Util;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.Reports
{
    public partial class XtraFormFaaliyet : XtraFormBase, IReport
    {
        public int SectionStart { get; set; } = 4;

        public XtraFormFaaliyet()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots

        #region Add Events

        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                this.barManagerControl1.gridControl1.Visible = false;
                CreateFirmLookup();
                CreatePeriodLookup();
                CreateFirstDate();
                foreach (BarItem barItem in barManagerControl1.barManagerForm.Items)
                {
                    if (barItem.Name == "firstDate")
                    {
                        ((DevExpress.XtraBars.BarEditItem) barItem).EditValue = DateTime.Today;
                    }
                }

                CreateRefreshButton();

                barManager1.BeginUpdate();

                var parentTools = this.barManagerControl1.barManagerForm.Bars[0];

                this.barManager1.Bars.Add(parentTools);

                barManager1.EndUpdate();

                commonBar1.Merge(parentTools);
                RefreshGridData();
            };

            
        }

        #endregion


        #region Refresh Grid Data

        public void RefreshGridData()
        {

     
            #region Sorgular
            //SqlText = SqlReports.TumDovizKurlari;
            _reportTag = RaporcuV2.Helper.ReportTags.KurTumu;
            List<Kur> tumDovizKurlariList = GetPocoList<Kur>();

            //SqlText = SqlReports.KasaHareketleriYeni;
            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetKasaHareketleri;
            List<KasaHareketleri> kasaHareketleri = GetPocoList<KasaHareketleri>();

            #region Kasa Toplamları

            // Kasa Toplamları 
            double kasaTl = 0;
            double kasaUsd = 0;
            double kasaEuro = 0;

            kasaTl = kasaHareketleri.Where(a => a.CURCODE == "TL").Sum(a => a.GIREN) -
                     kasaHareketleri.Where(a => a.CURCODE == "TL").Sum(a => a.CIKAN);
            kasaUsd = kasaHareketleri.Where(a => a.CURCODE == "USD").Sum(a => a.GIREN) -
                      kasaHareketleri.Where(a => a.CURCODE == "USD").Sum(a => a.CIKAN);
            kasaEuro = kasaHareketleri.Where(a => a.CURCODE == "EUR").Sum(a => a.GIREN) -
                       kasaHareketleri.Where(a => a.CURCODE == "EUR").Sum(a => a.CIKAN);

            #endregion

            //SqlText = SqlReports.BankaHareketleriYeni;
            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetBankaHareketleri;
            List<KasaHareketleri> bankaHareketleri = GetPocoList<KasaHareketleri>();

            #region Banka Toplamları

            // Kasa Toplamları 
            double bankaTl = 0;double bankaUsd = 0;
            double bankaEuro = 0;

            bankaTl = bankaHareketleri.Where(a => a.CURCODE == "TL").Sum(a => a.GIREN) -
                     bankaHareketleri.Where(a => a.CURCODE == "TL").Sum(a => a.CIKAN);
            bankaUsd = bankaHareketleri.Where(a => a.CURCODE == "USD").Sum(a => a.GIREN) -
                      bankaHareketleri.Where(a => a.CURCODE == "USD").Sum(a => a.CIKAN);
            bankaEuro = bankaHareketleri.Where(a => a.CURCODE == "EUR").Sum(a => a.GIREN) -
                       bankaHareketleri.Where(a => a.CURCODE == "EUR").Sum(a => a.CIKAN);
            #endregion

            //SqlText = SqlReports.FaaliyetCekSenetHareketleri;
            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetCekSenetHareketleri;
            List<CekSenetHareketleri> cekSenetHareketleri = GetPocoList<CekSenetHareketleri>();


            //SqlText = SqlReports.FaaliyetKrediKartiHareketleri;
            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetKrediKartiHareketleri;
            List<KrediKartiHareketleri> kredikartlari = GetPocoList<KrediKartiHareketleri>();

            //SqlText = SqlReports.FirmaKrediKartiHareketleri;
            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetFirmaKrediKartiHareketleri;
            List<KrediKartiHareketleri> firmakredikartlari = GetPocoList<KrediKartiHareketleri>();


            //SqlText = SqlReports.SatisFaturalari;
            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetSatisFaturalari;
            List<Faturalar> satisFaturalari = GetPocoList<Faturalar>();


            //SqlText = SqlReports.AlisFaturalar;
            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetAlisFaturalari;
            List<Faturalar> alisFaturalar = GetPocoList<Faturalar>();

            #endregion


            spreadsheetControl1.BeginUpdate();
            // Kasa ve Kasa Detay Renkelnedirme
            BuiltInStyleId kasaBaslikStyle = BuiltInStyleId.Accent1;
            BuiltInTableStyleId kasaStyle = BuiltInTableStyleId.TableStyleMedium2;

            // Banka ve Banka HAreketerli Renklendirme
            BuiltInStyleId bankaBaslikStyle = BuiltInStyleId.Accent2;
            BuiltInTableStyleId bankaStyle = BuiltInTableStyleId.TableStyleMedium3;
            // Kredi Durumu 
            BuiltInStyleId krediBaslikStyle = BuiltInStyleId.Accent3;
            BuiltInTableStyleId krediStyle = BuiltInTableStyleId.TableStyleMedium4;

            // Çek Durumu 
            BuiltInStyleId cekBaslikStyle = BuiltInStyleId.Accent4;
            BuiltInTableStyleId cekStyle = BuiltInTableStyleId.TableStyleMedium5;

            // Çek Durumu 
            BuiltInStyleId cekDurumBaslikStyle = BuiltInStyleId.Accent6;
            BuiltInTableStyleId cekDurumStyle = BuiltInTableStyleId.TableStyleMedium7;

            int i = 1;

            int dataCount = 0;

            #region Excel Sheet Prepare

            IWorkbook workbook = spreadsheetControl1.Document;
            Worksheet ws = workbook.Worksheets[0];
            ws.ActiveView.PaperKind = PaperKind.A4;
            ws.ActiveView.Orientation = PageOrientation.Landscape;
            ws.ActiveView.Margins.Left = 0;
            ws.ActiveView.Margins.Top = 0;
            ws.ActiveView.Margins.Right = 0;
            ws.ActiveView.Margins.Bottom = 0;

            ws.Clear(ws.GetUsedRange());

            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToWidth = 1;
            ws.PrintOptions.FitToHeight = 0;

            #endregion

            #region Headers

            #region KurEuro
            TrCurr = 20;
            //SqlText = SqlReports.Kur;
            _reportTag = RaporcuV2.Helper.ReportTags.Kur;
            List<Kur> Kur = GetPocoList<Kur>();

            double kur = 0;

            if (Kur.Count > 0)
            {
                kur = (double)Kur.FirstOrDefault().RATES1;
            }

            ws.Cells["B1"].Value = kur;
           // ws.Cells["B1"].Style.NumberFormat = "€#,##0.00000";
            ws.Cells["F1"].Value = kur;
            //ws.Cells["F1"].Style.NumberFormat = "€#,##0.00000";

            #endregion
            #region KurDolar
            TrCurr = 1;
            //SqlText = SqlReports.Kur;
            _reportTag = RaporcuV2.Helper.ReportTags.Kur;
            List<Kur> Kur1 = GetPocoList<Kur>();

            double kur1 = 0;

            if (Kur1.Count > 0)
            {
                kur1 = (double)Kur1.FirstOrDefault().RATES1;
            }

            ws.Cells["C1"].Value = kur1;
           // ws.Cells["C1"].Style.NumberFormat = "$#,##0.00000";
            ws.Cells["G1"].Value = kur1;
            //ws.Cells["G1"].Style.NumberFormat = "$#,##0.00000";

            #endregion

            #region BaslikDoviz

            ws.Cells["B2"].Value = "TOPLAM TL :";
            ws.Cells["B3"].Value = "TOPLAM EURO :";
            ws.Cells["B4"].Value = "TOPLAM USD :";
            ws.Cells["B5"].Value = "TOPLAM NAKDİN TL KARŞILIĞI :";
            ws.Range["B2:B5"].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Right;


            ws.Cells["C2"].Value = kasaTl + bankaTl;
            ws.Cells["C3"].Value = kasaEuro + bankaEuro;
            ws.Cells["C4"].Value = kasaUsd + bankaUsd;
            ws.Cells["C5"].Style.NumberFormat = "#,##0.00000";
           

            ws.Range["C2:C5"].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Right;
            ws.Cells["C5"].Formula = "SUM(C2+(C3*B1)+(C4*C1))";



            #endregion

            //Range range = ws.Range[$"A{i}:C{i}"];
            var range = ws.Range[$"A{i}"];
            ws.MergeCells(range);
            range.Value = string.Format("{0} Faaliyet Raporu", FirstDate.ToString("dd MMMM yyyy , dddd"));
            //range.Style = workbook.Styles[BuiltInStyleId.Heading1];

            #endregion

            #region Kasa Hareketleri

            i = ws.GetUsedRange().RowCount + 2;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:C{i}"];
            ws.MergeCells(range);
            range.Value = "Kasa Hareketleri";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            #endregion
            

            var currencyList = kasaHareketleri.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            bool isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;

                int dataRowCount = kasaHareketleri.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tbl = ws.Tables.Add(ws[$"A{i}:C{dataCount}"], true);
                tbl.ShowTotals = true; tbl.AutoFilter.Disable();
                tbl.Style = workbook.TableStyles[kasaStyle];
                tbl.DataRange.NumberFormat = "#,##0.00";
                tbl.Columns[0].Name = $"{Labels.Aciklama}";
                tbl.Columns[0].TotalRowLabel = $"{curCode.pb} Kasa Toplamı :";
                tbl.Columns[1].Name = Labels.Giren;
                tbl.Columns[2].Name = Labels.Cikan;
                foreach (var item in kasaHareketleri.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.ACIKLAMA;
                    ws.Cells[$"B{i}"].Value = item.GIREN;
                    ws.Cells[$"C{i}"].Value = item.CIKAN;
                }

                tbl.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tbl.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i= i+2;
                range = ws.Range[$"B{i}:C{i}"];
                ws.MergeCells(range);
                ws.Cells[$"A{i}"].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Right;
                ws.Cells[$"A{i}"].Style.Font.Bold = true;
                ws.Cells[$"A{i}"].Value = "BAKİYE";

                range.Value = kasaHareketleri.Where(a => a.CURCODE == curCode.pb).Sum(s => s.GIREN) -
                              kasaHareketleri.Where(a => a.CURCODE == curCode.pb).Sum(s => s.CIKAN);
                //TlKasa = Convert.ToDouble(range.Value) ;
                range = ws.Range[$"A{i}:C{i}"];
                range.Style = workbook.Styles[kasaBaslikStyle];
                range.Style.Font.Bold = true;
                range.NumberFormat = "#,##0.00";}
            #endregion

            #region Banka Hareketleri

            i = ws.GetUsedRange().RowCount + 2;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:C{i}"];
            ws.MergeCells(range);
            range.Value = "Banka Hareketleri";
            range.Style = workbook.Styles[bankaBaslikStyle];
            i++;

            #endregion


            currencyList = bankaHareketleri.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;

                int dataRowCount = bankaHareketleri.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tbl = ws.Tables.Add(ws[$"A{i}:C{dataCount}"], true);
                tbl.ShowTotals = true; tbl.AutoFilter.Disable();
                tbl.Style = workbook.TableStyles[bankaStyle];
                tbl.DataRange.NumberFormat = "#,##0.00";
                tbl.Columns[0].Name = $"{Labels.Aciklama}";
                tbl.Columns[0].TotalRowLabel = $"{curCode.pb} Banka Toplamı :";
                tbl.Columns[1].Name = Labels.Giren;
                tbl.Columns[2].Name = Labels.Cikan;
                foreach (var item in bankaHareketleri.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.ACIKLAMA;
                    ws.Cells[$"B{i}"].Value = item.GIREN;
                    ws.Cells[$"C{i}"].Value = item.CIKAN;
                }

                tbl.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tbl.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = i + 2;
                range = ws.Range[$"B{i}:C{i}"];
                ws.MergeCells(range);
                ws.Cells[$"A{i}"].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Right;
                ws.Cells[$"A{i}"].Style.Font.Bold = true;
                ws.Cells[$"A{i}"].Value = "BAKİYE";

                range.Value = bankaHareketleri.Where(a => a.CURCODE == curCode.pb).Sum(s => s.GIREN) -
                              bankaHareketleri.Where(a => a.CURCODE == curCode.pb).Sum(s => s.CIKAN);

                range = ws.Range[$"A{i}:C{i}"];
                range.Style = workbook.Styles[bankaBaslikStyle];
                range.Style.Font.Bold = true;
                range.NumberFormat = "#,##0.00";
            }
            #endregion

            #region Çek - Senet Hareketleri

            i = 3;

            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"E{i}:H{i}"];
            ws.MergeCells(range);
            range.Value = "Çek-Senet Hareketleri";
            range.Style = workbook.Styles[bankaBaslikStyle];
            i++;

            #endregion


            currencyList = cekSenetHareketleri.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                i = i ;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;

                int dataRowCount = cekSenetHareketleri.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tbl = ws.Tables.Add(ws[$"E{i}:H{dataCount}"], true);
                tbl.ShowTotals = true; tbl.AutoFilter.Disable();
                tbl.Style = workbook.TableStyles[bankaStyle];
                tbl.DataRange.NumberFormat = "#,##0.00";
                tbl.Columns[0].Name = $"{Labels.VadeGunu}";
                tbl.Columns[0].DataRange.NumberFormat = "dd/mm/yyyy";
                tbl.Columns[1].Name = $"{Labels.Aciklama}";
                tbl.Columns[0].TotalRowLabel = $"{curCode.pb} Çek-Senet Toplamı :";
                tbl.Columns[2].Name = Labels.Giren;
                tbl.Columns[3].Name = Labels.Cikan;
                foreach (var item in cekSenetHareketleri.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"E{i}"].Value = item.VADE;
                    ws.Cells[$"F{i}"].Value = item.ACIKLAMA;
                    ws.Cells[$"G{i}"].Value = item.GIREN;
                    ws.Cells[$"H{i}"].Value = item.CIKAN;

                }

                tbl.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tbl.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = i + 2;range = 
                ws.Range[$"G{i}:H{i}"];ws.MergeCells(range);
                ws.Cells[$"E{i}"].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Right;
                ws.Cells[$"E{i}"].Style.Font.Bold = true;
                ws.Cells[$"E{i}"].Value = "BAKİYE";

                range.Value = cekSenetHareketleri.Where(a => a.CURCODE == curCode.pb).Sum(s => s.GIREN) -
                              cekSenetHareketleri.Where(a => a.CURCODE == curCode.pb).Sum(s => s.CIKAN);

                range = ws.Range[$"E{i}:H{i}"];
                range.Style = workbook.Styles[bankaBaslikStyle];
                range.Style.Font.Bold = true;
                range.NumberFormat = "#,##0.00";
            }
            #endregion

            #region Firma Kredi Kartı Ödemeleri
            i = i + 2;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"E{i}:G{i}"];
            ws.MergeCells(range);
            range.Value = "Firma Kredi Kartı Ödemeler";
            range.Style = workbook.Styles[cekBaslikStyle];
            i++;

            #endregion



            isFirstRow = true;
            i = i ;
            if (!isFirstRow)
            {
                i++;
            }

            isFirstRow = false;

            int rowCount  = firmakredikartlari.Count;
            dataCount = rowCount + i;
            Table tableFirmaKrediKart = ws.Tables.Add(ws[$"E{i}:G{dataCount}"], true);
            tableFirmaKrediKart.ShowTotals = true;
            tableFirmaKrediKart.AutoFilter.Disable();
            tableFirmaKrediKart.Style = workbook.TableStyles[cekStyle];

            tableFirmaKrediKart.DataRange.NumberFormat = "#,##0.00";
            tableFirmaKrediKart.Columns[0].Name = $"{Labels.BankaHesapUnvani}";
            tableFirmaKrediKart.Columns[0].TotalRowLabel = $"Firma Kredi Kartı Toplamı :";


            tableFirmaKrediKart.Columns[1].Name = Labels.CariHesapUnvani;

            tableFirmaKrediKart.Columns[2].Name = Labels.Tutar;

            foreach (var item in firmakredikartlari)
            {

                i++;
                ws.Cells[$"E{i}"].Value = item.HESAPADI;
                ws.Cells[$"F{i}"].Value = item.DEFINITION_;
                ws.Cells[$"G{i}"].Value = item.Tutar;

            }
            tableFirmaKrediKart.TotalRowRange.NumberFormat = "#,##0.00";
            foreach (TableColumn column in tableFirmaKrediKart.Columns)
            {
                if (column.Index != 0)
                    column.TotalRowFunction = TotalRowFunction.Sum;
            }


            #endregion

            #region Pos Tahsilat 
            i = i + 4;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"E{i}:G{i}"];
            ws.MergeCells(range);
            range.Value = "Pos Tahsilat ";
            range.Style = workbook.Styles[cekBaslikStyle];
            i++;

            #endregion

            isFirstRow = true;
            i = i ;
            if (!isFirstRow)
            {
                i++;
            }

            isFirstRow = false;

            rowCount = kredikartlari.Count;
            dataCount = rowCount + i;
            Table tableKrediKart = ws.Tables.Add(ws[$"E{i}:G{dataCount}"], true);
            tableKrediKart.ShowTotals = true;
            tableKrediKart.AutoFilter.Disable();
            tableKrediKart.Style = workbook.TableStyles[cekStyle];

            tableKrediKart.DataRange.NumberFormat = "#,##0.00";
            tableKrediKart.Columns[0].Name = $"{Labels.BankaHesapUnvani}";
            tableKrediKart.Columns[0].TotalRowLabel = $"Pos Toplamı :";
            tableKrediKart.Columns[1].Name = Labels.CariHesapUnvani;
            tableKrediKart.Columns[2].Name = Labels.Tutar;

            foreach (var item in kredikartlari)
            {

                i++;
                ws.Cells[$"E{i}"].Value = item.HESAPADI;
                ws.Cells[$"F{i}"].Value = item.DEFINITION_;
                ws.Cells[$"G{i}"].Value = item.Tutar;

            }
            tableKrediKart.TotalRowRange.NumberFormat = "#,##0.00";
            foreach (TableColumn column in tableKrediKart.Columns)
            {
                if (column.Index != 0)
                    column.TotalRowFunction = TotalRowFunction.Sum;
            }


            #endregion

            #region Satışlar
            i = i + 4;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"E{i}:G{i}"];
            ws.MergeCells(range);
            range.Value = "Satışlar ";
            range.Style = workbook.Styles[cekBaslikStyle];
            i++;

            #endregion


            isFirstRow = true;
            i = i ;
            if (!isFirstRow)
            {
                i++;
            }

            isFirstRow = false;

            rowCount = satisFaturalari.Count;
            dataCount = rowCount + i;
            Table tableSatisFaturalari = ws.Tables.Add(ws[$"E{i}:G{dataCount}"], true);
            tableSatisFaturalari.ShowTotals = true;
            tableSatisFaturalari.AutoFilter.Disable();
            tableSatisFaturalari.Style = workbook.TableStyles[cekStyle];

            tableSatisFaturalari.DataRange.NumberFormat = "#,##0.00";
            tableSatisFaturalari.Columns[0].Name = $"{Labels.VadeGunu}";
            tableSatisFaturalari.Columns[0].DataRange.NumberFormat = "dd/mm/yyyy";
            tableSatisFaturalari.Columns[0].TotalRowLabel = $"Satışlar Toplamı :";
            tableSatisFaturalari.Columns[1].Name = Labels.CariHesapUnvani;
            tableSatisFaturalari.Columns[2].Name = Labels.Tutar;

            foreach (var item in satisFaturalari)
            {

                i++;
                ws.Cells[$"E{i}"].Value = item.VADE;
                ws.Cells[$"F{i}"].Value = item.DEFINITION_;
                ws.Cells[$"G{i}"].Value = item.Tutar;

            }
            tableSatisFaturalari.TotalRowRange.NumberFormat = "#,##0.00";
            foreach (TableColumn column in tableSatisFaturalari.Columns)
            {
                if (column.Index != 0)
                    column.TotalRowFunction = TotalRowFunction.Sum;
            }


            #endregion

            #region Alışlar
            i = i + 4;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"E{i}:H{i}"];
            ws.MergeCells(range);
            range.Value = "Alışlar ";
            range.Style = workbook.Styles[cekBaslikStyle];
            i++;

            #endregion


            isFirstRow = true;
            i = i ;
            if (!isFirstRow)
            {
                i++;
            }

            isFirstRow = false;

            rowCount = alisFaturalar.Count;
            dataCount = rowCount + i;
            Table tableAlisFaturalari = ws.Tables.Add(ws[$"E{i}:H{dataCount}"], true);
            tableAlisFaturalari.ShowTotals = true;
            tableAlisFaturalari.AutoFilter.Disable();
            tableAlisFaturalari.Style = workbook.TableStyles[cekStyle];

            tableAlisFaturalari.DataRange.NumberFormat = "#,##0.00";
            tableAlisFaturalari.Columns[0].Name = $"{Labels.FaturaTarihi}";
            tableAlisFaturalari.Columns[0].DataRange.NumberFormat = "dd/mm/yyyy";
            tableAlisFaturalari.Columns[0].TotalRowLabel = $"Alışlar Toplamı :";

            tableAlisFaturalari.Columns[1].Name = Labels.CariHesapUnvani;

            tableAlisFaturalari.Columns[2].Name = Labels.VadeGunu;
            tableAlisFaturalari.Columns[2].DataRange.NumberFormat = "dd/mm/yyyy";
            tableAlisFaturalari.Columns[3].Name = Labels.Tutar;

            foreach (var item in alisFaturalar)
            {

                i++;
                ws.Cells[$"E{i}"].Value = item.DATE_;
                ws.Cells[$"F{i}"].Value = item.DEFINITION_;
                ws.Cells[$"G{i}"].Value = item.VADE;
                ws.Cells[$"H{i}"].Value = item.Tutar;
            }
            tableAlisFaturalari.TotalRowRange.NumberFormat = "#,##0.00";
            foreach (TableColumn column in tableAlisFaturalari.Columns)
            {
                if (column.Index != 0)
                    column.TotalRowFunction = TotalRowFunction.Sum;
            }


            #endregion

            spreadsheetControl1.EndUpdate();
        }


        #endregion

        #endregion

    }
}