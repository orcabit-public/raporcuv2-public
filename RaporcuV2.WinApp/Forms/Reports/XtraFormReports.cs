﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using DevExpress.Utils.Extensions;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model;
using RaporcuV2.Model.Reports;
using System.Drawing;
using System;

namespace RaporcuV2.WinApp.Forms.Reports
{
    public partial class XtraFormReports  : XtraFormBase, IReport
    {
        public XtraFormReports()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events
        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                CreateFirmLookup();
                CreatePeriodLookup();
                CreateRefreshButton();
                CreateFirstDate();
              
            };
        }
        #endregion




        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
            SqlText = SqlReports.KasaHareketleri;
            RefreshGridData<KasaHareketleri>();

            ExportDeductList();

        }

        #endregion



        #region Export Deduct List

        public void ExportDeductList()
        {
            #region TlKasaHareketleri
            TrCurr = 0;
            SqlText = SqlReports.KasaHareketleri;
            List<KasaHareketleri> source = GetPocoList<KasaHareketleri>();
            SqlText = SqlReports.KasaToplami;
            List<Toplam> sourceToplam = GetPocoList<Toplam>();

            ExcelPackage package = new ExcelPackage();
            var ws = package.Workbook.Worksheets.Add("Faaliyet Raporu");
            ws.Cells["B1"].Value = FirstDate;
            ws.Cells["B1"].Style.Numberformat.Format = "d mmmm yyyy dddd";
            ws.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["G1"].Value = FirstDate;
            ws.Cells["G1"].Style.Numberformat.Format = "d mmmm yyyy dddd";
            ws.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            TrCurr = 20;
            SqlText = SqlReports.Kur;
            List<Kur> Kur = GetPocoList<Kur>();

            double kur = 0;
            
            if (Kur.Count > 0)
            {
                kur = (double)Kur.FirstOrDefault().RATES1;
            }

            ws.Cells["C1"].Value = kur;
            ws.Cells["C1"].Style.Numberformat.Format= "€#,##0.00000";
            ws.Cells["H1"].Value = kur;
            ws.Cells["H1"].Style.Numberformat.Format = "€#,##0.00000";
            TrCurr = 1;
            SqlText = SqlReports.Kur;
            List<Kur> Kur1 = GetPocoList<Kur>();
            double kur1 = 0;
            if (Kur1.Count > 0)
            {
                kur1 = (double)Kur1.FirstOrDefault().RATES1;
            }
            ws.Cells["D1"].Value = kur1;
            ws.Cells["D1"].Style.Numberformat.Format = "$#,##0.00000";
            ws.Cells["I1"].Value = kur1;
            ws.Cells["I1"].Style.Numberformat.Format = "$#,##0.00000";

            #region Header
            ws.Cells["B2"].Value = "TOPLAM TL :";
            ws.Cells["B3"].Value = "TOPLAM EURO :";
            ws.Cells["B4"].Value = "TOPLAM USD :";
            ws.Cells["B5"].Value = "TOPLAM NAKDİN TL KARŞILIĞI :";
            ws.Cells["B2:B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            ws.Cells["B2:B5"].Style.Font.Bold = true;
            ws.Cells["C2:D2"].Merge = true;
            ws.Cells["C3:D3"].Merge = true;
            ws.Cells["C4:D4"].Merge = true;
            ws.Cells["C5:D5"].Merge = true;           
            ws.Cells["C2:D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            #region Başlık Renklendir
            //ws.Cells["B3:D3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            //ws.Cells["B3:D3"].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
            //ws.Cells["B3:D3"].Style.Font.Color.SetColor(Color.Red);
            #endregion

            ws.Cells["B6"].Value = "TL Kasa Hareketleri";
            ws.Cells["B6"].Style.Font.Size = 11;
            ws.Cells["B6:D6"].Merge = true;
            ws.Cells["B6"].Style.Font.Bold = true;
            ws.Cells["B6"].Style.WrapText = true;
            ws.Cells["B6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["B8"].Value = Util.Labels.Aciklama;
            ws.Cells["C8"].Value = Util.Labels.Giren;
            ws.Cells["D8"].Value = Util.Labels.Cikan;
            ws.Cells["B8"+":D8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["B8"+":D8"].Style.Font.Bold = true;

            foreach (Toplam itemToplam in sourceToplam)
            {
                ws.Cells["C" + 7].Value = " Devir :";
                ws.Cells["C" + 7+ ":D" + 7].Style.Font.Bold = true;
                ws.Cells["C" + 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells["D" + 7].Value = itemToplam.TOPLAM;
                ws.Cells["D" + 7].Style.Numberformat.Format = "₺#,##0.00";
            }

            #endregion

            int rowIndexBegin = 9;
            int rowIndexCurrentRecord = rowIndexBegin;
            foreach (KasaHareketleri item in source)
            {

                ws.Cells["B" + rowIndexCurrentRecord].Value = item.ACIKLAMA;
                ws.Cells["C" + rowIndexCurrentRecord].Value = item.GIREN;
                ws.Cells["D" + rowIndexCurrentRecord].Value = item.CIKAN;
                ws.Cells["C" + rowIndexCurrentRecord + ":C" + rowIndexCurrentRecord].Style.Numberformat.Format = "₺#,##0.00";
                ws.Cells["D" + rowIndexCurrentRecord + ":D" + rowIndexCurrentRecord].Style.Numberformat.Format = "₺#,##0.00";
                
                rowIndexCurrentRecord++;
            }
            ws.Column(1).Width = 8.29;
            ws.Column(2).Width = 102.57;
            ws.Column(3).Width = 24.57;
            ws.Column(4).Width = 24.57;
            int rowIndexEndOrders = rowIndexCurrentRecord - 1;
            var cellGrandTotalLabel = ws.Cells["B" + rowIndexCurrentRecord];
            cellGrandTotalLabel.Style.Font.Bold = true;
            cellGrandTotalLabel.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabel.Value = "TOPLAM";

            ws.Cells["B" + rowIndexCurrentRecord + ":C" + rowIndexCurrentRecord].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormula = ws.Cells["C" + rowIndexCurrentRecord];
            cellGrandTotalFormula.Style.Font.Bold = true;

            string sumFormula = "SUM(R[" + (rowIndexBegin - rowIndexCurrentRecord) + "]C:R[-1]C)";



            ws.Cells["B" + rowIndexCurrentRecord + ":D" + rowIndexCurrentRecord].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormulaD = ws.Cells["D" + rowIndexCurrentRecord];
            cellGrandTotalFormulaD.Style.Font.Bold = true;

            string sumFormulaD = "SUM(R[" + (rowIndexBegin - rowIndexCurrentRecord) + "]C:R[-1]C)";

            if (rowIndexBegin == rowIndexCurrentRecord)
            {
                cellGrandTotalFormula.FormulaR1C1 = "0";
                cellGrandTotalFormulaD.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula.FormulaR1C1 = sumFormula;
                cellGrandTotalFormulaD.FormulaR1C1 = sumFormulaD;
            }


            #region BAKİYE
            int rowIndexEndOrdersB = rowIndexCurrentRecord + 1;
            var cellGrandTotalLabelB = ws.Cells["B" + rowIndexEndOrdersB];
            cellGrandTotalLabelB.Style.Font.Bold = true;
            cellGrandTotalLabelB.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabelB.Value = "BAKİYE";

            ws.Cells[ "C" + rowIndexEndOrdersB + ":D" + rowIndexEndOrdersB].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormulaB = ws.Cells["C" + rowIndexEndOrdersB];
            cellGrandTotalFormulaB.Style.Font.Bold = true;
            ws.Cells["C" + rowIndexEndOrdersB + ":D" + rowIndexEndOrdersB].Merge = true;
            ws.Cells["C" + rowIndexEndOrdersB + ":D" + rowIndexEndOrdersB].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            string sumFormulaB = "SUM(D7"+"+C"+ rowIndexCurrentRecord + "-D"+ rowIndexCurrentRecord + ")";
            
            cellGrandTotalFormulaB.FormulaR1C1 = sumFormulaB;
            #endregion

            //ws.PrinterSettings.PrintArea = ws.Cells["B2:K" + rowIndexCurrentRecord];
            ws.Cells["B6:D" + rowIndexCurrentRecord].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["B6:D" + rowIndexCurrentRecord].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["B6:D" + rowIndexCurrentRecord].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["B6:D" + rowIndexCurrentRecord].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells["B7:D" + rowIndexCurrentRecord].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B7:D" + rowIndexCurrentRecord].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
           // ws.Cells["B3:D" + rowIndexCurrentRecord].Style.Font.Color.SetColor(Color.Red);
            
            #endregion

            #region UsdKasaHareketleri

            TrCurr = 1;
            SqlText = SqlReports.KasaHareketleri;
            List<KasaHareketleri> source1 = GetPocoList<KasaHareketleri>();
            #region Header

            ws.Cells["B" + (rowIndexCurrentRecord + 2)].Value = "USD Kasa Hareketleri";
            ws.Cells["B" + (rowIndexCurrentRecord + 2)].Style.Font.Size = 11;
            ws.Cells["B" + (rowIndexCurrentRecord + 2) + ":D" + (rowIndexCurrentRecord + 2)].Merge = true;
            ws.Cells["B" + (rowIndexCurrentRecord + 2)].Style.Font.Bold = true;
            ws.Cells["B" + (rowIndexCurrentRecord + 2)].Style.WrapText = true;
            ws.Cells["B" + (rowIndexCurrentRecord + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            ws.Cells["B" + (rowIndexCurrentRecord + 4)].Value = Util.Labels.Aciklama;
            ws.Cells["C" + (rowIndexCurrentRecord + 4)].Value = Util.Labels.Giren;
            ws.Cells["D" + (rowIndexCurrentRecord + 4)].Value = Util.Labels.Cikan;
            ws.Cells["B" + (rowIndexCurrentRecord + 4) + ":D" + (rowIndexCurrentRecord + 4)].Style.Font.Bold = true;
            ws.Cells["B" + (rowIndexCurrentRecord + 4) + ":D" + (rowIndexCurrentRecord + 4)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            #endregion
            SqlText = SqlReports.KasaToplami;
            List<Toplam> sourceToplam1 = GetPocoList<Toplam>();
            foreach (Toplam itemToplam1 in sourceToplam1)
            {
                ws.Cells["C" + (rowIndexCurrentRecord + 3)].Value = " Devir :";
                ws.Cells["C" + (rowIndexCurrentRecord + 3)+ ":D" + (rowIndexCurrentRecord + 3)].Style.Font.Bold = true;
                ws.Cells["C" + (rowIndexCurrentRecord + 3)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells["D" + (rowIndexCurrentRecord + 3)].Value = itemToplam1.TOPLAM;
                ws.Cells["D" + (rowIndexCurrentRecord + 3)].Style.Numberformat.Format = "$#,##0.00";
            }

            int rowIndexBegin1 = rowIndexEndOrders+6;
            int rowIndexCurrentRecord1 = rowIndexBegin1;


            foreach (KasaHareketleri item1 in source1)
            {


                ws.Cells["B" + (rowIndexCurrentRecord1)].Value = item1.ACIKLAMA;
                ws.Cells["C" + (rowIndexCurrentRecord1)].Value = item1.GIREN;
                ws.Cells["D" + (rowIndexCurrentRecord1)].Value = item1.CIKAN;
                ws.Cells["C" + (rowIndexCurrentRecord1) + ":C" + (rowIndexCurrentRecord1)].Style.Numberformat.Format = "$#,##0.00";
                ws.Cells["D" + (rowIndexCurrentRecord1) + ":D" + (rowIndexCurrentRecord1)].Style.Numberformat.Format = "$#,##0.00";
                

                rowIndexCurrentRecord1++;
            }

            int rowIndexEndOrders1 = (rowIndexCurrentRecord1+5) ;
            var cellGrandTotalLabel1 = ws.Cells["B" + rowIndexCurrentRecord1];
            cellGrandTotalLabel1.Style.Font.Bold = true;
            cellGrandTotalLabel1.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabel1.Value = "TOPLAM";

            ws.Cells["B" + rowIndexCurrentRecord1 + ":C" + rowIndexCurrentRecord1].Style.Numberformat.Format = "$#,##0.00";
            var cellGrandTotalFormula1 = ws.Cells["C" + rowIndexCurrentRecord1];
            cellGrandTotalFormula1.Style.Font.Bold = true;

            string sumFormula1 = "SUM(R[" + (rowIndexBegin1 -  rowIndexCurrentRecord1) + "]C:R[-1]C)";
            

            ws.Cells["B" + rowIndexCurrentRecord1 + ":D" + rowIndexCurrentRecord1].Style.Numberformat.Format = "$#,##0.00";
            var cellGrandTotalFormulaD1 = ws.Cells["D" + rowIndexCurrentRecord1];
            cellGrandTotalFormulaD1.Style.Font.Bold = true;

            string sumFormulaD1 = "SUM(R[" + (rowIndexBegin1 -  rowIndexCurrentRecord1) + "]C:R[-1]C)";
            

            if (rowIndexBegin1 == rowIndexCurrentRecord1)
            {
                cellGrandTotalFormula1.FormulaR1C1 = "0";
                cellGrandTotalFormulaD1.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula1.FormulaR1C1 = sumFormula1;
                cellGrandTotalFormulaD1.FormulaR1C1 = sumFormulaD1;
            }

            #region BAKİYE
            int rowIndexEndOrdersB1 = rowIndexCurrentRecord1 + 1;
            var cellGrandTotalLabelB1 = ws.Cells["B" + rowIndexEndOrdersB1];
            cellGrandTotalLabelB1.Style.Font.Bold = true;
            cellGrandTotalLabelB1.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabelB1.Value = "BAKİYE";

            ws.Cells["C" + rowIndexEndOrdersB1 + ":D" + rowIndexEndOrdersB1].Style.Numberformat.Format = "$#,##0.00";
            var cellGrandTotalFormulaB1 = ws.Cells["C" + rowIndexEndOrdersB1];
            cellGrandTotalFormulaB1.Style.Font.Bold = true;
            ws.Cells["C" + rowIndexEndOrdersB1 + ":D" + rowIndexEndOrdersB1].Merge = true;
            ws.Cells["C" + rowIndexEndOrdersB1 + ":D" + rowIndexEndOrdersB1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


            string sumFormulaB1 = "SUM(D"+(rowIndexCurrentRecord + 3) + "+C" + rowIndexCurrentRecord1 + "-D" + rowIndexCurrentRecord1 + ")";

            cellGrandTotalFormulaB1.FormulaR1C1 = sumFormulaB1;
            #endregion

            // ws.PrinterSettings.PrintArea = ws.Cells["B" + (rowIndexBegin1 - 2 ) + ":K" + (rowIndexCurrentRecord1)];
            ws.Cells["B" + (rowIndexBegin1 - 3) + ":D" + (rowIndexCurrentRecord1 )].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin1 - 3) + ":D" + (rowIndexCurrentRecord1 )].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin1 - 3) + ":D" + (rowIndexCurrentRecord1 )].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin1 - 3) + ":D" + (rowIndexCurrentRecord1 )].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin1 - 3) + ":D" + (rowIndexCurrentRecord1)].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + (rowIndexBegin1 - 3) + ":D" + (rowIndexCurrentRecord1)].Style.Fill.BackgroundColor.SetColor(Color.Lavender);
            

            #endregion

            #region EuroKasaHareketleri

            TrCurr = 20;
            SqlText = SqlReports.KasaHareketleri;
            List<KasaHareketleri> source2 = GetPocoList<KasaHareketleri>();
            #region Header

            ws.Cells["B" + (rowIndexCurrentRecord1 +2)].Value = "EURO Kasa Hareketleri";
            ws.Cells["B" + (rowIndexCurrentRecord1+ 2)].Style.Font.Size = 11;
            ws.Cells["B" + (rowIndexCurrentRecord1+ 2) + ":D" + (rowIndexCurrentRecord1 + 2)].Merge = true;
            ws.Cells["B" + (rowIndexCurrentRecord1+ 2)].Style.Font.Bold = true;
            ws.Cells["B" + (rowIndexCurrentRecord1+ 2)].Style.WrapText = true;
            ws.Cells["B" + (rowIndexCurrentRecord1+ 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            ws.Cells["B" + (rowIndexCurrentRecord1 + 4)].Value = Util.Labels.Aciklama;
            ws.Cells["C" + (rowIndexCurrentRecord1+  4)].Value = Util.Labels.Giren;
            ws.Cells["D" + (rowIndexCurrentRecord1 + 4)].Value = Util.Labels.Cikan;
            ws.Cells["B" + (rowIndexCurrentRecord1 + 4) + ":D" + (rowIndexCurrentRecord1 + 4)].Style.Font.Bold = true;
            ws.Cells["B" + (rowIndexCurrentRecord1 + 4) + ":D" + (rowIndexCurrentRecord1 + 4)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            #endregion

            SqlText = SqlReports.KasaToplami;
            List<Toplam> sourceToplam2 = GetPocoList<Toplam>();
            foreach (Toplam itemToplam2 in sourceToplam2)
            {
                ws.Cells["C" + (rowIndexCurrentRecord1 + 3)].Value = " Devir :";
                ws.Cells["C" + (rowIndexCurrentRecord1 + 3)+ ":D" + (rowIndexCurrentRecord1 + 3)].Style.Font.Bold = true;
                ws.Cells["C" + (rowIndexCurrentRecord1 + 3)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells["D" + (rowIndexCurrentRecord1 + 3)].Value = itemToplam2.TOPLAM;
                ws.Cells["D" + (rowIndexCurrentRecord1 + 3)].Style.Numberformat.Format = "€#,##0.00";
            }

            int rowIndexBegin2 = rowIndexEndOrders1 ;
            int rowIndexCurrentRecord2 = rowIndexBegin2;
            foreach (KasaHareketleri item2 in source2)
            {


                ws.Cells["B" + (rowIndexCurrentRecord2)].Value = item2.ACIKLAMA;
                ws.Cells["C" + (rowIndexCurrentRecord2)].Value = item2.GIREN;
                ws.Cells["D" + (rowIndexCurrentRecord2)].Value = item2.CIKAN;
                ws.Cells["C" + (rowIndexCurrentRecord2) + ":D" + (rowIndexCurrentRecord2)].Style.Numberformat.Format = "€#,##0.00";
                ws.Cells["D" + (rowIndexCurrentRecord2) + ":D" + (rowIndexCurrentRecord2)].Style.Numberformat.Format = "€#,##0.00";
              

                rowIndexCurrentRecord2++;
            }

            int rowIndexEndOrders2 = (rowIndexCurrentRecord2 + 5);
            var cellGrandTotalLabel2 = ws.Cells["B" + rowIndexCurrentRecord2];
            cellGrandTotalLabel2.Style.Font.Bold = true;
            cellGrandTotalLabel2.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabel2.Value = "TOPLAM";

            ws.Cells["B" + rowIndexCurrentRecord2 + ":C" + rowIndexCurrentRecord2].Style.Numberformat.Format = "€#,##0.00";
            var cellGrandTotalFormula2 = ws.Cells["C" + rowIndexCurrentRecord2];
            cellGrandTotalFormula2.Style.Font.Bold = true;

            string sumFormula2 = "SUM(R[" + (rowIndexBegin2 - rowIndexCurrentRecord2) + "]C:R[-1]C)";
          

            ws.Cells["B" + rowIndexCurrentRecord2 + ":D" + rowIndexCurrentRecord2].Style.Numberformat.Format = "€#,##0.00";
            var cellGrandTotalFormulaD2 = ws.Cells["D" + rowIndexCurrentRecord2];
            cellGrandTotalFormulaD2.Style.Font.Bold = true;

            string sumFormulaD2 = "SUM(R[" + (rowIndexBegin2 - rowIndexCurrentRecord2) + "]C:R[-1]C)";
       

            if (rowIndexBegin2 == rowIndexCurrentRecord2)
            {
                cellGrandTotalFormula2.FormulaR1C1 = "0";
                cellGrandTotalFormulaD2.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula2.FormulaR1C1 = sumFormula2;
                cellGrandTotalFormulaD2.FormulaR1C1 = sumFormulaD2;
            }

            #region BAKİYE
            int rowIndexEndOrdersB2 = rowIndexCurrentRecord2 + 1;
            var cellGrandTotalLabelB2 = ws.Cells["B" + rowIndexEndOrdersB2];
            cellGrandTotalLabelB2.Style.Font.Bold = true;
            cellGrandTotalLabelB2.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabelB2.Value = "BAKİYE";

            ws.Cells["C" + rowIndexEndOrdersB2 + ":D" + rowIndexEndOrdersB2].Style.Numberformat.Format = "€#,##0.00";
            var cellGrandTotalFormulaB2 = ws.Cells["C" + rowIndexEndOrdersB2];
            cellGrandTotalFormulaB2.Style.Font.Bold = true;
            ws.Cells["C" + rowIndexEndOrdersB2 + ":D" + rowIndexEndOrdersB2].Merge = true;
            ws.Cells["C" + rowIndexEndOrdersB2 + ":D" + rowIndexEndOrdersB2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


            string sumFormulaB2 = "SUM(D" + (rowIndexCurrentRecord1 + 3) + "+C" + rowIndexCurrentRecord2 + "-D" + rowIndexCurrentRecord2 + ")";

            cellGrandTotalFormulaB2.FormulaR1C1 = sumFormulaB2;
            #endregion


            // ws.PrinterSettings.PrintArea = ws.Cells["B" + (rowIndexBegin2 - 2) + ":K" + (rowIndexCurrentRecord2)];
            ws.Cells["B" + (rowIndexBegin2 - 3) + ":D" + (rowIndexCurrentRecord2)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin2 - 3) + ":D" + (rowIndexCurrentRecord2)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin2 - 3) + ":D" + (rowIndexCurrentRecord2)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin2 - 3) + ":D" + (rowIndexCurrentRecord2)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin2 - 3) + ":D" + (rowIndexCurrentRecord2)].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + (rowIndexBegin2 - 3) + ":D" + (rowIndexCurrentRecord2)].Style.Fill.BackgroundColor.SetColor(Color.Khaki);
            

            #endregion

            #region TlBankaHareketleri

            TrCurr = 0;
            SqlText = SqlReports.BankaHareketleri;
            List<KasaHareketleri> source3 = GetPocoList<KasaHareketleri>();
            #region Header

            ws.Cells["B" + (rowIndexCurrentRecord2 + 2)].Value = "TL Banka Hareketleri";
            ws.Cells["B" + (rowIndexCurrentRecord2 + 2)].Style.Font.Size = 11;
            ws.Cells["B" + (rowIndexCurrentRecord2 + 2) + 
                    ":D" + (rowIndexCurrentRecord2 + 2)].Merge = true;
            ws.Cells["B" + (rowIndexCurrentRecord2 + 2)].Style.Font.Bold = true;
            ws.Cells["B" + (rowIndexCurrentRecord2 + 2)].Style.WrapText = true;
            ws.Cells["B" + (rowIndexCurrentRecord2 + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["B" + (rowIndexCurrentRecord2 + 4)].Value = Util.Labels.Aciklama;
            ws.Cells["C" + (rowIndexCurrentRecord2 + 4)].Value = Util.Labels.Giren;
            ws.Cells["D" + (rowIndexCurrentRecord2 + 4)].Value = Util.Labels.Cikan;
            ws.Cells["B" + (rowIndexCurrentRecord2 + 4) + ":D" + (rowIndexCurrentRecord2 + 4)].Style.Font.Bold = true;
            ws.Cells["B" + (rowIndexCurrentRecord2 + 4) + ":D" + (rowIndexCurrentRecord2 + 4)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            #endregion

            SqlText = SqlReports.BankaHareketleriToplam;
            List<Toplam> sourceToplam3 = GetPocoList<Toplam>();
            foreach (Toplam itemToplam3 in sourceToplam3)
            {
                ws.Cells["C" + (rowIndexCurrentRecord2 + 3)].Value = " Devir :";
                ws.Cells["C" + (rowIndexCurrentRecord2 + 3)+ ":D" + (rowIndexCurrentRecord2 + 3)].Style.Font.Bold = true;
                ws.Cells["C" + (rowIndexCurrentRecord2 + 3)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells["D" + (rowIndexCurrentRecord2 + 3)].Value = itemToplam3.TOPLAM;
                ws.Cells["D" + (rowIndexCurrentRecord2 + 3)].Style.Numberformat.Format = "₺#,##0.00";
            }

            int rowIndexBegin3 = rowIndexEndOrders2;
            int rowIndexCurrentRecord3 = rowIndexBegin3;
            foreach (KasaHareketleri item3 in source3)
            {


                ws.Cells["B" + (rowIndexCurrentRecord3)].Value = item3.ACIKLAMA;
                ws.Cells["C" + (rowIndexCurrentRecord3)].Value = item3.GIREN;
                ws.Cells["D" + (rowIndexCurrentRecord3)].Value = item3.CIKAN;
                ws.Cells["C" + (rowIndexCurrentRecord3) + 
                        ":D" + (rowIndexCurrentRecord3)].Style.Numberformat.Format = "₺#,##0.00";
                ws.Cells["D" + (rowIndexCurrentRecord3) + 
                        ":D" + (rowIndexCurrentRecord3)].Style.Numberformat.Format = "₺#,##0.00";
                

                rowIndexCurrentRecord3++;
            }

            int rowIndexEndOrders3 = (rowIndexCurrentRecord3 + 5);
            var cellGrandTotalLabel3 = ws.Cells["B" + rowIndexCurrentRecord3];
            cellGrandTotalLabel3.Style.Font.Bold = true;
            cellGrandTotalLabel3.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabel3.Value = "TOPLAM";

            ws.Cells["B" + rowIndexCurrentRecord3 + ":C" + rowIndexCurrentRecord3].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormula3 = ws.Cells["C" + rowIndexCurrentRecord3];
            cellGrandTotalFormula3.Style.Font.Bold = true;

            string sumFormula3 = "SUM(R[" + (rowIndexBegin3 - rowIndexCurrentRecord3) + "]C:R[-1]C)";
            

            ws.Cells["B" + rowIndexCurrentRecord3 + ":D" + rowIndexCurrentRecord3].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormulaD3 = ws.Cells["D" + rowIndexCurrentRecord3];
            cellGrandTotalFormulaD3.Style.Font.Bold = true;

            string sumFormulaD3 = "SUM(R[" + (rowIndexBegin3 - rowIndexCurrentRecord3) + "]C:R[-1]C)";
           

            if (rowIndexBegin3 == rowIndexCurrentRecord3)
            {
                cellGrandTotalFormula3.FormulaR1C1 = "0";
                cellGrandTotalFormulaD3.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula3.FormulaR1C1 = sumFormula3;
                cellGrandTotalFormulaD3.FormulaR1C1 = sumFormula3;
            }

            #region BAKİYE
            int rowIndexEndOrdersB3 = rowIndexCurrentRecord3 + 1;
            var cellGrandTotalLabelB3 = ws.Cells["B" + rowIndexEndOrdersB3];
            cellGrandTotalLabelB3.Style.Font.Bold = true;
            cellGrandTotalLabelB3.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabelB3.Value = "BAKİYE";

            ws.Cells["C" + rowIndexEndOrdersB3 + ":D" + rowIndexEndOrdersB3].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormulaB3 = ws.Cells["C" + rowIndexEndOrdersB3];
            cellGrandTotalFormulaB3.Style.Font.Bold = true;
            ws.Cells["C" + rowIndexEndOrdersB3 + ":D" + rowIndexEndOrdersB3].Merge = true;
            ws.Cells["C" + rowIndexEndOrdersB3 + ":D" + rowIndexEndOrdersB3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


            string sumFormulaB3 = "SUM(D" + (rowIndexCurrentRecord2 + 3) + "+C" + rowIndexCurrentRecord3 + "-D" + rowIndexCurrentRecord3 + ")";

            cellGrandTotalFormulaB3.FormulaR1C1 = sumFormulaB3;
            #endregion

            //ws.PrinterSettings.PrintArea = ws.Cells["B" + (rowIndexBegin3 - 2) + ":K" + (rowIndexCurrentRecord3)];
            ws.Cells["B" + (rowIndexBegin3 - 3) + ":D" + (rowIndexCurrentRecord3)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin3 - 3) + ":D" + (rowIndexCurrentRecord3)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin3 - 3) + ":D" + (rowIndexCurrentRecord3)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin3 - 3) + ":D" + (rowIndexCurrentRecord3)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin3 - 3) + ":D" + (rowIndexCurrentRecord3)].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + (rowIndexBegin3 - 3) + ":D" + (rowIndexCurrentRecord3)].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
            

            #endregion

            #region USDBankaHareketleri

            TrCurr = 1;
            SqlText = SqlReports.BankaHareketleri;
            List<KasaHareketleri> source4 = GetPocoList<KasaHareketleri>();
            #region Header

            ws.Cells["B" + (rowIndexCurrentRecord3 + 2)].Value = "USD Banka Hareketleri";
            ws.Cells["B" + (rowIndexCurrentRecord3 + 2)].Style.Font.Size = 11;
            ws.Cells["B" + (rowIndexCurrentRecord3 + 2) +
                    ":D" + (rowIndexCurrentRecord3 + 2)].Merge = true;
            ws.Cells["B" + (rowIndexCurrentRecord3 + 2)].Style.Font.Bold = true;
            ws.Cells["B" + (rowIndexCurrentRecord3 + 2)].Style.WrapText = true;
            ws.Cells["B" + (rowIndexCurrentRecord3 + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["B" + (rowIndexCurrentRecord3 + 4)].Value = Util.Labels.Aciklama;
            ws.Cells["C" + (rowIndexCurrentRecord3 + 4)].Value = Util.Labels.Giren;
            ws.Cells["D" + (rowIndexCurrentRecord3 + 4)].Value = Util.Labels.Cikan;
            ws.Cells["B" + (rowIndexCurrentRecord3 + 4) + ":D" + (rowIndexCurrentRecord3 + 4)].Style.Font.Bold = true;
            ws.Cells["B" + (rowIndexCurrentRecord3 + 4) + ":D" + (rowIndexCurrentRecord3 + 4)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            #endregion

            SqlText = SqlReports.BankaHareketleriToplam;
            List<Toplam> sourceToplam4 = GetPocoList<Toplam>();
            foreach (Toplam itemToplam4 in sourceToplam4)
            {
                ws.Cells["C" + (rowIndexCurrentRecord3 + 3)].Value = " Devir :";
                ws.Cells["C" + (rowIndexCurrentRecord3 + 3)+ ":D" + (rowIndexCurrentRecord3 + 3)].Style.Font.Bold = true;
                ws.Cells["C" + (rowIndexCurrentRecord3 + 3)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells["D" + (rowIndexCurrentRecord3 + 3)].Value = itemToplam4.TOPLAM;
                ws.Cells["D" + (rowIndexCurrentRecord3 + 3)].Style.Numberformat.Format = "$#,##0.00";
            }

            int rowIndexBegin4 = rowIndexEndOrders3;
            int rowIndexCurrentRecord4 = rowIndexBegin4;
            foreach (KasaHareketleri item4 in source4)
            {


                ws.Cells["B" + (rowIndexCurrentRecord4)].Value = item4.ACIKLAMA;
                ws.Cells["C" + (rowIndexCurrentRecord4)].Value = item4.GIREN;
                ws.Cells["D" + (rowIndexCurrentRecord4)].Value = item4.CIKAN;
                ws.Cells["C" + (rowIndexCurrentRecord4) +
                        ":D" + (rowIndexCurrentRecord4)].Style.Numberformat.Format = "$#,##0.00";
                ws.Cells["D" + (rowIndexCurrentRecord4) +
                        ":D" + (rowIndexCurrentRecord4)].Style.Numberformat.Format = "$#,##0.00";
               

                rowIndexCurrentRecord4++;
            }

            int rowIndexEndOrders4 = (rowIndexCurrentRecord4 + 5);
            var cellGrandTotalLabel4 = ws.Cells["B" + rowIndexCurrentRecord4];
            cellGrandTotalLabel4.Style.Font.Bold = true;
            cellGrandTotalLabel4.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabel4.Value = "TOPLAM";

            ws.Cells["B" + rowIndexCurrentRecord4 + ":C" + rowIndexCurrentRecord4].Style.Numberformat.Format = "$#,##0.00";
            var cellGrandTotalFormula4 = ws.Cells["C" + rowIndexCurrentRecord4];
            cellGrandTotalFormula4.Style.Font.Bold = true;

            string sumFormula4 = "SUM(R[" + (rowIndexBegin4 - rowIndexCurrentRecord4) + "]C:R[-1]C)";
            

            ws.Cells["B" + rowIndexCurrentRecord4 + ":D" + rowIndexCurrentRecord4].Style.Numberformat.Format = "$#,##0.00";
            var cellGrandTotalFormulaD4 = ws.Cells["D" + rowIndexCurrentRecord4];
            cellGrandTotalFormulaD4.Style.Font.Bold = true;

            string sumFormulaD4 = "SUM(R[" + (rowIndexBegin4 - rowIndexCurrentRecord4) + "]C:R[-1]C)";
            


            if (rowIndexBegin4 == rowIndexCurrentRecord4)
            {
                cellGrandTotalFormula4.FormulaR1C1 = "0";
                cellGrandTotalFormulaD4.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula4.FormulaR1C1 = sumFormula4;
                cellGrandTotalFormulaD4.FormulaR1C1 = sumFormula4;
            }

            #region BAKİYE
            int rowIndexEndOrdersB4 = rowIndexCurrentRecord4 + 1;
            var cellGrandTotalLabelB4 = ws.Cells["B" + rowIndexEndOrdersB4];
            cellGrandTotalLabelB4.Style.Font.Bold = true;
            cellGrandTotalLabelB4.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabelB4.Value = "BAKİYE";

            ws.Cells["C" + rowIndexEndOrdersB4 + ":D" + rowIndexEndOrdersB4].Style.Numberformat.Format = "$#,##0.00";
            var cellGrandTotalFormulaB4 = ws.Cells["C" + rowIndexEndOrdersB4];
            cellGrandTotalFormulaB4.Style.Font.Bold = true;
            ws.Cells["C" + rowIndexEndOrdersB4 + ":D" + rowIndexEndOrdersB4].Merge = true;
            ws.Cells["C" + rowIndexEndOrdersB4 + ":D" + rowIndexEndOrdersB4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


            string sumFormulaB4 = "SUM(D" + (rowIndexCurrentRecord3 + 3) + "+C" + rowIndexCurrentRecord4 + "-D" + rowIndexCurrentRecord4 + ")";

            cellGrandTotalFormulaB4.FormulaR1C1 = sumFormulaB4;
            #endregion

            //ws.PrinterSettings.PrintArea = ws.Cells["B" + (rowIndexBegin4 - 2) + ":K" + (rowIndexCurrentRecord4)];
            ws.Cells["B" + (rowIndexBegin4 - 3) + ":D" + (rowIndexCurrentRecord4)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin4 - 3) + ":D" + (rowIndexCurrentRecord4)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin4 - 3) + ":D" + (rowIndexCurrentRecord4)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin4 - 3) + ":D" + (rowIndexCurrentRecord4)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin4 - 3) + ":D" + (rowIndexCurrentRecord4)].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + (rowIndexBegin4 - 3) + ":D" + (rowIndexCurrentRecord4)].Style.Fill.BackgroundColor.SetColor(Color.Lavender);
            

            #endregion

            #region EURBankaHareketleri

            TrCurr = 20;
            SqlText = SqlReports.BankaHareketleri;
            List<KasaHareketleri> source5 = GetPocoList<KasaHareketleri>();
            #region Header

            ws.Cells["B" + (rowIndexCurrentRecord4 + 2)].Value = "EURO Banka Hareketleri";
            ws.Cells["B" + (rowIndexCurrentRecord4 + 2)].Style.Font.Size = 11;
            ws.Cells["B" + (rowIndexCurrentRecord4 + 2) +
                    ":D" + (rowIndexCurrentRecord4 + 2)].Merge = true;
            ws.Cells["B" + (rowIndexCurrentRecord4 + 2)].Style.Font.Bold = true;
            ws.Cells["B" + (rowIndexCurrentRecord4 + 2)].Style.WrapText = true;
            ws.Cells["B" + (rowIndexCurrentRecord4 + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["B" + (rowIndexCurrentRecord4 + 4)].Value = Util.Labels.Aciklama;
            ws.Cells["C" + (rowIndexCurrentRecord4 + 4)].Value = Util.Labels.Giren;
            ws.Cells["D" + (rowIndexCurrentRecord4 + 4)].Value = Util.Labels.Cikan;
            ws.Cells["B" + (rowIndexCurrentRecord4 + 4) + ":D" + (rowIndexCurrentRecord4 + 4)].Style.Font.Bold = true;
            ws.Cells["B" + (rowIndexCurrentRecord4 + 4) + ":D" + (rowIndexCurrentRecord4 + 4)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            #endregion

            SqlText = SqlReports.BankaHareketleriToplam;
            List<Toplam> sourceToplam5 = GetPocoList<Toplam>();
            foreach (Toplam itemToplam5 in sourceToplam5)
            {
                ws.Cells["C" + (rowIndexCurrentRecord4 + 3)].Value = " Devir :";
                ws.Cells["C" + (rowIndexCurrentRecord4 + 3)+ ":D" + (rowIndexCurrentRecord4 + 3)].Style.Font.Bold = true;
                ws.Cells["C" + (rowIndexCurrentRecord4 + 3)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells["D" + (rowIndexCurrentRecord4 + 3)].Value = itemToplam5.TOPLAM;
                ws.Cells["D" + (rowIndexCurrentRecord4 + 3)].Style.Numberformat.Format = "€#,##0.00";
            }

            int rowIndexBegin5 = rowIndexEndOrders4;
            int rowIndexCurrentRecord5 = rowIndexBegin5;
            foreach (KasaHareketleri item5 in source5)
            {


                ws.Cells["B" + (rowIndexCurrentRecord5)].Value = item5.ACIKLAMA;
                ws.Cells["C" + (rowIndexCurrentRecord5)].Value = item5.GIREN;
                ws.Cells["D" + (rowIndexCurrentRecord5)].Value = item5.CIKAN;
                ws.Cells["C" + (rowIndexCurrentRecord5) +
                        ":D" + (rowIndexCurrentRecord5)].Style.Numberformat.Format = "€#,##0.00";
                ws.Cells["D" + (rowIndexCurrentRecord5) +
                        ":D" + (rowIndexCurrentRecord5)].Style.Numberformat.Format = "€#,##0.00";
                

                rowIndexCurrentRecord5++;
            }

            int rowIndexEndOrders5 = (rowIndexCurrentRecord5 + 5);
            var cellGrandTotalLabel5 = ws.Cells["B" + rowIndexCurrentRecord5];
            cellGrandTotalLabel5.Style.Font.Bold = true;
            cellGrandTotalLabel5.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabel5.Value = "TOPLAM";

            ws.Cells["B" + rowIndexCurrentRecord5 + 
                    ":C" + rowIndexCurrentRecord5].Style.Numberformat.Format = "€#,##0.00";
            var            cellGrandTotalFormula5 = ws.Cells["C" + 
                           rowIndexCurrentRecord5];
                           cellGrandTotalFormula5.Style.Font.Bold = true;

            string sumFormula5 = "SUM(R[" + (rowIndexBegin5 - rowIndexCurrentRecord5) + "]C:R[-1]C)";
            

            ws.Cells["B" + rowIndexCurrentRecord5 + ":D" + rowIndexCurrentRecord5].Style.Numberformat.Format = "€#,##0.00";
            var cellGrandTotalFormulaD5 = ws.Cells["D" + rowIndexCurrentRecord5];
            cellGrandTotalFormulaD5.Style.Font.Bold = true;

            string sumFormulaD5 = "SUM(R[" + (rowIndexBegin5 - rowIndexCurrentRecord5) + "]C:R[-1]C)";
            

            if (rowIndexBegin5 == rowIndexCurrentRecord5)
            {
                cellGrandTotalFormula5.FormulaR1C1 = "0";
                cellGrandTotalFormulaD5.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula5.FormulaR1C1 = sumFormula5;
                cellGrandTotalFormulaD5.FormulaR1C1 = sumFormula5;
            }

            #region BAKİYE
            int rowIndexEndOrdersB5 = rowIndexCurrentRecord5 + 1;
            var cellGrandTotalLabelB5 = ws.Cells["B" + rowIndexEndOrdersB5];
            cellGrandTotalLabelB5.Style.Font.Bold = true;
            cellGrandTotalLabelB5.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabelB5.Value = "BAKİYE";

            ws.Cells["C" + rowIndexEndOrdersB5 + ":D" + rowIndexEndOrdersB5].Style.Numberformat.Format = "€#,##0.00";
            var cellGrandTotalFormulaB5 = ws.Cells["C" + rowIndexEndOrdersB5];
            cellGrandTotalFormulaB5.Style.Font.Bold = true;
            ws.Cells["C" + rowIndexEndOrdersB5 + ":D" + rowIndexEndOrdersB5].Merge = true;
            ws.Cells["C" + rowIndexEndOrdersB5 + ":D" + rowIndexEndOrdersB5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


            string sumFormulaB5 = "SUM(D" + (rowIndexCurrentRecord4 + 3) + "+C" + rowIndexCurrentRecord5 + "-D" + rowIndexCurrentRecord5 + ")";

            cellGrandTotalFormulaB5.FormulaR1C1 = sumFormulaB5;
            #endregion

            #region UstBilgi

            #region Toplam TL

            ws.Cells["C" + 2].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormulaTL = ws.Cells["C" + 2];
            cellGrandTotalFormulaTL.Style.Font.Bold = true;
            cellGrandTotalFormulaTL.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            string sumFormulaTL = "SUM(C" + rowIndexEndOrdersB + "+C" + rowIndexEndOrdersB3 + ")";
            cellGrandTotalFormulaTL.FormulaR1C1 = sumFormulaTL;
            #endregion

            #region Toplam Euro

            ws.Cells["C" + 3].Style.Numberformat.Format = "€#,##0.00";
            var cellGrandTotalFormulaEur = ws.Cells["C" + 3];
            cellGrandTotalFormulaEur.Style.Font.Bold = true;
            cellGrandTotalFormulaEur.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            string sumFormulaEur = "SUM(C" + rowIndexEndOrdersB2 + "+C" + rowIndexEndOrdersB5 + ")";
            cellGrandTotalFormulaEur.FormulaR1C1 = sumFormulaEur;
            #endregion

            #region Toplam USD

            ws.Cells["C" + 4].Style.Numberformat.Format = "$#,##0.00";
            var cellGrandTotalFormulaUsd = ws.Cells["C" + 4];
            cellGrandTotalFormulaUsd.Style.Font.Bold = true;
            cellGrandTotalFormulaUsd.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            string sumFormulaUsd = "SUM(C" + rowIndexEndOrdersB1 + "+C" + rowIndexEndOrdersB4 + ")";
            cellGrandTotalFormulaUsd.FormulaR1C1 = sumFormulaUsd;
            #endregion

            #region TOPLAM NAKDİN TL KARŞILIĞI 

            ws.Cells["C" + 5].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormulaToplamTl = ws.Cells["C" + 5];
            cellGrandTotalFormulaToplamTl.Style.Font.Bold = true;
            cellGrandTotalFormulaToplamTl.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            string sumFormulaToplamTl = "SUM(C2" + "+(C3*C1)+(C4*D1)"+")";
           // ₺ $  €
            cellGrandTotalFormulaToplamTl.FormulaR1C1 = sumFormulaToplamTl;
            #endregion

            #endregion

            ws.PrinterSettings.PrintArea = ws.Cells["B2" + ":K" + (rowIndexCurrentRecord5)];
            ws.Cells["B" + (rowIndexBegin5 - 3) + ":D" + (rowIndexCurrentRecord5)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin5 - 3) + ":D" + (rowIndexCurrentRecord5)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin5 - 3) + ":D" + (rowIndexCurrentRecord5)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin5 - 3) + ":D" + (rowIndexCurrentRecord5)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + (rowIndexBegin5 - 3) + ":D" + (rowIndexCurrentRecord5)].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + (rowIndexBegin5 - 3) + ":D" + (rowIndexCurrentRecord5)].Style.Fill.BackgroundColor.SetColor(Color.Khaki);
            

            #endregion

            #region TlÇekSenetHareketleri
            TrCurr = 0;
            SqlText = SqlReports.CekSenetHareketleri;
            List<CekSenetHareketleri> source6 = GetPocoList<CekSenetHareketleri>();


            #region Header

            ws.Cells["F" + 2].Value = "TL Çek Senet Hareketleri";
            ws.Cells["F" + 2].Style.Font.Size = 11;
            ws.Cells["F2:I2"].Merge = true;
            ws.Cells["F2"].Style.Font.Bold = true;
            ws.Cells["F2"].Style.WrapText = true;
            ws.Cells["F2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["F" + 4].Value = Util.Labels.VadeGunu;
            ws.Cells["G" + 4].Value = Util.Labels.Aciklama;
            ws.Cells["H" + 4].Value = Util.Labels.Giren;
            ws.Cells["I" + 4].Value = Util.Labels.Cikan;
            ws.Cells["F4:I4"].Style.Font.Bold = true;
            ws.Cells["F4:I4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            
            #endregion

            SqlText = SqlReports.CekSenetToplam;
            List<Toplam> sourceToplam6 = GetPocoList<Toplam>();
            foreach (Toplam itemToplam6 in sourceToplam6)
            {
                ws.Cells["H" + 3].Value = " Devir :";
                ws.Cells["H" + 3+ ":I" + 3].Style.Font.Bold = true;
                ws.Cells["H" + 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells["I" + 3].Value = itemToplam6.TOPLAM;
                ws.Cells["I" + 3].Style.Numberformat.Format = "₺#,##0.00";
            }

            int rowIndexBegin6 = 5;
            int rowIndexCurrentRecord6 = rowIndexBegin6;
            foreach (CekSenetHareketleri item6 in source6)
            {

                ws.Cells["F" + rowIndexCurrentRecord6].Value = item6.VADE;
                ws.Cells["G" + rowIndexCurrentRecord6].Value = item6.ACIKLAMA;
                ws.Cells["H" + rowIndexCurrentRecord6].Value = item6.GIREN;
                ws.Cells["I" + rowIndexCurrentRecord6].Value = item6.CIKAN; 
                ws.Cells["F" + rowIndexCurrentRecord6 + ":F" + rowIndexCurrentRecord6].Style.Numberformat.Format = "dd - MM - yyyy";
                ws.Cells["H" + rowIndexCurrentRecord6 + ":H" + rowIndexCurrentRecord6].Style.Numberformat.Format = "₺#,##0.00";
                ws.Cells["I" + rowIndexCurrentRecord6 + ":I" + rowIndexCurrentRecord6].Style.Numberformat.Format = "₺#,##0.00";
                
                rowIndexCurrentRecord6++;
            }

            int rowIndexEndOrders6 = rowIndexCurrentRecord6 - 1;
            var cellGrandTotalLabel6 = ws.Cells["G" + rowIndexCurrentRecord6];
            cellGrandTotalLabel6.Style.Font.Bold = true;
            cellGrandTotalLabel6.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabel6.Value = "TOPLAM";

            ws.Cells["G" + rowIndexCurrentRecord6 + ":H" + rowIndexCurrentRecord6].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormula6 = ws.Cells["H" + rowIndexCurrentRecord6];
            cellGrandTotalFormula6.Style.Font.Bold = true;

            string sumFormula6 = "SUM(R[" + (rowIndexBegin6 - rowIndexCurrentRecord6) + "]C:R[-1]C)";
            

            ws.Cells["G" + rowIndexCurrentRecord6 + ":I" + rowIndexCurrentRecord6].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormulaD6 = ws.Cells["I" + rowIndexCurrentRecord6];
            cellGrandTotalFormulaD6.Style.Font.Bold = true;

            string sumFormulaD6 = "SUM(R[" + (rowIndexBegin6 - rowIndexCurrentRecord6) + "]C:R[-1]C)";

            if (rowIndexBegin6 == rowIndexCurrentRecord6)
            {
                cellGrandTotalFormula6.FormulaR1C1 = "0";
                cellGrandTotalFormulaD6.FormulaR1C1 = "0";
                
            }
            else
            {
                cellGrandTotalFormula6.FormulaR1C1 = sumFormula6;
                cellGrandTotalFormulaD6.FormulaR1C1 = sumFormula6;
            }

            #region BAKİYE
            int rowIndexEndOrdersB6 = rowIndexCurrentRecord6 + 1;
            var cellGrandTotalLabelB6 = ws.Cells["G" + rowIndexEndOrdersB6];
            cellGrandTotalLabelB6.Style.Font.Bold = true;
            cellGrandTotalLabelB6.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabelB6.Value = "BAKİYE";

            ws.Cells["H" + rowIndexEndOrdersB6 + ":I" + rowIndexEndOrdersB6].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormulaB6 = ws.Cells["H" + rowIndexEndOrdersB6];
            cellGrandTotalFormulaB6.Style.Font.Bold = true;
            ws.Cells["H" + rowIndexEndOrdersB6 + ":I" + rowIndexEndOrdersB6].Merge = true;
            ws.Cells["H" + rowIndexEndOrdersB6 + ":I" + rowIndexEndOrdersB6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


            string sumFormulaB6 = "SUM(I3" + "+H" + rowIndexCurrentRecord6 + "-I" + rowIndexCurrentRecord6 + ")";

            cellGrandTotalFormulaB6.FormulaR1C1 = sumFormulaB6;
            #endregion

            ws.Cells["F3:I" + rowIndexCurrentRecord6].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["F3:I" + rowIndexCurrentRecord6].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["F3:I" + rowIndexCurrentRecord6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["F3:I" + rowIndexCurrentRecord6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells["F3:I" + rowIndexCurrentRecord6].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["F3:I" + rowIndexCurrentRecord6].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
            

            #endregion

            #region UsdÇekSenetHareketleri

            TrCurr = 1;
            SqlText = SqlReports.CekSenetHareketleri;
            List<CekSenetHareketleri> source7 = GetPocoList<CekSenetHareketleri>();
            #region Header

            ws.Cells["F" + (rowIndexCurrentRecord6 + 2)].Value = "USD Çek Senet Hareketleri";
            ws.Cells["F" + (rowIndexCurrentRecord6 + 2)].Style.Font.Size = 11;
            ws.Cells["F" + (rowIndexCurrentRecord6 + 2) 
                  + ":I" + (rowIndexCurrentRecord6 + 2)].Merge = true;
            ws.Cells["F" + (rowIndexCurrentRecord6 + 2)].Style.Font.Bold = true;
            ws.Cells["F" + (rowIndexCurrentRecord6 + 2)].Style.WrapText = true;
            ws.Cells["F" + (rowIndexCurrentRecord6 + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["F" + (rowIndexCurrentRecord6 + 4)].Value = Util.Labels.VadeGunu;
            ws.Cells["G" + (rowIndexCurrentRecord6 + 4)].Value = Util.Labels.Aciklama;
            ws.Cells["H" + (rowIndexCurrentRecord6 + 4)].Value = Util.Labels.Giren;
            ws.Cells["I" + (rowIndexCurrentRecord6 + 4)].Value = Util.Labels.Cikan;
            ws.Cells["F" + (rowIndexCurrentRecord6 + 4) + ":I" + (rowIndexCurrentRecord6 + 4)].Style.Font.Bold = true;
            ws.Cells["F" + (rowIndexCurrentRecord6 + 4) + ":I" + (rowIndexCurrentRecord6 + 4)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            #endregion

            SqlText = SqlReports.CekSenetToplam;
            List<Toplam> sourceToplam7 = GetPocoList<Toplam>();
            foreach (Toplam itemToplam7 in sourceToplam7)
            {
                ws.Cells["H" + (rowIndexCurrentRecord6 + 3)].Value  = " Devir :";
                ws.Cells["H" + (rowIndexCurrentRecord6 + 3)+ ":I" + (rowIndexCurrentRecord6 + 3)].Style.Font.Bold = true;
                ws.Cells["H" + (rowIndexCurrentRecord6 + 3)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells["I" + (rowIndexCurrentRecord6 + 3)].Value  = itemToplam7.TOPLAM;
                ws.Cells["I" + (rowIndexCurrentRecord6 + 3)].Style.Numberformat.Format = "$#,##0.00";
            }

            int rowIndexBegin7 = rowIndexEndOrders6 + 6;
            int rowIndexCurrentRecord7 = rowIndexBegin7;
            foreach (CekSenetHareketleri item7 in source7)
            {

                ws.Cells["F" + (rowIndexCurrentRecord7)].Value = item7.VADE;
                ws.Cells["G" + (rowIndexCurrentRecord7)].Value = item7.ACIKLAMA;
                ws.Cells["H" + (rowIndexCurrentRecord7)].Value = item7.GIREN;
                ws.Cells["I" + (rowIndexCurrentRecord7)].Value = item7.CIKAN; 
                    ws.Cells["F" + (rowIndexCurrentRecord7) + ":F" + (rowIndexCurrentRecord7)].Style.Numberformat.Format = "dd - MM - yyyy";
                ws.Cells["H" + (rowIndexCurrentRecord7) + ":H" + (rowIndexCurrentRecord7)].Style.Numberformat.Format = "$#,##0.00";
                ws.Cells["I" + (rowIndexCurrentRecord7) + ":I" + (rowIndexCurrentRecord7)].Style.Numberformat.Format = "$#,##0.00";
                

                rowIndexCurrentRecord7++;
            }

            int rowIndexEndOrders7 = (rowIndexCurrentRecord7 + 5);
            var cellGrandTotalLabel7 = ws.Cells["G" + rowIndexCurrentRecord7];
            cellGrandTotalLabel7.Style.Font.Bold = true;
            cellGrandTotalLabel7.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabel7.Value = "TOPLAM";

            ws.Cells["G" + rowIndexCurrentRecord7 + ":H" + rowIndexCurrentRecord7].Style.Numberformat.Format = "$#,##0.00";
            var cellGrandTotalFormula7 = ws.Cells["H" + rowIndexCurrentRecord7];
            cellGrandTotalFormula7.Style.Font.Bold = true;

            string sumFormula7 = "SUM(R[" + (rowIndexBegin7 - rowIndexCurrentRecord7) + "]C:R[-1]C)";
            

            ws.Cells["G" + rowIndexCurrentRecord7 + ":I" + rowIndexCurrentRecord7].Style.Numberformat.Format = "$#,##0.00";
            var cellGrandTotalFormulaD7 = ws.Cells["I" + rowIndexCurrentRecord7];
            cellGrandTotalFormulaD7.Style.Font.Bold = true;

            string sumFormulaD7 = "SUM(R[" + (rowIndexBegin7 - rowIndexCurrentRecord7) + "]C:R[-1]C)";
            

            if (rowIndexBegin7 == rowIndexCurrentRecord7)
            {
                cellGrandTotalFormula7.FormulaR1C1 = "0";
                cellGrandTotalFormulaD7.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula7.FormulaR1C1 = sumFormula7;
                cellGrandTotalFormulaD7.FormulaR1C1 = sumFormula7;
            }

            #region BAKİYE
            int rowIndexEndOrdersB7 = rowIndexCurrentRecord7 + 1;
            var cellGrandTotalLabelB7 = ws.Cells["G" + rowIndexEndOrdersB7];
            cellGrandTotalLabelB7.Style.Font.Bold = true;
            cellGrandTotalLabelB7.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabelB7.Value = "BAKİYE";

            ws.Cells["H" + rowIndexEndOrdersB7 + ":I" + rowIndexEndOrdersB7].Style.Numberformat.Format = "$#,##0.00";
            var cellGrandTotalFormulaB7 = ws.Cells["H" + rowIndexEndOrdersB7];
            cellGrandTotalFormulaB7.Style.Font.Bold = true;
            ws.Cells["H" + rowIndexEndOrdersB7 + ":I" + rowIndexEndOrdersB7].Merge = true;
            ws.Cells["H" + rowIndexEndOrdersB7 + ":I" + rowIndexEndOrdersB7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


            string sumFormulaB7 = "SUM(I" + (rowIndexCurrentRecord6 + 3) + "+H" + rowIndexCurrentRecord7 + "-I" + rowIndexCurrentRecord7 + ")";
            
            

            cellGrandTotalFormulaB7.FormulaR1C1 = sumFormulaB7;
            #endregion

            //ws.PrinterSettings.PrintArea =             ws.Cells["F" + (rowIndexBegin7 - 3) + ":K" + (rowIndexCurrentRecord7)];
            ws.Cells["F" + (rowIndexBegin7 - 3) + ":I" + (rowIndexCurrentRecord7)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin7 - 3) + ":I" + (rowIndexCurrentRecord7)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin7 - 3) + ":I" + (rowIndexCurrentRecord7)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin7 - 3) + ":I" + (rowIndexCurrentRecord7)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin7 - 3) + ":I" + (rowIndexCurrentRecord7)].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["F" + (rowIndexBegin7 - 3) + ":I" + (rowIndexCurrentRecord7)].Style.Fill.BackgroundColor.SetColor(Color.Lavender);
            

            #endregion

            #region EuroÇekSenetHareketleri

            TrCurr = 20;
            SqlText = SqlReports.CekSenetHareketleri;
            List<CekSenetHareketleri> source8 = GetPocoList<CekSenetHareketleri>();
            #region Header

            ws.Cells["F" + (rowIndexCurrentRecord7 + 2)].Value = "EUR Çek Senet Hareketleri";
            ws.Cells["F" + (rowIndexCurrentRecord7 + 2)].Style.Font.Size = 11;
            ws.Cells["F" + (rowIndexCurrentRecord7 + 2)
                  + ":I" + (rowIndexCurrentRecord7 + 2)].Merge = true;
            ws.Cells["F" + (rowIndexCurrentRecord7 + 2)].Style.Font.Bold = true;
            ws.Cells["F" + (rowIndexCurrentRecord7 + 2)].Style.WrapText = true;
            ws.Cells["F" + (rowIndexCurrentRecord7 + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["F" + (rowIndexCurrentRecord7 + 4)].Value = Util.Labels.VadeGunu;
            ws.Cells["G" + (rowIndexCurrentRecord7 + 4)].Value = Util.Labels.Aciklama;
            ws.Cells["H" + (rowIndexCurrentRecord7 + 4)].Value = Util.Labels.Giren;
            ws.Cells["I" + (rowIndexCurrentRecord7 + 4)].Value = Util.Labels.Cikan;
            ws.Cells["F" + (rowIndexCurrentRecord7 + 4) + ":I" + (rowIndexCurrentRecord7 + 4)].Style.Font.Bold = true;
            ws.Cells["F" + (rowIndexCurrentRecord7 + 4) + ":I" + (rowIndexCurrentRecord7 + 4)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            #endregion

            SqlText = SqlReports.CekSenetToplam;
            List<Toplam> sourceToplam8 = GetPocoList<Toplam>();
            foreach (Toplam itemToplam8 in sourceToplam8)
            {
                ws.Cells["H" + (rowIndexCurrentRecord7 + 3)].Value = " Devir :";
                ws.Cells["H" + (rowIndexCurrentRecord7 + 3)+ ":I" + (rowIndexCurrentRecord7 + 3)].Style.Font.Bold = true;
                ws.Cells["H" + (rowIndexCurrentRecord7 + 3)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells["I" + (rowIndexCurrentRecord7 + 3)].Value = itemToplam8.TOPLAM;
                ws.Cells["I" + (rowIndexCurrentRecord7 + 3)].Style.Numberformat.Format = "€#,##0.00";
            }

            int rowIndexBegin8 = rowIndexEndOrders7 ;
            int rowIndexCurrentRecord8 = rowIndexBegin8;
            foreach (CekSenetHareketleri item8 in source8)
            {

                ws.Cells["F" + (rowIndexCurrentRecord8)].Value = item8.VADE;
                ws.Cells["G" + (rowIndexCurrentRecord8)].Value = item8.ACIKLAMA;
                ws.Cells["H" + (rowIndexCurrentRecord8)].Value = item8.GIREN;
                ws.Cells["I" + (rowIndexCurrentRecord8)].Value = item8.CIKAN;
                ws.Cells["F" + (rowIndexCurrentRecord8) + ":F" + (rowIndexCurrentRecord8)].Style.Numberformat.Format = "dd - MM - yyyy";
                ws.Cells["H" + (rowIndexCurrentRecord8) + ":I" + (rowIndexCurrentRecord8)].Style.Numberformat.Format = "€#,##0.00";
                ws.Cells["I" + (rowIndexCurrentRecord8) + ":I" + (rowIndexCurrentRecord8)].Style.Numberformat.Format = "€#,##0.00";
               

                rowIndexCurrentRecord8++;
            }

            int   rowIndexEndOrders8 = (rowIndexCurrentRecord8 + 5);
            var cellGrandTotalLabel8 = ws.Cells["G" + rowIndexCurrentRecord8];
            cellGrandTotalLabel8.Style.Font.Bold = true;
            cellGrandTotalLabel8.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabel8.Value = "TOPLAM";

            ws.Cells["G" + rowIndexCurrentRecord8 + ":H" + rowIndexCurrentRecord8].Style.Numberformat.Format = "€#,##0.00";
            var cellGrandTotalFormula8 = ws.Cells["H" + rowIndexCurrentRecord8];
            cellGrandTotalFormula8.Style.Font.Bold = true;

            string sumFormula8 = "SUM(R[" + (rowIndexBegin8 - rowIndexCurrentRecord8) + "]C:R[-1]C)";
            

            ws.Cells["G" + rowIndexCurrentRecord8 + ":I" + rowIndexCurrentRecord8].Style.Numberformat.Format = "€#,##0.00";
            var cellGrandTotalFormulaD8 = ws.Cells["I" + rowIndexCurrentRecord8];
            cellGrandTotalFormulaD8.Style.Font.Bold = true;

            string sumFormulaD8 = "SUM(R[" + (rowIndexBegin8 - rowIndexCurrentRecord8) + "]C:R[-1]C)";
           

            if (rowIndexBegin8 == rowIndexCurrentRecord8)
            {
                cellGrandTotalFormula8.FormulaR1C1 = "0";
                cellGrandTotalFormulaD8.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula8.FormulaR1C1 = sumFormula8;
                cellGrandTotalFormulaD8.FormulaR1C1 = sumFormula8;
            }

            #region BAKİYE
            int rowIndexEndOrdersB8 = rowIndexCurrentRecord8 + 1;
            var cellGrandTotalLabelB8 = ws.Cells["G" + rowIndexEndOrdersB8];
            cellGrandTotalLabelB8.Style.Font.Bold = true;
            cellGrandTotalLabelB8.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabelB8.Value = "BAKİYE";

            ws.Cells["H" + rowIndexEndOrdersB8 + ":I" + rowIndexEndOrdersB8].Style.Numberformat.Format = "€#,##0.00";
            var cellGrandTotalFormulaB8 = ws.Cells["H"+ rowIndexEndOrdersB8];
            cellGrandTotalFormulaB8.Style.Font.Bold = true;
            ws.Cells["H" + rowIndexEndOrdersB8 + ":I" + rowIndexEndOrdersB8].Merge = true;
            ws.Cells["H" + rowIndexEndOrdersB8 + ":I" + rowIndexEndOrdersB8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


            string sumFormulaB8 = "SUM(I" + (rowIndexCurrentRecord7 + 3) + "+H" + rowIndexCurrentRecord8 + "-I" + rowIndexCurrentRecord8 + ")";



            cellGrandTotalFormulaB8.FormulaR1C1 = sumFormulaB8;
            #endregion

            //ws.PrinterSettings.PrintArea =            ws.Cells["F" + (rowIndexBegin8 - 3) + ":K" + (rowIndexCurrentRecord8)];
            ws.Cells["F" + (rowIndexBegin8 - 3) + ":I" + (rowIndexCurrentRecord8)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin8 - 3) + ":I" + (rowIndexCurrentRecord8)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin8 - 3) + ":I" + (rowIndexCurrentRecord8)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin8 - 3) + ":I" + (rowIndexCurrentRecord8)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin8 - 3) + ":I" + (rowIndexCurrentRecord8)].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["F" + (rowIndexBegin8 - 3) + ":I" + (rowIndexCurrentRecord8)].Style.Fill.BackgroundColor.SetColor(Color.Khaki);
            

            #endregion

            #region FirmaKrediKartı

            TrCurr = 0;
            //SqlText = SqlReports.FirmaKrediKartiHareketleri;
            List<KrediKartiHareketleri> source9 = GetPocoList<KrediKartiHareketleri>();
            #region Header

            ws.Cells["F" + (rowIndexCurrentRecord8 + 3)].Value = "Firma Kredi Kartı Ödemeler";
            ws.Cells["F" + (rowIndexCurrentRecord8 + 3)].Style.Font.Size = 11;
            ws.Cells["F" + (rowIndexCurrentRecord8 + 3) + ":I" + (rowIndexCurrentRecord8 + 3)].Merge = true;
            ws.Cells["F" + (rowIndexCurrentRecord8 + 3)].Style.Font.Bold = true;
            ws.Cells["F" + (rowIndexCurrentRecord8 + 3)].Style.WrapText = true;
            ws.Cells["F" + (rowIndexCurrentRecord8 + 3)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["F" + (rowIndexCurrentRecord8 + 4)].Value = Util.Labels.Banka;
            ws.Cells["G" + (rowIndexCurrentRecord8 + 4)].Value = Util.Labels.CariHesapUnvani;
            ws.Cells["H" + (rowIndexCurrentRecord8 + 4)].Value = Util.Labels.Tutar;
            ws.Cells["H" + (rowIndexCurrentRecord8 + 4) + ":I" + (rowIndexCurrentRecord8 + 4)].Merge = true;
            ws.Cells["F" + (rowIndexCurrentRecord8 + 4) + ":I" + (rowIndexCurrentRecord8 + 4)].Style.Font.Bold = true;
            ws.Cells["F" + (rowIndexCurrentRecord8 + 4) + ":I" + (rowIndexCurrentRecord8 + 4)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            #endregion

            int rowIndexBegin9 = rowIndexEndOrders8;
            int rowIndexCurrentRecord9 = rowIndexBegin9;
            foreach (KrediKartiHareketleri item9 in source9)
            {
                ws.Cells["F" + (rowIndexCurrentRecord9)].Value = item9.HESAPADI;
                ws.Cells["G" + (rowIndexCurrentRecord9)].Value = item9.DEFINITION_;
                ws.Cells["H" + (rowIndexCurrentRecord9)].Value = item9.Tutar;
                ws.Cells["H" + (rowIndexCurrentRecord9) + ":I" + (rowIndexCurrentRecord9)].Merge = true;
                ws.Cells["H" + (rowIndexCurrentRecord9) + ":I" + (rowIndexCurrentRecord9)].Style.Numberformat.Format = "₺#,##0.00";
                

                rowIndexCurrentRecord9++;
            }

            int rowIndexEndOrders9 = (rowIndexCurrentRecord9 + 5);
            var cellGrandTotalLabel9 = ws.Cells["G" + rowIndexCurrentRecord9];
            cellGrandTotalLabel9.Style.Font.Bold = true;
            cellGrandTotalLabel9.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            cellGrandTotalLabel9.Value = "TOPLAM";
            ws.Cells["H" + (rowIndexCurrentRecord9) + ":I" + (rowIndexCurrentRecord9)].Merge = true;
            ws.Cells["H" + rowIndexCurrentRecord9 + ":I" + rowIndexCurrentRecord9].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormula9 = ws.Cells["H" + rowIndexCurrentRecord9];
            cellGrandTotalFormula9.Style.Font.Bold = true;

            string sumFormula9 = "SUM(R[" + (rowIndexBegin9 - rowIndexCurrentRecord9) + "]C:R[-1]C)";
            

            if (rowIndexBegin9 == rowIndexCurrentRecord9)
            {
                cellGrandTotalFormula9.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula9.FormulaR1C1 = sumFormula9;
            }

            //ws.PrinterSettings.PrintArea =            ws.Cells["F" + (rowIndexBegin9 - 2) + ":K" + (rowIndexCurrentRecord9)];
            ws.Cells["F" + (rowIndexBegin9 - 2) + ":I" + (rowIndexCurrentRecord9)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin9 - 2) + ":I" + (rowIndexCurrentRecord9)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin9 - 2) + ":I" + (rowIndexCurrentRecord9)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin9 - 2) + ":I" + (rowIndexCurrentRecord9)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin9 - 2) + ":I" + (rowIndexCurrentRecord9)].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["F" + (rowIndexBegin9 - 2) + ":I" + (rowIndexCurrentRecord9)].Style.Fill.BackgroundColor.SetColor(Color.MistyRose);


            #endregion

            #region Pos Tahsilat

            TrCurr = 0;
            //SqlText = SqlReports.KrediKartiHareketleri;
            List<KrediKartiHareketleri> source10 = GetPocoList<KrediKartiHareketleri>();
            #region Header

            ws.Cells["F" + (rowIndexCurrentRecord9 + 3)].Value = "Pos Tahsilat";
            ws.Cells["F" + (rowIndexCurrentRecord9 + 3)].Style.Font.Size = 11;
            ws.Cells["F" + (rowIndexCurrentRecord9 + 3)
                  + ":I" + (rowIndexCurrentRecord9 + 3)].Merge = true;
            ws.Cells["F" + (rowIndexCurrentRecord9 + 3)].Style.Font.Bold = true;
            ws.Cells["F" + (rowIndexCurrentRecord9 + 3)].Style.WrapText = true;
            ws.Cells["F" + (rowIndexCurrentRecord9 + 3)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["F" + (rowIndexCurrentRecord9 + 4)].Value = Util.Labels.Banka;
            ws.Cells["G" + (rowIndexCurrentRecord9 + 4)].Value = Util.Labels.CariHesapUnvani;
            ws.Cells["H" + (rowIndexCurrentRecord9 + 4)].Value = Util.Labels.Tutar;
            ws.Cells["H" + (rowIndexCurrentRecord9 + 4) + ":I" + (rowIndexCurrentRecord9 + 4)].Merge = true;
            ws.Cells["F" + (rowIndexCurrentRecord9 + 4) + ":I" + (rowIndexCurrentRecord9 + 4)].Style.Font.Bold = true;
            ws.Cells["F" + (rowIndexCurrentRecord9 + 4) + ":I" + (rowIndexCurrentRecord9 + 4)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            #endregion

            int rowIndexBegin10 = rowIndexEndOrders9;
            int rowIndexCurrentRecord10 = rowIndexBegin10;
            foreach (KrediKartiHareketleri item10 in source10)
            {


                ws.Cells["F" + (rowIndexCurrentRecord10)].Value = item10.HESAPADI;
                ws.Cells["G" + (rowIndexCurrentRecord10)].Value = item10.DEFINITION_;
                ws.Cells["H" + (rowIndexCurrentRecord10)].Value = item10.Tutar;
                ws.Cells["H" + (rowIndexCurrentRecord10) + ":I" + (rowIndexCurrentRecord10)].Merge = true;
                ws.Cells["H" + (rowIndexCurrentRecord10) + ":I" + (rowIndexCurrentRecord10)].Style.Numberformat.Format = "₺#,##0.00";
                
                

                rowIndexCurrentRecord10++;
            }

            int rowIndexEndOrders10 = (rowIndexCurrentRecord10 + 5);
            var cellGrandTotalLabel10 = ws.Cells["G" + rowIndexCurrentRecord10];
            cellGrandTotalLabel10.Style.Font.Bold = true;
            cellGrandTotalLabel10.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabel10.Value = "TOPLAM";
            ws.Cells["H" + (rowIndexCurrentRecord10) + ":I" + (rowIndexCurrentRecord10)].Merge = true;
            ws.Cells["H" + rowIndexCurrentRecord10 + ":I" + rowIndexCurrentRecord10].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormula10 = ws.Cells["H" + rowIndexCurrentRecord10];
            cellGrandTotalFormula10.Style.Font.Bold = true;

            string sumFormula10 = "SUM(R[" + (rowIndexBegin10 - rowIndexCurrentRecord10) + "]C:R[-1]C)";
            


            if (rowIndexBegin10 == rowIndexCurrentRecord10)
            {
                cellGrandTotalFormula10.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula10.FormulaR1C1 = sumFormula10;
            }

            //ws.PrinterSettings.PrintArea =            ws.Cells["F" + (rowIndexBegin10 - 2) + ":K" + (rowIndexCurrentRecord10)];
            ws.Cells["F" + (rowIndexBegin10 - 2) + ":I" + (rowIndexCurrentRecord10)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin10 - 2) + ":I" + (rowIndexCurrentRecord10)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin10 - 2) + ":I" + (rowIndexCurrentRecord10)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin10 - 2) + ":I" + (rowIndexCurrentRecord10)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin10 - 2) + ":I" + (rowIndexCurrentRecord10)].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["F" + (rowIndexBegin10 - 2) + ":I" + (rowIndexCurrentRecord10)].Style.Fill.BackgroundColor.SetColor(Color.Ivory);



            #endregion

            #region Satışlar

            TrCurr = 0;
            //SqlText = SqlReports.SatisFaturalari;
            List<Faturalar> source11 = GetPocoList<Faturalar>();
            #region Header

            ws.Cells["F" + (rowIndexCurrentRecord10 + 3)].Value = "Satışlar";
            ws.Cells["F" + (rowIndexCurrentRecord10 + 3)].Style.Font.Size = 11;
            ws.Cells["F" + (rowIndexCurrentRecord10 + 3)
                  + ":I" + (rowIndexCurrentRecord10 + 3)].Merge = true;
            ws.Cells["F" + (rowIndexCurrentRecord10 + 3)].Style.Font.Bold = true;
            ws.Cells["F" + (rowIndexCurrentRecord10 + 3)].Style.WrapText = true;
            ws.Cells["F" + (rowIndexCurrentRecord10 + 3)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["F" + (rowIndexCurrentRecord10 + 4)].Value = Util.Labels.VadeGunu;
            ws.Cells["G" + (rowIndexCurrentRecord10 + 4)].Value = Util.Labels.CariHesapUnvani;
            ws.Cells["H" + (rowIndexCurrentRecord10 + 4)].Value = Util.Labels.Tutar;
            ws.Cells["H" + (rowIndexCurrentRecord10 + 4) + ":I" + (rowIndexCurrentRecord10 + 4)].Merge = true;
            ws.Cells["F" + (rowIndexCurrentRecord10 + 4) + ":I" + (rowIndexCurrentRecord10 + 4)].Style.Font.Bold = true;
            ws.Cells["F" + (rowIndexCurrentRecord10 + 4) + ":I" + (rowIndexCurrentRecord10 + 4)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            #endregion

            int rowIndexBegin11 = rowIndexEndOrders10;
            int rowIndexCurrentRecord11 = rowIndexBegin11;
            foreach (Faturalar item11 in source11)
            {


                ws.Cells["F" + (rowIndexCurrentRecord11)].Value = item11.VADE;
                ws.Cells["G" + (rowIndexCurrentRecord11)].Value = item11.DEFINITION_;
                ws.Cells["H" + (rowIndexCurrentRecord11)].Value = item11.Tutar;
                ws.Cells["H" + (rowIndexCurrentRecord11) + ":I" + (rowIndexCurrentRecord11)].Merge = true;
                ws.Cells["F" + (rowIndexCurrentRecord11) + ":F" + (rowIndexCurrentRecord11)].Style.Numberformat.Format = "dd - MM - yyyy";
                ws.Cells["H" + (rowIndexCurrentRecord11) + ":I" + (rowIndexCurrentRecord11)].Style.Numberformat.Format = "₺#,##0.00";

                rowIndexCurrentRecord11++;
            }

            int rowIndexEndOrders11 = (rowIndexCurrentRecord11 + 5);
            var cellGrandTotalLabel11 = ws.Cells["G" + rowIndexCurrentRecord11];
            cellGrandTotalLabel11.Style.Font.Bold = true;
            cellGrandTotalLabel11.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabel11.Value = "TOPLAM";
            ws.Cells["H" + (rowIndexCurrentRecord11) + ":I" + (rowIndexCurrentRecord11)].Merge = true;
            ws.Cells["H" + rowIndexCurrentRecord11 + ":I" + rowIndexCurrentRecord11].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormula11 = ws.Cells["H" + rowIndexCurrentRecord11];
            cellGrandTotalFormula11.Style.Font.Bold = true;

            string sumFormula11 = "SUM(R[" + (rowIndexBegin11 - rowIndexCurrentRecord11) + "]C:R[-1]C)";
            

            if (rowIndexBegin11 == rowIndexCurrentRecord11)
            {
                cellGrandTotalFormula11.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula11.FormulaR1C1 = sumFormula11;
            }

            //ws.PrinterSettings.PrintArea =            ws.Cells["F" + (rowIndexBegin11 - 2) + ":K" + (rowIndexCurrentRecord11)];
            ws.Cells["F" + (rowIndexBegin11 - 2) + ":I" + (rowIndexCurrentRecord11)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin11 - 2) + ":I" + (rowIndexCurrentRecord11)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin11 - 2) + ":I" + (rowIndexCurrentRecord11)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin11 - 2) + ":I" + (rowIndexCurrentRecord11)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin11 - 2) + ":I" + (rowIndexCurrentRecord11)].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["F" + (rowIndexBegin11 - 2) + ":I" + (rowIndexCurrentRecord11)].Style.Fill.BackgroundColor.SetColor(Color.LightSalmon);


            #endregion

            #region Alışlar

            TrCurr = 0;
            //SqlText = SqlReports.AlisFaturalar;
            List<Faturalar> source12 = GetPocoList<Faturalar>();
            #region Header

            ws.Cells["F" + (rowIndexCurrentRecord11 + 3)].Value = "Alışlar";
            ws.Cells["F" + (rowIndexCurrentRecord11 + 3)].Style.Font.Size = 11;
            ws.Cells["F" + (rowIndexCurrentRecord11 + 3)
                  + ":I" + (rowIndexCurrentRecord11 + 3)].Merge = true;
            ws.Cells["F" + (rowIndexCurrentRecord11 + 3)].Style.Font.Bold = true;
            ws.Cells["F" + (rowIndexCurrentRecord11 + 3)].Style.WrapText = true;
            ws.Cells["F" + (rowIndexCurrentRecord11 + 3)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells["F" + (rowIndexCurrentRecord11 + 4)].Value = Util.Labels.FaturaTarihi;
            
            ws.Cells["G" + (rowIndexCurrentRecord11 + 4)].Value = Util.Labels.CariHesapUnvani;
            ws.Cells["H" + (rowIndexCurrentRecord11 + 4)].Value = Util.Labels.VadeGunu;
            ws.Cells["I" + (rowIndexCurrentRecord11 + 4)].Value = Util.Labels.Tutar;
            //ws.Cells["H" + (rowIndexCurrentRecord11 + 4) + ":I" + (rowIndexCurrentRecord11 + 4)].Merge = true;
            ws.Cells["F" + (rowIndexCurrentRecord11 + 4) + ":I" + (rowIndexCurrentRecord11 + 4)].Style.Font.Bold = true;
            ws.Cells["F" + (rowIndexCurrentRecord11 + 4) + ":I" + (rowIndexCurrentRecord11 + 4)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            #endregion

            int rowIndexBegin12 = rowIndexEndOrders11;
            int rowIndexCurrentRecord12 = rowIndexBegin12;
            foreach (Faturalar item12 in source12)
            {

                ws.Cells["F" + (rowIndexCurrentRecord12)].Value = item12.DATE_;
                ws.Cells["G" + (rowIndexCurrentRecord12)].Value = item12.DEFINITION_;
                ws.Cells["H" + (rowIndexCurrentRecord12)].Value = item12.VADE;
                ws.Cells["I" + (rowIndexCurrentRecord12)].Value = item12.Tutar;
                //ws.Cells["H" + (rowIndexCurrentRecord12) + ":I" + (rowIndexCurrentRecord12)].Merge = true;
                ws.Cells["F" + (rowIndexCurrentRecord12) + ":F" + (rowIndexCurrentRecord12)].Style.Numberformat.Format = "dd - MM - yyyy";
                ws.Cells["H" + (rowIndexCurrentRecord12) + ":H" + (rowIndexCurrentRecord12)].Style.Numberformat.Format = "dd - MM - yyyy";
                ws.Cells["I" + (rowIndexCurrentRecord12) + ":I" + (rowIndexCurrentRecord12)].Style.Numberformat.Format = "₺#,##0.00";


               

                rowIndexCurrentRecord12++;
            }

            int rowIndexEndOrders12 = (rowIndexCurrentRecord12 + 5);
            var cellGrandTotalLabel12 = ws.Cells["G" + rowIndexCurrentRecord12];
            cellGrandTotalLabel12.Style.Font.Bold = true;
            cellGrandTotalLabel12.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            cellGrandTotalLabel12.Value = "TOPLAM";
            ws.Cells["I" + (rowIndexCurrentRecord12) + ":I" + (rowIndexCurrentRecord12)].Merge = true;
            ws.Cells["I" + rowIndexCurrentRecord12 + ":I" + rowIndexCurrentRecord12].Style.Numberformat.Format = "₺#,##0.00";
            var cellGrandTotalFormula12 = ws.Cells["I" + rowIndexCurrentRecord12];
            cellGrandTotalFormula12.Style.Font.Bold = true;

            string sumFormula12 = "SUM(R[" + (rowIndexBegin12 - rowIndexCurrentRecord12) + "]C:R[-1]C)";
            

            if (rowIndexBegin12 == rowIndexCurrentRecord12)
            {
                cellGrandTotalFormula12.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula12.FormulaR1C1 = sumFormula12;
            }

            ws.PrinterSettings.PrintArea = ws.Cells["B1:D"+ (rowIndexCurrentRecord5+1)+","+"F1:I" + (rowIndexCurrentRecord12)];
            ws.Cells["F" + (rowIndexBegin12 - 2) + ":I" + (rowIndexCurrentRecord12)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin12 - 2) + ":I" + (rowIndexCurrentRecord12)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin12 - 2) + ":I" + (rowIndexCurrentRecord12)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin12 - 2) + ":I" + (rowIndexCurrentRecord12)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells["F" + (rowIndexBegin12 - 2) + ":I" + (rowIndexCurrentRecord12)].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["F" + (rowIndexBegin12 - 2) + ":I" + (rowIndexCurrentRecord12)].Style.Fill.BackgroundColor.SetColor(Color.LightGray);


            ws.Column(1).Width = 7.57;
            ws.Column(2).Width = 110.57;
            ws.Column(3).Width = 23.86;
            ws.Column(4).Width = 23.86;
            ws.Column(5).Width = 7.57;
            ws.Column(6).Width = 21.14;
            ws.Column(7).Width = 87.86;
            ws.Column(8).Width = 24.14;
            ws.Column(9).Width = 24.14;

            #endregion
            
            #region ExcelSave
            ws.PrinterSettings.FitToPage = true;
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Excel 2007 Dosyası|*.xlsx";
            save.OverwritePrompt = true;

            if (save.ShowDialog() == DialogResult.OK)
            {
                FileInfo newFile = new FileInfo(save.FileName);
                package.SaveAs(newFile);
            }
            #endregion
           
        }



        #endregion


    }
}