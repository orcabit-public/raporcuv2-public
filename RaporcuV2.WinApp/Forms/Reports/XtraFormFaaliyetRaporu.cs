﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Spreadsheet;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model.Reports;
using RaporcuV2.Util;
using RaporcuV2.WinApp.Forms.Customs;

namespace RaporcuV2.WinApp.Forms.Reports
{
    public partial class XtraFormFaaliyetRaporu : XtraFormBase, IReport
    {
        public int SectionStart { get; set; } = 4;
        private string _s = "A";
        private string _e = "N";
        private int _spaceRow = 2;

        public XtraFormFaaliyetRaporu()
        {
            InitializeComponent();
            this.barManagerControl1.gridControl1.Visible = false;
            AddEvents();}

        #region Methots

        #region Add Events

        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {

                CreateFirmLookup();
                CreatePeriodLookup();
                CreateFirstDate();
                foreach (BarItem barItem in barManagerControl1.barManagerForm.Items)
                {
                    if (barItem.Name == "firstDate")
                    {
                        ((DevExpress.XtraBars.BarEditItem)barItem).EditValue = DateTime.Today;

                    }

                }

                CreateRefreshButton();

                barManager1.BeginUpdate();

                var parentTools = this.barManagerControl1.barManagerForm.Bars[0];

                this.barManager1.Bars.Add(parentTools);

                barManager1.EndUpdate();

                commonBar1.Merge(parentTools);
            };

            //this.spreadsheetControl1.Dock = DockStyle.Fill;
        }

        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
            // Kasa ve Kasa Detay Renkelnedirme
            BuiltInStyleId kasaBaslikStyle = BuiltInStyleId.Accent1;
            BuiltInTableStyleId kasaStyle = BuiltInTableStyleId.TableStyleLight2;

            // Banka ve Banka HAreketerli Renklendirme
            BuiltInStyleId bankaBaslikStyle = BuiltInStyleId.Accent2;
            BuiltInTableStyleId bankaStyle = BuiltInTableStyleId.TableStyleLight3;
            // Kredi Durumu 
            BuiltInStyleId cekBaslikStyle = BuiltInStyleId.Accent3;
            BuiltInTableStyleId cekStyle = BuiltInTableStyleId.TableStyleLight4;

            // CARİ
            BuiltInStyleId cariBaslikStyle = BuiltInStyleId.Accent4;
            BuiltInTableStyleId cariStyle = BuiltInTableStyleId.TableStyleLight5;

            // Çek Durumu 
            BuiltInStyleId cekDurumBaslikStyle = BuiltInStyleId.Accent6;
            BuiltInTableStyleId cekDurumStyle = BuiltInTableStyleId.TableStyleMedium7;

            // Kredi Durumu 
            BuiltInStyleId krediBaslikStyle = BuiltInStyleId.Accent3;
            BuiltInTableStyleId krediStyle = BuiltInTableStyleId.TableStyleMedium4;



            #region Faaliyet Raporu

            #region Sorgular

            _reportTag = RaporcuV2.Helper.ReportTags.KurTumu;
            List<Kur> tumDovizKurlariList = GetPocoList<Kur>();

            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetKasaHareketleri;
            List<KasaHareketleri> kasaHareketleri = GetPocoList<KasaHareketleri>();
            #region Kasa Toplamları

            // Kasa Toplamları 
            //double kasaTl = 0;//double kasaUsd = 0;
            //double kasaEuro = 0;

            //kasaTl = kasaHareketleri.Where(a => a.CURCODE == "TL").Sum(a => a.GIREN) -
            //         kasaHareketleri.Where(a => a.CURCODE == "TL").Sum(a => a.CIKAN);
            //kasaUsd = kasaHareketleri.Where(a => a.CURCODE == "USD").Sum(a => a.GIREN) -
            //          kasaHareketleri.Where(a => a.CURCODE == "USD").Sum(a => a.CIKAN);
            //kasaEuro = kasaHareketleri.Where(a => a.CURCODE == "EUR").Sum(a => a.GIREN) -
            //           kasaHareketleri.Where(a => a.CURCODE == "EUR").Sum(a => a.CIKAN);

            #endregion
            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetBankaHareketleri;
            List<KasaHareketleri> bankaHareketleri = GetPocoList<KasaHareketleri>();
            #region Banka Toplamları

            // Kasa Toplamları 
            double bankaTl = 0; double bankaUsd = 0;
            double bankaEuro = 0;

            //bankaTl = bankaHareketleri.Where(a => a.CURCODE == "TL").Sum(a => a.GIREN) -
            //          bankaHareketleri.Where(a => a.CURCODE == "TL").Sum(a => a.CIKAN);
            //bankaUsd = bankaHareketleri.Where(a => a.CURCODE == "USD").Sum(a => a.GIREN) -
            //           bankaHareketleri.Where(a => a.CURCODE == "USD").Sum(a => a.CIKAN);
            //bankaEuro = bankaHareketleri.Where(a => a.CURCODE == "EUR").Sum(a => a.GIREN) -
            //            bankaHareketleri.Where(a => a.CURCODE == "EUR").Sum(a => a.CIKAN);

            #endregion

            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetCekSenetHareketleri;
            List<CekSenetHareketleri> cekSenetHareketleri = GetPocoList<CekSenetHareketleri>();

            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetKrediKartiHareketleri;
            List<KrediKartiHareketleri> kredikartlari = GetPocoList<KrediKartiHareketleri>();

            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetFirmaKrediKartiHareketleri;
            List<KrediKartiHareketleri> firmakredikartlari = GetPocoList<KrediKartiHareketleri>();

            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetSatisFaturalari;
            List<Faturalar> satisFaturalari = GetPocoList<Faturalar>();

            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetAlisFaturalari;
            List<Faturalar> alisFaturalar = GetPocoList<Faturalar>();

            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetCekDurum;
            List<AtaCekDurumu> FaaliyetCekDurum = GetPocoList<AtaCekDurumu>();

            #endregion


            #endregion


            spreadsheetControl1.BeginUpdate();




            // Rapor Grubu
            int i = 1;
            IWorkbook workbook = spreadsheetControl1.Document;
            Worksheet ws = workbook.Worksheets[0];
            ws.ActiveView.PaperKind = PaperKind.A4;
            ws.ActiveView.Orientation = PageOrientation.Landscape;
            ws.ActiveView.Margins.Left = 0;
            ws.ActiveView.Margins.Top = 0;
            ws.ActiveView.Margins.Right = 0;
            ws.ActiveView.Margins.Bottom = 0;

            ws.Clear(ws.GetUsedRange());
            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToWidth = 1;
            ws.PrintOptions.FitToHeight = 0;

            #region Headers

            #region KurEuro
            TrCurr = 20;
            //SqlText = SqlReports.Kur;
            _reportTag = RaporcuV2.Helper.ReportTags.Kur;
            List<Kur> Kur = GetPocoList<Kur>();

            double kur = 0;

            if (Kur.Count > 0)
            {
                kur = (double)Kur.FirstOrDefault().RATES1;
            }
       
            var range = ws.Range["B1"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[cekDurumBaslikStyle];
            ws.Cells["B1"].Value = kur;
             ws.Cells["B1"].Style.NumberFormat = "€#,##0.00000";
            ws.Cells["F1"].Value = kur;
            range = ws.Range["F1"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[cekDurumBaslikStyle];
            ws.Cells["F1"].Style.NumberFormat = "€#,##0.00000";

            #endregion
            #region KurDolar
            TrCurr = 1;
            //SqlText = SqlReports.Kur;
            _reportTag = RaporcuV2.Helper.ReportTags.Kur;
            List<Kur> Kur1 = GetPocoList<Kur>();

            double kur1 = 0;

            if (Kur1.Count > 0)
            {
                kur1 = (double)Kur1.FirstOrDefault().RATES1;
            }
            ws.Cells["C1"].Value = kur1;
            range = ws.Range["C1"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[krediBaslikStyle];
             ws.Cells["C1"].Style.NumberFormat = "$#,##0.00000";
            ws.Cells["G1"].Value = kur1;
            range = ws.Range["G1"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[krediBaslikStyle];
            ws.Cells["G1"].Style.NumberFormat = "$#,##0.00000";

            #endregion

            #region BaslikDoviz

            ws.Cells["B3"].Value = "TOPLAM TL :";
            ws.Cells["B4"].Value = "TOPLAM EURO :";
            ws.Cells["B5"].Value = "TOPLAM USD :";
            ws.Cells["B6"].Value = "TOPLAM NAKDİN TL KARŞILIĞI :";
            ws.Range["B3:B6"].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Right;


            // ws.Cells["C2"].Value = kasaTl + bankaTl;
            //ws.Cells["C2"].Formula = "SUM("+ cellGrandTotalTL + "(C3*B1)+(C4*C1))";
            //ws.Cells["C3"].Value = kasaEuro + bankaEuro;
            //ws.Cells["C4"].Value = kasaUsd + bankaUsd;

            
            //ws.Range["B1"].Style.NumberFormat = "#,##0.00000";

            ws.Range["C3:C6"].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Right;
            ws.Cells["C6"].Formula = "SUM(C3+(C4*B1)+(C5*C1))";



            #endregion

            //Range range = ws.Range[$"A{i}:C{i}"];
             range = ws.Range[$"A{i}"];
            ws.MergeCells(range);
            range.Value = string.Format("{0} Faaliyet Raporu", FirstDate.ToString("dd MMMM yyyy , dddd"));
            //range.Style = workbook.Styles[BuiltInStyleId.Heading1];

            #endregion


            #region Kasa Hareketleri
            int rowIndexKasaTL = 0;
            int rowIndexKasaEuro = 0;
            int rowIndexKasaDolar = 0;
            i = ws.GetUsedRange().RowCount + 2;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:C{i}"];
            ws.MergeCells(range);
            range.Value = "Kasa Hareketleri";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            int dataCount = 0;

            #endregion

            var currencyList = kasaHareketleri.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            bool isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = kasaHareketleri.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableKasa = ws.Tables.Add(ws[$"A{i}:C{dataCount}"], true);
                tableKasa.ShowTotals = true;
                tableKasa.AutoFilter.Disable();
                tableKasa.Style = workbook.TableStyles[kasaStyle];
                tableKasa.DataRange.NumberFormat = "#,##0.00";
                tableKasa.Columns[0].Name = $"{Labels.Aciklama}";
                tableKasa.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";

                tableKasa.Columns[1].Name = Labels.Giren;
                tableKasa.Columns[2].Name = Labels.Cikan;
                int KasaOncekiGunDevir = 0;
                foreach (var item in kasaHareketleri.Where(a => a.CURCODE == curCode.pb))
                {

                    i++;
                    ws.Cells[$"A{i}"].Value = item.ACIKLAMA;
                    ws.Cells[$"B{i}"].Value = item.GIREN;
                    ws.Cells[$"C{i}"].Value = item.CIKAN;

                }

                i = i + 2;
                range = ws.Range[$"B{i}:C{i}"];
                ws.MergeCells(range);
                ws.Cells[$"A{i}"].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Right;
                ws.Cells[$"A{i}"].Style.Font.Bold = true;
                ws.Cells[$"A{i}"].Value = "BAKİYE";
                
                if (curCode.pb=="TL")
                {
                    rowIndexKasaTL = i ;
                    
                }
                if (curCode.pb == "EUR")
                {
                    rowIndexKasaEuro = i;

                }
                if (curCode.pb == "USD")
                {rowIndexKasaDolar = i;

                }
                int bakiye = i - 1;
                range.Formula = "B" + bakiye + "-c" + bakiye ;

                //range.Value = kasaHareketleri.Where(a => a.CURCODE == curCode.pb).Sum(s => s.GIREN) -
                //              kasaHareketleri.Where(a => a.CURCODE == curCode.pb).Sum(s => s.CIKAN);

                range = ws.Range[$"A{i-1}:C{i}"];
                range.Style = workbook.Styles[kasaBaslikStyle];
                range.Style.Font.Bold = true;
                range.NumberFormat = "#,##0.00";
            foreach (TableColumn column in tableKasa.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

            }

            #endregion

            #region Banka Hareketleri 

            int rowIndexBankaTL = 0;
            int rowIndexBankaEuro = 0;
            int rowIndexBankaDolar = 0;
            i = ws.GetUsedRange().RowCount + 2;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"A{i}:C{i}"];
            ws.MergeCells(range);
            range.Value = "Banka Hareketleri";
            range.Style = workbook.Styles[bankaBaslikStyle];

            #endregion

            isFirstRow = true;
            currencyList = bankaHareketleri.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});

            foreach (var curCode in currencyList)
            {
                i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = bankaHareketleri.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableBanka = ws.Tables.Add(ws[$"A{i}:C{dataCount}"], true);
                tableBanka.ShowTotals = true;
                tableBanka.AutoFilter.Disable();
                tableBanka.Style = workbook.TableStyles[bankaStyle];
                tableBanka.DataRange.NumberFormat = "#,##0.00";

                tableBanka.Columns[0].Name = $"{Labels.Aciklama}";
                tableBanka.Columns[0].TotalRowLabel = $"{curCode.pb} Banka Toplamı : ";

                tableBanka.Columns[1].Name = Labels.Giren;
                tableBanka.Columns[2].Name = Labels.Cikan;

                foreach (var item in bankaHareketleri.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"A{i}"].Value = item.ACIKLAMA;
                    ws.Cells[$"B{i}"].Value = item.GIREN;
                    ws.Cells[$"C{i}"].Value = item.CIKAN;

                }

                i = i + 2;
                range = ws.Range[$"B{i}:C{i}"];
                ws.MergeCells(range);
                ws.Cells[$"A{i}"].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Right;
                ws.Cells[$"A{i}"].Style.Font.Bold = true;
                ws.Cells[$"A{i}"].Value = "BAKİYE";
                if (curCode.pb == "TL")
                {
                    rowIndexBankaTL = i;

                }
                if (curCode.pb == "EUR")
                {
                    rowIndexBankaEuro = i;

                }
                if (curCode.pb == "USD")
                {
                    rowIndexBankaDolar = i;

                }

                int bakiye = i- 1;
                range.Formula= "B" + bakiye + "-c" + bakiye;
                //range.Value = bankaHareketleri.Where(a => a.CURCODE == curCode.pb).Sum(s => s.GIREN) -
                //              bankaHareketleri.Where(a => a.CURCODE == curCode.pb).Sum(s => s.CIKAN);

                range = ws.Range[$"A{i - 1}:C{i}"];
                range.Style = workbook.Styles[bankaBaslikStyle];
                range.Style.Font.Bold = true;
                range.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableBanka.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                #region UstBilgi
                #region Toplam TL

                ws.Cells["C3"].Formula = "SUM(B" + rowIndexKasaTL + "+B" + rowIndexBankaTL + ")";

                #endregion
                #region Toplam EURO

                if ( rowIndexKasaEuro==0 )
                {
                    ws.Cells["C4"].Formula = "SUM(B" + rowIndexBankaEuro + ")";
                }
                else
                {
                    ws.Cells["C4"].Formula = "SUM(B" + rowIndexKasaEuro + "+B" + rowIndexBankaEuro + ")";
                }



                #endregion
                #region Toplam DOLAR

                if (rowIndexKasaDolar == 0)
                {
                    ws.Cells["C5"].Formula = "SUM(B" + rowIndexBankaDolar + ")";

                }

                else
                {
                    ws.Cells["C5"].Formula = "SUM(B" + rowIndexKasaDolar + "+B" + rowIndexBankaDolar + ")";

                }


                #endregion




                #endregion
            }

            #endregion

            //sağ taraf

            #region Çek-Senet Hareketleri

            dataCount = 0;
            i = 4;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"E{i}:H{i}"];ws.MergeCells(range);
            range.Value = "Çek-Senet Hareketleri";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = cekSenetHareketleri.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = cekSenetHareketleri.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableSenetHareketleri = ws.Tables.Add(ws[$"E{i}:H{dataCount}"], true);
                tableSenetHareketleri.ShowTotals = true;
                tableSenetHareketleri.AutoFilter.Disable();
                tableSenetHareketleri.Style = workbook.TableStyles[kasaStyle];
                tableSenetHareketleri.DataRange.NumberFormat = "#,##0.00";
                tableSenetHareketleri.Columns[0].Name = $"{Labels.VadeGunu}";
                tableSenetHareketleri.Columns[0].DataRange.NumberFormat = "dd/mm/yyyy";
                tableSenetHareketleri.Columns[1].Name = $"{Labels.Aciklama}";
                tableSenetHareketleri.Columns[0].TotalRowLabel = $"{curCode.pb} Çek-Senet Toplamı : ";
                tableSenetHareketleri.Columns[2].Name = Labels.Giren;
                tableSenetHareketleri.Columns[3].Name = Labels.Cikan;
                

                foreach (var item in cekSenetHareketleri.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"E{i}"].Value = item.VADE;
                    ws.Cells[$"F{i}"].Value = item.ACIKLAMA;
                    ws.Cells[$"G{i}"].Value = item.GIREN;
                    ws.Cells[$"H{i}"].Value = item.CIKAN;
                }

                                i = i + 2;range = 
                ws.Range[$"G{i}:H{i}"];ws.MergeCells(range);
                ws.Cells[$"E{i}"].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Right;
                ws.Cells[$"E{i}"].Style.Font.Bold = true;
                ws.Cells[$"E{i}"].Value = "BAKİYE";
                int bakiye = i - 1;
                range.Formula = "G" + bakiye + "-H" + bakiye;
                //range.Value = cekSenetHareketleri.Where(a => a.CURCODE == curCode.pb).Sum(s => s.GIREN) -
                //              cekSenetHareketleri.Where(a => a.CURCODE == curCode.pb).Sum(s => s.CIKAN);

                range = ws.Range[$"E{i - 1}:H{i}"];
                range.Style = workbook.Styles[bankaBaslikStyle];
                range.Style.Font.Bold = true;
                range.NumberFormat = "#,##0.00";foreach (TableColumn column in tableSenetHareketleri.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableSenetHareketleri.TotalRowRange.BottomRowIndex + 1) + _spaceRow;
            }

            #endregion

            #region Firma Kredi Kartı Ödemeleri

            #region Grup Başlığı Renklendir

            range = ws.Range[$"E{i}:G{i}"];
            ws.MergeCells(range);
            range.Value = "Firma Kredi Kartı Ödemeler";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = firmakredikartlari.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = firmakredikartlari.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tablefirmakredikartlari = ws.Tables.Add(ws[$"E{i}:G{dataCount}"], true);
                tablefirmakredikartlari.ShowTotals = true;
                tablefirmakredikartlari.AutoFilter.Disable();
                tablefirmakredikartlari.Style = workbook.TableStyles[kasaStyle];
                tablefirmakredikartlari.DataRange.NumberFormat = "#,##0.00";
                tablefirmakredikartlari.Columns[0].Name = $"{Labels.BankaHesapUnvani}";
                tablefirmakredikartlari.Columns[0].TotalRowLabel = $"Firma Kredi Kartı Toplamı : ";
                
                tablefirmakredikartlari.Columns[1].Name = Labels.CariHesapUnvani;
                tablefirmakredikartlari.Columns[2].Name = Labels.Tutar;

                foreach (var item in firmakredikartlari.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"E{i}"].Value = item.HESAPADI;
                    ws.Cells[$"F{i}"].Value = item.DEFINITION_;
                    ws.Cells[$"G{i}"].Value = item.Tutar;
                }
                
                tablefirmakredikartlari.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tablefirmakredikartlari.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tablefirmakredikartlari.TotalRowRange.BottomRowIndex + 1) + _spaceRow;
            }

            #endregion

            #region Pos Tahsilat 


            #region Grup Başlığı Renklendir


            range = ws.Range[$"E{i}:G{i}"];
            ws.MergeCells(range);
            range.Value = "Pos Tahsilat ";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = kredikartlari.GroupBy(a => a.CURCODE).Select(s => new {pb = s.Key});
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = kredikartlari.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableKrediKartlari = ws.Tables.Add(ws[$"E{i}:G{dataCount}"], true);
                tableKrediKartlari.ShowTotals = true;
                tableKrediKartlari.AutoFilter.Disable();
                tableKrediKartlari.Style = workbook.TableStyles[kasaStyle];
                tableKrediKartlari.DataRange.NumberFormat = "#,##0.00";
                tableKrediKartlari.Columns[0].Name = $"{Labels.BankaHesapUnvani}";
                tableKrediKartlari.Columns[0].TotalRowLabel = $"Pos Toplamı : ";
                tableKrediKartlari.Columns[1].Name = Labels.CariHesapUnvani;
                tableKrediKartlari.Columns[2].Name = Labels.Tutar;

                foreach (var item in kredikartlari.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"E{i}"].Value = item.HESAPADI;
                    ws.Cells[$"F{i}"].Value = item.DEFINITION_;
                    ws.Cells[$"G{i}"].Value = item.Tutar;
                }


                tableKrediKartlari.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableKrediKartlari.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tableKrediKartlari.TotalRowRange.BottomRowIndex + 1) + _spaceRow;
            }

            #endregion

            #region Satışlar


            #region Grup Başlığı Renklendir


            range = ws.Range[$"E{i}:G{i}"];
            ws.MergeCells(range);
            range.Value = "Satışlar ";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = satisFaturalari.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = satisFaturalari.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tablesatisFaturalari = ws.Tables.Add(ws[$"E{i}:G{dataCount}"], true);
                tablesatisFaturalari.ShowTotals = true;
                tablesatisFaturalari.AutoFilter.Disable();
                tablesatisFaturalari.Style = workbook.TableStyles[kasaStyle];
                tablesatisFaturalari.DataRange.NumberFormat = "#,##0.00";
                tablesatisFaturalari.Columns[0].Name = $"{Labels.VadeGunu}";
                tablesatisFaturalari.Columns[0].DataRange.NumberFormat = "dd/mm/yyyy";
                tablesatisFaturalari.Columns[0].TotalRowLabel = $"{curCode.pb} Satışlar Toplamı : ";
                tablesatisFaturalari.Columns[1].Name = Labels.CariHesapUnvani;
                tablesatisFaturalari.Columns[2].Name = Labels.Tutar;

                foreach (var item in satisFaturalari.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"E{i}"].Value = item.VADE;
                    ws.Cells[$"F{i}"].Value = item.DEFINITION_;
                    ws.Cells[$"G{i}"].Value = item.Tutar;
                }

                tablesatisFaturalari.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tablesatisFaturalari.Columns)
                {
                    if (column.Index != 0)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }

                i = (tablesatisFaturalari.TotalRowRange.BottomRowIndex + 1) + _spaceRow;
            }

            #endregion

            #region Alışlar


            #region Grup Başlığı Renklendir


            range = ws.Range[$"E{i}:H{i}"];
            ws.MergeCells(range);
            range.Value = "Alışlar ";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;

            #endregion

            currencyList = alisFaturalar.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = alisFaturalar.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tablealisFaturalar = ws.Tables.Add(ws[$"E{i}:H{dataCount}"], true);
                tablealisFaturalar.ShowTotals = true;
                tablealisFaturalar.AutoFilter.Disable();
                tablealisFaturalar.Style = workbook.TableStyles[kasaStyle];
                tablealisFaturalar.DataRange.NumberFormat = "#,##0.00";
                tablealisFaturalar.Columns[0].Name = $"{Labels.FaturaTarihi}";
                tablealisFaturalar.Columns[0].DataRange.NumberFormat = "dd/mm/yyyy";
                tablealisFaturalar.Columns[0].TotalRowLabel = $"{curCode.pb} Alışlar Toplamı : ";
                tablealisFaturalar.Columns[1].Name = Labels.CariHesapUnvani;
                tablealisFaturalar.Columns[2].Name = Labels.VadeGunu;
                tablealisFaturalar.Columns[2].DataRange.NumberFormat = "dd/mm/yyyy";
                tablealisFaturalar.Columns[3].Name = Labels.Tutar;

                foreach (var item in alisFaturalar.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"E{i}"].Value = item.DATE_;
                    ws.Cells[$"F{i}"].Value = item.DEFINITION_;
                    ws.Cells[$"G{i}"].Value = item.VADE;
                    ws.Cells[$"H{i}"].Value = item.Tutar;
                }

                tablealisFaturalar.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tablealisFaturalar.Columns)
                {
                    if (column.Index != 0) 
                    {

                        if (column.Name == "Tutar")
                        {
                            column.TotalRowFunction = TotalRowFunction.Sum;
                        }
                    }
                }

                i = (tablealisFaturalar.TotalRowRange.BottomRowIndex + 1) + _spaceRow;
            }

            #endregion

            

            #region Çek Durumu
            dataCount = 0;
            i = 4;
            SectionStart = i;

            #region Grup Başlığı Renklendir

            range = ws.Range[$"J{i}:O{i}"];
            ws.MergeCells(range);
            range.Value = "Müşteri Çek Senet Durum";
            range.Style = workbook.Styles[kasaBaslikStyle];
            i++;

            dataCount = 0;
            #endregion

            currencyList = FaaliyetCekDurum.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                //i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;
                int dataRowCount = FaaliyetCekDurum.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableCekDurumu = ws.Tables.Add(ws[$"J{i}:O{dataCount}"], true);
                tableCekDurumu.ShowTotals = true;
                tableCekDurumu.AutoFilter.Disable();
                tableCekDurumu.Style = workbook.TableStyles[kasaStyle];
                tableCekDurumu.DataRange.NumberFormat = "#,##0.00";
                tableCekDurumu.Columns[0].Name = $"{curCode.pb}";
                tableCekDurumu.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı : ";
                tableCekDurumu.Columns[1].Name = " ";
                tableCekDurumu.Columns[2].Name = "  ";
                tableCekDurumu.Columns[3].Name = "   ";
                tableCekDurumu.Columns[4].Name = Labels.Tutar;
                tableCekDurumu.Columns[5].Name = Labels.DovizTipi;
                foreach (var item in FaaliyetCekDurum.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"J{i}"].Value = item.DEFINITION_;
                    ws.Cells[$"K{i}"].Value = string.Empty;
                    ws.Cells[$"L{i}"].Value = string.Empty;
                    ws.Cells[$"M{i}"].Value = item.SAYI;
                    ws.Cells[$"N{i}"].Value = item.TUTAR;
                    ws.Cells[$"O{i}"].Value = item.CURCODE;
                }

                tableCekDurumu.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableCekDurumu.Columns)
                {
                    if (column.Name == "Tutar")
                    {
                        column.TotalRowFunction = TotalRowFunction.Sum;
                    }}

                i = (tableCekDurumu.TotalRowRange.BottomRowIndex) + _spaceRow;
            }

            #endregion

            #region Kredi Durumu 


            #region Grup Başlığı Renklendir

            range = ws.Range[$"J{i}:O{i}"];
            ws.MergeCells(range);
            range.Value = "Kredi Durumu";
            range.Style = workbook.Styles[krediBaslikStyle];
            i++;

            #endregion


            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetKrediDurumu;
            List<AtaKredi> krediDrum = GetPocoList<AtaKredi>();

            currencyList = krediDrum.GroupBy(a => a.DOVIZ).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
               // i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;

                int dataRowCount = krediDrum.Count(a => a.DOVIZ == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableKredi = ws.Tables.Add(ws[$"J{i}:O{dataCount}"], true);
                tableKredi.ShowTotals = true;
                tableKredi.AutoFilter.Disable();
                tableKredi.Style = workbook.TableStyles[krediStyle];
                tableKredi.DataRange.NumberFormat = "#,##0.00";
                tableKredi.Columns[0].Name = $"{curCode.pb} {Labels.KrediKodu}";
                tableKredi.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı :";
                tableKredi.Columns[1].Name = Labels.KrediAciklamasi;
                tableKredi.Columns[2].Name = Labels.DovizTipi;
                tableKredi.Columns[3].Name = Labels.KrediAnaPara;
                tableKredi.Columns[4].Name = Labels.KrediFaiz;
                tableKredi.Columns[5].Name = Labels.KrediToplam;

                foreach (var item in krediDrum.Where(a => a.DOVIZ == curCode.pb))
                {
                    i++;
                    ws.Cells[$"J{i}"].Value = item.CODE;
                    ws.Cells[$"K{i}"].Value = item.DEFINITION_;
                    ws.Cells[$"L{i}"].Value = item.DOVIZ;
                    ws.Cells[$"M{i}"].Value = item.TRTOTAL;
                    ws.Cells[$"N{i}"].Value = item.INTTOTAL;
                    ws.Cells[$"O{i}"].Value = item.BAKIYE;
                }

                tableKredi.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableKredi.Columns)
                {
                    if (column.Index > 2)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }
                i = (tableKredi.TotalRowRange.BottomRowIndex+1) + _spaceRow;
            }
            #endregion

            #region Müşteri Kendi Çek Senet Durumu 


            #region Grup Başlığı Renklendir

            range = ws.Range[$"J{i}:O{i}"];
            ws.MergeCells(range);
            range.Value = "Firma Çek Senet Durumu";
            range.Style = workbook.Styles[krediBaslikStyle];
            i++;

            #endregion


            _reportTag = RaporcuV2.Helper.ReportTags.FaaliyetMusteriCekSenetDurum;
            List<FaaliyetMusteriCekSenetDurumu> MusteriCekSenetDurumu = GetPocoList<FaaliyetMusteriCekSenetDurumu>();

            currencyList = MusteriCekSenetDurumu.GroupBy(a => a.CURCODE).Select(s => new { pb = s.Key });
            isFirstRow = true;
            foreach (var curCode in currencyList)
            {
                // i = ws.GetUsedRange().RowCount + 1;

                if (!isFirstRow)
                {
                    i++;
                }

                isFirstRow = false;

                int dataRowCount = MusteriCekSenetDurumu.Count(a => a.CURCODE == curCode.pb);
                dataCount = dataRowCount + i;
                Table tableMusteriCekSenetDurumu = ws.Tables.Add(ws[$"J{i}:O{dataCount}"], true);
                tableMusteriCekSenetDurumu.ShowTotals = true;
                tableMusteriCekSenetDurumu.AutoFilter.Disable();
                tableMusteriCekSenetDurumu.Style = workbook.TableStyles[krediStyle];
                tableMusteriCekSenetDurumu.DataRange.NumberFormat = "#,##0.00";
                tableMusteriCekSenetDurumu.Columns[0].DataRange.NumberFormat = "dd/mm/yyyy";
                tableMusteriCekSenetDurumu.Columns[0].Name = Labels.VadeGunu;
                tableMusteriCekSenetDurumu.Columns[0].TotalRowLabel = $"{curCode.pb} Toplamı :";
                tableMusteriCekSenetDurumu.Columns[1].Name = Labels.BankaUnvani;
                tableMusteriCekSenetDurumu.Columns[2].Name = Labels.CariHesapUnvani;
                tableMusteriCekSenetDurumu.Columns[3].Name = Labels.OzelKod;
                tableMusteriCekSenetDurumu.Columns[4].Name = Labels.Tutar;
                tableMusteriCekSenetDurumu.Columns[5].Name = Labels.EvrakTuru;
                


                foreach (var item in MusteriCekSenetDurumu.Where(a => a.CURCODE == curCode.pb))
                {
                    i++;
                    ws.Cells[$"J{i}"].Value = item.DUEDATE;
                    ws.Cells[$"K{i}"].Value = item.NAME;
                    ws.Cells[$"L{i}"].Value = item.CARI;
                    ws.Cells[$"M{i}"].Value = item.SPECODE;
                    ws.Cells[$"N{i}"].Value = item.TOTAL;
                    ws.Cells[$"O{i}"].Value = item.TYPE;
                }

                tableMusteriCekSenetDurumu.TotalRowRange.NumberFormat = "#,##0.00";
                foreach (TableColumn column in tableMusteriCekSenetDurumu.Columns)
                {
                    if (column.Index > 3)
                        column.TotalRowFunction = TotalRowFunction.Sum;
                }
                i = (tableMusteriCekSenetDurumu.TotalRowRange.BottomRowIndex + 1) + _spaceRow;
            }
            #endregion

            #endregion

            var allCell = ws.GetUsedRange();

            allCell.Alignment.WrapText = true;

            spreadsheetControl1.EndUpdate();
            ws.Range["C5"].Style.NumberFormat = "#,##0.00";

            #endregion
        }
    }

}