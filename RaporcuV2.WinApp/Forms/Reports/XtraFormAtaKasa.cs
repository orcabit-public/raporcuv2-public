﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using DevExpress.Utils.Extensions;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using RaporcuV2.Model.Muhasebe;
using RaporcuV2.WinApp.Forms.Customs;
using RaporcuV2.Helper.Sorgular;
using RaporcuV2.Model;
using RaporcuV2.Model.Reports;
using System.Drawing;
using System;


namespace RaporcuV2.WinApp.Forms.Reports
{
    public partial class XtraFormAtaKasa : XtraFormBase, IReport
    {
        public XtraFormAtaKasa()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Methots


        #region Add Events

        public void AddEvents()
        {
            this.Load += (sender, args) =>
            {
                CreateFirmLookup();
                CreatePeriodLookup();
                CreateRefreshButton();
                CreateFirstDate();
            };
        }

        #endregion
        
        #endregion

        #region Refresh Grid Data

        public void RefreshGridData()
        {
            SqlText = SqlReports.KasaHareketleri;
            RefreshGridData<KasaHareketleri>();

            ExportDeductList();

        }

        #endregion

        #region Export Deduct List

        public void ExportDeductList()
        {
            #region KasaDurumu
            
            TrCurr = 0;
            SqlText = SqlReports.AtaKasa;
            List<AtaKasa> source = GetPocoList<AtaKasa>();
            ExcelPackage package = new ExcelPackage();

            var ws = package.Workbook.Worksheets.Add("Kasa Raporu");
            ws.PrinterSettings.Orientation = eOrientation.Landscape;
            ws.Cells["B1"].Value = this.FirmName;
            ws.Cells["B1:O1"].Style.Font.Size = 16;
            ws.Cells["B1:O1"].Style.Font.Bold = true;
            ws.Cells["B1:O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            ws.Cells["B1:O1"].Merge = true;
            ws.Cells["B2"].Value = FirstDate;
            ws.Cells["C2"].Value = "Tarihli Kasa Durum Raporu";
            ws.Cells["B2"].Style.Numberformat.Format = "d mmmm yyyy dddd";
            ws.Cells["B2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            ws.Cells["B2:C2"].Style.Font.Bold = true;
            ws.Cells["B2:C2"].Style.Font.Size = 16;
            int IndexStart = 4;
            int IndexStartB = IndexStart + 1;

            #region Başlık Renklendir

            ws.Cells["B" + IndexStart].Value = "Kasa Durumu";
            ws.Cells["B" + IndexStart].Style.Font.Size = 14;
            ws.Cells["B" + IndexStart].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + IndexStart].Style.Fill.BackgroundColor.SetColor(Color.White);
            ws.Cells["B" + IndexStart].Style.Font.Color.SetColor(Color.Crimson);

            #endregion

            ws.Cells["B" + IndexStart].Style.Font.Bold = true;
            ws.Cells["B" + IndexStart].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells["B" + IndexStartB].Value = Util.Labels.KasaKodu;
            ws.Cells["C" + IndexStartB].Value = Util.Labels.Aciklama;
            ws.Cells["D" + IndexStartB].Value = Util.Labels.DunDevir;
            ws.Cells["E" + IndexStartB].Value = Util.Labels.Borc;
            ws.Cells["F" + IndexStartB].Value = Util.Labels.Alacak;
            ws.Cells["G" + IndexStartB].Value = Util.Labels.Bakiye;
            ws.Cells["H" + IndexStartB].Value = Util.Labels.YarinDevir;
            ws.Cells["B" + IndexStartB + ":H" + IndexStartB].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + IndexStartB + ":H" + IndexStartB].Style.Fill.BackgroundColor.SetColor(Color.Crimson);
            ws.Cells["B" + IndexStartB + ":H" + IndexStartB].Style.Font.Color.SetColor(Color.White);



            int rowIndexBegin = IndexStart + 2;
            int rowIndexCurrentRecord = rowIndexBegin;
            foreach (AtaKasa item in source)
            {

                ws.Cells["B" + rowIndexCurrentRecord].Value = item.KODU;
                ws.Cells["C" + rowIndexCurrentRecord].Value = item.ACIKLAMA;
                ws.Cells["D" + rowIndexCurrentRecord].Value = item.DEVIR;
                ws.Cells["E" + rowIndexCurrentRecord].Value = item.BORC;
                ws.Cells["F" + rowIndexCurrentRecord].Value = item.ALACAK;
                ws.Cells["G" + rowIndexCurrentRecord].Value = item.BAKIYE;
                ws.Cells["H" + rowIndexCurrentRecord].Value = item.YARIN;
                ws.Cells["D" + rowIndexCurrentRecord + ":H" + rowIndexCurrentRecord].Style.Numberformat.Format ="#,##0.00";


                rowIndexCurrentRecord++;
            }
            
            ws.Column(1).Width = 250.57;
            ws.Column(2).Width = 8.29;
            ws.Column(3).Width = 8.57;
            ws.Column(4).Width = 8.57;



            rowIndexCurrentRecord = rowIndexCurrentRecord - 1;
            ws.Cells["B" + IndexStartB + ":H" + rowIndexCurrentRecord].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB + ":H" + rowIndexCurrentRecord].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB + ":H" + rowIndexCurrentRecord].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB + ":H" + rowIndexCurrentRecord].Style.Border.Bottom.Style =
                ExcelBorderStyle.Thin;




            #endregion

            #region KasaHareketleriTL

            TrCurr = 0;
            SqlText = SqlReports.AtaKasaHareketleri;
            List<AtaKasaHareketleri> source1 = GetPocoList<AtaKasaHareketleri>();

            int IndexStart1 = 4;
            int IndexStartB1 = IndexStart + 1;

            #region Başlık Renklendir

            ws.Cells["J" + IndexStart1].Value = "Kasa Hareketleri";
            ws.Cells["J" + IndexStart1].Style.Font.Size = 14;
            ws.Cells["J" + IndexStart1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["J" + IndexStart1].Style.Fill.BackgroundColor.SetColor(Color.White);
            ws.Cells["J" + IndexStart1].Style.Font.Color.SetColor(Color.Crimson);

            #endregion

            ws.Cells["J" + IndexStart1].Style.Font.Bold = true;
            ws.Cells["J" + IndexStart1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells["J" + IndexStartB1].Value = Util.Labels.KasaAdi;
            ws.Cells["K" + IndexStartB1].Value = Util.Labels.Aciklama;
            ws.Cells["L" + IndexStartB1].Value = Util.Labels.IslemAciklama;
            ws.Cells["M" + IndexStartB1].Value = Util.Labels.Borc;
            ws.Cells["N" + IndexStartB1].Value = Util.Labels.Alacak;
            ws.Cells["O" + IndexStartB1].Value = Util.Labels.Bolum;
            ws.Cells["J" + IndexStartB1 + ":O" + IndexStartB1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["J" + IndexStartB1 + ":O" + IndexStartB1].Style.Fill.BackgroundColor.SetColor(Color.Crimson);
            ws.Cells["J" + IndexStartB1 + ":O" + IndexStartB1].Style.Font.Color.SetColor(Color.White);

            int rowIndexBegin1 = IndexStart1 + 2;
            int rowIndexCurrentRecord1 = rowIndexBegin1;
            foreach (AtaKasaHareketleri item1 in source1)
            {

                ws.Cells["J" + rowIndexCurrentRecord1].Value = item1.NAME;
                ws.Cells["K" + rowIndexCurrentRecord1].Value = item1.CUSTTITLE;
                ws.Cells["L" + rowIndexCurrentRecord1].Value = item1.LINEEXP;
                ws.Cells["M" + rowIndexCurrentRecord1].Value = item1.BORC;
                ws.Cells["N" + rowIndexCurrentRecord1].Value = item1.ALACAK;
                ws.Cells["O" + rowIndexCurrentRecord1].Value = item1.DEPARTMENT;
                ws.Cells["M" + rowIndexCurrentRecord1 + ":N" + rowIndexCurrentRecord1].Style.Numberformat.Format = "₺#,##0.00";


                rowIndexCurrentRecord1++;
            }

            
            var cellGrandTotalFormula1 = ws.Cells["M" + rowIndexCurrentRecord1];
            cellGrandTotalFormula1.Style.Font.Bold = true;
            cellGrandTotalFormula1.Style.Numberformat.Format = "₺#,##0.00";
            string sumFormula1 = "SUM(R[" + (rowIndexBegin1 - rowIndexCurrentRecord1) + "]C:R[-1]C)";

            var cellGrandTotalFormulaD1 = ws.Cells["N" + rowIndexCurrentRecord1];
            cellGrandTotalFormulaD1.Style.Font.Bold = true;
            cellGrandTotalFormulaD1.Style.Numberformat.Format = "₺#,##0.00";
            string sumFormulaD1 = "SUM(R[" + (rowIndexBegin1 - rowIndexCurrentRecord1) + "]C:R[-1]C)";


            if (rowIndexBegin1 == rowIndexCurrentRecord1)
            {
                cellGrandTotalFormula1.FormulaR1C1 =  "0";
                cellGrandTotalFormulaD1.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula1.FormulaR1C1 = sumFormula1;
                cellGrandTotalFormulaD1.FormulaR1C1 = sumFormulaD1;
            }

            //ws.Column(1).Width = 8.29;
            //ws.Column(2).Width = 102.57;
            //ws.Column(3).Width = 24.57;
            //ws.Column(4).Width = 24.57;

            rowIndexCurrentRecord1 = rowIndexCurrentRecord1 - 1;
            ws.Cells["J" + IndexStartB1 + ":O" + rowIndexCurrentRecord1].Style.Border.Left.Style =ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB1 + ":O" + rowIndexCurrentRecord1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB1 + ":O" + rowIndexCurrentRecord1].Style.Border.Right.Style =ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB1 + ":O" + rowIndexCurrentRecord1].Style.Border.Bottom.Style =ExcelBorderStyle.Thin;

            //ws.Cells[ws.Dimension.Address].AutoFitColumns();  son satırda olacak


            #endregion

            #region KasaHareketleriUSD

            TrCurr = 1;
            SqlText = SqlReports.AtaKasaHareketleri;
            List<AtaKasaHareketleri> source2 = GetPocoList<AtaKasaHareketleri>();

            int IndexStart2 = rowIndexCurrentRecord1 + 2;
            int IndexStartB2 = IndexStart2 + 1;


            ws.Cells["J" + IndexStart2].Style.Font.Bold = true;
            ws.Cells["J" + IndexStart2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells["J" + IndexStartB2].Value = Util.Labels.KasaAdi;
            ws.Cells["K" + IndexStartB2].Value = Util.Labels.Aciklama;
            ws.Cells["L" + IndexStartB2].Value = Util.Labels.IslemAciklama;
            ws.Cells["M" + IndexStartB2].Value = Util.Labels.Borc;
            ws.Cells["N" + IndexStartB2].Value = Util.Labels.Alacak;
            ws.Cells["J" + IndexStartB2 + ":N" + IndexStartB2].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["J" + IndexStartB2 + ":N" + IndexStartB2].Style.Fill.BackgroundColor.SetColor(Color.Crimson);
            ws.Cells["J" + IndexStartB2 + ":N" + IndexStartB2].Style.Font.Color.SetColor(Color.White);

            int rowIndexBegin2 = IndexStart2 + 2;
            int rowIndexCurrentRecord2 = rowIndexBegin2;
            foreach (AtaKasaHareketleri item2 in source2)
            {

                ws.Cells["J" + rowIndexCurrentRecord2].Value = item2.NAME;
                ws.Cells["K" + rowIndexCurrentRecord2].Value = item2.CUSTTITLE;
                ws.Cells["L" + rowIndexCurrentRecord2].Value = item2.LINEEXP;
                ws.Cells["M" + rowIndexCurrentRecord2].Value = item2.BORC;
                ws.Cells["N" + rowIndexCurrentRecord2].Value = item2.ALACAK;
                ws.Cells["M" + rowIndexCurrentRecord2 + ":N" + rowIndexCurrentRecord2].Style.Numberformat.Format =
                    "#,##0.00";


                rowIndexCurrentRecord2++;
            }

            var cellGrandTotalFormula2 = ws.Cells["M" + rowIndexCurrentRecord2];
            cellGrandTotalFormula2.Style.Font.Bold = true;

            string sumFormula2 = "SUM(R[" + (rowIndexBegin2 - rowIndexCurrentRecord2) + "]C:R[-1]C)";



            var cellGrandTotalFormulaD2 = ws.Cells["N" + rowIndexCurrentRecord2];
            cellGrandTotalFormulaD2.Style.Font.Bold = true;

            string sumFormulaD2 = "SUM(R[" + (rowIndexBegin2 - rowIndexCurrentRecord2) + "]C:R[-1]C)";

            ws.Cells["M" + rowIndexCurrentRecord2 + ":N" + rowIndexCurrentRecord2].Style.Numberformat.Format = "$#,##0.00";
            if (rowIndexBegin2 == rowIndexCurrentRecord2)
            {
                cellGrandTotalFormula2.FormulaR1C1 = "0";
                cellGrandTotalFormulaD2.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula2.FormulaR1C1 = sumFormula2;
                cellGrandTotalFormulaD2.FormulaR1C1 = sumFormulaD2;
            }


            rowIndexCurrentRecord2 = rowIndexCurrentRecord2 - 1;
            ws.Cells["J" + IndexStartB2 + ":N" + rowIndexCurrentRecord2].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB2 + ":N" + rowIndexCurrentRecord2].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB2 + ":N" + rowIndexCurrentRecord2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB2 + ":N" + rowIndexCurrentRecord2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

            //ws.Cells[ws.Dimension.Address].AutoFitColumns();  son satırda olacak


            #endregion

            #region KasaHareketleriEURO

            TrCurr = 20;
            SqlText = SqlReports.AtaKasaHareketleri;
            List<AtaKasaHareketleri> source3 = GetPocoList<AtaKasaHareketleri>();

            int IndexStart3 = rowIndexCurrentRecord2 + 2;
            int IndexStartB3 = IndexStart3 + 1;


            ws.Cells["J" + IndexStart3].Style.Font.Bold = true;
            ws.Cells["J" + IndexStart3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells["J" + IndexStartB3].Value = Util.Labels.KasaAdi;
            ws.Cells["K" + IndexStartB3].Value = Util.Labels.Aciklama;
            ws.Cells["L" + IndexStartB3].Value = Util.Labels.IslemAciklama;
            ws.Cells["M" + IndexStartB3].Value = Util.Labels.Borc;
            ws.Cells["N" + IndexStartB3].Value = Util.Labels.Alacak;
            ws.Cells["J" + IndexStartB3 + ":N" + IndexStartB3].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["J" + IndexStartB3 + ":N" + IndexStartB3].Style.Fill.BackgroundColor.SetColor(Color.Crimson);
            ws.Cells["J" + IndexStartB3 + ":N" + IndexStartB3].Style.Font.Color.SetColor(Color.White);

            int rowIndexBegin3 = IndexStart3 + 2;
            int rowIndexCurrentRecord3 = rowIndexBegin3;
            foreach (AtaKasaHareketleri item3 in source3)
            {

                ws.Cells["J" + rowIndexCurrentRecord3].Value = item3.NAME;
                ws.Cells["K" + rowIndexCurrentRecord3].Value = item3.CUSTTITLE;
                ws.Cells["L" + rowIndexCurrentRecord3].Value = item3.LINEEXP;
                ws.Cells["M" + rowIndexCurrentRecord3].Value = item3.BORC;
                ws.Cells["N" + rowIndexCurrentRecord3].Value = item3.ALACAK;
                ws.Cells["M" + rowIndexCurrentRecord3 + ":N" + rowIndexCurrentRecord3].Style.Numberformat.Format = "#,##0.00";


                rowIndexCurrentRecord3++;
            }

            var cellGrandTotalFormula3 = ws.Cells["M" + rowIndexCurrentRecord3];
            cellGrandTotalFormula3.Style.Font.Bold = true;

            string sumFormula3 = "SUM(R[" + (rowIndexBegin3 - rowIndexCurrentRecord3) + "]C:R[-1]C)";

            var cellGrandTotalFormulaD3 = ws.Cells["N" + rowIndexCurrentRecord3];
            cellGrandTotalFormulaD3.Style.Font.Bold = true;

            string sumFormulaD3 = "SUM(R[" + (rowIndexBegin3 - rowIndexCurrentRecord3) + "]C:R[-1]C)";

            ws.Cells["M" + rowIndexCurrentRecord3 + ":N" + rowIndexCurrentRecord3].Style.Numberformat.Format = "€#,##0.00";
            if (rowIndexBegin3 == rowIndexCurrentRecord3)
            {
                cellGrandTotalFormula3.FormulaR1C1 = "0";
                cellGrandTotalFormulaD3.FormulaR1C1 = "0";
            }
            else
            {
                cellGrandTotalFormula3.FormulaR1C1 = sumFormula3;
                cellGrandTotalFormulaD3.FormulaR1C1 = sumFormulaD3;
            }

            rowIndexCurrentRecord3 = rowIndexCurrentRecord3 - 1;
            ws.Cells["J" + IndexStartB3 + ":N" + rowIndexCurrentRecord3].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB3 + ":N" + rowIndexCurrentRecord3].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB3 + ":N" + rowIndexCurrentRecord3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB3 + ":N" + rowIndexCurrentRecord3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

            //ws.Cells[ws.Dimension.Address].AutoFitColumns();  son satırda olacak


            #endregion

            #region BankaDurumu

            TrCurr = 0;
            SqlText = SqlReports.BankaDurumu;
            List<AtaBanka> source4 = GetPocoList<AtaBanka>();

            int IndexStart4 = rowIndexCurrentRecord3 + 4;
            int IndexStartB4 = IndexStart4 + 1;
            #region Başlık Renklendir

            ws.Cells["B" + IndexStart4].Value = "Banka Durumu";
            ws.Cells["B" + IndexStart4].Style.Font.Size = 14;
            ws.Cells["B" + IndexStart4].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + IndexStart4].Style.Fill.BackgroundColor.SetColor(Color.White);
            ws.Cells["B" + IndexStart4].Style.Font.Color.SetColor(Color.Blue);

            #endregion
            ws.Cells["B" + IndexStart4].Style.Font.Bold = true;
            ws.Cells["B" + IndexStart4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells["B" + IndexStartB4].Value = Util.Labels.BankaKodu;
            ws.Cells["C" + IndexStartB4].Value = Util.Labels.BankaUnvani;
            ws.Cells["D" + IndexStartB4].Value = Util.Labels.DunDevir;
            ws.Cells["E" + IndexStartB4].Value = Util.Labels.Borc;
            ws.Cells["F" + IndexStartB4].Value = Util.Labels.Alacak;
            ws.Cells["G" + IndexStartB4].Value = Util.Labels.Bakiye;
            ws.Cells["H" + IndexStartB4].Value = Util.Labels.YarinDevir;
            ws.Cells["B" + IndexStartB4 + ":H" + IndexStartB4].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + IndexStartB4 + ":H" + IndexStartB4].Style.Fill.BackgroundColor.SetColor(Color.Blue);
            ws.Cells["B" + IndexStartB4 + ":H" + IndexStartB4].Style.Font.Color.SetColor(Color.White);

            int rowIndexBegin4 = IndexStart4 + 2;
            int rowIndexCurrentRecord4 = rowIndexBegin4;
            foreach (AtaBanka item4 in source4)
            {
                // Kredi Kartları Banka Toplamlarında Gelmeyecek
                bool isCreditCard = item4.KODU.StartsWith("01.03");
                if (!isCreditCard)
                {

                    ws.Cells["B" + rowIndexCurrentRecord4].Value = item4.KODU;
                    ws.Cells["C" + rowIndexCurrentRecord4].Value = item4.ACIKLAMA;
                    ws.Cells["D" + rowIndexCurrentRecord4].Value = item4.DEVIR;
                    ws.Cells["E" + rowIndexCurrentRecord4].Value = item4.BORC;
                    ws.Cells["F" + rowIndexCurrentRecord4].Value = item4.ALACAK;
                    ws.Cells["G" + rowIndexCurrentRecord4].Value = item4.BAKIYE;
                    ws.Cells["H" + rowIndexCurrentRecord4].Value = item4.YARIN;
                    ws.Cells["D" + rowIndexCurrentRecord4 + ":H" + rowIndexCurrentRecord4].Style.Numberformat.Format =
                        "#,##0.00";

                    rowIndexCurrentRecord4++;
                }
            }


            rowIndexCurrentRecord4 = rowIndexCurrentRecord4 - 1;
            ws.Cells["B" + IndexStartB4 + ":H" + rowIndexCurrentRecord4].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB4 + ":H" + rowIndexCurrentRecord4].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB4 + ":H" + rowIndexCurrentRecord4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB4 + ":H" + rowIndexCurrentRecord4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

            #region Banka Durum Toplam Yarına Devir Sütünü

            #region Alper yaptığı
            int rowIndexCurrentRecordToplam1 = (rowIndexCurrentRecord4) + 1;

            //TL Toplmı koddan toplama yapıyor bu nedenle kontrol edilmesi gerekiyor 
            ws.Cells["G" + rowIndexCurrentRecordToplam1].Value = "TL";
            ws.Cells["H" + rowIndexCurrentRecordToplam1].Value = source4.Where(a => a.KODU.StartsWith("01.01")).Sum(a => a.YARIN);
            ws.Cells["G" + (rowIndexCurrentRecordToplam1) + ":H" + (rowIndexCurrentRecordToplam1)].Style.Font.Bold = true;
            ws.Cells["H" + rowIndexCurrentRecordToplam1 + ":H" + rowIndexCurrentRecordToplam1].Style.Numberformat.Format = "#,##0.00";
            rowIndexCurrentRecordToplam1++;
            //USD 
            ws.Cells["G" + rowIndexCurrentRecordToplam1].Value = "USD";
            ws.Cells["H" + rowIndexCurrentRecordToplam1].Value = source4.Where(a => a.KODU.StartsWith("02.01")).Sum(a => a.YARIN);
            ws.Cells["G" + (rowIndexCurrentRecordToplam1) + ":H" + (rowIndexCurrentRecordToplam1)].Style.Font.Bold = true;
            ws.Cells["H" + rowIndexCurrentRecordToplam1 + ":H" + rowIndexCurrentRecordToplam1].Style.Numberformat.Format = "#,##0.00";
            rowIndexCurrentRecordToplam1++;

            //EURO
            ws.Cells["G" + rowIndexCurrentRecordToplam1].Value = "EURO";
            ws.Cells["H" + rowIndexCurrentRecordToplam1].Value = source4.Where(a => a.KODU.StartsWith("03.01")).Sum(a => a.YARIN);
            ws.Cells["G" + (rowIndexCurrentRecordToplam1) + ":H" + (rowIndexCurrentRecordToplam1)].Style.Font.Bold = true;
            ws.Cells["H" + rowIndexCurrentRecordToplam1 + ":H" + rowIndexCurrentRecordToplam1].Style.Numberformat.Format = "#,##0.00";
            rowIndexCurrentRecordToplam1++;


            //SqlText = SqlReports.AtaBankaToplami;
            //List<AtaBankaToplam> BankasourceToplam1 = GetPocoList<AtaBankaToplam>();


            //int rowIndexCurrentRecordToplam1 = (rowIndexCurrentRecord4)+1;

            //foreach (AtaBankaToplam ataBankaToplami in BankasourceToplam1)
            //{
            //    ws.Cells["D" + rowIndexCurrentRecordToplam1].Value = ataBankaToplami.CURCODE;
            //    ws.Cells["E" + rowIndexCurrentRecordToplam1].Value = ataBankaToplami.BORC;
            //    ws.Cells["F" + rowIndexCurrentRecordToplam1].Value = ataBankaToplami.ALACAK;
            //    ws.Cells["G" + rowIndexCurrentRecordToplam1].Value = ataBankaToplami.BORC - ataBankaToplami.ALACAK;

            //    ws.Cells["D" + (rowIndexCurrentRecordToplam1) + ":F" + (rowIndexCurrentRecordToplam1)].Style.Font.Bold = false;
            //    ws.Cells["E" + rowIndexCurrentRecordToplam1 + ":F" + rowIndexCurrentRecordToplam1].Style.Numberformat.Format = "#,##0.00";

            //    rowIndexCurrentRecordToplam1++;
            //}

            #endregion

            //var sum = BankasourceToplam1.Where(a => a.CURCODE == "TL").Sum(a => a.BORC);

            #endregion

            //ws.Cells[ws.Dimension.Address].AutoFitColumns();  son satırda olacak


            #endregion

            #region BankaHareketleri

            TrCurr = 0;
            SqlText = SqlReports.AtaBankaHareketleri;
            List<AtaBankaHareketleri> source5 = GetPocoList<AtaBankaHareketleri>();

            #region Başlık Renklendir

            ws.Cells["J" + IndexStart4].Value = "Banka Hareketleri";
            ws.Cells["J" + IndexStart4].Style.Font.Size = 14;
            ws.Cells["J" + IndexStart4].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["J" + IndexStart4].Style.Fill.BackgroundColor.SetColor(Color.White);
            ws.Cells["J" + IndexStart4].Style.Font.Color.SetColor(Color.Blue);

            #endregion

            ws.Cells["J" + IndexStart4].Style.Font.Bold = true;
            ws.Cells["J" + IndexStart4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells["J" + IndexStartB4].Value = Util.Labels.Banka;
            ws.Cells["K" + IndexStartB4].Value = Util.Labels.CariHesapUnvani;
            ws.Cells["L" + IndexStartB4].Value = Util.Labels.IslemAciklama;
            ws.Cells["M" + IndexStartB4].Value = Util.Labels.Borc;
            ws.Cells["N" + IndexStartB4].Value = Util.Labels.Alacak;
            ws.Cells["J" + IndexStartB4 + ":N" + IndexStartB4].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["J" + IndexStartB4 + ":N" + IndexStartB4].Style.Fill.BackgroundColor.SetColor(Color.Blue);
            ws.Cells["J" + IndexStartB4 + ":N" + IndexStartB4].Style.Font.Color.SetColor(Color.White);

            int rowIndexBegin5 = IndexStart4 + 2;
            int rowIndexCurrentRecord5 = rowIndexBegin5;
            foreach (AtaBankaHareketleri item5 in source5)
            {

                ws.Cells["J" + rowIndexCurrentRecord5].Value = item5.DEFINITION_;
                ws.Cells["K" + rowIndexCurrentRecord5].Value = item5.CARI;
                ws.Cells["L" + rowIndexCurrentRecord5].Value = item5.LINEEXP;
                ws.Cells["M" + rowIndexCurrentRecord5].Value = item5.DVZALACAK;
                ws.Cells["N" + rowIndexCurrentRecord5].Value = item5.DVZBORC; 
                ws.Cells["M" + rowIndexCurrentRecord5 + ":N" + rowIndexCurrentRecord5].Style.Numberformat.Format = "#,##0.00";


                rowIndexCurrentRecord5++;
            }


            rowIndexCurrentRecord5 = rowIndexCurrentRecord5 - 1;
            ws.Cells["J" + IndexStartB4 + ":N" + rowIndexCurrentRecord5].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB4 + ":N" + rowIndexCurrentRecord5].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB4 + ":N" + rowIndexCurrentRecord5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB4 + ":N" + rowIndexCurrentRecord5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells[ws.Dimension.Address].AutoFitColumns();

            #region Banka Hareket Durum Toplam

            //***************************************************
            rowIndexCurrentRecord5++;
            //TL Toplmı koddan toplama yapıyor bu nedenle kontrol edilmesi gerekiyor 
            ws.Cells["L" + rowIndexCurrentRecord5].Value = "TL";
            ws.Cells["M" + rowIndexCurrentRecord5].Value = source5.Where(a => a.DEFINITION_.Contains("TL")).Sum(a => a.DVZALACAK);
            ws.Cells["N" + rowIndexCurrentRecord5].Value = source5.Where(a => a.DEFINITION_.Contains("TL")).Sum(a => a.DVZBORC);
            ws.Cells["L" + (rowIndexCurrentRecord5) + ":N" + (rowIndexCurrentRecord5)].Style.Font.Bold = true;
            ws.Cells["L" + rowIndexCurrentRecord5 + ":N" + rowIndexCurrentRecord5].Style.Numberformat.Format = "#,##0.00";
            rowIndexCurrentRecord5++;

            //USD 
            ws.Cells["L" + rowIndexCurrentRecord5].Value = "USD";
            ws.Cells["M" + rowIndexCurrentRecord5].Value = source5.Where(a => a.DEFINITION_.Contains("USD")).Sum(a => a.DVZALACAK);
            ws.Cells["N" + rowIndexCurrentRecord5].Value = source5.Where(a => a.DEFINITION_.Contains("USD")).Sum(a => a.DVZBORC);
            ws.Cells["L" + (rowIndexCurrentRecord5) + ":N" + (rowIndexCurrentRecord5)].Style.Font.Bold = true;
            ws.Cells["L" + rowIndexCurrentRecord5 + ":N" + rowIndexCurrentRecord5].Style.Numberformat.Format = "#,##0.00";
            rowIndexCurrentRecord5++;

            //EURO
            ws.Cells["L" + rowIndexCurrentRecord5].Value = "EURO";
            ws.Cells["M" + rowIndexCurrentRecord5].Value = source5.Where(a => a.DEFINITION_.Contains("EURO")).Sum(a => a.DVZALACAK);
            ws.Cells["N" + rowIndexCurrentRecord5].Value = source5.Where(a => a.DEFINITION_.Contains("EURO")).Sum(a => a.DVZBORC);
            ws.Cells["L" + (rowIndexCurrentRecord5) + ":N" + (rowIndexCurrentRecord5)].Style.Font.Bold = true;
            ws.Cells["L" + rowIndexCurrentRecord5 + ":N" + rowIndexCurrentRecord5].Style.Numberformat.Format = "#,##0.00";
            rowIndexCurrentRecord5++;

            //KREDİ KARTI
            ws.Cells["L" + rowIndexCurrentRecord5].Value = "KREDİ KARTI";
            ws.Cells["M" + rowIndexCurrentRecord5].Value = source5.Where(a => a.DEFINITION_.Contains("KREDİ KARTI")).Sum(a => a.DVZALACAK);
            ws.Cells["N" + rowIndexCurrentRecord5].Value = source5.Where(a => a.DEFINITION_.Contains("KREDİ KARTI")).Sum(a => a.DVZBORC);
            ws.Cells["L" + (rowIndexCurrentRecord5) + ":N" + (rowIndexCurrentRecord5)].Style.Font.Bold = true;
            ws.Cells["L" + rowIndexCurrentRecord5 + ":N" + rowIndexCurrentRecord5].Style.Numberformat.Format = "#,##0.00";
            rowIndexCurrentRecord5++;


            #endregion

            #endregion

            #region KrediDurumu


            SqlText = SqlReports.KrediDurumu;
            List<AtaKredi> source6 = GetPocoList<AtaKredi>();
            int rowIndex = 0;
            if (rowIndexCurrentRecord5> rowIndexCurrentRecord4)
            {
                rowIndex = rowIndexCurrentRecord5;
            }
            else
            {
                rowIndex = rowIndexCurrentRecord4;
            }
            int  IndexStart6 = rowIndex + 4;
            int IndexStartB6 = IndexStart6 + 1;
            #region Başlık Renklendir

            ws.Cells["B" + IndexStart6].Value = "Kredi Durumu";
            ws.Cells["B" + IndexStart6].Style.Font.Size = 14;
            ws.Cells["B" + IndexStart6].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + IndexStart6].Style.Fill.BackgroundColor.SetColor(Color.White);
            ws.Cells["B" + IndexStart6].Style.Font.Color.SetColor(Color.Coral);

            #endregion

            #region Başlık Renklendir Emanet

            ws.Cells["J" + IndexStart6].Value = "Emanet Hesaplar Durumu";
            ws.Cells["J" + IndexStart6].Style.Font.Size = 14;
            ws.Cells["J" + IndexStart6].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["J" + IndexStart6].Style.Fill.BackgroundColor.SetColor(Color.White);
            ws.Cells["J" + IndexStart6].Style.Font.Color.SetColor(Color.Aqua);
            ws.Cells["j" + IndexStart6].Style.Font.Bold = true;
            #endregion
            ws.Cells["B" +  IndexStart6].Style.Font.Bold = true;
            ws.Cells["B" +  IndexStart6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells["B" + IndexStartB6].Value = Util.Labels.KrediKodu;
            ws.Cells["C" + IndexStartB6].Value = Util.Labels.KrediAciklamasi;
            ws.Cells["D" + IndexStartB6].Value = Util.Labels.Bakiye;
            ws.Cells["E" + IndexStartB6].Value = Util.Labels.DovizTipi;

            ws.Cells["B" + IndexStartB6 + ":E" + IndexStartB6].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + IndexStartB6 + ":E" + IndexStartB6].Style.Fill.BackgroundColor.SetColor(Color.Coral);
            ws.Cells["B" + IndexStartB6 + ":E" + IndexStartB6].Style.Font.Color.SetColor(Color.White);

            int rowIndexBegin6 = IndexStart6 + 2;
            int rowIndexCurrentRecord6 = rowIndexBegin6;
            foreach (AtaKredi item6 in source6)
            {
                ws.Cells["B" + rowIndexCurrentRecord6].Value = item6.CODE;
                ws.Cells["C" + rowIndexCurrentRecord6].Value = item6.DEFINITION_;
                ws.Cells["D" + rowIndexCurrentRecord6].Value = item6.BAKIYE;
                ws.Cells["E" + rowIndexCurrentRecord6].Value = item6.DOVIZ;

                ws.Cells["D" + rowIndexCurrentRecord6 + ":D" + rowIndexCurrentRecord6].Style.Numberformat.Format = "#,##0.00";


                rowIndexCurrentRecord6++;
            }


                 rowIndexCurrentRecord6 =        rowIndexCurrentRecord6 - 1;
            ws.Cells["B" + IndexStartB6 + ":E" + rowIndexCurrentRecord6].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB6 + ":E" + rowIndexCurrentRecord6].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB6 + ":E" + rowIndexCurrentRecord6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB6 + ":E" + rowIndexCurrentRecord6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

            //ws.Cells[ws.Dimension.Address].AutoFitColumns();  son satırda olacak


            #endregion

            #region PortfoyCekSenetCari

            TrCurr = 0;
            SqlText = SqlReports.PortfoyCekSenetCari;
            List<AtaPortfoyCekSenetCari> source7 = GetPocoList<AtaPortfoyCekSenetCari>();

            int IndexStart7 = rowIndexCurrentRecord6 + 4;
            int IndexStartB7 = IndexStart7 + 1;

            #region Başlık Renklendir

            ws.Cells["B" + IndexStart7].Value = "Portföy Çek Senet Müşteri Toplamı";
            ws.Cells["B" + IndexStart7].Style.Font.Size = 14;
            ws.Cells["B" + IndexStart7].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + IndexStart7].Style.Fill.BackgroundColor.SetColor(Color.White);
            ws.Cells["B" + IndexStart7].Style.Font.Color.SetColor(Color.DarkMagenta);
            ws.Cells["B" + IndexStart7].Style.Font.Bold = true;
            #endregion

            #region Başlık Renklendir

            ws.Cells["J" + IndexStart7].Value = "Portföy Çek Senet Toplamı";
            ws.Cells["J" + IndexStart7].Style.Font.Size = 14;
            ws.Cells["J" + IndexStart7].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["J" + IndexStart7].Style.Fill.BackgroundColor.SetColor(Color.White);
            ws.Cells["J" + IndexStart7].Style.Font.Color.SetColor(Color.DarkMagenta);
            ws.Cells["J" + IndexStart7].Style.Font.Bold = true;
            #endregion

            ws.Cells["B" +  IndexStart7].Style.Font.Bold = true;
            ws.Cells["B" +  IndexStart7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells["B" + IndexStartB7].Value = Util.Labels.CariHesapUnvani;
            ws.Cells["C" + IndexStartB7].Value = Util.Labels.CekSenetSayi;
            ws.Cells["D" + IndexStartB7].Value = Util.Labels.TL;
            ws.Cells["E" + IndexStartB7].Value = Util.Labels.USD;
            ws.Cells["F" + IndexStartB7].Value = Util.Labels.EURO;
            ws.Cells["B" + IndexStartB7 + ":F" + IndexStartB7].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + IndexStartB7 + ":F" + IndexStartB7].Style.Fill.BackgroundColor.SetColor(Color.DarkMagenta);
            ws.Cells["B" + IndexStartB7 + ":F" + IndexStartB7].Style.Font.Color.SetColor(Color.White);

            int rowIndexBegin7 = IndexStart7 + 2;
            int rowIndexCurrentRecord7 = rowIndexBegin7;
            foreach (AtaPortfoyCekSenetCari item7 in source7)
            {
                ws.Cells["B" + rowIndexCurrentRecord7].Value = item7.CARIISIM;
                ws.Cells["C" + rowIndexCurrentRecord7].Value = item7.SAYI;
                ws.Cells["D" + rowIndexCurrentRecord7].Value = item7.TL;
                ws.Cells["E" + rowIndexCurrentRecord7].Value = item7.DOLAR;
                ws.Cells["F" + rowIndexCurrentRecord7].Value = item7.EURO;

                ws.Cells["D" + rowIndexCurrentRecord7 + ":F" + rowIndexCurrentRecord7].Style.Numberformat.Format = "#,##0.00";


                rowIndexCurrentRecord7++;
            }


            rowIndexCurrentRecord7 = rowIndexCurrentRecord7 - 1;
            ws.Cells["B" + IndexStartB7 + ":F" + rowIndexCurrentRecord7].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB7 + ":F" + rowIndexCurrentRecord7].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB7 + ":F" + rowIndexCurrentRecord7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB7 + ":F" + rowIndexCurrentRecord7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

            //ws.Cells[ws.Dimension.Address].AutoFitColumns();  son satırda olacak


            #endregion

            #region MüşteriÇekSenetToplam

            TrCurr = 0;
            SqlText = SqlReports.MusteriCekSenetToplam;
            List<AtaMusteriCekSenetToplami> source8 = GetPocoList<AtaMusteriCekSenetToplami>();

            ws.Cells["J" +  IndexStart7].Style.Font.Bold = true;
            ws.Cells["J" +  IndexStart7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells["J" + IndexStartB7].Value = Util.Labels.EvrakTuru;
            ws.Cells["K" + IndexStartB7].Value = Util.Labels.Tutar;
            ws.Cells["L" + IndexStartB7].Value = Util.Labels.DovizTipi;
            ws.Cells["J" + IndexStartB7 + ":L" + IndexStartB7].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["J" + IndexStartB7 + ":L" + IndexStartB7].Style.Fill.BackgroundColor.SetColor(Color.DarkMagenta);
            ws.Cells["J" + IndexStartB7 + ":L" + IndexStartB7].Style.Font.Color.SetColor(Color.White);

            int rowIndexBegin8 = IndexStart7 + 2;
            int rowIndexCurrentRecord8 = rowIndexBegin8;
            foreach (AtaMusteriCekSenetToplami item8 in source8)
            {
                ws.Cells["J" + rowIndexCurrentRecord8].Value = item8.TUR;
                ws.Cells["K" + rowIndexCurrentRecord8].Value = item8.TUTAR;
                ws.Cells["L" + rowIndexCurrentRecord8].Value = item8.DOVIZ;

                ws.Cells["K" + rowIndexCurrentRecord8 + ":K" + rowIndexCurrentRecord8].Style.Numberformat.Format = "#,##0.00";

                rowIndexCurrentRecord8++;
            }

            // ws.PrinterSettings.PrintArea = ws.Cells["B1:D"+ (rowIndexCurrentRecord5+1)+","+"F1:I" + (rowIndexCurrentRecord12)];
            //ws.PrinterSettings.PrintArea = ws.Cells["A1:N35,A37:N6"];

            rowIndexCurrentRecord8 = rowIndexCurrentRecord8 - 1;
            ws.Cells["J" + IndexStartB7 + ":L" + rowIndexCurrentRecord8].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB7 + ":L" + rowIndexCurrentRecord8].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB7 + ":L" + rowIndexCurrentRecord8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + IndexStartB7 + ":L" + rowIndexCurrentRecord8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells[ws.Dimension.Address].AutoFitColumns();

            #endregion

            #region Çek Durumu

            TrCurr = 0;
            SqlText = SqlReports.CekDurumu;
            List<AtaCekDurumu> source9 = GetPocoList<AtaCekDurumu>();

            int rowIndex1 = 0;
            if (rowIndexCurrentRecord8 > rowIndexCurrentRecord7)
            {
                rowIndex1 = rowIndexCurrentRecord8;
            }
            else
            {
                rowIndex1 = rowIndexCurrentRecord7;
            }
            int IndexStart9 = rowIndex1 + 4;
            int IndexStartB9 = IndexStart9 + 1;

            #region Başlık Renklendir

            ws.Cells["B" + IndexStart9].Value = "Çek Durumu";
            ws.Cells["B" + IndexStart9].Style.Font.Size = 14;
            ws.Cells["B" + IndexStart9].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + IndexStart9].Style.Fill.BackgroundColor.SetColor(Color.White);
            ws.Cells["B" + IndexStart9].Style.Font.Color.SetColor(Color.DarkMagenta);

            #endregion

            #region Başlık Renklendir

            ws.Cells["J" + IndexStart9].Value = "Portfoy ve Takas Çekleri";
            ws.Cells["J" + IndexStart9].Style.Font.Size = 14;
            ws.Cells["J" + IndexStart9].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["J" + IndexStart9].Style.Fill.BackgroundColor.SetColor(Color.White);
            ws.Cells["J" + IndexStart9].Style.Font.Color.SetColor(Color.DarkMagenta);
            ws.Cells["J" + IndexStart9].Style.Font.Bold = true;
            #endregion

            ws.Cells["B" +  IndexStart9].Style.Font.Bold = true;
            ws.Cells["B" +  IndexStart9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells["B" + IndexStartB9].Value = Util.Labels.CariHesapUnvani;
            ws.Cells["C" + IndexStartB9].Value = Util.Labels.CekSayi;
            ws.Cells["D" + IndexStartB9].Value = Util.Labels.Tutar;
            ws.Cells["E" + IndexStartB9].Value = Util.Labels.DovizTipi;
            ws.Cells["B" + IndexStartB9 + ":E" + IndexStartB9].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + IndexStartB9 + ":E" + IndexStartB9].Style.Fill.BackgroundColor.SetColor(Color.DarkMagenta);
            ws.Cells["B" + IndexStartB9 + ":E" + IndexStartB9].Style.Font.Color.SetColor(Color.White);

            int rowIndexBegin9 = IndexStart9 + 2;
            int rowIndexCurrentRecord9 = rowIndexBegin9;
            foreach (AtaCekDurumu item9 in source9)
            {
                ws.Cells["B" + rowIndexCurrentRecord9].Value = item9.DEFINITION_;
                ws.Cells["C" + rowIndexCurrentRecord9].Value = item9.SAYI;
                ws.Cells["D" + rowIndexCurrentRecord9].Value = item9.TUTAR;
                ws.Cells["E" + rowIndexCurrentRecord9].Value = item9.CURCODE;


                ws.Cells["D" + rowIndexCurrentRecord9 + ":D" + rowIndexCurrentRecord9].Style.Numberformat.Format = "#,##0.00";

                rowIndexCurrentRecord9++;
            }


            if (rowIndexBegin9 == rowIndexCurrentRecord9)
            {

            }
            else
            {
                rowIndexCurrentRecord9 = rowIndexCurrentRecord9 - 1;
            }

            ws.Cells["B" + IndexStartB9 + ":E" + rowIndexCurrentRecord9].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB9 + ":E" + rowIndexCurrentRecord9].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB9 + ":E" + rowIndexCurrentRecord9].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB9 + ":E" + rowIndexCurrentRecord9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

            //ws.Cells[ws.Dimension.Address].AutoFitColumns();  son satırda olacak

            #region Çek Durum Toplamı

            rowIndexCurrentRecord9++;
            ws.Cells["C" + rowIndexCurrentRecord9].Value = "TL";
            ws.Cells["D" + rowIndexCurrentRecord9].Value = source9.Where(a => a.CURCODE == "TL").Sum(a => a.TUTAR);
            ws.Cells["D" + rowIndexCurrentRecord9 + ":D" + rowIndexCurrentRecord9].Style.Numberformat.Format = "#,##0.00";
            ws.Cells["C" + (rowIndexCurrentRecord9) + ":D" + (rowIndexCurrentRecord9)].Style.Font.Bold = true;
            rowIndexCurrentRecord9++;

            ws.Cells["C" + rowIndexCurrentRecord9].Value = "USD";
            ws.Cells["D" + rowIndexCurrentRecord9].Value = source9.Where(a => a.CURCODE == "USD").Sum(a => a.TUTAR);
            ws.Cells["D" + rowIndexCurrentRecord9 + ":D" + rowIndexCurrentRecord9].Style.Numberformat.Format = "#,##0.00";
            ws.Cells["C" + (rowIndexCurrentRecord9) + ":D" + (rowIndexCurrentRecord9)].Style.Font.Bold = true;
            rowIndexCurrentRecord9++;

            ws.Cells["C" + rowIndexCurrentRecord9].Value = "EURO";
            ws.Cells["D" + rowIndexCurrentRecord9].Value = source9.Where(a => a.CURCODE == "EURO").Sum(a => a.TUTAR);
            ws.Cells["D" + rowIndexCurrentRecord9 + ":D" + rowIndexCurrentRecord9].Style.Numberformat.Format = "#,##0.00";
            ws.Cells["C" + (rowIndexCurrentRecord9) + ":D" + (rowIndexCurrentRecord9)].Style.Font.Bold = true;
            
            
            #endregion

            #endregion

            #region Portfoy ve takas çekleri

            SqlText = SqlReports.PortfoyVeTakasCekleri;
            List<AtaPortfoyVeTakasCekleri> source10 = GetPocoList<AtaPortfoyVeTakasCekleri>();

            ws.Cells["J" +  IndexStart9].Style.Font.Bold = true;
            ws.Cells["J" +  IndexStart9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells["J" + IndexStartB9].Value = Util.Labels.CariBanka;
            ws.Cells["K" + IndexStartB9].Value = Util.Labels.VadeTarihi;
            ws.Cells["L" + IndexStartB9].Value = Util.Labels.Tutar;
            ws.Cells["M" + IndexStartB9].Value = Util.Labels.DovizTipi;
            ws.Cells["J" + IndexStartB9 + ":M" + IndexStartB9].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["J" + IndexStartB9 + ":M" + IndexStartB9].Style.Fill.BackgroundColor.SetColor(Color.DarkMagenta);
            ws.Cells["J" + IndexStartB9 + ":M" + IndexStartB9].Style.Font.Color.SetColor(Color.White);

            int rowIndexBegin10 = IndexStart9 + 2;
            int rowIndexCurrentRecord10 = rowIndexBegin10;
            foreach (AtaPortfoyVeTakasCekleri item10 in source10)
            {
                ws.Cells["J" + rowIndexCurrentRecord10].Value = item10.DEFINITION_;
                ws.Cells["K" + rowIndexCurrentRecord10].Value = item10.DUEDATE;
                ws.Cells["L" + rowIndexCurrentRecord10].Value = item10.TRNET;
                ws.Cells["M" + rowIndexCurrentRecord10].Value = item10.CURCODE;
                ws.Cells["K" + rowIndexCurrentRecord10 + ":K" + rowIndexCurrentRecord10].Style.Numberformat.Format = "dd- MM -yyyy";
                ws.Cells["L" + rowIndexCurrentRecord10 + ":L" + rowIndexCurrentRecord10].Style.Numberformat.Format = "#,##0.00";

                rowIndexCurrentRecord10++;
            }

            // ws.PrinterSettings.PrintArea = ws.Cells["B1:D"+ (rowIndexCurrentRecord5+1)+","+"F1:I" + (rowIndexCurrentRecord12)];
            //ws.PrinterSettings.PrintArea = ws.Cells["A1:N35,A37:N6"];
            if (rowIndexBegin10== rowIndexCurrentRecord10)
            {

            }
            else
            {
                rowIndexCurrentRecord10 = rowIndexCurrentRecord10 - 1;
            }

            
            ws.Cells["J" + rowIndexBegin10 + ":M" + rowIndexCurrentRecord10].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + rowIndexBegin10 + ":M" + rowIndexCurrentRecord10].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + rowIndexBegin10 + ":M" + rowIndexCurrentRecord10].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["J" + rowIndexBegin10 + ":M" + rowIndexCurrentRecord10].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Cells[ws.Dimension.Address].AutoFitColumns();

            //ws.PrinterSettings.PrintArea = ws.Cells["B1:N35"];

            #endregion

            #region Portfoy ve takas çekleri Toplamı

            rowIndexCurrentRecord10++;
            ws.Cells["M" + rowIndexCurrentRecord10].Value = "TL";
            ws.Cells["L" + rowIndexCurrentRecord10].Value = source10.Where(a => a.CURCODE == "TL").Sum(a => a.TRNET);
            ws.Cells["L" + rowIndexCurrentRecord10 + ":M" + rowIndexCurrentRecord10].Style.Numberformat.Format = "#,##0.00";
            ws.Cells["L" + (rowIndexCurrentRecord10) + ":M" + (rowIndexCurrentRecord10)].Style.Font.Bold = true;
            rowIndexCurrentRecord10++;

            ws.Cells["M" + rowIndexCurrentRecord10].Value = "USD";
            ws.Cells["L" + rowIndexCurrentRecord10].Value = source10.Where(a => a.CURCODE == "USD").Sum(a => a.TRNET);
            ws.Cells["L" + rowIndexCurrentRecord10 + ":M" + rowIndexCurrentRecord10].Style.Numberformat.Format = "#,##0.00";
            ws.Cells["L" + (rowIndexCurrentRecord10) + ":M" + (rowIndexCurrentRecord10)].Style.Font.Bold = true;
            rowIndexCurrentRecord10++;

            ws.Cells["M" + rowIndexCurrentRecord10].Value = "EURO";
            ws.Cells["L" + rowIndexCurrentRecord10].Value = source10.Where(a => a.CURCODE == "EURO").Sum(a => a.TRNET);
            ws.Cells["L" + rowIndexCurrentRecord10 + ":M" + rowIndexCurrentRecord10].Style.Numberformat.Format = "#,##0.00";
            ws.Cells["L" + (rowIndexCurrentRecord10) + ":M" + (rowIndexCurrentRecord10)].Style.Font.Bold = true;


            #endregion

            #region Teminat Mektupları

            TrCurr = 0;
            SqlText = SqlReports.TeminatMektuplari;
            List<AtaTeminatMektuplari> sourceTeminat = GetPocoList<AtaTeminatMektuplari>();

            int IndexStart11 = rowIndexCurrentRecord10 + 4;
            int IndexStartB11 = IndexStart11 + 1;

            #region Başlık Renklendir

            ws.Cells["B" + IndexStart11].Value = "Teminat Mektubu Detayı";
            ws.Cells["B" + IndexStart11].Style.Font.Size = 14;
            ws.Cells["B" + IndexStart11].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + IndexStart11].Style.Fill.BackgroundColor.SetColor(Color.White);
            ws.Cells["B" + IndexStart11].Style.Font.Color.SetColor(Color.Red);

            #endregion


            ws.Cells["B" +  IndexStart11].Style.Font.Bold = true;
            ws.Cells["B" +  IndexStart11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells["B" + IndexStartB11].Value = Util.Labels.BankaHesapUnvani;
            ws.Cells["C" + IndexStartB11].Value = Util.Labels.KimeVerildigi;
            ws.Cells["D" + IndexStartB11].Value = Util.Labels.BelgeNo;
            ws.Cells["E" + IndexStartB11].Value = Util.Labels.Tutar;
            ws.Cells["F" + IndexStartB11].Value = Util.Labels.DovizTipi;
            ws.Cells["G" + IndexStartB11].Value = Util.Labels.BaslangicTarihi;
            ws.Cells["H" + IndexStartB11].Value = Util.Labels.BitisTarihi;
            //ws.Cells["I" + IndexStartB11].Value = Util.Labels.Portfoy;

            ws.Cells["B" + IndexStartB11 + ":H" + IndexStartB11].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells["B" + IndexStartB11 + ":H" + IndexStartB11].Style.Fill.BackgroundColor.SetColor(Color.DarkMagenta);
            ws.Cells["B" + IndexStartB11 + ":H" + IndexStartB11].Style.Font.Color.SetColor(Color.White);

            int rowIndexBegin11 = IndexStart11 + 2;
            int rowIndexCurrentRecord11 = rowIndexBegin11;
            foreach (var item in sourceTeminat)
            {
                ws.Cells["B" + rowIndexCurrentRecord11].Value = item.BANKAADI;
                ws.Cells["C" + rowIndexCurrentRecord11].Value = item.OWING;
                ws.Cells["D" + rowIndexCurrentRecord11].Value = item.TMEKTUKNO;
                ws.Cells["E" + rowIndexCurrentRecord11].Value = item.TRNET;
                ws.Cells["F" + rowIndexCurrentRecord11].Value = item.PB;
                ws.Cells["G" + rowIndexCurrentRecord11].Value = item.BEGDATE;
                ws.Cells["H" + rowIndexCurrentRecord11].Value = item.ENDDATE;
                // ws.Cells["I" + rowIndexCurrentRecord11].Value = item.PORTFOYNO;
                ws.Cells["G" + rowIndexCurrentRecord11 + ":H" + rowIndexCurrentRecord11].Style.Numberformat.Format = "dd- MM -yyyy";
                ws.Cells["E" + rowIndexCurrentRecord11 + ":E" + rowIndexCurrentRecord11].Style.Numberformat.Format = "#,##0.00";

                rowIndexCurrentRecord11++;
            }

            if (rowIndexBegin11 == rowIndexCurrentRecord11)
            {

            }
            else
            {
                rowIndexCurrentRecord11 = rowIndexCurrentRecord11 - 1;
            }

            ws.Cells["B" + IndexStartB11 + ":H" + rowIndexCurrentRecord11].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB11 + ":H" + rowIndexCurrentRecord11].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB11 + ":H" + rowIndexCurrentRecord11].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ws.Cells["B" + IndexStartB11 + ":H" + rowIndexCurrentRecord11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

            //ws.Cells[ws.Dimension.Address].AutoFitColumns();  son satırda olacak

            #region Çek Durum Toplamı

            rowIndexCurrentRecord11++;
            ws.Cells["D" + rowIndexCurrentRecord11].Value = "TL";
            ws.Cells["E" + rowIndexCurrentRecord11].Value = sourceTeminat.Where(a => a.PB == "TL").Sum(a => a.TRNET);
            ws.Cells["E" + rowIndexCurrentRecord11 + ":E" + rowIndexCurrentRecord11].Style.Numberformat.Format = "#,##0.00";
            ws.Cells["D" + (rowIndexCurrentRecord11) + ":E" + (rowIndexCurrentRecord11)].Style.Font.Bold = true;
            rowIndexCurrentRecord11++;

            ws.Cells["D" + rowIndexCurrentRecord11].Value = "USD";
            ws.Cells["E" + rowIndexCurrentRecord11].Value = sourceTeminat.Where(a => a.PB == "USD").Sum(a => a.TRNET);
            ws.Cells["E" + rowIndexCurrentRecord11 + ":E" + rowIndexCurrentRecord11].Style.Numberformat.Format = "#,##0.00";
            ws.Cells["D" + (rowIndexCurrentRecord11) + ":E" + (rowIndexCurrentRecord11)].Style.Font.Bold = true;
            rowIndexCurrentRecord11++;

            ws.Cells["D" + rowIndexCurrentRecord11].Value = "EURO";
            ws.Cells["E" + rowIndexCurrentRecord11].Value = sourceTeminat.Where(a => a.PB == "EURO").Sum(a => a.TRNET);
            ws.Cells["E" + rowIndexCurrentRecord11 + ":E" + rowIndexCurrentRecord11].Style.Numberformat.Format = "#,##0.00";
            ws.Cells["D" + (rowIndexCurrentRecord11) + ":E" + (rowIndexCurrentRecord11)].Style.Font.Bold = true;


            #endregion

            #endregion

            #endregion

        #region ExcelSave

        ws.PrinterSettings.FitToPage = true;
        SaveFileDialog save = new SaveFileDialog();
        save.Filter = "Excel 2007 Dosyası|*.xlsx";
        save.OverwritePrompt = true;

        if (save.ShowDialog() == DialogResult.OK)
        {
            FileInfo newFile = new FileInfo(save.FileName);
            package.SaveAs(newFile);

        }

        #endregion

        }


    }
}
