﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Data.PLinq;
using DevExpress.Data.XtraReports.Wizard;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using RaporcuV2.DataAccess;
using RaporcuV2.Model;
using DevExpress.Utils.Win;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraExport;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using Microsoft.Win32;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using RaporcuV2.Helper;
using DevExpress.Spreadsheet;
using DevExpress.XtraSpreadsheet;
using System.Data.SqlClient;
using System.IO;
using RaporcuV2.Model.Database;
using RaporcuV2.Util;

namespace RaporcuV2.WinApp.Forms.Customs
{
    public partial class XtraFormBase : DevExpress.XtraEditors.XtraForm
    {
        //public delegate void OnReady(object sender, EventArgs e);

        //public event OnReady Ready;

        public XtraFormBase()
        {
            InitializeComponent();
            
        }

        public bool IsSchedule { get; set; } = false;
        public Model.Database.Schedule Schedule { get; set; }

        #region Get Logo Dbname

        public string LogoDbName
        {
            get
            {
               return SingleConnection.DbName;
            }
        }
        #endregion

        #region Grid Save

        public void GridSave(){

            this.Shown += (sender, args) =>
            {
                this.Shown -= null;

                XtraForm parentForm = GetParentForm(this);
                string parentFormName = parentForm.Name;
                string name = this.barManagerControl1.gridView1.Name;
                GridView gridView = this.barManagerControl1.gridView1;
                GridControl gridControl = this.barManagerControl1.gridControl1;
                string regKey = string.Format("Software\\Raporcu\\{0}.{1}", parentFormName, gridControl);

                Layout layout = new Layout
                {
                    FormName = parentFormName,
                    GridName = gridControl.Name,
                    UserId = UserInfo.GetUserId()
                };

                try
                {
                    byte[] byteArray = null;
                    MemoryStream stream = null;
                    if (!IsSchedule)
                    {
                        RaporcuDbContext cnt = new RaporcuDbContext();

                        bool any = cnt.Layouts.Any(a =>
                            a.FormName == layout.FormName && a.GridName == layout.GridName &&
                            a.UserId == layout.UserId);
                        if (any)
                        {
                            var item = cnt.Layouts.FirstOrDefault(a =>
                                a.FormName == layout.FormName && a.GridName == layout.GridName &&
                                a.UserId == layout.UserId);

                            byteArray = Encoding.ASCII.GetBytes(item.XmlLayout);
                        }
                    }
                    else
                    {
                        byteArray = Encoding.ASCII.GetBytes(Schedule.Layout);
                    }
                    stream = new MemoryStream(byteArray);
                    gridView.RestoreLayoutFromStream(stream);

                }
                catch (Exception e)
                {

                }
                XtraFormBase_Ready(sender, args);
            };

            this.Closed += (sender, args) => {
                try
                {
                    Layout layout = GetRuntimeLayout();
                    RaporcuDbContext cnt = new RaporcuDbContext();

                    bool any = cnt.Layouts.Any(a =>
                        a.FormName == layout.FormName && a.GridName == layout.GridName && a.UserId == layout.UserId);
                    if (!any)
                    {
                        cnt.Layouts.Add(layout);
                    }
                    else
                    {
                        Layout item = cnt.Layouts.FirstOrDefault(a =>
                            a.FormName == layout.FormName && a.GridName == layout.GridName && a.UserId == layout.UserId);
                        item.XmlLayout = layout.XmlLayout;
                        cnt.Entry(item).State = EntityState.Modified;
                    }
                    cnt.SaveChanges();
                }
                catch (Exception e)
                {

                }};
        }

        private Layout GetRuntimeLayout()
        {
            Stream str = new System.IO.MemoryStream();

            XtraForm parentForm = GetParentForm(this);
            string parentFormName = parentForm.Name;
            string name = this.barManagerControl1.gridView1.Name;
            GridView gridView = this.barManagerControl1.gridView1;
            GridControl gridControl = this.barManagerControl1.gridControl1;
            string regKey = string.Format("Software\\Raporcu\\{0}.{1}", parentFormName, gridControl);
            gridView.SaveLayoutToStream(str);
            str.Seek(0, System.IO.SeekOrigin.Begin);
            StreamReader reader = new StreamReader(str);
            string stream = reader.ReadToEnd();
            Layout layout = new Layout
            {
                FormName = parentFormName,
                GridName = gridControl.Name,
                XmlLayout = stream,
                UserId = UserInfo.GetUserId()
            };

            return layout;
        }
        private void XtraFormBase_Ready(object sender, EventArgs e)
        {
            EventHandler afterBuildPages = null;
            afterBuildPages = (s, evnt) =>
            {
                PrintingSystemBase pb = s as PrintingSystemBase;
                pb.AfterBuildPages -= afterBuildPages;
            };

            barManagerControl1.gridView1.PrintInitialize += (s, evnt) =>
            {
                PrintingSystemBase pb = evnt.PrintingSystem as PrintingSystemBase;
                pb.ShowPrintStatusDialog = false;
                pb.PageSettings.Landscape = true;
                pb.PageSettings.MarginsF.Left = 32;
                pb.PageSettings.MarginsF.Right = 32;
                pb.PageSettings.MarginsF.Top = 120;
                pb.PageSettings.MarginsF.Bottom = 150;
                pb.AfterBuildPages += afterBuildPages;
            };

            if (this.IsSchedule)
            {
                string fileName = $"C:\\test\\{this.Text}_{DateTime.Now.ToString("yyyyMMyyyyHHssmm")}.pdf";

                this.Hide();

                this.barManagerControl1.gridView1.OptionsPrint.PrintHeader = true;
                this.barManagerControl1.gridView1.OptionsPrint.PrintFooter = true;

                PrintingSystem printingSystem1 = new PrintingSystem();
                PrintableComponentLink printableComponentLink1 = new PrintableComponentLink();
                printableComponentLink1.Component = this.barManagerControl1.gridControl1;
                printableComponentLink1.PrintingSystem = printingSystem1;
                printableComponentLink1.Images.Add(Image.FromFile("c:\\test\\raporcu.png"));
                PageHeaderArea pgHArea = new PageHeaderArea()
                {
                    Font = new Font("Arial", 10, FontStyle.Bold),
                    LineAlignment = BrickAlignment.Center
                };

                pgHArea.Content.AddRange(new string[] {this.FirmName, this.Text, "[Page # of Pages #]"});

                PageFooterArea pgFArea = new PageFooterArea()
                {
                    //Font = new Font("Arial", 10, FontStyle.Bold)
                };
                pgFArea.Content.AddRange(new string[] { "[Date Printed] - [Time Printed]", "", "[Image 0]" });

                printableComponentLink1.PageHeaderFooter = new PageHeaderFooter(pgHArea, pgFArea);
                System.IO.MemoryStream ms = new System.IO.MemoryStream();

                printableComponentLink1.ExportToPdf(ms);

                Util.Mail mail = new Mail();
                mail.SendMail(ms,this.Text);

                this.DialogResult = DialogResult.OK;
            }
        }
        
        #endregion

        #region Add Events
        public void AddEvents()
        {
            this.barManagerControl1.gridView1.FocusedRowChanged += (sender, arg) =>
            {
                GridView grid = sender as GridView;
                SelectedRowValue = grid.GetFocusedRow();
            };
        }
        #endregion

        #region Filters

        private int _year;
        private int _trCurr;
        public int _divisionNr;
        private DateTime _firstDate;
        private DateTime _lastDate;
        private int _firmNr;
        private string _firmName;
        private int _firmNr1;
        private int _periodNr;
        private object _selectedRowValue;

        public object SelectedRowValue
        {
            get
            {
                return _selectedRowValue;
            }
            set
            {
                _selectedRowValue = value;
            }
        }
        public string FirstDateSql { get; set; }
        public string LastDateSql { get; set; }
        public int DivisionNr
        {
            get { return _divisionNr; }
            set { _divisionNr = value; }
        }

        public int TrCurr { get; set; }
        public int CekTuru { get; set; }
        
        public int FaturaTuru { get; set; }
        public int HizmetTuru { get; set; }
        public int CreditCard { get; set; }

        public int FirmNr
        {
            get
            {
                return _firmNr;
            }
            set
            {
                _firmNr = value;
                SetPeriodData();
                SetBranchData();
            }
        }

        public string FirmName
        {
            get
            {
                return _firmName;
            }
            set
            {
                _firmName = value;

            }
        }

        public string FirmNrStr => FirmNr.ToString().PadLeft(3, '0');

        public int PeriodNr
        {
            get { return _periodNr; }
            set { _periodNr = value; }
        }

        public string PeriodNrStr => PeriodNr.ToString().PadLeft(2, '0');

        public DateTime FirstDate
        {
            get
            {
                return _firstDate;
            }
            set
            {
                if (value.Year < 1950)
                    value = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                    
                    FirstDateSql = string.Format("{0:yyyy-MM-dd}", value);
                _firstDate = value;
            }
        }

        public DateTime LastDate
        {
            get
            {
                return _lastDate;
            }
            set
            {
                if (value.Year < 1950)
                    value = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                LastDateSql = string.Format("{0:yyyy-MM-dd}", value);

                _lastDate = value;
            }
        }

        #endregion
        //        public void CreateFirmLookup(BarManager bar)
      //  {

         //   bar.ForceInitialize();

        #region Get Firms

        public void CreateFirmLookup()
        {
            RaporcuDbContext cnt = new RaporcuDbContext();

            BarManager bar = barManagerControl1.barManagerForm;
            RepositoryItemLookUpEdit _lookupEdit = new RepositoryItemLookUpEdit();
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"Software", true);
            if (rk != null)
            {
                RegistryKey subKey = rk.OpenSubKey("Raporcu");

                try
                {
                    FirmNr = Convert.ToInt32((string)subKey.GetValue("FirmaNo"));
                    if (FirmNr>0)
                    {

                        _firmNr1 = FirmNr;
                    }
                    
                }
                catch (Exception )
                {

                 
                }
            }
            _lookupEdit = bar.RepositoryItems.Add("LookUpEdit") as DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit;
            string firmSql = string.Format("SELECT NR,convert(varchar,NR)+ ' - ' + NAME as [NAME] FROM {0}.dbo.L_CAPIFIRM  ORDER BY NR DESC", LogoDbName);
            var list = cnt.Database.SqlQuery<LogoFirm>(firmSql).ToList();
            _lookupEdit.DataSource = list;
            //_lookupEdit.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NR", "Firma No"));
            _lookupEdit.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Firma Tanımı"));
            _lookupEdit.KeyMember = "NR";
            _lookupEdit.ValueMember = "NR";
            _lookupEdit.DisplayMember = "NAME";
            _lookupEdit.DropDownRows = list.Count+1;
            _lookupEdit.NullText = "Firma Seçiniz...";

            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = _lookupEdit;
            item.Width = 300;
            item.Id = 2;
            item.Caption = "Firma";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Up);
            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                try
                {
                    
                    var edit = obj as DevExpress.XtraBars.BarEditItem;
                    RepositoryItemLookUpEdit lookUp = edit.Edit as RepositoryItemLookUpEdit;
                    FirmNr = Convert.ToInt32(edit.EditValue.ToString());
                    //MessageBox.Show(DivisionNr.ToString());
                    if (FirmNr > 0)
                    {
                       var editText = edit.Edit.GetDisplayText(FirmNr);
                        SetPeriodData();
                        if (!IsSchedule)
                        {
                            FirmName = editText;
                        }
                    }
                }
                catch (Exception)
                {

                    
                }
      
            };
            if (_firmNr1>0)
            {
                item.EditValue = _firmNr1;
                
            }
            else
            {
                item.EditValue = list.FirstOrDefault().NR;
                if(!IsSchedule)
                {
                    FirmName = list.FirstOrDefault().NAME;
                }
            }
            
        }

        #endregion

        #region Get Firms Customs

        public void CreateFirmLookupCustoms(BarManager bar)
        {
            RaporcuDbContext cnt = new RaporcuDbContext();
            bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            RepositoryItemLookUpEdit _lookupEditCustoms = new RepositoryItemLookUpEdit();

            RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"Software", true);
            if (rk != null)
            {
                RegistryKey subKey = rk.OpenSubKey("Raporcu");

                try
                {
                    FirmNr = Convert.ToInt32((string)subKey.GetValue("FirmaNo"));
                    if (FirmNr > 0)
                    {

                        _firmNr1 = FirmNr;
                    }

                }
                catch (Exception)
                {


                }



            }

            _lookupEditCustoms = bar.RepositoryItems.Add("LookUpEdit") as DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit;

            string firmSql = string.Format("SELECT NR,convert(varchar,NR)+ ' - ' + NAME as [NAME] FROM {0}.dbo.L_CAPIFIRM  ORDER BY NR DESC", LogoDbName);
            var list = cnt.Database.SqlQuery<LogoFirm>(firmSql).ToList();


            _lookupEditCustoms.DataSource = list;
            //_lookupEdit.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NR", "Firma No"));
            _lookupEditCustoms.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Firma Tanımı"));
            _lookupEditCustoms.KeyMember = "NR";
            _lookupEditCustoms.ValueMember = "NR";
            _lookupEditCustoms.DisplayMember = "NAME";
            _lookupEditCustoms.DropDownRows = list.Count + 1;
            _lookupEditCustoms.NullText = "Firma Seçiniz...";

            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = _lookupEditCustoms;
            item.Width = 300;
            item.Id = 2;
            item.Caption = "Firma";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Up);
            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                try
                {
                    var edit = obj as DevExpress.XtraBars.BarEditItem;
                    FirmNr = Convert.ToInt32(edit.EditValue.ToString());
                    //MessageBox.Show(DivisionNr.ToString());
                    if (FirmNr > 0)
                    {
                        SetPeriodData();

                    }
                }
                catch (Exception)
                {

                    
                }
                
            };
            if (_firmNr1 > 0)
            {

                item.EditValue = _firmNr1;
            }
            else
            {

                item.EditValue = list.FirstOrDefault().NR;
            }

        }

        #endregion

        #region Get Period

        public List<LogoFirm> GetPeriodData()
        {
            RaporcuDbContext cnt = new RaporcuDbContext();

            return cnt.Database.SqlQuery<LogoFirm>
            (
                string.Format(
                "select NR,RIGHT('00'+cast(NR as varchar),2) as  NAME from {1}.dbo.L_CAPIPERIOD WHERE FIRMNR = {0} order by NR DESC",
                FirmNr,LogoDbName)).ToList();
        }
        public void SetPeriodData()
        {
            LookupEditPeriod.DataSource = GetPeriodData();
            if(LookupEditPeriod.OwnerEdit != null)
            LookupEditPeriod.OwnerEdit.ItemIndex = 0;
        }

        public RepositoryItemLookUpEdit LookupEditPeriod = new RepositoryItemLookUpEdit();
        public void CreatePeriodLookup()
        {
            BarManager bar = barManagerControl1.barManagerForm;
            
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            LookupEditPeriod =
                bar.RepositoryItems.Add("LookUpEdit") as DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit;
            List<LogoFirm> list = GetPeriodData();
            LookupEditPeriod.DataSource = list;
            LookupEditPeriod.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NR", "Firma No"));
            LookupEditPeriod.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Dönem Tanımı"));
            LookupEditPeriod.KeyMember = "NR";
            LookupEditPeriod.ValueMember = "NR";
            LookupEditPeriod.DisplayMember = "NAME";
            LookupEditPeriod.DropDownRows = list.Count + 1;
            LookupEditPeriod.NullText = "Dönem Seçiniz...";
            
            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = LookupEditPeriod;
            item.Width = 50;
            item.Id = 2;

            item.Caption = "Dönem";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Up);

            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                var edit = obj as DevExpress.XtraBars.BarEditItem;
                PeriodNr = Convert.ToInt32(edit.EditValue.ToString());
            };
            item.EditValue = list.FirstOrDefault().NR;
        }

        
        public void CreatePeriodLookupCustoms(BarManager bar)
        {
            RepositoryItemLookUpEdit LookupEditPeriod = new RepositoryItemLookUpEdit();
            bar = barManagerControl1.barManagerForm;

            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            LookupEditPeriod =
                bar.RepositoryItems.Add("LookUpEdit") as DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit;
            List<LogoFirm> list = GetPeriodData();
            LookupEditPeriod.DataSource = list;
            //_lookupEdit.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NR", "Firma No"));
            LookupEditPeriod.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "Dönem Tanımı"));
            LookupEditPeriod.KeyMember = "NR";
            LookupEditPeriod.ValueMember = "NR";
            LookupEditPeriod.DisplayMember = "NAME";
            LookupEditPeriod.DropDownRows = list.Count + 1;
            LookupEditPeriod.NullText = "Dönem Seçiniz...";

            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = LookupEditPeriod;
            item.Width = 50;
            item.Id = 2;

            item.Caption = "Dönem";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Up);

            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                var edit = obj as DevExpress.XtraBars.BarEditItem;
                PeriodNr = Convert.ToInt32(edit.EditValue.ToString());
            };
            item.EditValue = list.FirstOrDefault().NR;
        }

        #endregion

        #region Create Divisions

        public List<LogoFirm> GetBranchData()
        {
            RaporcuDbContext cnt = new RaporcuDbContext();

            return cnt.Database.SqlQuery<LogoFirm>(
                    string.Format(
                        "SELECT CONVERT(SMALLINT,-1) as NR,'Tümü' as NAME " +
                        "union all " +
                        "SELECT NR,convert(varchar,NR)+ ' - ' + NAME as [NAME] FROM {1}.dbo.l_CAPIDIV WHERE FIRMNR = {0}  ORDER BY NR",
                        FirmNr,LogoDbName))
                .ToList();
        }

        public void SetBranchData()
        {
            LookupEditBranch.DataSource = GetBranchData();
        }

        public RepositoryItemLookUpEdit LookupEditBranch = new RepositoryItemLookUpEdit();

        private string _sqlText;

        public void CreateDivLookup()
        {
            BarManager bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            LookupEditBranch = bar.RepositoryItems.Add("LookUpEdit") as DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit;
            var list = GetBranchData();
            LookupEditBranch.DataSource = list;
            LookupEditBranch.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "İşyeri Tanımı"));
            LookupEditBranch.KeyMember = "NR";
            LookupEditBranch.ValueMember = "NR";
            LookupEditBranch.DisplayMember = "NAME";
            LookupEditBranch.DropDownRows = 5;// list.Count + 1;
            LookupEditBranch.NullText = "İşyeri Seçiniz...";

            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = LookupEditBranch;
            //item.EditValue = "İşyeri Seçiniz...";
            item.Width = 100;
            item.Id = 2;

            item.Caption = "İş Yeri";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Up);

            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                var edit = obj as DevExpress.XtraBars.BarEditItem;
                DivisionNr = Convert.ToInt32(edit.EditValue.ToString());
            };
            item.EditValue = list.FirstOrDefault().NR;
        }

        #endregion

        #region Create Divisions Customs
        public void CreateDivLookupCustoms(BarManager bar)
        {
            bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            LookupEditBranch = bar.RepositoryItems.Add("LookUpEdit") as DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit;
            var list = GetBranchData();
            LookupEditBranch.DataSource = list;
            LookupEditBranch.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NAME", "İşyeri Tanımı"));
            LookupEditBranch.KeyMember = "NR";
            LookupEditBranch.ValueMember = "NR";
            LookupEditBranch.DisplayMember = "NAME";
            LookupEditBranch.DropDownRows = 5;// list.Count + 1;
            LookupEditBranch.NullText = "İşyeri Seçiniz...";

            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = LookupEditBranch;
            //item.EditValue = "İşyeri Seçiniz...";
            item.Width = 100;
            item.Id = 2;

            item.Caption = "İş Yeri";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Up);

            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                var edit = obj as DevExpress.XtraBars.BarEditItem;
                DivisionNr = Convert.ToInt32(edit.EditValue.ToString());
            };
            item.EditValue = list.FirstOrDefault().NR;
        }
        #endregion

        #region Create Date

        public RepositoryItemDateEdit DateEditFirst = new RepositoryItemDateEdit();
        public void CreateFirstDate()
        {
            //this.DateEditFirst.Properties.Mask.Culture = new System.Globalization.CultureInfo("tr-TR");
            //this.DateEditFirst.Properties.Mask.EditMask = "dd/MM/yyyy";
            //this.DateEditFirst.Properties.Mask.UseMaskAsDisplayFormat = true;
            //this.DateEditFirst.Properties.Mask.MaskType = MaskType.DateTime;

            //this.DateEditFirst.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            //this.DateEditFirst.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            //this.DateEditFirst.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            //this.DateEditFirst.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            //this.DateEditFirst.Properties.Mask.EditMask = "dd/MM/yyyy";

            BarManager bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            DateEditFirst = bar.RepositoryItems.Add("DateEdit") as RepositoryItemDateEdit;
            
            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = DateEditFirst;
            //item.EditValue = "İşyeri Seçiniz...";
            item.Width = 90;
            item.Id = 2;
            item.Name = "firstDate";
            item.Caption = "İlk";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Today);

            //item.Edit.DisplayFormat.Format = new CultureInfo("tr-TR");
            //item.Edit.DisplayFormat.FormatString = "dd/MM/yyyy";

            //item.Edit.EditFormat.Format = new CultureInfo("tr-TR");
            //item.Edit.EditFormat.FormatString = "dd/MM/yyyy";
            
            //item.Edit.EditFormat.FormatType = FormatType.DateTime;
            


            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                var edit = obj as DevExpress.XtraBars.BarEditItem;
                FirstDate = Convert.ToDateTime(edit.EditValue.ToString());
            };
            item.EditValue = new DateTime(DateTime.Today.Year, 1, 1);}

        public RepositoryItemDateEdit DateEditLast = new RepositoryItemDateEdit();
        public void CreateLastDate()
        {
         
            BarManager bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            DateEditFirst = bar.RepositoryItems.Add("DateEdit") as RepositoryItemDateEdit;

            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = DateEditFirst;
            //item.EditValue = "İşyeri Seçiniz...";
            item.Width = 90;
            item.Id = 2;

            item.Caption = "Son";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Today);

            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                var edit = obj as DevExpress.XtraBars.BarEditItem;
                LastDate = Convert.ToDateTime(edit.EditValue.ToString());
            };
            item.EditValue = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
        }

        #endregion

        #region Create Currency



        public RepositoryItemRadioGroup CurrencyRadioGroup = new RepositoryItemRadioGroup();


        public void CreateCurrency()
        {

            BarManager bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            CurrencyRadioGroup = bar.RepositoryItems.Add("RadioGroup") as DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup;
            
            CurrencyRadioGroup.Items.Add(new RadioGroupItem(0,"TL"));
            CurrencyRadioGroup.Items.Add(new RadioGroupItem(1,"$"));
            CurrencyRadioGroup.Items.Add(new RadioGroupItem(20, "€"));
            CurrencyRadioGroup.Items.Add(new RadioGroupItem(17, "GBP"));

            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = CurrencyRadioGroup;
           
            item.Width = 170;
            item.Id = 2;

            item.Caption = "Döviz";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Up);

            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                var edit = obj as DevExpress.XtraBars.BarEditItem;
                TrCurr = Convert.ToInt32(edit.EditValue.ToString());
            };
            item.EditValue = 0;
        }

        public void CreateCurrencyCustom(BarManager bar)
        {

            bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            CurrencyRadioGroup = bar.RepositoryItems.Add("RadioGroup") as DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup;

            CurrencyRadioGroup.Items.Add(new RadioGroupItem(0, "TL"));
            CurrencyRadioGroup.Items.Add(new RadioGroupItem(1, "$"));
            CurrencyRadioGroup.Items.Add(new RadioGroupItem(20, "€"));

            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = CurrencyRadioGroup;

            item.Width = 100;
            item.Id = 2;

            item.Caption = "Döviz";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Up);

            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                var edit = obj as DevExpress.XtraBars.BarEditItem;
                TrCurr = Convert.ToInt32(edit.EditValue.ToString());
            };
            item.EditValue = 0;
        }


        #endregion

        #region Çek Türü



        public RepositoryItemRadioGroup CekTuruRadioGroup = new RepositoryItemRadioGroup();


        public void CreateCekTuru()
        {

            BarManager bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            CekTuruRadioGroup = bar.RepositoryItems.Add("RadioGroup") as DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup;

            CekTuruRadioGroup.Items.Add(new RadioGroupItem(1, "Müşteri Çeki"));
            CekTuruRadioGroup.Items.Add(new RadioGroupItem(3, "Kendi Çekimiz"));
            CekTuruRadioGroup.Items.Add(new RadioGroupItem(2, "Müşteri Senedi"));
            CekTuruRadioGroup.Items.Add(new RadioGroupItem(4, "Kendi Senedimiz"));

            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = CekTuruRadioGroup;

            item.Width = 400;
            item.Id = 4;

            item.Caption = "Çek Türü";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Up);

            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                var edit = obj as DevExpress.XtraBars.BarEditItem;
                CekTuru =Convert.ToInt32(edit.EditValue.ToString());
            };
            item.EditValue = 1;
        }

        public void CreateCekTuruCustom(BarManager bar)
        {

            bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            CekTuruRadioGroup = bar.RepositoryItems.Add("RadioGroup") as DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup;

            CekTuruRadioGroup.Items.Add(new RadioGroupItem(1, "Müşteri Çeki"));
            CekTuruRadioGroup.Items.Add(new RadioGroupItem(3, "Kendi Çekimiz"));
            CekTuruRadioGroup.Items.Add(new RadioGroupItem(2, "Müşteri Senedi"));
            CekTuruRadioGroup.Items.Add(new RadioGroupItem(4, "Kendi Senedimiz"));

            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = CekTuruRadioGroup;

            item.Width = 400;
            item.Id = 1;

            item.Caption = "Çek Türü";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Up);

            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                var edit = obj as DevExpress.XtraBars.BarEditItem;
                CekTuru = Convert.ToInt32(edit.EditValue.ToString());
            };
            item.EditValue = 1;
        }


        #endregion

        #region Create Fatura Türü



        public RepositoryItemRadioGroup FaturaRadioGroup = new RepositoryItemRadioGroup();


        public void CreateFaturaTuru()
        {

            BarManager bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            FaturaRadioGroup = bar.RepositoryItems.Add("RadioGroup") as DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup;

            FaturaRadioGroup.Items.Add(new RadioGroupItem(1, "Alış"));
            FaturaRadioGroup.Items.Add(new RadioGroupItem(7, "Perakende"));
            FaturaRadioGroup.Items.Add(new RadioGroupItem(8, "Satış"));

            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = FaturaRadioGroup;
            item.Width = 200;
            item.Id = 2;

            item.Caption = "Fatura Türü";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Up);

            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                var edit = obj as DevExpress.XtraBars.BarEditItem;
                FaturaTuru = Convert.ToInt32(edit.EditValue.ToString());
            };
            item.EditValue = 1;
        }


        #endregion

        #region Create Hizmet Türü



        public RepositoryItemRadioGroup HizmetRadioGroup = new RepositoryItemRadioGroup();


        public void CreateHizmetTuru()
        {

            BarManager bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            HizmetRadioGroup = bar.RepositoryItems.Add("RadioGroup") as DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup;

            HizmetRadioGroup.Items.Add(new RadioGroupItem(4, "Alınan Hizmet"));
            HizmetRadioGroup.Items.Add(new RadioGroupItem(9, "Verilen Hizmet"));

            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = HizmetRadioGroup;
            item.Width = 100;
            item.Id = 2;

            item.Caption = "Hizmet Türü";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Up);

            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                var edit = obj as DevExpress.XtraBars.BarEditItem;
                HizmetTuru = Convert.ToInt32(edit.EditValue.ToString());
            };
            item.EditValue = 4;
        }


        #endregion

        #region Create CreditCard

        public RepositoryItemRadioGroup CreditCardRadioGroup = new RepositoryItemRadioGroup();
        public void CreateCreditCard()
        {
            BarManager bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarEditItem item;
            bar.ForceInitialize();
            CurrencyRadioGroup = bar.RepositoryItems.Add("RadioGroup") as DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup;

            CurrencyRadioGroup.Items.Add(new RadioGroupItem(70, "Kredi Kartı"));
            CurrencyRadioGroup.Items.Add(new RadioGroupItem(72, "Firma Kredi Kartı"));

            item = new DevExpress.XtraBars.BarEditItem(bar);
            item.Edit = CurrencyRadioGroup;
            //item.EditValue = "İşyeri Seçiniz...";
            item.Width = 210;
            item.Id = 2;

            item.Caption = "Tür";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.ImageOptions.Image =
                DevExpress.Images.ImageResourceCache.Default.GetImage(DevExpress.Images.DXImages.Up);

            bar.Bars[0].AddItem(item);

            item.EditValueChanged += (obj, evn) =>
            {
                var edit = obj as DevExpress.XtraBars.BarEditItem;
                CreditCard = Convert.ToInt32(edit.EditValue.ToString());
            };
            item.EditValue = 70;
        }


        #endregion

        #region Create Refresh Button

        public DevExpress.XtraBars.BarButtonItem RefreshButtonEdit = new DevExpress.XtraBars.BarButtonItem();

        public void CreateRefreshButton()
        {
            BarManager bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarButtonItem item;
            bar.ForceInitialize();

            item = RefreshButtonEdit;
            item.Id = 2;
            item.Caption = "Yenile";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            //item.Glyph = DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png");

            item.Glyph = DevExpress.Images.ImageResourceCache.Default.GetImageById(DevExpress.Images.DXImages.Refresh,
                DevExpress.Utils.Design.ImageSize.Size16x16, DevExpress.Utils.Design.ImageType.Colored);



            bar.Bars[0].AddItem(item);

            item.ItemClick += (obj, evn) => { InvokeMethodByName(this.MethodNameRefreshData, null); };

            DevExpress.XtraBars.BarButtonItem exportExcel = new BarButtonItem()
            {
                Id = 100,
                Caption = "Dışarı Aktar",
                PaintStyle = BarItemPaintStyle.CaptionGlyph,
                Glyph = DevExpress.Images.ImageResourceCache.Default.GetImageById(
                    DevExpress.Images.DXImages.ExportToXLSX,
                    DevExpress.Utils.Design.ImageSize.Size16x16, DevExpress.Utils.Design.ImageType.Colored),
            };
            exportExcel.ItemClick += (sender, args) =>
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "Excel 2007 Dosyası|*.xlsx";
                save.OverwritePrompt = true;

                if (save.ShowDialog() == DialogResult.OK)
                {
                    barManagerControl1.gridView1.ExportToXlsx(save.FileName);
                }
            };

            bar.Bars[0].AddItem(exportExcel);

            #region ÖnizlemeGöster

            DevExpress.XtraBars.BarButtonItem preview = new BarButtonItem()
            {
                Id = 101,
                Caption = "Önizleme",
                PaintStyle = BarItemPaintStyle.CaptionGlyph,
                Glyph = DevExpress.Images.ImageResourceCache.Default.GetImageById(
                    DevExpress.Images.DXImages.Preview,
                    DevExpress.Utils.Design.ImageSize.Size16x16, DevExpress.Utils.Design.ImageType.Colored),
            };
            preview.ItemClick += (sender, args) =>
            {
                EventHandler afterBuildPages = null;
                afterBuildPages = (s, e) =>
                {
                    PrintingSystemBase pb = s as PrintingSystemBase;
                    pb.AfterBuildPages -= afterBuildPages;

                    };

                barManagerControl1.gridView1.PrintInitialize += (s, e) =>
                {
                    
                    PrintingSystemBase pb = e.PrintingSystem as PrintingSystemBase;

                    pb.PageSettings.Landscape = true;
                    pb.PageSettings.MarginsF.Left = 12;
                    pb.PageSettings.MarginsF.Right = 12;
                    pb.PageSettings.MarginsF.Top = 12;
                    pb.PageSettings.MarginsF.Bottom = 12;
                    pb.AfterBuildPages += afterBuildPages;

                };

                barManagerControl1.gridView1.ShowRibbonPrintPreview();

            };
            bar.Bars[0].AddItem(preview);


            #endregion

            bar.ForceInitialize();

            CreateScheduleButton();

        }

        #endregion

        #region Create Refresh Button Customs

        public void CreateRefreshButtonCustoms(BarManager bar)
        {
            DevExpress.XtraBars.BarButtonItem RefreshButtonEdit = new DevExpress.XtraBars.BarButtonItem();
            bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarButtonItem item;
            bar.ForceInitialize();

            item = RefreshButtonEdit;
            item.Id = 2;
            item.Caption = "Yenile";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.Glyph = DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png");

            bar.Bars[0].AddItem(item);

            item.ItemClick += (obj, evn) =>
            {
                InvokeMethodByName(this.MethodNameRefreshData, null);

            };
        }

        #endregion

        #region Create Pivot Button

        public DevExpress.XtraBars.BarButtonItem PivotButtonEdit = new DevExpress.XtraBars.BarButtonItem();
        public void CreatePivotButton()
        {
            BarManager bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarButtonItem item;
            bar.ForceInitialize();

            item = PivotButtonEdit;
            item.Id = 3;
            item.Caption = "Pivot";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.Glyph = DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png");

            bar.Bars[0].AddItem(item);

            item.ItemClick += (obj, evn) =>
            {
                //InvokeMethodByName(this.MethodNameRefreshData, null);
                object source = this.barManagerControl1.gridControl1.DataSource;
                DataTable dt = new DataTable();
                foreach (GridColumn column in this.barManagerControl1.gridView1.VisibleColumns)
                {
                    dt.Columns.Add(column.CustomizationSearchCaption, column.ColumnType);
                }
                for (int i = 0; i < this.barManagerControl1.gridView1.DataRowCount; i++)
                {
                    DataRow row = dt.NewRow();
                    foreach (GridColumn column in this.barManagerControl1.gridView1.VisibleColumns)
                    {
                        row[column.CustomizationSearchCaption] = this.barManagerControl1.gridView1.GetRowCellValue(i, column);
                    }
                    dt.Rows.Add(row);
                }
                XtraFormPivot frm = new XtraFormPivot(dt,this.Name,this.Text);
                frm.ShowDialog();

            };


        }

        #endregion

        #region Create Schedule Button

        public DevExpress.XtraBars.BarButtonItem ScheduleButtonEdit = new DevExpress.XtraBars.BarButtonItem();
        public void CreateScheduleButton()
        {
            BarManager bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarButtonItem item;
            bar.ForceInitialize();

            item = ScheduleButtonEdit;
            item.Id = 3;
            item.Caption = "Otomatik Rapor";
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.Glyph = DevExpress.Images.ImageResourceCache.Default.GetImage("images/scheduling/time_16x16.png");

            bar.Bars[0].AddItem(item);

            item.ItemClick += (obj, evn) =>
            {
                //MessageBox.Show("nanikk");
                RaporcuDbContext cnt = new RaporcuDbContext();
                Model.Database.Schedule schedule = new Model.Database.Schedule()
                {
                    FormName = this.Name,
                    Layout = GetRuntimeLayout().XmlLayout,
                    ReportParameter = this.SqlText,
                    FirmName = this.FirmName
                };
                cnt.Schedules.Add(schedule);
                cnt.SaveChanges();
            };}

        #endregion

        #region Create Button //------------------------------------------//

        public DevExpress.XtraBars.BarButtonItem CreateCustomButtonEdit = new DevExpress.XtraBars.BarButtonItem();
        public void CreateCustomButton(string pCaption)
        {
            BarManager bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarButtonItem item;
            bar.ForceInitialize();

            item = CreateCustomButtonEdit;
            item.Id = 2;
            item.Caption = pCaption;
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.Glyph = DevExpress.Images.ImageResourceCache.Default.GetImage("images/chart/labelsnone2_16x16.png");

            bar.Bars[0].AddItem(item);

            item.ItemClick += (obj, evn) =>
            {
                InvokeMethodByName(this.MethodNameCustomRefreshData, null);

            };
        }

        #endregion

        #region Create Refresh Button   ////////////////////////////////////

        public void CreateButtonCustoms(BarManager bar,string pCaption)
        {
            DevExpress.XtraBars.BarButtonItem CreateCustomButtonEdit = new DevExpress.XtraBars.BarButtonItem();
            bar = barManagerControl1.barManagerForm;
            DevExpress.XtraBars.BarButtonItem item;
            bar.ForceInitialize();

            item = CreateCustomButtonEdit;
            item.Id = 2;
            item.Caption = pCaption;
            item.PaintStyle = BarItemPaintStyle.CaptionGlyph;
            item.Glyph = DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png");

            bar.Bars[0].AddItem(item);

            item.ItemClick += (obj, evn) =>
            {
                InvokeMethodByName(this.MethodNameRefreshData, null);

            };
        }

        #endregion


        #region GetData

        #region CLR Sql Text

        protected ReportTags _reportTag;

        public string SqlText
        {

            get
            {
                string divisionNr = DivisionNr.ToString();
                if (DivisionNr == -1)
                {
                    divisionNr = "%";
                }

                string filter = string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10}", LogoDbName, FirmNrStr, PeriodNrStr, divisionNr, FirstDateSql,
                    LastDateSql, TrCurr, CreditCard, FaturaTuru, CekTuru, HizmetTuru);
                char[] delimiterChars = { ';' };
                string[] filt = filter.Split(delimiterChars);


                if (IsSchedule)
                {
                    return SqlTextCustom;
                }
                return string.Format("EXEC orca_uspExec '{0}','{1}'", (int)_reportTag, filter );
            }
            set { _sqlText = value; }
        }

        #endregion

        public string SqlTextCustom
        {
            /*
                0 -> DbName
                1 -> Firma No
                2 -> Dönem No
                3 -> İş Yeri No
                4 -> İlk Tarih
                5 -> Son Tarih
                6 -> Tr Curr 
                7 -> Kredi Kartı Türü
                8 -> Fatura Türü
                9  -> Cek Türü
                10 -> Hizmet Türü
            */

            get
            {
                string divisionNr = DivisionNr.ToString();
                if(DivisionNr == -1)
                {
                    divisionNr = "%";
                }

                return string.Format(_sqlText, LogoDbName, FirmNrStr, PeriodNrStr, divisionNr, FirstDateSql,
                    LastDateSql, TrCurr, CreditCard, FaturaTuru, CekTuru, HizmetTuru);
            }
            set { _sqlText = value; }
        }

        public void RefreshGridData()
        {
            GridSave();
            barManagerControl1.gridControl1.DataSource = null;
            string connectionString = SingleConnection.ConString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                DataTable dt = new DataTable();
                if(!IsSchedule)
                {
                    SqlTextCustom = SqlText;
                }

                SqlCommand cmd = new SqlCommand(SqlTextCustom, conn);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                // this will query your database and return the result to your datatable
                da.Fill(dt);
                barManagerControl1.gridControl1.DataSource = dt;
                foreach (GridColumn column in barManagerControl1.gridView1.Columns)
                {
                    if (column.ColumnType.Name == "Double")
                    {
                        column.DisplayFormat.FormatType = FormatType.Numeric;
                        column.DisplayFormat.FormatString = "{0:N2}";

                        if (column.Summary.Count > 0)
                            continue;
                        column.Summary.Add(new GridColumnSummaryItem
                        {
                            FieldName = column.FieldName,
                            DisplayFormat = "{0:N2}",
                            SummaryType = SummaryItemType.Sum

                        });

                    }
                }
                barManagerControl1.gridView1.BestFitColumns();
            }
        }

        public void RefreshGridDataCustom(string sqlTextCustom)
        {
            GridSave();
            barManagerControl1.gridControl1.DataSource = null;
            string connectionString = SingleConnection.ConString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                DataTable dt = new DataTable();
                SqlTextCustom = sqlTextCustom ?? SqlText;
                SqlCommand cmd = new SqlCommand(SqlTextCustom, conn);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                // this will query your database and return the result to your datatable
                da.Fill(dt);
                barManagerControl1.gridControl1.DataSource = dt;
                foreach (GridColumn column in barManagerControl1.gridView1.Columns)
                {
                    if (column.ColumnType.Name == "Double")
                    {
                        column.DisplayFormat.FormatType = FormatType.Numeric;
                        column.DisplayFormat.FormatString = "{0:N2}";

                        if (column.Summary.Count > 0)
                            continue;
                        column.Summary.Add(new GridColumnSummaryItem
                        {
                            FieldName = column.FieldName,
                            DisplayFormat = "{0:N2}",
                            SummaryType = SummaryItemType.Sum

                        });

                    }
                }
                barManagerControl1.gridView1.BestFitColumns();
            }
        }

        public void RefreshGridData<T>()
        {
            PLinqServerModeSource result = GetList<T>();
            // barManagerControl1.gridView1.Columns.Clear();
            barManagerControl1.gridControl1.DataSource = null;

            barManagerControl1.gridControl1.DataSource = result;
            GridSave();
            foreach (GridColumn column in barManagerControl1.gridView1.Columns)
            {
                if (column.ColumnType.Name == "Double")
                {
                    column.DisplayFormat.FormatType = FormatType.Numeric;
                    column.DisplayFormat.FormatString = "{0:N2}";

                    if (column.Summary.Count > 0)
                        continue;
                    column.Summary.Add(new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        DisplayFormat = "{0:N2}",
                        SummaryType = SummaryItemType.Sum

                    });

                }
            }
            
            barManagerControl1.gridView1.BestFitColumns();
        }

        public void RefreshPivotData<T>()
        {
            PLinqServerModeSource result = GetList<T>();
            barManagerControl1.gridView1.Columns.Clear();

            barManagerControl1.gridControl1.DataSource = result;
            GridSave();
            foreach (GridColumn column in barManagerControl1.gridView1.Columns)
            {
                if (column.ColumnType.Name == "Double")
                {
                    column.DisplayFormat.FormatType = FormatType.Numeric;
                    column.DisplayFormat.FormatString = "{0:N2}";

                    if (column.Summary.Count > 0)
                        continue;
                    column.Summary.Add(new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        DisplayFormat = "{0:N2}",
                        SummaryType = SummaryItemType.Sum

                    });
                }
            }
         

            barManagerControl1.gridView1.BestFitColumns();
        }

        public void Renklendirme(int pFieldNumber, int pFieldNumber1)
        {
            barManagerControl1.gridView1.Columns[pFieldNumber].AppearanceHeader.BackColor = System.Drawing.Color.LightGreen;
            barManagerControl1.gridView1.Columns[pFieldNumber].AppearanceCell.BackColor = System.Drawing.Color.LightGreen;
            barManagerControl1.gridView1.Columns[pFieldNumber1].AppearanceHeader.BackColor = System.Drawing.Color.LightCoral;
            barManagerControl1.gridView1.Columns[pFieldNumber1].AppearanceCell.BackColor = System.Drawing.Color.LightCoral;

        }

        public void ChangeDigitSize(string pFieldName,int pDigitSize)
        {


            foreach (GridColumn column in barManagerControl1.gridView1.Columns)
            {
                if (column.ColumnType.Name == "Double" && column.FieldName == pFieldName)
                {
                    column.DisplayFormat.FormatType = FormatType.Numeric;
                    column.DisplayFormat.FormatString = "{0:N"+pDigitSize.ToString()+"}";

                    if (column.Summary.Count > 0)
                        continue;
                    column.Summary.Add(new GridColumnSummaryItem
                    {
                        FieldName = column.FieldName,
                        DisplayFormat = "{0:N" + pDigitSize.ToString() + "}",
                        SummaryType = SummaryItemType.Sum

                    });

                }
            }
        }

        public PLinqServerModeSource GetList<T>()
        {
            RaporcuDbContext cnt = new RaporcuDbContext();
            return new PLinqServerModeSource()
            {
                Source = cnt.Database.SqlQuery<T>(SqlText).ToList()
            };
        }
        public List<T> GetPocoList<T>()
        {
            try
            {
                RaporcuDbContext cnt = new RaporcuDbContext();
                return cnt.Database.SqlQuery<T>(SqlText).ToList();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            return null;
        }

        #endregion

        #region ShortCuts
        /// <summary>
        /// Browser Refresh Methot Name ( See also IBrowser )
        /// </summary>
        public string MethodNameRefreshData = "RefreshGridData";
        public string MethodNameCustomRefreshData = "CustomRefreshGridData";

        /// <summary>
        /// Browser Form Shortcuts Definitions
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            //if (keyData == (Keys.Escape))
            //{
            //    this.Close();
            //    return true;

            //}
            //else 
            if (keyData == Keys.F5)
            {
                InvokeMethodByName(this.MethodNameRefreshData, null);
                return true;

            }
            //else if (keyData == Keys.Delete)
            //{
            //    InvokeMethodByName(this.MethodNameDeleteData, null);
            //    return true;

            //}
            //else if (keyData == Keys.Enter)
            //{
            //    InvokeMethodByName(this.MethodNameEditData, new object[] { false });
            //    return true;

            //}
            //else if (keyData == Keys.Insert)
            //{
            //    InvokeMethodByName(this.MethodNameEditData, new object[] { true });
            //    return true;

            //}
            return base.ProcessCmdKey(ref msg, keyData);
        }
        #endregion

        #region Invoke

        #region Get Parent Form

        /// <summary>
        /// Get parent form 
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public XtraForm GetParentForm(Control control)
        {
            if (control.Parent is XtraForm)
                return control.Parent as XtraForm;
            return control.FindForm() as XtraForm;
        }

        #endregion

        #region Invoke Methot

        /// <summary>
        /// Invoke methot from parent form
        /// </summary>
        /// <param name="methotName"></param>
        /// <param name="parameters"></param>
        private void InvokeMethodByName(string methotName, object[] parameters)
        {
            XtraForm parentForm = this.GetParentForm(this);
            if (!String.IsNullOrEmpty(methotName) && parentForm != null && parentForm.GetType().GetMethod(methotName) != null)
            {
                parentForm.GetType().GetMethod(methotName).Invoke(parentForm, parameters);
            }
            else
            {
                XtraMessageBox.Show("BrowserForm Interface must be parent form base!", "Invoke Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        #endregion

        #endregion



        public void FillExcel<T>(SpreadsheetControl spreadsheetControl,string reportTitle) 
        {
            _reportTag = RaporcuV2.Helper.ReportTags.CariMuhasebeKontrol;
           List<T> dataList = GetPocoList<T>();

            var dataType = dataList.FirstOrDefault();

            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(dataType);

            #region Excel List

            // Kasa ve Kasa Detay Renkelnedirme
            DevExpress.Spreadsheet.BuiltInStyleId headerStyle = BuiltInStyleId.Accent1;
            BuiltInTableStyleId rowStyle = BuiltInTableStyleId.TableStyleLight2;

            spreadsheetControl.BeginUpdate();

            #region Rapor Grubu

            // Rapor Grubu
            int i = 1;
            IWorkbook workbook = spreadsheetControl.Document;
            Worksheet ws = workbook.Worksheets[0];
            ws.ActiveView.PaperKind = System.Drawing.Printing.PaperKind.A4;
            ws.ActiveView.Orientation = PageOrientation.Landscape;
            ws.ActiveView.Margins.Left = 0;
            ws.ActiveView.Margins.Top = 0;
            ws.ActiveView.Margins.Right = 0;
            ws.ActiveView.Margins.Bottom = 0;

            ws.Clear(ws.GetUsedRange());

            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToWidth = 1;
            ws.PrintOptions.FitToHeight = 0;

            var range = ws.Range[$"A{i}:I{i}"];
            ws.MergeCells(range);
            range.Style = workbook.Styles[BuiltInStyleId.Title];
            ws.Cells[$"A{i}"].Value = FirmName;
            i++;

            range = ws.Range.FromLTRB(0, 0, properties.Count - 1, 0); // [$"A{i}:I{i}"];
            ws.MergeCells(range);
            range.Value = reportTitle;
            range.Style = workbook.Styles[BuiltInStyleId.Heading1];

            i = ws.GetUsedRange().RowCount;
            //SectionStart = i;

            int dataCount = 0;
            int dataRowCount = dataList.Count();
            dataCount = dataRowCount + i;

            Table dataTable = ws.Tables.Add(dataList, i, 0);
            dataTable.ShowTotals = true;
            dataTable.AutoFilter.Disable();
            dataTable.Style = workbook.TableStyles[rowStyle];
            dataTable.DataRange.NumberFormat = "#,##0.00";
            dataTable.TotalRowRange.NumberFormat = "#,##0.00";

            System.Collections.IList list = properties;
            for (int i1 = 0; i1 < list.Count; i1++)
            {
                PropertyDescriptor property = (PropertyDescriptor)list[i1];
                //Display Name Almak için
                string displayName = property.DisplayName;
                dataTable.Columns[i1].Name = $"{displayName}";
                // Özellik Set
                string name = property.PropertyType.Name;
                if (name == "Double")
                {
                    dataTable.Columns[i1].TotalRowFunction = TotalRowFunction.Sum;
                }
            }


            #endregion

            spreadsheetControl.EndUpdate();

            #endregion
        }


        #region Spread Sheet


        #endregion

    }
}