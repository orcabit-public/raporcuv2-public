﻿namespace RaporcuV2.WinApp.Forms.Customs
{
    partial class XtraFormBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barManagerControl1 = new RaporcuV2.WinApp.Forms.Customs.BarManagerControl();
            this.SuspendLayout();
            // 
            // barManagerControl1
            // 
            this.barManagerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.barManagerControl1.Location = new System.Drawing.Point(0, 0);
            this.barManagerControl1.Name = "barManagerControl1";
            this.barManagerControl1.Size = new System.Drawing.Size(753, 404);
            this.barManagerControl1.TabIndex = 0;
            // 
            // XtraFormBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 404);
            this.Controls.Add(this.barManagerControl1);
            this.Name = "XtraFormBase";
            this.Text = "Form Base";
            this.ResumeLayout(false);

        }

        #endregion

        public BarManagerControl barManagerControl1;
    }
}