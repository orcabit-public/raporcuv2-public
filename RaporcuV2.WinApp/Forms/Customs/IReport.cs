﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaporcuV2.WinApp.Forms.Customs
{
    public interface IReport
    {
        void RefreshGridData();
    }
}
