﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
//using DevExpress.XtraPrinting.Export.Pdf;
using RaporcuV2.DataAccess;
using RaporcuV2.Model.Reports;

namespace RaporcuV2.WinApp.Forms.Customs
{
    public partial class BarManagerControl : DevExpress.XtraEditors.XtraUserControl
    {
        public static List<AlisFaturalari> List { get; internal set; }

        public BarManagerControl()
        {
            InitializeComponent();
            AddEvents();
           
        }

        private void AddEvents()
        {
            this.gridControl1.MainView.PopulateColumns();
            gridView1.OptionsLayout.Columns.StoreAllOptions = true;

            gridView1.OptionsLayout.Columns.AddNewColumns = true;
            gridView1.OptionsLayout.Columns.RemoveOldColumns = true;


            gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            gridView1.OptionsLayout.StoreFormatRules = true;
            gridView1.OptionsLayout.StoreAppearance = true;
            gridView1.OptionsLayout.StoreVisualOptions = true;
            gridView1.OptionsLayout.StoreDataSettings = true;

            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.BestFitColumns();
            this.gridView1.OptionsSelection.InvertSelection = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedRow = true;

            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridView1.PopupMenuShowing += GridView_PopupMenuShowing;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.Appearance.Row.FontSizeDelta = Convert.ToInt32(SingleConnection.FontSize);

            this.barManagerForm.Merge += (sender, args) =>
            {
                var parentTools = barManagerForm.Bars[0];
                var childTools = args.ChildManager.Bars[0];
                if (childTools != null)
                    parentTools.Merge(childTools);
            };
            this.barManagerForm.UnMerge += (sender, args) =>
            {
                barManagerForm.Bars[0].UnMerge();
            };
            this.gridView1.RowCellClick += (sender, args) =>
            {
                DXPopupMenu formatRulesMenu = new DXPopupMenu();
                if (args.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    GridFormatRuleMenuItems items = new GridFormatRuleMenuItems(gridView1, args.Column, formatRulesMenu.Items);
                    if (items.Count > 0)
                        MenuManagerHelper.ShowMenu(formatRulesMenu, gridControl1.LookAndFeel, gridControl1.MenuManager, gridControl1, new Point(args.X, args.Y));
                }
            };

        }

        #region Row Menu
        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
  
            //GridView view = sender as GridView;
            //// Check whether a row is right-clicked.
            //if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
            //{

            //    int rowHandle = e.HitInfo.RowHandle;
            //    //e.Menu.Items.Add(CreateRowSubMenu(view, rowHandle));
            //    foreach (DXMenuItem item in CreateRowMenu())
            //    {
            //        e.Menu.Items.Add(item);
            //    }
            //}
        }
        private List<DXMenuItem> CreateRowMenu()
        {


            List<DXMenuItem> list = new List<DXMenuItem>();

            DXSubMenuItem subMenu = new DXSubMenuItem("Dışarı Veri Aktarım");

            DXMenuItem menuItemXlsx = new DXMenuItem("Excel Aktarım", new EventHandler(OnExportXls));
            menuItemXlsx.Image = DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png");
            //menuItemXlsx.Image = DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png");
            subMenu.Items.Add(menuItemXlsx);
            subMenu.BeginGroup = true;
            list.Add(subMenu);
            

            return list;
        }


        private void OnExportXls(object sender, EventArgs eventArgs)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Excel 2007 Dosyası|*.xlsx";
            save.OverwritePrompt = true;

            if (save.ShowDialog() == DialogResult.OK)
            {
                this.gridView1.ExportToXlsx(save.FileName);
            }
        }
        #endregion



    }
}
