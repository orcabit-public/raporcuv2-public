﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Data.PLinq;
using DevExpress.Data.XtraReports.Wizard.Native;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using RaporcuV2.DataAccess;
using RaporcuV2.Model;
using DevExpress.Utils.Win;
using DevExpress.XtraCharts.Designer;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using Microsoft.Win32;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPivotGrid;
using System.Drawing.Imaging;
using System.IO;

namespace RaporcuV2.WinApp.Forms.Customs
{
    public partial class XtraFormPivot : DevExpress.XtraEditors.XtraForm
    {
        private readonly object _pivotData;
        private readonly string _name;

        public XtraFormPivot(object pivotData, string name, string caption)
        {
            InitializeComponent();
            this.Text = string.Format("{0} - Pivot Tablo Görünümü", caption);

            _pivotData = pivotData;
            _name = name;
            GridSave();
            this.pivotGridControl1.DataSource = _pivotData;
            pivotGridControl1.RetrieveFields(PivotArea.FilterArea, true);

            foreach (PivotGridField field in pivotGridControl1.Fields)
            {
                Type type = field.DataType;
                if (type == typeof(double))
                {

                    field.ValueFormat.FormatType = FormatType.Numeric;
                    field.ValueFormat.FormatString = "N2";
                    field.CellFormat.FormatType = FormatType.Numeric;
                    field.CellFormat.FormatString = "N2";

                }
            };
            pivotGridControl1.BestFit();

            chartControl1.DoubleClick += (sender, args) =>
            {
                ChartDesigner chartDesigner = new ChartDesigner(chartControl1);
                //chartModule.BeforeShowDesigner();
                DialogResult dialogResult = chartDesigner.ShowDialog();

                //chartModule.AfterShowDesigner();
                //chartModule.UpdateControls();
                
            };


        }

            #region Grid Save

            public void GridSave()
            {

                this.Shown += (sender, args) =>
                {


                    string regKey = string.Format("Software\\Raporcu\\{0}.{1}", _name, "pivot");

                    try
                    {
                        pivotGridControl1.RestoreLayoutFromRegistry(regKey);
                        pivotGridControl1.BestFit();

                    }
                    catch (Exception e)
                    {

                    }
                };
                this.Closed += (sender, args) => {

                    string regKey = string.Format("Software\\Raporcu\\{0}.{1}", _name, "pivot");
                    try
                    {
                        pivotGridControl1.SaveLayoutToRegistry(regKey);
                    }
                    catch (Exception e)
                    {

                    }
                };
            }
        #endregion

        private void ResimAktarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Resim Dosyası|*.Jpeg";
            save.OverwritePrompt = true;

            if (save.ShowDialog() == DialogResult.OK)
            {
                chartControl1.ExportToImage(save.FileName, ImageFormat.Jpeg);
            }
        }

        private void excelAktarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Excel 2007 Dosyası|*.xlsx";
            save.OverwritePrompt = true;

            if (save.ShowDialog() == DialogResult.OK)
            {
                this.pivotGridControl1.ExportToXlsx(save.FileName);
            }
        }
    }
}