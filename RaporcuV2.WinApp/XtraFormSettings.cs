﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RaporcuV2.DataAccess;
using RaporcuV2.Lms;
using RaporcuV2.Model.Database;

namespace RaporcuV2.WinApp
{
    public partial class XtraFormSettings : DevExpress.XtraEditors.XtraForm
    {
        public XtraFormSettings()
        {
            InitializeComponent();
            AddEvents();
        }

        #region Add Events

        public void AddEvents()
        {
            this.Load += (sender, arg) =>
            {
                UserNameTextEdit.Text = SingleConnection.UserId;
                UserPassTextEdit.Text = SingleConnection.Pass;
                ServerNameTextEdit.Text = SingleConnection.ServerName;
                DbNameTextEdit.Text = SingleConnection.DbName;
                FirmaTextEdit.Text = SingleConnection.FirmaNo;
                FontSizeTextEdit.Text = SingleConnection.FontSize;
                textEditSerialNr.Text = SingleConnection.Serial;
            };

            simpleButtonSave.Click += (sender, arg) =>
            {
                SingleConnection.UserId = UserNameTextEdit.Text;
                SingleConnection.Pass = UserPassTextEdit.Text;
                SingleConnection.ServerName = ServerNameTextEdit.Text;
                SingleConnection.DbName = DbNameTextEdit.Text;
                SingleConnection.FirmaNo = FirmaTextEdit.Text;
                SingleConnection.FontSize = FontSizeTextEdit.Text;
                SingleConnection.Serial = textEditSerialNr.Text;

                ExchageResult license = LmsService.CheckLicense();

                if (!license.Status)
                {
                    XtraMessageBox.Show(license.Message, "Lisans", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                this.DialogResult = DialogResult.OK;
            };
            simpleButtonCancel.Click += (sender, arg) => { this.Close(); };

        }

        #endregion


    }
}