﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaporcuV2.Model.Database
{
    public class Session: BaseEntity
    {
        /// <summary>
        /// Session Primary key
        /// </summary>
        [Key, Display(Name = Labels.SessionId), DataType(DataType.Text)]
        public Guid SessionId { get; set; }
        /// <summary>
        /// Host name form application
        /// </summary>
        [Required, MaxLength(DbConstants.nameLenght), Display(Name = Labels.HostName), DataType(DataType.Text)]
        public string HostName { get; set; }
        /// <summary>
        /// Login Date & Time
        /// </summary>
        [Display(Name = Labels.LoginTime), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public DateTime LoginTime { get; set; }
        /// <summary>
        /// Login name form application 
        /// </summary>
        [Required, MaxLength(DbConstants.nameLenght), Display(Name = Labels.userUserName), DataType(DataType.Text)]
        public string LoginName { get; set; }
        /// <summary>
        /// Login Date & Time
        /// </summary>
        [Display(Name = Labels.LastActivityTime), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public DateTime LastActivityTime { get; set; }
    }
}
