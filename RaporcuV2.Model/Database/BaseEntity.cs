﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Database
{
    public abstract class BaseEntity
    {
        /// <summary>
        /// Record Created Date&time
        /// </summary>
        [Display(Name = Labels.createdDate), DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? CreatedDate { get; set; }
        /// <summary>
        /// Record Created User Id
        /// </summary>
        [Display(Name = Labels.createdUser, AutoGenerateField = false)]
        public int? CreatedUserId { get; set; }
        /// <summary>
        /// Record Updated Date & Time
        /// </summary>
        [Display(Name = Labels.UpdatedDate), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Record Updated User Id
        /// </summary>
        [Display(Name = Labels.UpdatedUser, AutoGenerateField = false)]
        public int? UpdatedUserId { get; set; }
    }
}
