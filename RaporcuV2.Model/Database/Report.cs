﻿using System.ComponentModel.DataAnnotations;

namespace RaporcuV2.Model.Database
{
    public class Report
    {
        [Key, Display(Order = -1)]

        public int ReportId { get; set; }
        public int TagId { get; set; }
        public string Value { get; set; }
    }
}