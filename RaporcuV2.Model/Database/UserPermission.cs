﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Database
{
    public class UserPermission:BaseEntity
    {
        /// <summary>
        /// RoleWarehouse Primarykey
        /// </summary>
        [Key, Display(Order = -1)]
        public int UserPermissionId { get; set; }
        /// <summary>
        /// Role Id 
        /// </summary>
        [Display(Order = -1)]
        public int UserId { get; set; }
        /// <summary>
        /// Role Added Permission Id 
        /// </summary>
        [Display(Order = 0)]
        public int PermissionId { get; set; }
        /// <summary>
        /// Role Added Permission Id 
        /// </summary>[Display(Order = 1)]
        public bool PermissionStatus { get; set; }

        //[ForeignKey("UserId"), Display(Order = -1)]
        //public virtual User User { get; set; }

        [ForeignKey("PermissionId"), Display(Order = -1)]
        public virtual Permission Permission { get; set; }
    }
}
