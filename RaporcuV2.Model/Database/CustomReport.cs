﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using RaporcuV2.Util;

namespace RaporcuV2.Model.Database
{
    public class CustomReport 
    {
        [Key, Display(Order = -1)]
        public int CustomReportId { get; set; }
        [Required, MaxLength(DbConstants.codeLenght), Display(Name = Labels.customReportName), DataType(DataType.Text)]
        public string CustomReportName { get; set; }
        [Required, Display(Name = Labels.customReportSql), DataType(DataType.Text)]
        public string CustomReportSql { get; set; }
        [Required, Display(Name = Labels.customReportTag), DataType(DataType.Text)]
        public int CustomReportTag { get; set; }
    }
}
