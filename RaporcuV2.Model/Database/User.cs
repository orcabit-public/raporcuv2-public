﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Database
{
    public class User : BaseEntity
    {
        /// <summary>
        /// User Primary key
        /// </summary>
        [Key, Display(Order = -1)]

        public int UserId { get; set; }
        /// <summary>
        /// User name form application login must be unique
        /// </summary>
        [Required, MaxLength(DbConstants.codeLenght), Display(Name = Labels.userUserName), DataType(DataType.Text)]
        public string UserName { get; set; }
        /// <summary>
        /// User Password
        /// </summary>
        [Required, MaxLength(DbConstants.codeLenght), Display(Name = Labels.userPassword), DataType(DataType.Password)]
        public string Password { get; set; }
        /// <summary>
        /// User Full Name
        /// </summary>
        [Required, MaxLength(DbConstants.nameLenght), Display(Name = Labels.userFullName), DataType(DataType.Text)]
        public string FullName { get; set; }

        /// <summary>
        /// User Email Adress
        /// </summary>
        [MaxLength(DbConstants.codeLenght), Display(Name = Labels.userEmail), DataType(DataType.EmailAddress)]
        public string EMail { get; set; }
    }
}