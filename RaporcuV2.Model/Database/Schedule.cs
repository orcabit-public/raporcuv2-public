﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaporcuV2.Model.Database
{
    public class Schedule
    {
        [Key] public int ScheduleId { get; set; }
        public string FormName { get; set; }
        public string FirmName { get; set; }
        public string ReportParameter { get; set; }
        public string Layout { get; set; }
    }
}
