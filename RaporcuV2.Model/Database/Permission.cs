﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Database
{
    public class Permission
    {
        [Key, Display(Order = -1)]
        public int PermissionId { get; set; }

        [Required, Display(Name = Labels.permissionTag)]
        public int PermissionTag { get; set; }

        [Required, MaxLength(DbConstants.nameLenght), Display(Name = Labels.permissionName)]
        public string PermissionName { get; set; }
    }
}
