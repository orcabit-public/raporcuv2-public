﻿using System.ComponentModel.DataAnnotations;

namespace RaporcuV2.Model.Database
{
    public class Layout
    {
        [Key, Display(Order = -1)]
        public int LayoutId { get; set; }
        public string FormName { get; set; }
        public string GridName { get; set; }
        public string XmlLayout { get; set; }
        public int UserId { get; set; }
    }
}