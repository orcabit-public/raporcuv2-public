﻿using System.ComponentModel.DataAnnotations;

namespace RaporcuV2.Model.Database
{
    public class Setting
    {
        [Key]
        public int SettingId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}