﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RaporcuV2.Model.Database
{
    public class UserDashboard : BaseEntity
    {
        [Key, Display(Order = -1)]
        public int UserDashBoardId { get; set; }

        public int UserId { get; set; }
        public byte[] Content { get; set; }

        [ForeignKey("UserId"), Display(Order = -1)]
        public User User { get; set; }

    }
}