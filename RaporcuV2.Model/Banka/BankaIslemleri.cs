﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Banka
{
    public class BankaIslemleri
    {
        [DisplayName(Labels.Tarih)]
        public DateTime DATE_ { get; set; }
        [DisplayName(Labels.FisNo)]
        public string FICHENO { get; set; }

        [DisplayName(Labels.IslemTuru)]
        public string ISLEMTURU { get; set; }
        [DisplayName(Labels.BankaKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.BankaUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string CLCODE { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string CLDEFINITION_ { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.Aciklama)]
        public string GENEXP1 { get; set; }
        [DisplayName(Labels.Aciklama1)]
        public string GENEXP2 { get; set; }
        [DisplayName(Labels.Borc)]
        public double BORC { get; set; }
        [DisplayName(Labels.Alacak)]
        public double ALACAK { get; set; }
        [DisplayName(Labels.DovizTipi)]
        public string PARABIRIMI { get; set; }
    }
}
