﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Banka
{
    public class KrediKartiCiroDetay
    {

        [DisplayName(Labels.CariHesapKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.BankaHesapKodu)]
        public string HESAPKODU { get; set; }
        [DisplayName(Labels.BankaHesapUnvani)]
        public string HESAPADI { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.OzelKod2)]
        public string SPECODE2 { get; set; }
        [DisplayName(Labels.YetkiKod)]
        public string CYPHCODE { get; set; }
        [DisplayName(Labels.VadeAyi)]
        public string VadeAyi { get; set; }
        [DisplayName(Labels.VadeYili)]
        public Int32 VadeYili { get; set; }
        [DisplayName(Labels.VadeTarihi)]
        public DateTime VadeTarihi { get; set; }
        [DisplayName(Labels.FisTarihi)]
        public DateTime FisTarihi { get; set; }
        [DisplayName(Labels.FisAyi)]
        public Int32 FisAyi { get; set; }
        [DisplayName(Labels.Kur)]
        public double KUR { get; set; }
        [DisplayName(Labels.Tutar)]
        public double Tutar { get; set; }
        [DisplayName(Labels.FisNo)]
        public string FisNo { get; set; }
        [DisplayName(Labels.Status)]
        public string STATUS { get; set; }

        [DisplayName(Labels.FisTuru)]
        public string KartTuru { get; set; }






    }
}
