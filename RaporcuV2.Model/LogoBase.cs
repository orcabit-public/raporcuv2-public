﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model
{
    
    public class LogoBase
    {
        [DisplayName("Referans No")]
        public int LOGICALREF { get; set; }
        [DisplayName("Kodu")]
        public string CODE { get; set; }
        [DisplayName("Açıklama")]
        public string DEFINITION_ { get; set; }
    }
    public class LogoFirm
    {
        public Int16 NR { get; set; }
        public string NAME { get; set; }
    }
    public class LogoPeriod
    {
        public Int16 FIRMNR { get; set; }

        public Int16 NR { get; set; }
    }
    public class LogoDiv
    {
        public Int16 FIRMNR { get; set; }
        public Int16 NR { get; set; }
        public string NAME { get; set; }
    }

    public class FirmList
    {
        public FirmList()
        {
            this.Period = new List<LogoPeriod>();
        }
        public Int16 NR { get; set; }
        public string NAME { get; set; }

        public virtual List<LogoPeriod> Period { get; set; }
    }
}
