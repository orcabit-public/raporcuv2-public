﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model
{
    public class DbSetting
    {
        [DisplayName("Sunucı Adı")]
        public string ServerName { get; set; }
        [DisplayName("Database Adı")]
        public string DbName { get; set; }
        [DisplayName("Kullanıcı Adı")]
        public string UserName { get; set; }
        [DisplayName("Şifre")]
        public string UserPass { get; set; }
    }
}
