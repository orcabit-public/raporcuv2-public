﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using RaporcuV2.Util;

namespace RaporcuV2.Model.Fatura
{
    public class Fatura
    {
        [DisplayName(Labels.FisRef)]
        public Int32 LOGICALREF { get; set; }
        [DisplayName(Labels.FisAyi)]
        public Int32 AY { get; set; }
        [DisplayName(Labels.FisTuru)]
        public string durum { get; set; }

        [DisplayName(Labels.Tarih)]
        public DateTime DATE_ { get; set; }
        [DisplayName(Labels.IslemTarihi)]
        public DateTime DOCDATE { get; set; }
        [DisplayName(Labels.FisNo)]
        public string FICHENO { get; set; }
        [DisplayName(Labels.BelgeNo)]
        public string DOCODE { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.Aciklama)]
        public string GENEXP1 { get; set; }
        [DisplayName(Labels.CariOzelKod)]
        public string CSPECODE { get; set; }
        [DisplayName(Labels.CariOzelKod2)]
        public string CSPECODE2 { get; set; }
        [DisplayName(Labels.SatisElemaniKodu)]
        public string SATISKODU { get; set; }
        [DisplayName(Labels.SatisElemaniAdi)]
        public string SATISADI { get; set; }
        [DisplayName(Labels.YetkiKod)]
        public string CYPHCODE { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.ProjeAdi)]
        public string NAME { get; set; }
        [DisplayName(Labels.DovizTipi)]
        public string DOVIZTIPI { get; set; }
        [DisplayName(Labels.KdvsizTutar)]
        public double KDVSIZTUTAR { get; set; }
        [DisplayName(Labels.KdvliTutarTl)]
        public double KDVLITL { get; set; }
        [DisplayName(Labels.KdvliTutarId)]
        public double KDVLIDOVIZTUTAR { get; set; }
    }
}
