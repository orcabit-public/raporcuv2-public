﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Fatura
{


        public class FaturaVadeToplam
        {
            [DisplayName(Labels.FisTuru)]
            public string TURU { get; set; }
            [DisplayName(Labels.VadeAyi)]
            public Int32 FaturaAyi { get; set; }
            [DisplayName(Labels.Yil)]
            public Int32 YIL { get; set; }
            [DisplayName(Labels.Gun)]
            public double GUN { get; set; }
            [DisplayName(Labels.Tutar)]
            public double TOTAL { get; set; }

        }

}
