﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Fatura
{
    public class FaturaOdemeHareketleri
    {
        [DisplayName(Labels.KapanmaDurumu)]
        public string KapanmaDurumu { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.IslemTarihi)]
        public DateTime procDATE { get; set; }
        [DisplayName(Labels.OdemeTarihi)]
        public DateTime DATE_ { get; set; }
        [DisplayName(Labels.Tutar)]
        public double Odenen { get; set; }
        [DisplayName(Labels.DovizTipi)]
        public string DOVIZ { get; set; }
      

    }
}
