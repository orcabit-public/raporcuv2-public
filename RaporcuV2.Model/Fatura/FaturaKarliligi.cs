﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Fatura
{

    [Serializable()]
    public class FaturaKarliligi 
    {
        [DisplayName(Labels.FaturaTarihi)]
        public DateTime FaturaTarihi { get; set; }
        [DisplayName(Labels.FisAyi)]
        public Int32 AY { get; set; }
        [DisplayName("Hafta")]
        public Int32 HAFTA { get; set; }
        [DisplayName(Labels.FaturaNo)]
        public string FatNo { get; set; }
        [DisplayName(Labels.FaturaOzelKod)]
        public string FSPECODE { get; set; }
        [DisplayName(Labels.BelgeNo)]
        public string BelgeNo { get; set; }
        [DisplayName(Labels.FatYetkiKod)]
        public string FCYPHCODE { get; set; }
        [DisplayName(Labels.ProjeKodu)]
        public string PROJEKODU { get; set; }
        [DisplayName(Labels.ProjeAdi)]
        public string PROJEADI { get; set; }
        [DisplayName(Labels.SatisElemaniKodu)]
        public string SATISKODU { get; set; }
        [DisplayName(Labels.SatisElemaniAdi)]
        public string SATISADI { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string CariHesapKodu { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string CariHesapUnvani { get; set; }
        [DisplayName(Labels.CariOzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.CariOzelKod2)]
        public string SPECODE2 { get; set; }
        [DisplayName(Labels.CariYetkiKod)]
        public string CYPHCODE { get; set; }
        [DisplayName(Labels.StokKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.StokAdi)]
        public string NAME { get; set; }
        [DisplayName(Labels.GrupKodu)]
        public string STGRPCODE { get; set; }
        [DisplayName(Labels.Miktar)]
        public double AMOUNT { get; set; }
        [DisplayName(Labels.BirimFiyat)]
        public double PRICE { get; set; }
        [DisplayName(Labels.ToplamSatisTutari)]         
        public double LINENET { get; set; }
        [DisplayName(Labels.SonSatinalmaBirimFiyat)]    
        public double SONALIMBRFIYAT { get; set; }
        [DisplayName(Labels.SsKar)]                     
        public double KAR { get; set; }
        [DisplayName(Labels.SonSKarOrani)]              
        public double YUZDE { get; set; }
        [DisplayName(Labels.OrtBirimFiyat)]             
        public double ORTALAMA { get; set; }
        [DisplayName(Labels.OrtalamaKar)]               
        public double ORTKAR { get; set; }
        [DisplayName(Labels.OrtalamaKarOrani)]          
        public double ORTALAMAYUZDE { get; set; }

    }
}
