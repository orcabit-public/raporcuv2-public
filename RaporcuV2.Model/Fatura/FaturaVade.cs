﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Fatura
{
    public class FaturaVade
    {
        [DisplayName(Labels.FisTuru)]
        public string TURU { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }

        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.OzelKod2)]
        public string SPECODE2 { get; set; }
        [DisplayName(Labels.YetkiKod)]
        public string CYPHCODE { get; set; }
        [DisplayName(Labels.SatisElemaniKodu)]
        public string SATISKODU { get; set; }
        [DisplayName(Labels.SatisElemaniAdi)]
        public string SATISADI { get; set; }
        [DisplayName(Labels.IslemTarihi)]
        public DateTime PROCDATE { get; set; }
        [DisplayName(Labels.VadeTarihi)]
        public DateTime DATE_ { get; set; }
        [DisplayName(Labels.VadeAyi)]
        public string VadeAyii { get; set; }
        [DisplayName(Labels.VadeYili)]
        public Int32 VadeYili { get; set; }
        [DisplayName(Labels.VadeGunu)]
        public Int32 VADE { get; set; }

        [DisplayName(Labels.BelgeNo)]
        public string DOCODE { get; set; }
        [DisplayName(Labels.FisNo)]
        public string FICHENO { get; set; }
        [DisplayName("Ödeme P.Kodu")]
        public string ODEMEKODU { get; set; }
        [DisplayName("Ödeme P.Açıklaması")]
        public string ODEMEACIKLAMASI { get; set; }

        [DisplayName(Labels.FaturaOzelKod)]
        public string FATURASPECODE { get; set; }
        [DisplayName(Labels.Tutar)]
        public double TOTAL { get; set; }

        [DisplayName(Labels.DovizTipi)]
        public string DOVIZ { get; set; }
        [DisplayName(Labels.Odenen)]
        public double ODENEN { get; set; }
        [DisplayName(Labels.KapanmaDurumu)]
        public string KapanmaDurumu { get; set; }
        [DisplayName("İptal Durumu")]
        public string IPTAL { get; set; }



    }

}
