﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using RaporcuV2.Util;

namespace RaporcuV2.Model.Fatura
{
    public class FaturaDetay
    {

        [DisplayName(Labels.FisTuru)]
        public string FATTURU { get; set; }
        [DisplayName(Labels.GrupKodu)]
        public string GRUPKODU { get; set; }
        [DisplayName(Labels.Tarih)]
        public DateTime TARIH { get; set; }
        [DisplayName(Labels.FisAyi)]
        public int AY { get; set; }
        [DisplayName(Labels.FisHaftasi)]
        public int HAFTA { get; set; }
        [DisplayName(Labels.FisNo)]
        public string FISNO { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string KOD { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string CARIHESAP { get; set; }
        [DisplayName(Labels.Sehir)]
        public string CITY { get; set; }
        [DisplayName(Labels.StokKodu)]
        public string SHKODU { get; set; }
        [DisplayName(Labels.StokAdi)]
        public string SHADI { get; set; }
        [DisplayName(Labels.StokAdi2)]
        public string SHADI2 { get; set; }
        [DisplayName(Labels.Miktar)]
        public double MIKTAR { get; set; }
        [DisplayName(Labels.StokBirimKodu)]
        public string BRMKODU { get; set; }
        [DisplayName(Labels.BirimFiyat)]
        public double FIYAT { get; set; }
        [DisplayName(Labels.Indirim)]
        public double INDIRIM { get; set; }
        [DisplayName(Labels.KdvMatrahi)]
        public double KDVMATRAH { get; set; }
        [DisplayName(Labels.Kdv)]
        public double KDV { get; set; }
        [DisplayName(Labels.KdvTutari)]
        public double KDVTUTARI { get; set; }
        //[DisplayName(Labels.ToplamSatisTutari)]
        //public double TOTALDISCOUNTED { get; set; }
        //[DisplayName(Labels.NetSatirToplami)]
        //public double NETTOPLAM { get; set; }
        [DisplayName(Labels.DovizTipi)]
        public string PB { get; set; }
        [DisplayName(Labels.Kur)]
        public double KUR { get; set; }
        [DisplayName(Labels.Aciklama)]
        public string LINEEXP { get; set; }
        [DisplayName(Labels.GrupKoduAciklama)]
        public string GRUPACIKLAMA { get; set; }

        [DisplayName(Labels.FaturaAciklama)]
        public string FATURAACIKLAMA { get; set; }

        [DisplayName(Labels.CariOzelKod)]
        public string CARIOZELKOD { get; set; }
        [DisplayName(Labels.StokOzelKod)]
        public string MALZEMEOZELKOD { get; set; }

        [DisplayName(Labels.CariYetkiKod)]
        public string CARIYETKIKODU { get; set; }

        //public string STOKTUTARI { get; set; }
        //public string STOKMALIYETI { get; set; }

    }

    public class FaturaDetay2
    {

        [DisplayName(Labels.FisTuru)]
        public string FATTURU { get; set; }
        
        [DisplayName(Labels.FaturaTarihi)]
        public DateTime FATURATARIHI { get; set; }
        [DisplayName("Fatura Yıl")]
        public int FATURAYIL { get; set; }
        [DisplayName("Fatura Ay")]
        public int FATURAAY { get; set; }
        [DisplayName("Fatura Hafta")]
        public int FATURAHAFTA { get; set; }

        [DisplayName("İrsaliye Tarihi")]
        public DateTime IRSALIYETARIHI { get; set; }
        [DisplayName("İrsaliye Yıl")]
        public int IRSALIYEYIL { get; set; }
        [DisplayName("İrsaliye Ay")]
        public int IRSALIYEAY { get; set; }
        [DisplayName("İrsaliye Hafta")]
        public int IRSALIYEHAFTA { get; set; }
        [DisplayName("Fatura Fiş No")]
        public string FATURAFISNO { get; set; }
        [DisplayName("Fatura Belge No")]
        public string FATURABELGENO { get; set; }
        [DisplayName("Fatura Özel Kod")]
        public string FATURAOZELKOD { get; set; }
        [DisplayName("İrsaliye Fiş No")]
        public string IRSALIYEFISNO { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string KOD { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string CARIHESAP { get; set; }
        [DisplayName("Cari Özel Kod")]
        public string CARIOZELKOD1 { get; set; }
        [DisplayName("Cari Özel Kod2")]
        public string CARIOZELKOD2 { get; set; }
        [DisplayName("Cari Özel Kod3")]
        public string CARIOZELKOD3 { get; set; }
        [DisplayName("Cari Özel Kod4")]
        public string CARIOZELKOD4 { get; set; }
        [DisplayName("Cari Özel Kod5")]
        public string CARIOZELKOD5 { get; set; }
        [DisplayName("Cari Yetki Kodu")]
        public string CARIYETKIKODU { get; set; }
        [DisplayName("Şehir")]
        public string CITY { get; set; }
        [DisplayName("İlçe")]
        public string TOWN { get; set; }
        [DisplayName("İş Yeri")]
        public string ISYERI { get; set; }
        [DisplayName("Bölüm")]
        public string BOLUM { get; set; }
        [DisplayName("Fabrika")]
        public string FABRIKA { get; set; }
        [DisplayName("Ambar")]
        public string AMBAR { get; set; }
        [DisplayName("Fatura Proje Kod")]
        public string FATURAPROJEKOD { get; set; }
        [DisplayName("Fatura Proje Açıklaması")]
        public string FATURAPROJEACIKLAMA { get; set; }
        [DisplayName("Grup Kodu")]
        public string GRUPKODU { get; set; }
        [DisplayName("Grup Açıklaması")]
        public string GRUPACIKLAMA { get; set; }
        [DisplayName("Tür")]
        public string LINETYPE { get; set; }
        [DisplayName("Malzeme/Hizmet Kodu")]
        public string SHKODU { get; set; }
        [DisplayName("Malzeme/Hizmet Açıklaması")]
        public string SHADI { get; set; }
        [DisplayName("Malzeme/Hizmet Açıklaması 2")]
        public string SHADI2 { get; set; }
        [DisplayName("Malzeme/Hizmet Özel Kod")]
        public string MALZEMEOZELKOD { get; set; }

        [DisplayName("Satır Açıklaması")]
        public string LINEEXP { get; set; }
        [DisplayName("Fatura Açıklaması")]
        public string FATURAACIKLAMA { get; set; }
        [DisplayName("Marka Kodu")]
        public string MARKAKOD { get; set; }
        [DisplayName("Marka Açıklaması")]
        public string MARKAACIKLAMA { get; set; }
        [DisplayName("Hareket Özel Kodu")]
        public string HAREKETOZELKOD { get; set; }
        [DisplayName("Hareket Özel Kodu 2")]
        public string HAREKETOZELKOD2 { get; set; }
        [DisplayName("Satış Elemanı Kodu")]
        public string FATURASATISELEMANIKOD { get; set; }
        [DisplayName("Satış Elemanı Açıklaması")]
        public string FATURASATISELAMANI { get; set; }
        [DisplayName(Labels.Miktar)]
        public double MIKTAR { get; set; }
        [DisplayName("Birim Kodu")]
        public string BRMKODU { get; set; }

        [DisplayName("TL Birim Fiyat")]
        public double TLFIYAT { get; set; }
        [DisplayName("TL İndirim")]
        public double TLINDIRIM { get; set; }
        [DisplayName("TL Net Tutar")]
        public double TLNETTUTAR { get; set; }
        [DisplayName("KDV Oran")]
        public double KDV { get; set; }
        [DisplayName("TL KDV Tutarı")]
        public double TLKDVTUTARI { get; set; }

        [DisplayName("TL KDVLI Tutar")]
        public double TLKDVLITUTAR { get; set; }
        [DisplayName("Döviz Türü")]
        public string PB { get; set; }
        [DisplayName("Kur")]
        public double KUR { get; set; }
        [DisplayName("Raporlama Kuru")]
        public double RAPORLAMAKURU { get; set; }

        [DisplayName("Döviz Birim Fiyat")]
        public double ISLEMDOVIZLIFIYAT { get; set; }
        [DisplayName("Döviz İndirim")]
        public double ISLEMDOVIZLIINDIRIM { get; set; }
        [DisplayName("Döviz Net Tutar")]
        public double ISLEMDOVIZLINETTUTAR { get; set; }
        [DisplayName("Döviz KDV Tutarı")]
        public double ISLEMDOVIZLIKDVTUTARI { get; set; }
        [DisplayName("Döviz KDVLI Tutar")]
        public double ISLEMDOVIZLIKDVLITUTAR { get; set; }

        [DisplayName("R.Döviz Birim Fiyat")]
        public double RAPORLAMADOVIZLIFIYAT { get; set; }
        [DisplayName("R.Döviz İndirim")]
        public double RAPORLAMADOVIZLIINDIRIM { get; set; }
        [DisplayName("R.Döviz Net Tutar")]
        public double RAPORLAMADOVIZLINETTUTAR { get; set; }
        [DisplayName("R.Döviz KDV Tutarı")]
        public double RAPORLAMADOVIZLIKDVTUTARI { get; set; }
        [DisplayName("R.Döviz KDVLI Tutar")]
        public double RAPORLAMADOVIZLIKDVLITUTAR { get; set; }

        [DisplayName("Stok Tutarı")]
        public string STOKTUTARI { get; set; }
        [DisplayName("Stok Maliyeti")]
        public string STOKMALIYETI { get; set; }
        [DisplayName("Ödeme Kodu")]
        public string ODEMEKODU { get; set; }
        [DisplayName("Ödeme Açıklaması")]
        public string ODEMEACIKLAMA { get; set; }

    



    }
}