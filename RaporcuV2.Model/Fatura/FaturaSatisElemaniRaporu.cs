﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Fatura
{
    public class FaturaSatisElemaniRaporu
    {
        [DisplayName(Labels.FisTuru)]
        public string durum { get; set; }
        [DisplayName(Labels.Tarih)]
        public DateTime DATE_ { get; set; }
        [DisplayName(Labels.FisNo)]
        public string FICHENO { get; set; }
        [DisplayName(Labels.BelgeNo)]
        public string DOCODE { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.CariOzelKod)]
        public string CSPECODE { get; set; }
        [DisplayName(Labels.CariOzelKod2)]
        public string CSPECODE2 { get; set; }
        [DisplayName(Labels.SatisElemaniKodu)]
        public string SATISKODU { get; set; }
        [DisplayName(Labels.SatisElemaniAdi)]
        public string SATISADI { get; set; }
        [DisplayName(Labels.YetkiKod)]
        public string CYPHCODE { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.ProjeAdi)]
        public string NAME { get; set; }
        [DisplayName(Labels.DovizTipi)]
        public string DOVIZTIPI { get; set; }
        [DisplayName(Labels.StokAdi)]
        public string ITEMNAME { get; set; }
        [DisplayName(Labels.StokKodu)]
        public string ITEMCODE { get; set; }
        [DisplayName(Labels.Miktar)]
        public double AMOUNT { get; set; }
        [DisplayName(Labels.StokBirimAdi)]
        public string UNIT { get; set; }
        [DisplayName(Labels.BirimFiyat)]
        public double PRICE { get; set; }
        [DisplayName(Labels.Kur)]
        public double TRRATE { get; set; }
        [DisplayName(Labels.NetSatirToplami)]
        public double LINENET { get; set; }
        [DisplayName(Labels.Indirim)]
        public double INDIRIM { get; set; }
        [DisplayName(Labels.Kdv)]
        public double VAT { get; set; }
        [DisplayName(Labels.KdvTutari)]
        public double VATAMNT { get; set; }
        [DisplayName(Labels.DovizTutari)]
        public double DOVIZTUTARI { get; set; }

    }
}
