﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Fatura
{
    public class FaturaSatirlari
    {
        //  NAME            

        [DisplayName(Labels.FisTuru)]
        public string IslemTuru { get; set; }
        [DisplayName(Labels.StokKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.StokAdi)]
        public string NAME { get; set; }
        [DisplayName(Labels.Miktar)]
        public double Miktar { get; set; }
        [DisplayName(Labels.Fiyati)]
        public double Fiyati { get; set; }
        [DisplayName(Labels.SatiTutari)]
        public double SatirTutari { get; set; }
        [DisplayName(Labels.Masraflar)]
        public double Masraflar { get; set; }
        [DisplayName(Labels.Iskonto)]
        public double Iskonto { get; set; }
        [DisplayName(Labels.KdvMatrahi)]
        public double KdvMatrahi { get; set; }
        [DisplayName(Labels.KdvTutari)]
        public double KdvTutari { get; set; }
        [DisplayName(Labels.Tutar)]
        public double ToplamTutar { get; set; }
        [DisplayName(Labels.HizmetAciklamasi)]
        public string HizmetAdi { get; set; }
        
    }
}
