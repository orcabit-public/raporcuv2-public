﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Hizmet
{
    public class HizmetToplamlari
    {

        private double? _totalsCashamnt1;
        private double? _totalsCashamnt2;
        private double? _totalsCashamnt3;
        private double? _totalsCashamnt4;
        private double? _totalsCashamnt5;
        private double? _totalsCashamnt6;
        private double? _totalsCashamnt7;
        private double? _totalsCashamnt8;
        private double? _totalsCashamnt9;
        private double? _totalsCashamnt10;
        private double? _totalsCashamnt11;
        private double? _totalsCashamnt12;


        [DisplayName(Labels.HizmetKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.HizmetAciklamasi)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.Yil)]
        public int? YEAR_ { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.OzelKod2)]
        public string SPECODE2 { get; set; }

        [DisplayName("Toplam Tutar"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n2}")]

        public double TOTALS_CASHAMNT_ALL { get; set; }

        [DisplayName(Labels.Hizmet_CASHAMNT1), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n2}")]
        public double? TOTALS_CASHAMNT_1  
        {
            get
            {
                if (_totalsCashamnt1 == null)
                {
                    _totalsCashamnt1= 0;
                }
                return _totalsCashamnt1;
            }

            set
            {
                _totalsCashamnt1 = value;
            }
        }


        [DisplayName(Labels.Hizmet_CASHAMNT2), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n2}")]
        public double? TOTALS_CASHAMNT_2
        {
            get
            {
                if (_totalsCashamnt2 == null)
                {
                    _totalsCashamnt2= 0;
                }
                return _totalsCashamnt2;
            }

            set
            {
                _totalsCashamnt2 = value;
            }
        }


        [DisplayName(Labels.Hizmet_CASHAMNT3), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n2}")]
        public double? TOTALS_CASHAMNT_3
        {
            get
            {
                if (_totalsCashamnt3 == null)
                {
                    _totalsCashamnt3= 0;
                }
                return _totalsCashamnt3;
            }

            set
            {
                _totalsCashamnt3 = value;
            }
        }

        [DisplayName(Labels.Hizmet_CASHAMNT4), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n2}")]
        public double? TOTALS_CASHAMNT_4
        {
            get
            {
                if (_totalsCashamnt4 == null)
                {
                    _totalsCashamnt4= 0;
                }
                return _totalsCashamnt4;
            }

            set
            {
                _totalsCashamnt4 = value;
            }
        }

        [DisplayName(Labels.Hizmet_CASHAMNT5), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n2}")]
        public double? TOTALS_CASHAMNT_5
        {
            get
            {
                if (_totalsCashamnt5 == null)
                {
                    _totalsCashamnt5= 0;
                }
                return _totalsCashamnt5;
            }

            set
            {
                _totalsCashamnt5 = value;
            }
        }


        [DisplayName(Labels.Hizmet_CASHAMNT6), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n2}")]
        public double? TOTALS_CASHAMNT_6
        {
            get
            {
                if (_totalsCashamnt6 == null)
                {
                    _totalsCashamnt6= 0;
                }
                return _totalsCashamnt6;
            }

            set
            {
                _totalsCashamnt6 = value;
            }
        }

        [DisplayName(Labels.Hizmet_CASHAMNT7), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n2}")]
        public double? TOTALS_CASHAMNT_7
        {
            get
            {
                if (_totalsCashamnt7 == null)
                {
                    _totalsCashamnt7= 0;
                }
                return _totalsCashamnt7;
            }

            set
            {
                _totalsCashamnt7 = value;
            }
        }

        [DisplayName(Labels.Hizmet_CASHAMNT8), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n2}")]
        public double? TOTALS_CASHAMNT_8
        {
            get
            {
                if (_totalsCashamnt8 == null)
                {
                    _totalsCashamnt8 = 0;
                }
                return _totalsCashamnt8;
            }

            set
            {
                _totalsCashamnt8 = value;
            }
        }

        [DisplayName(Labels.Hizmet_CASHAMNT9), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n2}")]
        public double? TOTALS_CASHAMNT_9
        {
            get
            {
                if (_totalsCashamnt9 == null)
                {
                    _totalsCashamnt9= 0;
                }
                return _totalsCashamnt9;
            }

            set
            {
                _totalsCashamnt9 = value;
            }
        }

        [DisplayName(Labels.Hizmet_CASHAMNT10), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n2}")]
        public double? TOTALS_CASHAMNT_10
        {
            get
            {
                if (_totalsCashamnt10 == null)
                {
                    _totalsCashamnt10= 0;
                }
                return _totalsCashamnt10;
            }

            set
            {
                _totalsCashamnt10 = value;
            }
        }

        [DisplayName(Labels.Hizmet_CASHAMNT11), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n2}")]
        public double? TOTALS_CASHAMNT_11
        {
            get
            {
                if (_totalsCashamnt11 == null)
                {
                    _totalsCashamnt11= 0;
                }
                return _totalsCashamnt11;
            }

            set
            {
                _totalsCashamnt11 = value;
            }
        }

        [DisplayName(Labels.Hizmet_CASHAMNT12), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n2}")]
        public double? TOTALS_CASHAMNT_12
        {
            get
            {
                if (_totalsCashamnt12 == null)
                {
                    _totalsCashamnt12= 0;
                }
                return (double) _totalsCashamnt12;
            }

            set
            {
                _totalsCashamnt12 = value;
            }
        }

    }
}
