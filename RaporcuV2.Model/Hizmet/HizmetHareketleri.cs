﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Hizmet
{
    public class HizmetHareketleri
    {
        [DisplayName(Labels.Tarih)]
        public DateTime DATE_ { get; set; }
        [DisplayName(Labels.FisTuru)]
        public string HIZMETTURU { get; set; }
        [DisplayName(Labels.HizmetKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.HizmetAciklamasi)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.BelgeNo)]
        public string BELGENO { get; set; }
        [DisplayName(Labels.FisNo)]
        public string FISNO { get; set; }
        [DisplayName(Labels.FaturaOzelKod)]
        public string FATURAOZELKOD { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string CARIKOD { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string CARIACIKLAMA { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.HizmetOzelKod)]
        public string HIZOZELKOD { get; set; }
        [DisplayName(Labels.HareketOzelKod)]
        public string HAREKETOZELKOD { get; set; }
        [DisplayName(Labels.HareketOzelKod2)]
        public string HAREKETOZELKOD2 { get; set; }

        [DisplayName(Labels.Aciklama)]
        public string SATIRACIKLAMASI { get; set; }
        
        [DisplayName(Labels.Miktar)]
        public double Miktar { get; set; }
        [DisplayName(Labels.Fiyati)]
        public double Fiyat { get; set; }
        [DisplayName(Labels.SatiTutari)]
        public double SATIRTUTARI { get; set; }
        [DisplayName(Labels.KdvTutari)]
        public double KDVTUTARI { get; set; }
        [DisplayName(Labels.KdvliTutarTl)]
        public double KDVLITUTAR { get; set; }
        [DisplayName(Labels.DovizTipi)]
        public string PARABIRIMI { get; set; }

        [DisplayName(Labels.FaturaAciklama)]
        public string FATURAACIKLAMA { get; set; }


    }
}
