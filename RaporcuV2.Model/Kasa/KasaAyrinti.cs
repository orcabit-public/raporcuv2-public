﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Kasa
{
    public class KasaAyrinti
    {

        [DisplayName(Labels.KasaAdi)]
        public string KasaAdi { get; set; }
        [DisplayName(Labels.KasaKodu)]
        public string KasaKodu { get; set; }
        [DisplayName(Labels.IslemTarihi)]
        public DateTime IslemTarihi { get; set; }
        [DisplayName(Labels.BelgeNo)]
        public string KasaIslemNo { get; set; }
        [DisplayName(Labels.Muhasebelesme)]
        public string Muhasebelesme { get; set; }
        [DisplayName(Labels.IslemTuru)]
        public string IslemTuru { get; set; }
        [DisplayName(Labels.Borc)]
        public double BORC { get; set; }
        [DisplayName(Labels.Alacak)]
        public double ALACAK { get; set; }
        [DisplayName(Labels.Bakiye)]
        public double BAKIYE
        {
            get
            {
                return BORC - ALACAK;
            }
        }
        [DisplayName(Labels.DovizTipi)]
        public string PARABIRIMI { get; set; }
        [DisplayName(Labels.Aciklama)]
        public string KasaFisiAciklamasi { get; set; }

        
    }
}
