﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using RaporcuV2.Util;

namespace RaporcuV2.Model.Kasa
{
    public class KasaToplamBakiye
    {
        [DisplayName(Labels.KasaAdi)]
        public string KASAADI { get; set; }
        [DisplayName(Labels.Bakiye)]
        public double BAKIYE { get; set; }

    }
}
