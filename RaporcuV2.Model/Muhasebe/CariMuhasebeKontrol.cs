﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Muhasebe
{
    public class CariMuhasebeKontrol
    {
        [DisplayName("Cari Hesap Kodu")]
        public string CODE { get; set; }
        [DisplayName("Cari Hesap Ünvanı")]
        public string DEFINITION_ { get; set; }
        [DisplayName("Muh. Hesap Kodu")]
        public string MCODE { get; set; }
        [DisplayName("Muh. Hesap Adı")]
        public string MDEFINITION_ { get; set; }
        [DisplayName("Cari Borç")]
        public double CB { get; set; }
        [DisplayName("Cari Alacak")]
        public double CA { get; set; }
        [DisplayName("Cari Bakiye")]
        public double CBAKIYE { get; set; }
        [DisplayName("Muhasebe Borç")]
        public double MB { get; set; }
        [DisplayName("Muhasebe Alacak")]
        public double MA { get; set; }
        [DisplayName("Muhasebe Bakiye")]
        public double MBAKIYE { get; set; }

        [DisplayName("FARK")]
        public double FARK
        {
            get
            {
                return Math.Round(CBAKIYE - MBAKIYE, 2);
            }
            set
            {
                value = FARK;}
        }

    }

}
