﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace RaporcuV2.Model.Muhasebe
{
    public class GenelDurum
    {
        [DisplayName("Varlıklar")]
        public string VARLIKLAR { get; set; }

        [DisplayName("Varlık Tutarı")]
        public double VARLIKTUTARI { get; set; }

        [DisplayName("Borçlar")]
        public string BORCLAR { get; set; }

        [DisplayName("Borç Tutarı")]
        public double BORCTUTARI { get; set; }


        
    }
}
