﻿namespace RaporcuV2.Model.Muhasebe
{
    public class Bilanco
    {
        public int TYPENR { get; set; }
        public string ACODE { get; set; }
        public string ADEFINITION_ { get; set; }
        public string BCODE { get; set; }
        public string BDEFINITION { get; set; }
        public string CODE { get; set; }
        public string DEFINITION_ { get; set; }
        public double BALANCE { get; set; }
    }
}