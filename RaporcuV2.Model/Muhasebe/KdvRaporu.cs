﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Muhasebe
{
    public class KdvRaporu
    {
    //    [DisplayName("Ay")]
    //    public Int32 VATMONTH { get; set; }

        [DisplayName("Kdv Oranı")]
        public double VAT { get; set; }

        [DisplayName("Satış Kdv Matrah")]
        public double SVATMATRAH { get; set; }

        [DisplayName("Satış Kdv Tutarı")]
        public double SVATAMNT { get; set; }

        [DisplayName("Alış İade Matrahı")]
        public double AIVATMATRAH { get; set; }
        [DisplayName("Alış İade Kdv Tutarı")]
        public double AIVATAMNT { get; set; }
        [DisplayName("Alış Kdv Matrahı")]
        public double AVATMATRAH { get; set; }
        [DisplayName("Alış Kdv Tutarı")]
        public double AVATAMNT { get; set; }
        [DisplayName("Satış İade Matrahı")]
        public double SIVATMATRAH { get; set; }
        [DisplayName("Satış İade Kdv Tutarı")]
        public double SIVATAMNT { get; set; }
        [DisplayName("Serbest Meslek Makbuzu Matrahı")]
        public double CLVATMATRAH { get; set; }

        [DisplayName("Serbest Meslek Makbuzu Kdv Tutarı")]
        public double CLVATAMNT { get; set; }
    }
   
                        
    
}
