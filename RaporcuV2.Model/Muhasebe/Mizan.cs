﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Muhasebe
{
    public class Mizan
    {
        [DisplayName("Hesap Sayısı")]
        public int BOLD { get; set; }

        [DisplayName("Seviye")]
        public int LEVEL { get; set; }

        [DisplayName("Hesap Kodu")]
        public string CODE { get; set; }

        [DisplayName("Ana Hesap")]
        public string HESAPKODU { get; set; }
        [DisplayName("Hesap Açıklaması")]
        public string DEFINITION_ { get; set; }

        [DisplayName("TL Borç")]
        public double TLBORC { get; set; }

        [DisplayName("TL Alacak")]
        public double TLALACAK { get; set; }

        [DisplayName("TL Bakiye")]
        public double TLBAKIYE { get; set; }
        [DisplayName("USD Borç")]
        public double USDBORC { get; set; }

        [DisplayName("USD Alacak")]
        public double USDALACAK { get; set; }

        [DisplayName("USD Bakiye")]
        public double USDBAKIYE { get; set; }

        [DisplayName("EURO Borç")]
        public double EUROBORC { get; set; }

        [DisplayName("EURO Alacak")]
        public double EUROALACAK { get; set; }

        [DisplayName("EURO Bakiye")]
        public double EUROBAKIYE { get; set; }

        [DisplayName("GBP Borç")]
        public double GBPBORC { get; set; }

        [DisplayName("GBP Alacak")]
        public double GBPALACAK { get; set; }

        [DisplayName("GBP Bakiye")]
        public double GBPBAKIYE { get; set; }


              
    }
}
