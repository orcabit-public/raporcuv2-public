﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Muhasebe
{
    public class FormBaBs
    {
        [DisplayName("Form Tipi")]
        public string FORMTIPI { get; set; }
        [DisplayName("Ay")]
        public Int32 AY { get; set; }
        [DisplayName("Ay")]
        public string AYSTR { get; set; }
        [DisplayName("Yıl")]
        public Int32 YIL { get; set; }
        [DisplayName("Fatura Sayısı")]
        public Int32 FATURA_SAYISI { get; set; }
        [DisplayName("Cari Adı")]
        public string UNVANI { get; set; }
        [DisplayName("Vergi No")]
        public string VERGI_NO { get; set; }
        [DisplayName("İskonto")]
        public double ISKONTO { get; set; }
        [DisplayName("Kdv Tutar")]
        public double KDV_TUTAR { get; set; }
        [DisplayName("Net Toplam")]
        public double NET_MATRAH { get; set; }
        [DisplayName("Toplam Tutar")]
        public double TOPLAM_TUTAR { get; set; }
        [DisplayName("E-Mail")]
        public string EMAILADDR { get; set; }
    }
}
