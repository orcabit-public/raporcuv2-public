﻿namespace RaporcuV2.Model.Muhasebe
{
    public class GelirTablosu
    {
        public string GRPCODE { get; set; }
        public string GRPDEFINITION_ { get; set; }
        public string CODE { get; set; }
        public string DEFINITION_ { get; set; }
        public double BALANCE { get; set; }
        public int CARPAN { get; set; }
    }
}