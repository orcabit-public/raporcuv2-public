﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.CariHesap
{
    public class BorcAlacakDurumRaporu
    {
        [DisplayName("Son Hareket")]
        public DateTime SONHAREKET { get; set; }
        [DisplayName("Cari Hesap Kodu")]
        public string CODE { get; set; }
        [DisplayName("Cari Hesap Ünvanı")]
        public string DEFINITION_ { get; set; }
        [DisplayName("Borç")]
        public double BORC { get; set; }
        [DisplayName("Alacak")]
        public double ALACAK { get; set; }
        [DisplayName("Borç Bakiye")]
        public double BORCBAKIYE { get; set; }
        [DisplayName("Alacak Bakiye")]
        public double ALACAKBAKIYE { get; set; }
        [DisplayName("Bakiye")]
        public double BAKIYE { get; set; }
        [DisplayName("Özel Kod")]
        public string SPECODE { get; set; }
        [DisplayName("Özel Kod2")]
        public string SPECODE2 { get; set; }
        [DisplayName("Yetki Kodu")]
        public string CYPHCODE { get; set; }
        [DisplayName("Tel No")]
        public string TELNRS1 { get; set; }
        [DisplayName("E-Mail")]
        public string EMAILADDR { get; set; }

    }
}
