﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.CariHesap
{
    public class AcikBakiye
    {
        [DisplayName(Labels.CariHesapKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.OzelKod2)]
        public string SPECODE2 { get; set; }
        [DisplayName(Labels.YetkiKod)]
        public string CYPHCODE { get; set; }
        [DisplayName("TL Açık Bakiye")]
        public double TL { get; set; }
        [DisplayName("USD Açık Bakiye")]
        public double USD { get; set; }
        [DisplayName("Euro Açık Bakiye")]
        public double EUR { get; set; }
        [DisplayName("Gbp Açık Bakiye")]
        public double GBP { get; set; }
       
    }
}
