﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.CariHesap
{
    public class HesapIslemleri
    {
        [DisplayName(Labels.Tarih)]
        public DateTime DATE_ { get; set; }
        [DisplayName(Labels.FisNo)]
        public string TRANNO { get; set; }
        [DisplayName(Labels.BelgeNo)]
        public string DOCODE { get; set; }
        [DisplayName(Labels.FisTuru)]
        public string FisTuru { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.OzelKod2)]
        public string SPECODE2 { get; set; }
        [DisplayName(Labels.YetkiKod)]
        public string CYPHCODE { get; set; }
        [DisplayName(Labels.Aciklama)]
        public string LINEEXP { get; set; }
        [DisplayName(Labels.Aciklama1)]
        public string GENEXP1 { get; set; }
        [DisplayName(Labels.DovizTipi)]
        public string DOVIZTIPI { get; set; }
        [DisplayName(Labels.Borc)]
        public double BORC { get; set; }
        [DisplayName(Labels.Alacak)]
        public double ALACAK { get; set; }
        [DisplayName(Labels.Tutar)]
        public double TLTUTAR { get; set; }

        [DisplayName(Labels.BorcDoviz)]
        public double BORCKDOVIZ { get; set; }
        [DisplayName(Labels.AlacakDoviz)]
        public double ALACAKDOVIZ { get; set; }

        [DisplayName(Labels.DovizTutari)]
        public double DOVIZTUTAR { get; set; }
    }
}
