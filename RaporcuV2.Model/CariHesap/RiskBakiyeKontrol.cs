﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.CariHesap
{
    public class CariCekRaporu
    {
        [DisplayName("Cari Hesap Kodu")]
        public string CODE { get; set; }
        [DisplayName("Cari Hesap Ünvanı")]
        public string DEFINITION_ { get; set; }
        [DisplayName("Portföy No")]
        public string PORTFOYNO { get; set; }
        [DisplayName("Borçlu")]
        public string OWING { get; set; }
        [DisplayName("Tutar")]
        public double TOTAL { get; set; }
        [DisplayName("TL Tutar")]
        public double AMOUNT { get; set; }
        [DisplayName("Kur")]
        public string CURCODE { get; set; }
        [DisplayName("Yıl")]
        public int YEAR { get; set; }
        [DisplayName("Ay")]
        public int MONTH { get; set; }
        [DisplayName("Hafta")]
        public int WEEK { get; set; }
    }
    public class RiskBakiyeKontrol
    {
        [DisplayName("Cari Hesap Kodu")]
        public string CODE { get; set; }
        [DisplayName("Cari Hesap Ünvanı")]
        public string DEFINITION_ { get; set; }
        [DisplayName("Özel Kod")]
        public string SPECODE { get; set; }
        [DisplayName("Özel Kod2")]
        public string SPECODE2 { get; set; }
        [DisplayName("Yetki Kodu")]
        public string CYPHCODE { get; set; }
        [DisplayName("TL Bakiye")]
        public double TLBAKIYE { get; set; }
        [DisplayName("Çek Şahsi")]
        public double CEKSAHSI { get; set; }
        [DisplayName("Çek Müşteri")]
        public double CEKMUSTERI { get; set; }
        [DisplayName("Senet Toplam")]
        public double SENETTOPLAM { get; set; }
        [DisplayName("Toplam Risk")]
        public double TOPLAMRISK { get; set; }
        [DisplayName("Fatura Toplamı")]
        public double FATTOPLAM { get; set; }
        [DisplayName("Sipariş Toplamı")]
        public double STOPLAM { get; set; }



    }

    public class CariSiparisliBakiye
    {
        [DisplayName("Cari Hesap Kodu")]
        public string CODE { get; set; }
        [DisplayName("Cari Hesap Ünvanı")]
        public string DEFINITION_ { get; set; }
        [DisplayName("Özel Kod")]
        public string SPECODE { get; set; }
        [DisplayName("Özel Kod2")]
        public string SPECODE2 { get; set; }
        [DisplayName("Yetki Kodu")]
        public string CYPHCODE { get; set; }

        
        
        [DisplayName("TL Bakiye")]
        public double TLBAKIYE { get; set; }
        
        [DisplayName("Sipariş TL Toplamı")]
        public double TRSIPARISTOTAL { get; set; }

        [DisplayName("Sipariş Dahil TL Bakiye")]
        public double TLTOPLAMBAKIYE
        {
            get { return TRSIPARISTOTAL + TLBAKIYE; }
            set
            {

            }
        }

        [DisplayName("USD Bakiye")]
        public double USDBAKIYE { get; set; }

        [DisplayName("Sipariş USD Toplamı")]
        public double USDSIPARISTOTAL { get; set; }

        [DisplayName("Sipariş Dahil USD Bakiye")]
        public double USDTOPLAMBAKIYE
        {
            get { return USDSIPARISTOTAL + USDBAKIYE; }
            set
            {

            }
        }

        [DisplayName("EURO Bakiye")]
        public double EUROBAKIYE { get; set; }

        [DisplayName("Sipariş EURO Toplamı")]
        public double EUROSIPARISTOTAL { get; set; }

        [DisplayName("Sipariş Dahil EURO Bakiye")]
        public double EUROTOPLAMBAKIYE
        {
            get { return EUROSIPARISTOTAL + EUROBAKIYE; }
            set
            {

            }
        }

    }

    public class CariSiparisListesi
    {
        [DisplayName(Labels.Tarih)]
        public DateTime DATE_ { get; set; }
        [DisplayName(Labels.FisNo)]
        public string TRANNO { get; set; }
        [DisplayName(Labels.BelgeNo)]
        public string DOCODE { get; set; }
        [DisplayName(Labels.FisTuru)]
        public string FisTuru { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.OzelKod2)]
        public string SPECODE2 { get; set; }
        [DisplayName(Labels.YetkiKod)]
        public string CYPHCODE { get; set; }
        [DisplayName(Labels.Aciklama)]
        public string LINEEXP { get; set; }
        [DisplayName(Labels.Aciklama1)]
        public string GENEXP1 { get; set; }
        [DisplayName(Labels.DovizTipi)]
        public string DOVIZTIPI { get; set; }
        [DisplayName(Labels.Borc)]
        public double BORC { get; set; }
        [DisplayName(Labels.Alacak)]
        public double ALACAK { get; set; }
        [DisplayName(Labels.BorcDoviz)]
        public double BORCKDOVIZ { get; set; }
        [DisplayName(Labels.AlacakDoviz)]
        public double ALACAKDOVIZ { get; set; }
    }

    public class SiparisliCariEkstre 
      {
        [DisplayName(Labels.Tarih)]
        public DateTime DATE_ { get; set; }
        [DisplayName(Labels.FisNo)]
        public string TRANNO { get; set; }
        [DisplayName(Labels.BelgeNo)]
        public string DOCODE { get; set; }
        [DisplayName(Labels.FisTuru)]
        public string FisTuru { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.OzelKod2)]
        public string SPECODE2 { get; set; }
        [DisplayName(Labels.YetkiKod)]
        public string CYPHCODE { get; set; }
        [DisplayName(Labels.Aciklama)]
        public string LINEEXP { get; set; }
        [DisplayName(Labels.Aciklama1)]
        public string GENEXP1 { get; set; }
        [DisplayName(Labels.DovizTipi)]
        public string DOVIZTIPI { get; set; }
        [DisplayName(Labels.Borc)]
        public double BORC { get; set; }
        [DisplayName(Labels.Alacak)]
        public double ALACAK { get; set; }
        [DisplayName(Labels.BorcDoviz)]
        public double BORCKDOVIZ { get; set; }
        [DisplayName(Labels.AlacakDoviz)]
        public double ALACAKDOVIZ { get; set; }
    }
}
