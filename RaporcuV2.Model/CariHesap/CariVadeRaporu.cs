﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.CariHesap
{
    public class CariVadeRaporu
    {
        [DisplayName("Cari Hesap Kodu")]
        public string CODE { get; set; }
        [DisplayName("Cari Hesap Ünvanı")]
        public string DEFINITION_ { get; set; }
        [DisplayName("Özel Kod")]
        public string SPECODE { get; set; }
        [DisplayName("Özel Kod2")]
        public string SPECODE2 { get; set; }
        [DisplayName("Yetki Kodu")]
        public string CYPHCODE { get; set; }
        [DisplayName("Borç")]
        public double BORC { get; set; }
        [DisplayName("Alacak")]
        public double ALACAK { get; set; }
        [DisplayName("Bakiye")]
        public double BAKIYE { get; set; }
        [DisplayName("Borç Ortalama Gün")]
        public Int64 BORC_ORT_GUN { get; set; }
        [DisplayName("Borç Ortalama Tarih")]
        public DateTime BORC_ORT_TARIH { get; set; }
        [DisplayName("Alacak Ortalama Gün")]
        public Int64 ALACAK_ORT_GUN { get; set; }
        [DisplayName("Alacak Ortalama Tarih")]
        public DateTime? ALACAK_ORT_TARIH { get; set; }
        [DisplayName("Kapalı Ortalama Gün")]
        public Int64 KAPALI_ORT_GUN { get; set; }
        [DisplayName("Açık Ortalama Gün")]
        public Int64 ACIK_ORT_GUN { get; set; }
        [DisplayName("Açık Ortalama Tarih")]
        public DateTime? ACIK_ORT_TARIH { get; set; }
        [DisplayName("Bakiye Ortalama Vade Gün")]
        public Int64 BAKIYE_ORT_VADE_GUN { get; set; }
        [DisplayName("Bakiye Ortalama Vade Tarih")]
        public DateTime? BAKIYE_ORT_VADE_TARIH { get; set; }

        [DisplayName("Vadesi Geçmiş")]
        public double GUNDEN_ONCE { get; set; }
        [DisplayName("Vadesi Gelecek")]
        public double GUNDEN_SONRA { get; set; }
        [DisplayName("Son Tahsilat Tarihi")]
        public DateTime? SON_TAHSILAT_TARIHI { get; set; }

        [DisplayName("Son Satış Tarihi")]
        public DateTime? SON_SATIS_TARIHI { get; set; }

        [DisplayName("Toplam Risk")]
        public double TOPLAMRISK { get; set; }

        [DisplayName("Ödeme Kodu")]
        public string ODEMEKODU { get; set; }
        [DisplayName("Ödeme Açıklama")]
        public string ODEMEACIKLAMA { get; set; }
        
    }
}
