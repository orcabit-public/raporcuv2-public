﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Reports
{
    public class Rapor
    {
        [DisplayName("Cari Hesap Ünvanı")]
        public string DEFINITION_ { get; set; }
        [DisplayName("Tarih")]
        public DateTime DATE_ { get; set; }
        
    }
}
