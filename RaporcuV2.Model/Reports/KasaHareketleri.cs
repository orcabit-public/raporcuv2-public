﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using RaporcuV2.Util;

namespace RaporcuV2.Model.Reports
{
    public class KasaHareketleri
    {
        [DisplayName(Labels.Aciklama)]
        public string ACIKLAMA { get; set; }

        [DisplayName(Labels.Giren)]
        public double GIREN { get; set; }
        [DisplayName(Labels.Cikan)]
        public double CIKAN { get; set; }

        public Int16 CCURRENCY { get; set; }
        public string CURCODE { get; set; }
    }

    public class Toplam
    {

        [DisplayName(Labels.Devir)]
        public double? TOPLAM { get; set; }

    }

    public class Kur
    {

        [DisplayName(Labels.Kur)]
        public double? RATES1 { get; set; }

    }

    public class CekSenetHareketleri
    {
        [DisplayName(Labels.VadeGunu)]
        public DateTime VADE { get; set; }
        [DisplayName(Labels.Aciklama)]
        public string ACIKLAMA { get; set; }
        [DisplayName(Labels.Giren)]
        public double ?GIREN { get; set; }
        [DisplayName(Labels.Cikan)]
        public double ?CIKAN { get; set; }
        public Int16 TRCURR { get; set; }
        public string CURCODE { get; set; }
    }
    public class KrediKartiHareketleri
    {
        [DisplayName(Labels.Banka)]
        public string HESAPADI { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.Tutar)]
        public double? Tutar { get; set; }
        public string CURCODE { get; set; }

    }

    public class Faturalar
    {
        [DisplayName(Labels.FaturaTarihi)]
        public DateTime? DATE_ { get; set; }
        [DisplayName(Labels.VadeGunu)]
        public DateTime? VADE { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.Tutar)]
        public double? Tutar { get; set; }
        public string CURCODE { get; set; }

    }

    public class FaaliyetMusteriCekSenetDurumu
    {
        [DisplayName(Labels.VadeGunu)]
        public DateTime DUEDATE { get; set; }
        [DisplayName(Labels.BankaUnvani)]
        public string NAME { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string CARI { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.Tutar)]
        public double? TOTAL { get; set; }
        [DisplayName(Labels.DovizTipi)]
        public string CURCODE { get; set; }
        [DisplayName(Labels.EvrakTuru)]
        public string TYPE { get; set; }
        
    }


    public class AtaKasa
    {
        [DisplayName(Labels.KasaKodu)]
        public string  KODU { get; set; }

        [DisplayName(Labels.Aciklama)]
        public string ACIKLAMA { get; set; }
        [DisplayName(Labels.DunDevir)]
        public decimal? DEVIR { get; set; }
        [DisplayName(Labels.Borc)]
        public decimal? BORC { get; set; }
        [DisplayName(Labels.Alacak)]
        public decimal? ALACAK { get; set; }
        [DisplayName(Labels.Bakiye)]
        public decimal? BAKIYE { get; set; }
        [DisplayName(Labels.YarinDevir)]
        public decimal? YARIN { get; set; }

    }

    public class AtaKasaHareketleri
    {


        [DisplayName(Labels.KasaAdi)]
        public string NAME { get; set; }

        [DisplayName(Labels.Aciklama)]
        public string CUSTTITLE { get; set; }
        [DisplayName(Labels.IslemAciklama)]
        public string LINEEXP { get; set; }

        private double _borc;

        [DisplayName(Labels.Borc)]
        public double BORC
        {
            get { return _borc; }
            set
            {
                try
                {
                    _borc = value;
                }
                catch (Exception e)
                {
                    _borc = 0;
                }
            }
        }

        private double _alacak;

        [DisplayName(Labels.Alacak)]
        public double ALACAK
        {
            get { return Math.Abs(_alacak); }
            set
            {
                try
                {
                    _alacak = value;
                }
                catch (Exception e)
                {
                    _alacak = 0;
                }
            }
        }
        [DisplayName(Labels.Bolum)]
        public string DEPARTMENT { get; set; }
    }


    public class AtaBanka
    {
        [DisplayName(Labels.BankaKodu)]
        public string KODU { get; set; }

        [DisplayName(Labels.Aciklama)]
        public string ACIKLAMA { get; set; }
        [DisplayName(Labels.DunDevir)]
        public decimal DEVIR { get; set; }
        private decimal _borc;
        [DisplayName(Labels.Borc)]
        public decimal BORC
        {
            get { return _borc; }
            set
            {
                try
                {
                    _borc = value;
                }
                catch (Exception e)
                {
                    _borc = 0;
                }
            }
        }
        private decimal _alacak;
        [DisplayName(Labels.Alacak)]
        public decimal ALACAK
        {
            get { return _alacak; }
            set
            {
                try
                {
                    _alacak = value;
                }
                catch (Exception e)
                {
                    _alacak = 0;
                }
            }
        }
        [DisplayName(Labels.Bakiye)]
        public decimal BAKIYE { get; set; }
        [DisplayName(Labels.YarinDevir)]
        public decimal YARIN { get; set; }

        public int CURRENCY { get; set; }
        public string CURCODE { get; set; }
    }

    public class AtaBankaToplam
    {
        [DisplayName(Labels.DovizTipi)]
        public string CURCODE { get; set; }
        private decimal _borc;
        [DisplayName(Labels.Borc)]

        public decimal BORC
        {
            get { return _borc; }
            set
            {
                try
                {
                    _borc = value;
                }
                catch (Exception e)
                {
                    _borc = 0;
                }
            }
        }
        private decimal _alacak;
        [DisplayName(Labels.Alacak)]
        public decimal ALACAK
        {
            get { return _alacak; }
            set
            {
                try
                {
                    _alacak = value;
                }
                catch (Exception e)
                {
                    _alacak = 0;
                }
            }
        }

    }

    public class AtaBankaHareketleri
    {


        [DisplayName(Labels.BankaUnvani)]
        public string DEFINITION_ { get; set; }

        [DisplayName(Labels.Aciklama)]
        public string LINEEXP { get; set; }

        [DisplayName(Labels.CariHesapUnvani)]
        public string CARI { get; set; }

        [DisplayName(Labels.Borc)]
        public double DVZBORC { get; set; }

        [DisplayName(Labels.Alacak)]
        public double DVZALACAK { get; set; }

        [DisplayName(Labels.Borc)]
        public double TLBORC { get; set; }

        [DisplayName(Labels.Alacak)]
        public double TLALACAK { get; set; }

        public int TRCURR { get; set; }
        public string DVZCINS { get; set; }

    }


    public class AtaKredi
    {
        [DisplayName(Labels.KrediKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.KrediAciklamasi)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.Bakiye)]
        public double BAKIYE { get; set; }
        public double TRTOTAL { get; set; }
        public double INTTOTAL { get; set; }
        [DisplayName(Labels.DovizTipi)]
        public string DOVIZ { get; set; }

    }

    public class AtaPortfoyCekSenetCari
    {
        [DisplayName(Labels.CariHesapUnvani)]
        public string CARIISIM { get; set; }

        [DisplayName(Labels.CekSenetSayi)]
        public int SAYI { get; set; }

        private double _TL;
        [DisplayName(Labels.TL)]
        public double TL
        {
            get { return _TL; }
            set
            {
                try
                {
                    _TL = value;
                }
                catch (Exception e)
                {
                    _TL = 0;
                }
            }
        }

        private double _USD;
        [DisplayName(Labels.USD)]
        public double DOLAR
        {
            get { return _USD; }
            set
            {
                try
                {
                    _USD = value;
                }
                catch (Exception e)
                {
                    _USD = 0;
                }
            }
        }

        private double _EURO;
        [DisplayName(Labels.EURO)]
        public double EURO
        {
            get { return _EURO; }
            set
            {
                try
                {
                    _EURO = value;
                }
                catch (Exception e)
                {
                    _EURO = 0;
                }
            }
        }


    }

    public class AtaMusteriCekSenetToplami
    {
        [DisplayName(Labels.EvrakTuru)]
        public string TUR { get; set; }


        private double _Tutar;
        [DisplayName(Labels.Tutar)]
        public double TUTAR
        {
            get { return _Tutar; }
            set
            {
                try
                {
                    _Tutar = value;
                }
                catch (Exception e)
                {
                    _Tutar = 0;
                }
            }
        }


        [DisplayName(Labels.DovizTipi)]
        public string DOVIZ { get; set; }


    }

    public class AtaCekDurumu
    {
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.CekSayi)]
        public int SAYI { get; set; }


        private double _Tutar;
        [DisplayName(Labels.Tutar)]
        public double TUTAR
        {
            get { return _Tutar; }
            set
            {
                try
                {
                    _Tutar = value;
                }
                catch (Exception e)
                {
                    _Tutar = 0;
                }
            }
        }


        [DisplayName(Labels.DovizTipi)]
        public string CURCODE { get; set; }


    }


    public class AtaPortfoyVeTakasCekleri
    {
        [DisplayName(Labels.CariBanka)]
        public string DEFINITION_ { get; set; }

        [DisplayName(Labels.VadeTarihi)]
        public DateTime? DUEDATE { get; set; }


        private double _Tutar;
        [DisplayName(Labels.Tutar)]
        public double TRNET
        {
            get { return _Tutar; }
            set
            {
                try
                {
                    _Tutar = value;
                }
                catch (Exception e)
                {
                    _Tutar = 0;
                }
            }
        }


        [DisplayName(Labels.DovizTipi)]
        public string CURCODE { get; set; }


    }

    public class AtaTeminatMektuplari
    {
        public string PORTFOYNO { get; set; }
        public string TMEKTUKNO { get; set; }
        public string OWING { get; set; }
        public string BANKAKOD { get; set; }
        public string BANKAADI { get; set; }
        public string MBANKADI { get; set; }
        public DateTime BEGDATE { get; set; }
        public DateTime ENDDATE { get; set; }
        public string SUREDURUMU { get; set; }
        public string PB { get; set; }
        public double TRNET { get; set; }

    }
}
