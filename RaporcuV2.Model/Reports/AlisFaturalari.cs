﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using RaporcuV2.Util;

namespace RaporcuV2.Model.Reports
{
    public class AlisFaturalari
    {
        [DisplayName(Labels.Tarih)]
        public DateTime DATE_ { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.VadeTarihi)]
        public DateTime VADE { get; set; }
        [DisplayName(Labels.Tutar)]
        public double TUTAR { get; set; }
    }
}
