﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.CekSenet
{
    public class CekSenet
    {
        [DisplayName("Banka-Cari Kodu")]
        public string CODE { get; set; }
        [DisplayName("Banka-Cari Adı")]
        public string DEFINITION_ { get; set; }
        [DisplayName("Türü")]
        public string TURU { get; set; }
        [DisplayName("Borçlu")]
        public string OWING { get; set; }
        [DisplayName("Portföy No")]
        public string PORTFOYNO { get; set; }
        [DisplayName("Seri No")]
        public string NEWSERINO { get; set; }
        [DisplayName("Banka Adı")]
        public string BANKNAME { get; set; }
        [DisplayName("Durumu")]
        public string DURUMU { get; set; }
        [DisplayName("Bordro Tarihi")]
        public DateTime? DATE_ { get; set; }
        [DisplayName("Tanzim Tarihi")]
        public DateTime? SETDATE { get; set; }
        [DisplayName("Vade")]
        public DateTime? DUEDATE { get; set; }
        [DisplayName("Şehir")]
        public string CITY { get; set; }
        [DisplayName("Tutar")]
        public double TRNET { get; set; }
        [DisplayName("Döviz")]
        public string DOVIZ { get; set; }

    }
}
