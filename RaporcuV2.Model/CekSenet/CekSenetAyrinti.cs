﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.CekSenet
{
    public class CekSenetAyrinti
    {
        [DisplayName("Vade")]
        public DateTime? VADE { get; set; }
        [DisplayName("Bordro Tarihi")]
        public DateTime? TARIH { get; set; }
        [DisplayName("Banka Adı")]
        public string BANKNAME { get; set; }
        [DisplayName("Müşteri")]
        public string MUSTERI { get; set; }
        [DisplayName("Borçlu")]
        public string BORCLU { get; set; }
        [DisplayName("Şehir")]
        public string SEHIR { get; set; }
        [DisplayName("Seri No")]
        public string SERINO { get; set; }
        [DisplayName("Portföy No")]
        public string PORTFOYNO { get; set; }
        [DisplayName("Modül Adı")]
        public string MODULEXP { get; set; }
        [DisplayName("Durumu")]
        public string STATUSEXP { get; set; }
        [DisplayName("Türü")]
        public string PROCTYPEEXP { get; set; }
        [DisplayName("Cari Adı")]
        public string CARDEXP { get; set; }
        [DisplayName("Devir")]
        public string DEVIREXP { get; set; }
        [DisplayName("Çek/Senet")]
        public string TYPEEXP { get; set; }
        [DisplayName("Tutar")]
        public double TUTAR { get; set; }

    }
}

