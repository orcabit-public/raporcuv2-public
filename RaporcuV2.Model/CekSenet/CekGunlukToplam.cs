﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.CekSenet
{
    public class CekGunlukToplam
    {
        [DisplayName(Labels.CariHesapKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName("Tahsil Edilen")]
        public double TAHSILCEK { get; set; }
        [DisplayName("Bekleyen Çek")]
        public double BEKLEYENCEK { get; set; }


    }
}
