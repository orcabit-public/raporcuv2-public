﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.CekSenet
{
    public class CekToplam
    {
            
        [DisplayName(Labels.VadeYili)]
        public int YIL { get; set; }
        [DisplayName(Labels.VadeAyi)]
        public int AY { get; set; }
        [DisplayName(Labels.VadeAyi)]
        public string DONEM { get; set; }
        [DisplayName(Labels.Tutar)]
        public double AMOUNT { get; set; }
       


    }

    public class CekToplamGiris
    {

        [DisplayName("Giriş Yılı")]
        public int YIL { get; set; }
        [DisplayName("Giriş Ayı")]
        public int AY { get; set; }
        [DisplayName("Giriş Ayı")]
        public string DONEM { get; set; }
        [DisplayName("Ortalama Vade")]
        public double GUN { get; set; }
        [DisplayName(Labels.Tutar)]
        public double TOPLAM { get; set; }





    }
}
