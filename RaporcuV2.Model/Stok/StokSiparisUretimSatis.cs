﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Stok
{
    public class StokSiparisUretimSatis
    {
        [DisplayName(Labels.StokKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.StokAdi)]
        public string NAME { get; set; }
        [DisplayName(Labels.GrupKodu)]
        public string STGRPCODE { get; set; }
        [DisplayName(Labels.YetkiKod)]
        public string CYPHCODE { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.OzelKod2)]
        public string SPECODE2 { get; set; }
        [DisplayName(Labels.MarkaKodu)]
        public string DESCR { get; set; }
        [DisplayName(Labels.StokBirimKodu)]
        public string UNITCODE { get; set; }
        [DisplayName(Labels.StokBirimAdi)]
        public string UNITNAME { get; set; }
        [DisplayName("Stok Miktarı")]
        public double STOCKAMOUNT { get; set; }
        [DisplayName("Sipariş Miktarı")]
        public double ORDERAMOUNT { get; set; }
        [DisplayName("Üretim Miktarı")]
        public double PRODUCEAMOUNT { get; set; }
        [DisplayName("Sevk Miktarı")]
        public double SHIPPEDAMOUNT { get; set; }
        [DisplayName("Toplam Satış KDV.siz")]
        public double TOTALSALESPRICEKDVSIZ { get; set; }
        [DisplayName("Toplam Satış Fiyatı KDV.li")]
        public double TOTALSALESPRICE { get; set; }

    }
}
