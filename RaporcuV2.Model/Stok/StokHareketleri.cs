﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Stok
{
    public class StokHareketleri
    {
        [DisplayName(Labels.StokKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.StokAdi)]
        public string NAME { get; set; }
        [DisplayName(Labels.StokAdi2)]
        public string NAME3 { get; set; }
        [DisplayName(Labels.GrupKodu)]
        public string STGRPCODE { get; set; }
        [DisplayName(Labels.YetkiKod)]
        public string CYPHCODE { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.OzelKod2)]
        public string SPECODE2 { get; set; }
        [DisplayName(Labels.MarkaKodu)]
        public string DESCR { get; set; }
        [DisplayName("Devreden Miktar")]
        public double DEVREDENMIKTAR { get; set; }
        [DisplayName("DB Fiyat")]
        public double DBFIYAT { get; set; }
        [DisplayName("Devreden Toplam")]
        public double DEVREDENTOPLAM { get; set; }
        [DisplayName("Alım Miktar")]
        public double ALIMMIKTAR { get; set; }
        [DisplayName("Ab Fiyat")]
        public double ABFIYAT { get; set; }
        [DisplayName("Alım Toplam")]
        public double ALIMTOPLAM { get; set; }
        [DisplayName("Satış Miktar")]
        public double SATISMIKTAR { get; set; }
        [DisplayName("S B Fiyat")]
        public double SBFIYAT { get; set; }
        [DisplayName("Sarf Miktar")]
        public double SARFMIKTAR { get; set; }
        [DisplayName("SF Fiyat")]
        public double SFFIYAT { get; set; }
        [DisplayName("Sarf Toplam")]
        public double SARFTOPLAM { get; set; }
        [DisplayName("Kalan Miktar")]
        public double KALANMIKTAR { get; set; }
        [DisplayName("Satış Toplam")]
        public double SATISTOPLAM { get; set; }
        [DisplayName("Kar")]
        public double KAR { get; set; }
        [DisplayName("SMM")]
        public double SMM { get; set; }
        [DisplayName("Satış İade Miktar")]
        public double SATISIADEMIKTAR { get; set; }
        [DisplayName("Satış İade Toplam")]
        public double SATISIADETOPLAM { get; set; }
        [DisplayName("Satış İade B Fiyat")]
        public double SATISIADEBIRIMFIYAT { get; set; }
        [DisplayName("Alış İade Miktar")]
        public double ALISIADEMIKTAR { get; set; }
        [DisplayName("Alış İade Toplam")]
        public double ALISIADETOPLAM { get; set; }
        [DisplayName("Alış İade B Fiyat")]
        public double ALISIADEBIRIMFIYAT { get; set; }
        

    }
}
