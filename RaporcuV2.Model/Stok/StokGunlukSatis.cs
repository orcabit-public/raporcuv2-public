﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Stok
{
    public class StokGunlukSatis
    {
        [DisplayName(Labels.StokKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.StokAdi)]
        public string NAME { get; set; }
        [DisplayName(Labels.GrupKodu)]
        public string STGRPCODE { get; set; }
        [DisplayName(Labels.YetkiKod)]
        public string CYPHCODE { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.OzelKod2)]
        public string SPECODE2 { get; set; }
        [DisplayName(Labels.MarkaKodu)]
        public string DESCR { get; set; }
        [DisplayName(Labels.StokBirimKodu)]
        public string BIRIMKODU { get; set; }
        [DisplayName(Labels.StokBirimAdi)]
        public string BIRIMADI { get; set; }
        [DisplayName("Stok Miktarı")]
        public double STOKMIKTARI { get; set; }
        [DisplayName("Üretim Miktarı")]
        public double URETIMMIKTAR { get; set; }
        [DisplayName("Sevk Miktarı")]
        public double SEVKMIKTARI { get; set; }
        [DisplayName("Birim Fiyat")]
        public double BIRIMFIYAT { get; set; }
        [DisplayName("Toplam TL Tutar")]
        public double TOPLAMTUTAR { get; set; }
        [DisplayName("Toplam Döviz Tutar")]
        public double DOVIZTUTAR { get; set; }
        [DisplayName("Döviz Türü")]
        public string DOVIZTIPI { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string CARIKOD { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string CARIADI { get; set; }
        [DisplayName("Cari Özel Kod")]
        public string COZELKOD { get; set; }
        [DisplayName("Cari Özel Kod2")]
        public string COZELKOD2 { get; set; }
    }
}
