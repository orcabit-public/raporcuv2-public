﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Stok
{
    public class SevkiyatTakip
    {
        [DisplayName(Labels.CariHesapKodu)]
        public string CARIKOD { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string CARIACIKLAMA { get; set; }
        [DisplayName(Labels.SiparisTarihi)]
        public DateTime SIPDATE { get; set; }
        [DisplayName(Labels.SiparisFisNo)]
        public string FISNO { get; set; }
        [DisplayName(Labels.StokKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.StokAdi)]
        public string NAME { get; set; }
        [DisplayName(Labels.StokAdi2)]
        public string NAME2 { get; set; }
        [DisplayName(Labels.GrupKodu)]
        public string GRUPKODU { get; set; }

        [DisplayName(Labels.CariOzelKod)]
        public string CARIOZELKOD { get; set; }
        [DisplayName(Labels.Sehir)]
        public string CITY { get; set; }
        [DisplayName(Labels.Ilce)]
        public string TOWN { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string MALZEMEOZELKOD { get; set; }
        [DisplayName(Labels.SiparisMiktar)]
        public double SIPARISMIKTAR { get; set; }        
        [DisplayName(Labels.SiparisBekleyenMiktar)]
        public double BEKLEYENMIKTAR { get; set; }        

        [DisplayName(Labels.StokMiktari)]
        public double STOKMIKTARI { get; set; }
        [DisplayName(Labels.TeminMiktari)]
        public double TEMINMIKTARI { get; set; }

        [DisplayName("LT")]
        public double LT { get; set; }

        [DisplayName("DESI")]
        public double DESI { get; set; }


    }
}
