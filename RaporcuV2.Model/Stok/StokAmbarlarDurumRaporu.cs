﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Stok
{
    public class StokAmbarlarDurumRaporu
    {
        [DisplayName(Labels.StokKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.StokAdi)]
        public string NAME { get; set; }
        [DisplayName(Labels.StokBirimKodu)]
        public string BIRIMKODU { get; set; }
        [DisplayName(Labels.StokBirimAdi)]
        public string BIRIMADI { get; set; }
        [DisplayName(Labels.GrupKodu)]
        public string STGRPCODE { get; set; }

        [DisplayName(Labels.GrupKoduAciklama)]
        public string GRUPKODUACIKLAMASI { get; set; }
        [DisplayName("Açıklama2")]
        public string ACIKLAMA2 { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.OzelKod2)]
        public string SPECODE2 { get; set; }
        [DisplayName(Labels.MarkaKodu)]
        public string MARKAKODU { get; set; }
        [DisplayName("Marka Açıklaması")]
        public string DESCR { get; set; }
        [DisplayName("Genel")]
        public double GENEL { get; set; }
        [DisplayName("Ambar 0")]
        public double Ambar0 { get; set; }
        [DisplayName("Ambar 1")]
        public double Ambar1 { get; set; }
        [DisplayName("Ambar 2")]
        public double Ambar2 { get; set; }
        [DisplayName("Ambar 3")]
        public double Ambar3 { get; set; }
        [DisplayName("Ambar 4")]
        public double Ambar4 { get; set; }
        [DisplayName("Alış Fiyat")]
        public double AFIYAT { get; set; }
        [DisplayName("Satış Fiyat")]
        public double SFIYAT { get; set; }
        [DisplayName("Alım Değeri")]
        public double ADEGER { get; set; }
        [DisplayName("Satış Değeri")]
        public double SDEGER { get; set; }



    }
}
