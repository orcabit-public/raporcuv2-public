﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Stok
{
    public class StokAlisSatisFiyatAnaliz
    {
        [DisplayName(Labels.StokKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.StokAdi)]
        public string NAME { get; set; }
        [DisplayName(Labels.GrupKodu)]
        public string STGRPCODE { get; set; }
        [DisplayName("Açıklama2")]
        public string ACIKLAMA2 { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.OzelKod2)]
        public string SPECODE2 { get; set; }
        [DisplayName(Labels.MarkaKodu)]
        public string DESCR { get; set; }
        [DisplayName("Ambar")]
        public string Ambar { get; set; }
        [DisplayName(Labels.StokMiktari)]
        public double Miktari { get; set; }
        [DisplayName(Labels.StokBirimKodu)]
        public string Birimi { get; set; }
        [DisplayName("Son Fiyat")]
        public double Son { get; set; }
        [DisplayName("Ortalama Fiyat")]
        public double Ortalam { get; set; }
        [DisplayName("En Düşük Fiyat")]
        public double EnDusuk { get; set; }
        [DisplayName("En Yüksek Fiyat")]
        public double EnYuksek { get; set; }
    }
}
