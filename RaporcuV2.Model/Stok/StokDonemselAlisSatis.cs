﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Stok
{
    public class StokDonemselAlisSatis
    {
        [DisplayName(Labels.StokKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.StokAdi)]
        public string NAME { get; set; }
        [DisplayName(Labels.GrupKodu)]
        public string STGRPCODE { get; set; }
        [DisplayName(Labels.YetkiKod)]
        public string CYPHCODE { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.OzelKod2)]
        public string SPECODE2 { get; set; }
        [DisplayName(Labels.MarkaKodu)]
        public string DESCR { get; set; }
        [DisplayName("Günlük Miktar")]
        public double GUNLUK { get; set; }
        [DisplayName("Günlük Fiyat")]
        public double TOPTANSATISFIYATGUNLUK { get; set; }
        [DisplayName("Haftalık Miktar")]
        public double HAFTALIK { get; set; }
        [DisplayName("Haftalık Fiyat")]
        public double TOPTANSATISFIYATHAFTALIK { get; set; }
        [DisplayName("Aylık Miktar")]
        public double AYLIK { get; set; }
        [DisplayName("Aylık Fiyat")]
        public double TOPTANSATISFIYATAYLIK { get; set; }
        [DisplayName("Yıllık Miktar")]
        public double YILLIK { get; set; }
        [DisplayName("Yıllık Fiyat")]
        public double TOPTANSATISFIYATYILLIK { get; set; }
    }
}
