﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Stok
{
    public class IrsaliyeDetay
    {
                      
        [DisplayName(Labels.FisTuru)]
        public string IrsaliyeTuru { get; set; }
        [DisplayName("Malzeme Grubu")]
        public string MalzemeGrubu { get; set; }
        [DisplayName(Labels.FisNo)]
        public string FICHENO { get; set; }
        [DisplayName(Labels.FisTarihi)]
        public DateTime TARIH { get; set; }
        [DisplayName(Labels.FisAyi)]
        public int AY { get; set; }
        [DisplayName(Labels.FisHaftasi)]
        public int HAFTA { get; set; }
        [DisplayName(Labels.StokKodu)]
        public string SHKODU { get; set; }
        [DisplayName(Labels.StokAdi)]
        public string SHADI { get; set; }
        [DisplayName(Labels.StokAdi2)]
        public string NAME3 { get; set; }
        [DisplayName(Labels.GrupKodu)]
        public string GRUPKODU { get; set; }
        [DisplayName(Labels.GrupKoduAciklama)]
        public string GRUPACIKLAMA { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.Aciklama)]
        public string LINEEXP { get; set; }
        [DisplayName(Labels.Miktar)]
        public double MIKTAR { get; set; }
        [DisplayName(Labels.StokBirimKodu)]
        public string BRMKODU { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string KOD { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string CARIHESAP { get; set; }
        [DisplayName(Labels.Sehir)]
        public string CITY { get; set; }
        
    }
}
