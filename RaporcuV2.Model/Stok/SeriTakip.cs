﻿using RaporcuV2.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RaporcuV2.Model.Stok
{
    public class SeriTakip
    {
        [DisplayName(Labels.Tarih)]
        public DateTime DATE_ { get; set; }
        [DisplayName(Labels.SeriNo)]
        public string SERINO { get; set; }
        [DisplayName(Labels.StokKodu)]
        public string ICODE { get; set; }
        [DisplayName(Labels.StokAdi)]
        public string NAME { get; set; }
        [DisplayName(Labels.OzelKod)]
        public string SPECODE { get; set; }
        [DisplayName(Labels.GrupKodu)]
        public string STGRPCODE { get; set; }
        [DisplayName(Labels.MarkaKodu)]
        public string DESCR { get; set; }
        [DisplayName(Labels.CariHesapKodu)]
        public string CODE { get; set; }
        [DisplayName(Labels.CariHesapUnvani)]
        public string DEFINITION_ { get; set; }
        [DisplayName(Labels.IslemTuru)]
        public string DURUM { get; set; }
        [DisplayName(Labels.DovizTipi)]
        public string PARABIRIMI { get; set; }
        [DisplayName(Labels.StokBirimKodu)]
        public string UNAME { get; set; }
        [DisplayName(Labels.Tutar)]
        public double TUTAR { get; set; }


    }
}
