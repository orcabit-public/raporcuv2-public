﻿using RaporcuV2.Model.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace RaporcuV2.DataAccess
{
    public class RaporcuDbContext : DbContext
    {
        public RaporcuDbContext() : base(SingleConnection.ConString)
        {

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<RaporcuDbContext, Migrations.Configuration>());
            this.Configuration.AutoDetectChangesEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //#region Warehouse
            ///// FOREIGN KEY cycles or multiple cascade paths 
            //modelBuilder.Entity<Warehouse>()
            //.HasRequired(p => p.Branch)
            //.WithMany()
            //.WillCascadeOnDelete(false);
            //#endregion
        }

        public override int SaveChanges()
        {
            AddTimestamps();
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);
                var fullErrorMessage = string.Join("\r\n", errorMessages);
                var exceptionMessage = string.Concat("Kayıt Hatası: ", fullErrorMessage);
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }

            //return base.SaveChanges();
        }

        private void AddTimestamps()
        {
            int userId = 1;
            try
            {
                userId = 1; // Current.CurrentUser.UserId;
            }
            catch (Exception e)
            {

            }

            var entities = ChangeTracker
                .Entries()
                .Where(x => x.Entity is BaseEntity &&
                            (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entity in entities)
                if (entity.State == EntityState.Added)
                {
                    ((BaseEntity) entity.Entity).CreatedDate = DateTime.Now;
                    ((BaseEntity) entity.Entity).CreatedUserId = userId;
                }
                else if (entity.State == EntityState.Modified)
                {
                    ((BaseEntity) entity.Entity).UpdateDate = DateTime.Now;
                    ((BaseEntity) entity.Entity).UpdatedUserId = userId;
                }
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<UserPermission> UserPermissions { get; set; }
        public DbSet<CustomReport> CustomReports { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Layout> Layouts { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<UserDashboard> UserDashboards { get; set; }
    }
}