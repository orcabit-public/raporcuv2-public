﻿using System.Collections.Generic;
using System.Deployment.Application;
using RaporcuV2.Helper.Sorgular;

namespace RaporcuV2.DataAccess.Migrations
{
    using RaporcuV2.Model.Database;
    using RaporcuV2.Helper;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RaporcuV2.DataAccess.RaporcuDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "RaporcuV2.DataAccess.RaporcuDbContext";
            AutomaticMigrationDataLossAllowed = true;
            
        }

        protected override void Seed(RaporcuV2.DataAccess.RaporcuDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            RaporcuDbContext cnt = new RaporcuDbContext();
            int count = cnt.Settings.Count();
            if (count == 0)
            {
                cnt.Settings.Add(new Setting() {Name = "Version", Value = "1.0.0.0"});
                cnt.SaveChanges();
            }
            Setting setting = cnt.Settings.FirstOrDefault(a => a.Name == "Version");

            bool isUpdate = false;
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                Version myVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion;
                Version verison = new Version(setting.Value);
                if (myVersion > verison)
                {
                    isUpdate = true;
                }
            }
            else
            {
                isUpdate = true;
            }

            if (isUpdate)
            {
                #region Deploy Report 

                List<Report> reports = new List<Report>();

                reports.Add((new Report() {TagId = (int) ReportTags.AcikBakiye, Value = SqlCariHesap.AcikBakiye}));
                reports.Add((new Report() {TagId = (int) ReportTags.RiskBakiyeKontrol, Value = SqlCariHesap.RiskBakiyeKontrol}));
                reports.Add((new Report() {TagId = (int) ReportTags.BorcAlacakDurum, Value = SqlCariHesap.BorcAlacakDurumRaporu}));
                

                reports.Add((new Report() {TagId = (int) ReportTags.Hesapislemleri, Value = SqlCariHesap.HesapIslemleri}));
                reports.Add((new Report() {TagId = (int) ReportTags.CariSiparisListesi, Value = SqlCariHesap.CariSiparisListesi }));
                reports.Add((new Report() {TagId = (int) ReportTags.CariVade, Value = SqlCariHesap.CariVadeRaporu}));
                reports.Add((new Report() {TagId = (int) ReportTags.CariCek, Value = SqlCariHesap.CariCekRaporu}));
                reports.Add((new Report() {TagId = (int) ReportTags.SiparisliCariEkstre, Value = SqlCariHesap.SiparisliCariEkstre }));
                reports.Add((new Report() { TagId = (int)ReportTags.SiparisliBakiye, Value = SqlCariHesap.SiparisliBakiyeRaporu }));

                reports.Add((new Report() { TagId = (int)ReportTags.CariMuhasebeKontrol, Value = SqlMuhasebe.MuhasebeCariKontrol}));
                reports.Add((new Report() { TagId = (int)ReportTags.Mizan, Value = SqlMuhasebe.Mizan}));
                reports.Add((new Report() { TagId = (int)ReportTags.BABS, Value = SqlMuhasebe.FormBaBs}));
                reports.Add((new Report() { TagId = (int)ReportTags.KdvRaporu, Value = SqlMuhasebe.KdvRaporu}));

                reports.Add((new Report() { TagId = (int)ReportTags.Bankaislemleri, Value = SqlBanka.BankaIslemleri }));
                reports.Add((new Report() { TagId = (int)ReportTags.BankaBakiye, Value = SqlBanka.BankaBakiye }));
                reports.Add((new Report() { TagId = (int)ReportTags.KrediKarti, Value = "" }));
                reports.Add((new Report() { TagId = (int)ReportTags.KrediKartiCiroDetay, Value = SqlBanka.KrediKartiCiroDetay }));

                reports.Add((new Report() { TagId = (int)ReportTags.StokHareketleri, Value = SqlStok.StokHareketleri }));
                reports.Add((new Report() { TagId = (int)ReportTags.SiparisuretimSatis, Value = SqlStok.StokSiparisUretimSatis }));
                reports.Add((new Report() { TagId = (int)ReportTags.DonemselStokAlisSatis, Value = SqlStok.DonemselStokAlisSatis }));
                reports.Add((new Report() { TagId = (int)ReportTags.StokAlisSatisFiyatAnaliz, Value = SqlStok.StokAlisSatisFiyatAnaliz }));
                reports.Add((new Report() { TagId = (int)ReportTags.AmbarlarDurumRaporu, Value = SqlStok.AmbarlarDurumRaporu }));
                reports.Add((new Report() { TagId = (int)ReportTags.GunlikSatis, Value = SqlStok.GunlukSatis }));
                reports.Add((new Report() { TagId = (int)ReportTags.SeriTakip, Value = SqlStok.SeriTakip }));
                reports.Add((new Report() { TagId = (int)ReportTags.SevkiyatPlani, Value = SqlStok.SevkiyatPlani }));
                reports.Add((new Report() { TagId = (int)ReportTags.IrsaliyeDetay, Value = SqlStok.IrsaliyeDetay }));

                // reports.Add((new Report() { TagId = (int)ReportTags.KasaRaporu, Value = SqlKasa.KasaToplamBakiye}));
                reports.Add((new Report() { TagId = (int)ReportTags.KasaHareket, Value = SqlKasa.KasaHareketler }));

                reports.Add((new Report() { TagId = (int)ReportTags.AtaKasa, Value = SqlReports.AtaKasa}));
                reports.Add((new Report() { TagId = (int)ReportTags.AtaKasaHareketleri, Value = SqlReports.AtaKasaHareketleri }));
                reports.Add((new Report() { TagId = (int)ReportTags.AtaBankaDurumu, Value = SqlReports.BankaDurumu }));
                reports.Add((new Report() { TagId = (int)ReportTags.AtaBankaHareketleri, Value = SqlReports.AtaBankaHareketleri }));
                reports.Add((new Report() { TagId = (int)ReportTags.AtaKrediDurumu, Value = SqlReports.KrediDurumu }));
                reports.Add((new Report() { TagId = (int)ReportTags.AtaPortfoyCekSenetCari, Value = SqlReports.PortfoyCekSenetCari }));
                reports.Add((new Report() { TagId = (int)ReportTags.AtaMusteriCekSenetToplam, Value = SqlReports.MusteriCekSenetToplam }));
                reports.Add((new Report() { TagId = (int)ReportTags.AtaCekDurumu, Value = SqlReports.CekDurumu }));
                reports.Add((new Report() { TagId = (int)ReportTags.AtaPortfoyVeTakasCekleri, Value = SqlReports.PortfoyVeTakasCekleri }));
                reports.Add((new Report() { TagId = (int)ReportTags.AtaTeminatMektuplari, Value = SqlReports.TeminatMektuplari }));
                

                reports.Add((new Report() { TagId = (int)ReportTags.OdemePlaniAlacakKasalar, Value = SqlNakitAkis.AlacakKasalar }));
                reports.Add((new Report() { TagId = (int)ReportTags.OdemePlaniAlacakBankalar, Value = SqlNakitAkis.AlacakBankalar }));
                reports.Add((new Report() { TagId = (int)ReportTags.OdemePlaniAlacakCekSenet, Value = SqlNakitAkis.AlacakCekSenet}));
                reports.Add((new Report() { TagId = (int)ReportTags.OdemePlaniAlacakCariHesap, Value = SqlNakitAkis.AlacakCariHesap }));
                reports.Add((new Report() { TagId = (int)ReportTags.OdemePlaniBorcKrediler, Value = SqlNakitAkis.BorcKrediler }));
                reports.Add((new Report() { TagId = (int)ReportTags.OdemePlaniBorcKredilerDetay, Value = SqlNakitAkis.BorcKredilerDetay }));
                reports.Add((new Report() { TagId = (int)ReportTags.OdemePlaniBorcCekler, Value = SqlNakitAkis.BorcCekler }));
                reports.Add((new Report() { TagId = (int)ReportTags.OdemePlaniBorcKrediKarti, Value = SqlNakitAkis.BorcKrediKarti}));
                reports.Add((new Report() { TagId = (int)ReportTags.OdemePlaniBorcKrediKartiDetay, Value = SqlNakitAkis.BorcKrediKartiDetay}));
                reports.Add((new Report() { TagId = (int)ReportTags.OdemePlaniBorcCariHesap, Value = SqlNakitAkis.BorcCariHesap }));

                reports.Add((new Report() { TagId = (int)ReportTags.NakitAkisVarliklar, Value = SqlNakitAkis.NakitAkisVarliklar }));
                reports.Add((new Report() { TagId = (int)ReportTags.NakitAkisBorclarKredi, Value = SqlNakitAkis.NakitAkisBorclarKredi }));
                reports.Add((new Report() { TagId = (int)ReportTags.NakitAkisBorclarCeklerimiz, Value = SqlNakitAkis.NakitAkisBorclarCeklerimiz }));
                reports.Add((new Report() { TagId = (int)ReportTags.NakitAkisDetay, Value = SqlNakitAkis.NakitAkisDetay }));

                reports.Add((new Report() { TagId = (int)ReportTags.CekSenet, Value = SqlCekSenet.CekSenet}));
                reports.Add((new Report() { TagId = (int)ReportTags.CekSenetAyrinti, Value = SqlCekSenet.CekSenetAyrinti }));
                reports.Add((new Report() { TagId = (int)ReportTags.CekToplam, Value = SqlCekSenet.CekToplam }));
                reports.Add((new Report() { TagId = (int)ReportTags.CekGirisToplam, Value = SqlCekSenet.CekGirisToplam}));

                reports.Add((new Report() { TagId = (int)ReportTags.Faturalar, Value = SqlFatura.Faturalar }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaturaKarliligi, Value = SqlFatura.FaturaKarliligi }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaturaVade, Value = SqlFatura.FaturaVade }));
                reports.Add((new Report() { TagId = (int)ReportTags.SatisElemaniRaporu, Value = SqlFatura.SatisElemaniRaporu }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaturaVadeToplam, Value = SqlFatura.FaturaVadeToplam }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaturaDetay, Value = SqlFatura.FaturaDetay }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaturaDetay2, Value = SqlFatura.FaturaDetay2 }));

                reports.Add((new Report() { TagId = (int)ReportTags.HizmetHareketleri, Value = SqlHizmet.HizmetHareketleri }));
                reports.Add((new Report() { TagId = (int)ReportTags.HizmetToplamlari, Value = SqlHizmet.HizmetToplamlari }));

                reports.Add((new Report() { TagId = (int)ReportTags.FaaliyetAlisFaturalari, Value = SqlReports.FaaliyetAlisFaturalari }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaaliyetBankaHareketleri, Value = SqlReports.FaaliyetBankaHareketleri }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaaliyetCekSenetHareketleri, Value = SqlReports.FaaliyetCekSenetHareketleri }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaaliyetFirmaKrediKartiHareketleri, Value = SqlReports.FaaliyetFirmaKrediKartiHareketleri }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaaliyetKasaHareketleri, Value = SqlReports.FaaliyetKasaHareketleri }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaaliyetKrediKartiHareketleri, Value = SqlReports.FaaliyetKrediKartiHareketleri }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaaliyetSatisFaturalari, Value = SqlReports.FaaliyetSatisFaturalari }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaaliyetCekDurum, Value = SqlReports.FaaliyetCekDurum }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaaliyetKrediDurumu, Value = SqlReports.FaaliyetKrediDurumu }));
                reports.Add((new Report() { TagId = (int)ReportTags.FaaliyetMusteriCekSenetDurum, Value=SqlReports.FaaliyetMusteriCekSenetDurum }));

                reports.Add((new Report() { TagId = (int)ReportTags.Kur, Value = SqlReports.Kur }));
                reports.Add((new Report() { TagId = (int)ReportTags.KurTumu, Value = SqlReports.TumDovizKurlari }));
                reports.Add((new Report() { TagId = (int)ReportTags.KurBuGun, Value = SqlReports.KurBuGun }));

                reports.Add((new Report() { TagId = (int)ReportTags.AtaSmm, Value = SqlReports.AtaSmm}));
                reports.Add((new Report() { TagId = (int)ReportTags.AtaKarZarar, Value = SqlReports.AtaKarZarar}));

                reports.Add((new Report() { TagId = (int)ReportTags.Bilanco, Value = SqlMuhasebe.Bilanco}));
                reports.Add((new Report() { TagId = (int)ReportTags.GelirTablosu, Value = SqlMuhasebe.GelirTablosu}));


                #endregion

                #region Deploy Report Add Database

                foreach (var item in reports)
                {
                    if (!cnt.Reports.Any(a => a.TagId == item.TagId))
                    {
                        cnt.Reports.AddOrUpdate(item);
                    }
                    else
                    {
                        Report report = cnt.Reports.FirstOrDefault(a => a.TagId == item.TagId);
                        report.Value = item.Value;
                        cnt.Entry(report).State = EntityState.Modified;}
                    cnt.SaveChanges();
                }

                #endregion

                #region Create Admin Users

                User user = new User
                {
                    UserName = "admin",
                    FullName = "admin",
                    Password = "admin",
                    EMail = "admin@admin.com"

                };
                if (!cnt.Users.Any())
                {
                    cnt.Users.Add(user);
                    cnt.SaveChanges();
                }

                #endregion

                #region Create Permissions

                List<Permission> permissionList = new List<Permission>();

                #region Muhasebe

                permissionList.Add(new Permission() {PermissionName = "Muhasebe Menüsü", PermissionTag = 100});
                permissionList.Add(new Permission() {PermissionName = "Cari Muhasebe Kontrol", PermissionTag = 101});
                permissionList.Add(new Permission() {PermissionName = "Cari Muhasebe Bakiye", PermissionTag = 102});
                permissionList.Add(new Permission() {PermissionName = "Mizan", PermissionTag = 103});
                permissionList.Add(new Permission() {PermissionName = "Genel Durum", PermissionTag = 104});
                permissionList.Add(new Permission() {PermissionName = "Kdv Raporu", PermissionTag = 105});
                permissionList.Add(new Permission() {PermissionName = "BA - BS", PermissionTag = 106});

                #endregion

                #region Cari Hesap

                permissionList.Add(new Permission() {PermissionName = "Cari Hesap Menüsü", PermissionTag = 200});
                permissionList.Add(new Permission() {PermissionName = "Açık Bakiye", PermissionTag = 201});
                permissionList.Add(new Permission() {PermissionName = "Ödeme Alacak Takip", PermissionTag = 202});
                permissionList.Add(new Permission() {PermissionName = "Risk Bakiye Kontrol", PermissionTag = 203});
                permissionList.Add(new Permission() {PermissionName = "Cari Borç Alacak Durum Raporu", PermissionTag = 204});
                permissionList.Add(new Permission() {PermissionName = "Cari Borç Alacak Toplamları", PermissionTag = 205});
                permissionList.Add(new Permission() {PermissionName = "Satış Alış Cariye Oranı", PermissionTag = 206});
                permissionList.Add(new Permission() {PermissionName = "Hesap Ýþlemleri", PermissionTag = 207});
                permissionList.Add(new Permission() {PermissionName = "Cari Hesap Hareket Sayısı", PermissionTag = 208});
                permissionList.Add(new Permission() {PermissionName = "Cari Vade Raporu", PermissionTag = 209});
                permissionList.Add(new Permission() {PermissionName = "Cari Çek Raporu", PermissionTag = 210});
                permissionList.Add(new Permission() {PermissionName = "Cari Sipariş Listesi", PermissionTag = 211});
                permissionList.Add(new Permission() {PermissionName = "Siparişli Cari Hesap Ektresi", PermissionTag = 212 });
                permissionList.Add(new Permission() {PermissionName = "Cari Siparişli Bakiye Raporu", PermissionTag = 213 });

                #endregion

                #region Banka

                permissionList.Add(new Permission() {PermissionName = "Banka Menüsü", PermissionTag = 300});
                permissionList.Add(new Permission() {PermissionName = "Banka işlemleri", PermissionTag = 301});
                permissionList.Add(new Permission() {PermissionName = "Banka Bakiye", PermissionTag = 302});
                permissionList.Add(new Permission() {PermissionName = "Kredi Kartı", PermissionTag = 303});
                permissionList.Add(new Permission() {PermissionName = "Kredi Kartı Ciro Detay", PermissionTag = 304});

                #endregion

                #region Stok

                permissionList.Add(new Permission() {PermissionName = "Stok Menüsü", PermissionTag = 400});
                permissionList.Add(new Permission() {PermissionName = "Stok Hareketleri", PermissionTag = 401});
                permissionList.Add(new Permission() {PermissionName = "Sipariş - Üretim - Satış", PermissionTag = 402});
                permissionList.Add(new Permission() {PermissionName = "Dönemsel Stok Alış - Satış", PermissionTag = 403});
                permissionList.Add(new Permission() {PermissionName = "Stok Alış - Satış Fiyat Analiz", PermissionTag = 404});
                permissionList.Add(new Permission() {PermissionName = "Ambarlar Durum Raporu", PermissionTag = 405});
                permissionList.Add(new Permission() {PermissionName = "Günlük Satış", PermissionTag = 406});
                permissionList.Add(new Permission() {PermissionName = "Seri Takip", PermissionTag = 407});
                permissionList.Add(new Permission() {PermissionName = "Sevkiyat Planı", PermissionTag = 408 });
                permissionList.Add(new Permission() { PermissionName = "Irsaliye Detay", PermissionTag = 409 });

                #endregion

                #region Kasa

                permissionList.Add(new Permission() {PermissionName = "Kasa Menüsü", PermissionTag = 500});
                permissionList.Add(new Permission() {PermissionName = "Kasa Raporu", PermissionTag = 501});

                #endregion

                #region Çek Senet

                permissionList.Add(new Permission() {PermissionName = "Çek - Senet Menüsü", PermissionTag = 600});
                permissionList.Add(new Permission() {PermissionName = "Çek - Senet", PermissionTag = 601});
                permissionList.Add(new Permission() {PermissionName = "Çek - Senet Ayrıntı", PermissionTag = 602});
                permissionList.Add(new Permission() {PermissionName = "Çek Toplam", PermissionTag = 603});

                #endregion

                #region Fatura

                permissionList.Add(new Permission() {PermissionName = "Fatura Menüsü", PermissionTag = 700});
                permissionList.Add(new Permission() {PermissionName = "Faturalar", PermissionTag = 701});
                permissionList.Add(new Permission() {PermissionName = "Fatura Karlılığı", PermissionTag = 702});
                permissionList.Add(new Permission() {PermissionName = "Fatura Vade", PermissionTag = 703});
                permissionList.Add(new Permission() {PermissionName = "Satış Elemanı Raporu", PermissionTag = 704});
                permissionList.Add(new Permission() {PermissionName = "Fatura Vade Toplam", PermissionTag = 705});
                permissionList.Add(new Permission() { PermissionName = "Fatura Detay", PermissionTag = 706 });
                permissionList.Add(new Permission() { PermissionName = "Fatura Detay2", PermissionTag = 707 });

                #endregion

                #region Hizmet

                permissionList.Add(new Permission() {PermissionName = "Hizmet Menüsü", PermissionTag = 800});
                permissionList.Add(new Permission() {PermissionName = "Hizmet Hareketleri", PermissionTag = 801});
                permissionList.Add(new Permission() {PermissionName = "Hizmet Toplamları", PermissionTag = 802});
                permissionList.Add(new Permission() {PermissionName = "Faaliyet Raporu", PermissionTag = 803});


                #endregion

                #region Ayarlar

                permissionList.Add(new Permission() {PermissionName = "Ayarlar Menüsü", PermissionTag = 10000});
                permissionList.Add(new Permission() {PermissionName = "Bağlantı Ayarları", PermissionTag = 10001});
                permissionList.Add(new Permission() {PermissionName = "Kullanıcılar", PermissionTag = 10002});
                permissionList.Add(new Permission() {PermissionName = "Tanımlı Raporlar", PermissionTag = 10003});
                

                #endregion

                #region Ayarlar

                permissionList.Add(new Permission(){PermissionName = "Tanımlı Raporlar Menüsü", PermissionTag = 1000000});

                #endregion

                #region Permission Add Database

                foreach (var item in permissionList)
                {
                    if (!cnt.Permissions.Any(a => a.PermissionTag == item.PermissionTag))
                    {
                        cnt.Permissions.Add(item);
                        cnt.SaveChanges();
                    }
                }

                #endregion

                #region Admin Permission

                permissionList = cnt.Permissions.ToList();
                List<User> userList = cnt.Users.ToList();

                foreach (User appUser in userList)
                {
                    #region Add User Permission

                    foreach (var item in permissionList)
                    {
                        if (!cnt.UserPermissions.Any(a =>
                            a.UserId == appUser.UserId && a.PermissionId == item.PermissionId))
                        {
                            UserPermission userPermission = new UserPermission()
                            {
                                PermissionStatus = true,
                                PermissionId = item.PermissionId,
                                UserId = appUser.UserId
                            };

                            cnt.UserPermissions.Add(userPermission);
                            cnt.SaveChanges();
                        }
                    }

                    #endregion
                }



                #endregion

                #endregion

                #region Update Version

                if (ApplicationDeployment.IsNetworkDeployed)
                {
                    Setting version = cnt.Settings.FirstOrDefault(a => a.Name == "Version");
                    version.Value = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
                    cnt.Entry(version).State = EntityState.Modified;
                    cnt.SaveChanges();
                }

                #endregion
            }
        }

    }
}
