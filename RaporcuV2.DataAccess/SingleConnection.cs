﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.AccessControl;
using System.Text;

namespace RaporcuV2.DataAccess
{
    public class SingleConnection
    {
        private SingleConnection() { }
        private static SingleConnection _ConsString = null;
        private String _String = null;
        private static string _serverName;
        private static string _dbName;
        private static string _userId;
        private static string _pass;
        private static string _serial;
        private static string _FirmaNo;
        private static string _FontSize="0";
        private static string appName = "Raporcu";

        #region Registry Connection Settings


        #region ServerName

        public static string ServerName
        {
            get
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"Software", true);
                if (rk != null)
                {
                    RegistryKey subKey = rk.OpenSubKey(appName);
                    if (subKey != null)
                        _serverName = (string)subKey.GetValue("DataSource");
                }
                return _serverName;
            }
            set
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey("Software", true);
                RegistrySecurity rs = new RegistrySecurity();
                string user = Environment.UserDomainName + "\\" + Environment.UserName;

                rs.AddAccessRule(new RegistryAccessRule(user,
                    RegistryRights.WriteKey | RegistryRights.ReadKey | RegistryRights.Delete,
                    InheritanceFlags.None,
                    PropagationFlags.None,
                    AccessControlType.Allow));
                rk = rk.CreateSubKey(appName, RegistryKeyPermissionCheck.Default, rs);
                // Write encrypted string, initialization vector and key to the registry
                rk.SetValue("DataSource", value);
                _serverName = value;
            }
        }

        #endregion

        #region DbName
        public static string DbName
        {
            get
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"Software", true);
                if (rk != null)
                {
                    RegistryKey subKey = rk.OpenSubKey(appName);
                    if (subKey != null)
                        _dbName = (string)subKey.GetValue("InitialCatalog");
                }
                return _dbName;
            }
            set
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey("Software", true);
                RegistrySecurity rs = new RegistrySecurity();
                string user = Environment.UserDomainName + "\\" + Environment.UserName;

                rs.AddAccessRule(new RegistryAccessRule(user,
                    RegistryRights.WriteKey | RegistryRights.ReadKey | RegistryRights.Delete,
                    InheritanceFlags.None,
                    PropagationFlags.None,
                    AccessControlType.Allow));
                rk = rk.CreateSubKey(appName, RegistryKeyPermissionCheck.Default, rs);
                // Write encrypted string, initialization vector and key to the registry
                rk.SetValue("InitialCatalog", value);
                _dbName = value;
            }
        }

        #endregion

        #region FirmaNo
        public static string FirmaNo
        {
            get
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"Software", true);
                if (rk != null)
                {
                    RegistryKey subKey = rk.OpenSubKey(appName);
                    if (subKey != null)
                        _FirmaNo = (string)subKey.GetValue("FirmaNo");
                }
                return _FirmaNo;
            }
            set
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey("Software", true);
                RegistrySecurity rs = new RegistrySecurity();
                string user = Environment.UserDomainName + "\\" + Environment.UserName;

                rs.AddAccessRule(new RegistryAccessRule(user,
                    RegistryRights.WriteKey | RegistryRights.ReadKey | RegistryRights.Delete,
                    InheritanceFlags.None,
                    PropagationFlags.None,
                    AccessControlType.Allow));
                rk = rk.CreateSubKey(appName, RegistryKeyPermissionCheck.Default, rs);
                // Write encrypted string, initialization vector and key to the registry
                rk.SetValue("FirmaNo", value);
                _FirmaNo = value;
            }
        }

        #endregion

        #region UserId

        public static string UserId
        {
            get
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"Software", true);
                if (rk != null)
                {
                    RegistryKey subKey = rk.OpenSubKey(appName);
                    if (subKey != null)
                        _userId = (string)subKey.GetValue("UserID");
                }
                return _userId;
            }
            set
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey("Software", true);
                RegistrySecurity rs = new RegistrySecurity();
                string user = Environment.UserDomainName + "\\" + Environment.UserName;

                rs.AddAccessRule(new RegistryAccessRule(user,
                    RegistryRights.WriteKey | RegistryRights.ReadKey | RegistryRights.Delete,
                    InheritanceFlags.None,
                    PropagationFlags.None,
                    AccessControlType.Allow));
                rk = rk.CreateSubKey(appName, RegistryKeyPermissionCheck.Default, rs);
                rk.SetValue("UserID", value);
                _userId = value;
            }
        }

        #endregion

        #region Pass
        public static string Pass
        {
            get
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"Software", true);
                if (rk != null)
                {
                    RegistryKey subKey = rk.OpenSubKey(appName);
                    if (subKey != null)
                        _pass = (string)subKey.GetValue("Password");
                }
                return _pass;
            }
            set
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey("Software", true);
                RegistrySecurity rs = new RegistrySecurity();
                string user = Environment.UserDomainName + "\\" + Environment.UserName;

                rs.AddAccessRule(new RegistryAccessRule(user,
                    RegistryRights.WriteKey | RegistryRights.ReadKey | RegistryRights.Delete,
                    InheritanceFlags.None,
                    PropagationFlags.None,
                    AccessControlType.Allow));
                rk = rk.CreateSubKey(appName, RegistryKeyPermissionCheck.Default, rs);
                rk.SetValue("Password", value);
                _pass = value;
            }
        }

        #endregion
        
        #region Serial
        public static string Serial
        {
            get
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"Software", true);
                if (rk != null)
                {
                    RegistryKey subKey = rk.OpenSubKey(appName);
                    if (subKey != null)
                        _serial = (string)subKey.GetValue("Serial");
                }
                return _serial;
            }
            set
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey("Software", true);
                RegistrySecurity rs = new RegistrySecurity();
                string user = Environment.UserDomainName + "\\" + Environment.UserName;

                rs.AddAccessRule(new RegistryAccessRule(user,
                    RegistryRights.WriteKey | RegistryRights.ReadKey | RegistryRights.Delete,
                    InheritanceFlags.None,
                    PropagationFlags.None,
                    AccessControlType.Allow));
                rk = rk.CreateSubKey(appName, RegistryKeyPermissionCheck.Default, rs);
                rk.SetValue("Serial", value);
                _serial = value;
            }
        }

        #endregion


        #region FontSize
        public static string FontSize
        {
            get
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"Software", true);
                if (rk != null)
                {
                    RegistryKey subKey = rk.OpenSubKey(appName);
                    if (subKey != null)
                        _FontSize = (string)subKey.GetValue("FontSize");
                }
                return _FontSize;
            }
            set
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey("Software", true);
                RegistrySecurity rs = new RegistrySecurity();
                string user = Environment.UserDomainName + "\\" + Environment.UserName;

                rs.AddAccessRule(new RegistryAccessRule(user,
                    RegistryRights.WriteKey | RegistryRights.ReadKey | RegistryRights.Delete,
                    InheritanceFlags.None,
                    PropagationFlags.None,
                    AccessControlType.Allow));
                rk = rk.CreateSubKey(appName, RegistryKeyPermissionCheck.Default, rs);
                rk.SetValue("FontSize", value);
                _FontSize = value;
            }
        }

        #endregion




        #endregion

        public static string ConString
        {
            get
            {
                if (_ConsString == null)
                {
                    _ConsString = new SingleConnection { _String = SingleConnection.Connect() };
                    return _ConsString._String;
                }
                else
                    return _ConsString._String;
            }
        }

        public static string Connect()
        {

            //Build an SQL connection string
            SqlConnectionStringBuilder sqlString = new SqlConnectionStringBuilder()
            {

                DataSource = ServerName, // Server name
                InitialCatalog = "OrcaReport", //DbName,  //Database
                UserID = UserId,         //Username
                Password = Pass,  //Password

            };

            return sqlString.ConnectionString;
        }


    }
}
