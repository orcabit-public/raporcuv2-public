﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.AccessControl;
using Microsoft.Win32;
using RaporcuV2.Model.Database;

namespace RaporcuV2.DataAccess
{
    public class UserInfo
    {
        private static User _loginUser { get; set; }
        private static Session _session { get; set; }
        private UserInfo() { }

        public static UserInfo CreateInstance()
        {
            if (_instance == null)
            {
                _instance = new UserInfo();
                _instance.User = _loginUser;
                _instance.Session = _session;
                RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"Software", true);
                if (rk != null)
                {
                    RegistryKey subKey = rk.OpenSubKey("Raporcu");
                    try
                    {
                        _instance.UserName = (string)subKey.GetValue("UserName");
                    }
                    catch (Exception)
                    {

                    }
                }
                RaporcuDbContext cnt = new RaporcuDbContext();
                List<UserPermission> myPermissions = cnt.UserPermissions.Where(ru => ru.UserId == _instance.User.UserId).ToList();

            }
            return _instance;
        }

        public static bool SetUserInfo(string userName, string password)
        {
            try
            {
                using (DataAccess.RaporcuDbContext cnt = new RaporcuDbContext())
                {
                    _loginUser = cnt.Users.FirstOrDefault(a => a.UserName == userName && a.Password == password);
                }

                if (_loginUser == null)
                {
                    return false;
                }

                SetUserSession();

                #region Save Register

                RegistryKey rk = Registry.CurrentUser.OpenSubKey("Software", true);
                RegistrySecurity rs = new RegistrySecurity();
                string user = Environment.UserDomainName + "\\" + Environment.UserName;

                rs.AddAccessRule(new RegistryAccessRule(user,
                    RegistryRights.WriteKey | RegistryRights.ReadKey | RegistryRights.Delete,
                    InheritanceFlags.None,
                    PropagationFlags.None,
                    AccessControlType.Allow));

                rk = rk.CreateSubKey("Raporcu", RegistryKeyPermissionCheck.Default, rs);
                // Write encrypted string, initialization vector and key to the registry
                rk.SetValue("UserName", userName);
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                
            }
            return false;
        }

        public static Guid SetUserSession()
        {
            //RemoveTimeoutedUserSession();

            using (DataAccess.RaporcuDbContext cnt = new RaporcuDbContext())
            {
                _session = new Session() { SessionId = Guid.NewGuid(), HostName = System.Environment.MachineName, LoginName = _loginUser.UserName,  LoginTime = DateTime.Now , LastActivityTime = DateTime.Now };
                cnt.Sessions.Add(_session);
                cnt.SaveChanges();

                return _session.SessionId;
            }
        }

        public static bool RefreshUserSession()
        {
            bool ret = false;
            using (DataAccess.RaporcuDbContext cnt = new RaporcuDbContext())
            {
                Session session = cnt.Sessions.FirstOrDefault(a => a.SessionId == _session.SessionId);
                if (session != null)
                {
                    session.LastActivityTime = DateTime.Now;
                    cnt.Entry(session).State = EntityState.Modified;
                    cnt.SaveChanges();

                    ret = true;
                }
            }

            RemoveTimeoutedUserSession();

            return ret;
        }

        public static void RemoveUserSession()
        {
            using (DataAccess.RaporcuDbContext cnt = new RaporcuDbContext())
            {
               Session session =  cnt.Sessions.FirstOrDefault(a => a.SessionId == _session.SessionId);
                if (session != null)    {
                    cnt.Sessions.Remove(session);
                    cnt.SaveChanges();
                }
            }
        }

        public static void RemoveTimeoutedUserSession()
        {
            using (DataAccess.RaporcuDbContext cnt = new RaporcuDbContext())
            {
                List<Session> sessions = cnt.Sessions.Where(a => a.SessionId != _session.SessionId && a.LastActivityTime <  DbFunctions.AddMinutes(DateTime.Now, -6)).ToList();
                if (sessions != null && sessions.Count > 0)
                {
                    cnt.Sessions.RemoveRange(sessions);
                    cnt.SaveChanges();
                }
            }
        }

        public static bool CheckUserSession()
        {
            //RemoveTimeoutedUserSession();

            using (DataAccess.RaporcuDbContext cnt = new RaporcuDbContext())
            {
                List<Session> sessions = cnt.Sessions.ToList();
                if (sessions.Count < 6)
                {
                    return true;
                }
            }
            return false;
        }

        public static string GetUserName()
        {
            return _loginUser.UserName;
        }
        public static int GetUserId()
        {
            return _loginUser.UserId;
        }

        private static UserInfo _instance { get; set; }
        public User User { get; set; }
        public Session Session { get; set; }
        public string UserName { get; private set; }
    }
}