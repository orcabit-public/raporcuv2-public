﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DevExpress.Data.Linq;
using DevExpress.Web;
using RaporcuV2.DataAccess;

namespace RaporcuV2.WebApp
{
    public partial class Default : System.Web.UI.Page
    {
        private static string KeyExpression = "Id";
        protected ASPxGridView GridViewList = CustomControl.CreateGridView(KeyExpression);
        protected ASPxGridViewExporter Exporter = CustomControl.CreateExporter();

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */

        }
        protected void Page_Load(object sender, EventArgs e)
        {


            if (IsCallback)
            {
                Thread.Sleep(500);
            }
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("Login.aspx");
            }
            //else
            //{
            //    CreatePageContent();
            //}
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "UpdateGridHeight()", true);
        }

        
    }
}