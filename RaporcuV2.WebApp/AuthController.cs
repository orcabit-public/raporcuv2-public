﻿using RaporcuV2.Model.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Newtonsoft.Json;
using RaporcuV2.DataAccess;

namespace RaporcuV2.WebApp
{
    public class AuthController
    {
        public static bool Login(string userName, string passWord)
        {
            //todo:Buraya EF ile tablodan sorgulayıp kontrol kısmı gelecek geri true döenecek doğruysa değilse false, eğer kullanıcı geçerli ise UserInfo set edilmeli cookie set edilmeli
            RaporcuDbContext cnt = new RaporcuDbContext();

            if (!cnt.Users.Any(a => a.UserName == userName && a.Password == passWord))
            {
                return false;
            }
            User user = cnt.Users.FirstOrDefault(a => a.UserName == userName && a.Password == passWord);
            //böylece User.Identity.IsAuthenticated true olacaktır ayrıca Kullanıcı bilgisini de okuyabileceğiz

            string customerSerializeObject = JsonConvert.SerializeObject(user);
            string userSerializeObject = JsonConvert.SerializeObject(user);

            FormsAuthenticationTicket authenticationTicket = new FormsAuthenticationTicket(1, customerSerializeObject,
                DateTime.Now, DateTime.Now.AddMinutes(3), false, userSerializeObject);
            string cookieString = FormsAuthentication.Encrypt(authenticationTicket);
            HttpCookie httpCookie = new HttpCookie(FormsAuthentication.FormsCookieName, cookieString);
            httpCookie.Expires = authenticationTicket.Expiration;
            httpCookie.Path = FormsAuthentication.FormsCookiePath;
            FormsAuthentication.SetAuthCookie(customerSerializeObject, false);

            return true;
        }
    }
}