﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using MenuItem = DevExpress.Web.MenuItem;

namespace RaporcuV2.WebApp
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CreateMenus();
            }
        }

        #region Methots

        protected void CreateMenus()
        {
            //TodayCurrency tc = new TodayCurrency();
            //var currencyUsdBuy = tc.Currency(TodayCurrency.CurrencyCode.USD, TodayCurrency.CurrencyType.BanknoteBuying);
            //var currencyUsdSell = tc.Currency(TodayCurrency.CurrencyCode.USD, TodayCurrency.CurrencyType.BanknoteSelling);
            HtmlForm pageForm = this.Page.Form;
            #region Label

            #region layoutItemUsdBuy

            LayoutItem layoutItemUsdBuy = new LayoutItem()
            {
                Caption = "Usd Alış",
                FieldName = "Usd",
                LayoutItemNestedControlCollection =
                {
                    new LayoutItemNestedControlContainer()
                    {
                        Controls =
                        {
                            new ASPxLabel()
                            {
                                //Text = currencyUsdBuy
                            },
                        }
                    }
                }
            };

            #endregion

            #region layoutItemUsdSell

            LayoutItem layoutItemUsdSell = new LayoutItem()
            {
                Caption = "Usd Satış",
                FieldName = "Usd",
                LayoutItemNestedControlCollection =
                {
                    new LayoutItemNestedControlContainer()
                    {
                        Controls =
                        {
                            new ASPxLabel()
                            {
                                //Text = currencyUsdSell
                            },
                        }
                    }
                }
            };

            #endregion

            #region Layout Group

            LayoutGroup layoutGroup = new LayoutGroup()
            {
                ColCount = 2,
                ShowCaption = DefaultBoolean.False,
                Items =
                {
                    layoutItemUsdBuy,
                    layoutItemUsdSell
                }
            };

            #endregion

            #region Layout

            ASPxFormLayout layoutCurrency = new ASPxFormLayout()
            {
                ID = "layout",
                Items =
                {
                    layoutGroup
                }
            };
            #endregion

            ASPxPanel currencyPanel = new ASPxPanel()
            {
                Controls =
                {
                    layoutCurrency
                }
            };


            #endregion

            #region Menu
            ASPxMenu menu = new ASPxMenu()
            {
                ClientIDMode = ClientIDMode.AutoID,

            };
            List<MenuItem> items = new List<MenuItem>();

            items.Add(new MenuItem()
            {
                Index = 0,
                Image =
                {
                    IconID = DevExpress.Web.ASPxThemes.IconID.SalesSalesanalysis32x32devav
                },
                Text = "Test",
                NavigateUrl = "~/Test.aspx"
            });
            items.Add(new MenuItem()
            {
                Index = 1,
                Image =
                {
                    IconID = DevExpress.Web.ASPxThemes.IconID.SalesSalesanalysis32x32devav
                },
                Text = "Test1",
                NavigateUrl = "~/Test1.aspx"
            });
            items.Add(new MenuItem()
            {
                Index = 1,
                Image =
                {
                    IconID = DevExpress.Web.ASPxThemes.IconID.BusinessobjectsBoPivotchartSvg32x32
                },
                Text = "Test2",
                NavigateUrl = "~/Test2.aspx"
            });
            //items.Add(new MenuItem()
            //{
            //    Index = 1,
            //    Image =
            //    {
            //        IconID  = DevExpress.Web.ASPxThemes.IconID.SalesSalesperiodyear32x32devav
            //    },
            //    Text = "Satış Elemanı Toplamları",
            //    NavigateUrl = "~/SalesmanTotal.aspx"
            //});
            //items.Add(new MenuItem()
            //{
            //    Index = 2,
            //    Image =
            //    {
            //        IconID  = DevExpress.Web.ASPxThemes.IconID.SalesSalesperiodmonth32x32devav
            //    },
            //    Text = "Satış / Müşteri Temsilcisi",
            //    NavigateUrl = "~/Salesman.aspx"
            //});
            //items.Add(new MenuItem()
            //{
            //    Index = 3,
            //    Image =
            //    {
            //        IconID  = DevExpress.Web.ASPxThemes.IconID.SalesSalesperiodlifetime32x32devav
            //    },
            //    Text = "Rezervede Bekleyen Ürünler",
            //    NavigateUrl = "~/ReservedWaitingProduct.aspx"
            //});


            menu.Items.AddRange(items);


            menu.ItemClick += Menu_ItemClick;

            ASPxPanel TopPanel = new ASPxPanel()
            {
                ID = "TopPanel",
                ClientIDMode = ClientIDMode.AutoID,
                FixedPosition = PanelFixedPosition.WindowTop,
                FixedPositionOverlap = true,
                Controls =
                {
                    menu,
                    //currencyPanel
                }
            };

            form1.Controls.Add(TopPanel);
            #endregion
        }

        private void Menu_ItemClick(object source, MenuItemEventArgs e)
        {
            var menu = source as ASPxMenu;
            if (e.Item.Index == 0)
            {
                Response.Redirect("~/TopSales.aspx");
            }
            else if (e.Item.Index == 1)
            {
                //Response.Redirect("~/OfferList.aspx", true);
            }
        }

        #endregion
    }
}