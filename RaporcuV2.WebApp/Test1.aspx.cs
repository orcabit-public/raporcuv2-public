﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DevExpress.Data.Linq;
using DevExpress.Web;
using RaporcuV2.DataAccess;

namespace RaporcuV2.WebApp
{
    public partial class Test1 : System.Web.UI.Page
    {
        private static string KeyExpression = "LOGICALREF";
        protected ASPxGridView GridViewList = CustomControl.CreateGridView(KeyExpression);
        protected ASPxGridViewExporter Exporter = CustomControl.CreateExporter();
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsCallback)
            {
                Thread.Sleep(500);
            }
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                CreatePageContent();
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "UpdateGridHeight()", true);
        }


        protected void CreatePageContent()
        {
            HtmlForm pageForm = this.Page.Form;



            GridViewList.SettingsResizing.ColumnResizeMode = ColumnResizeMode.Control; //(ColumnResizeMode)Enum.Parse(typeof(ColumnResizeMode), "NextColumn", true);
            GridViewList.SettingsResizing.Visualization = ResizingMode.Live; //  (ResizingMode)Enum.Parse(typeof(ResizingMode), "Live", true);
            GridViewList.Settings.HorizontalScrollBarMode = ScrollBarMode.Visible;
            GridViewList.SettingsBehavior.AllowEllipsisInText = true;

            GridViewList.Settings.ShowGroupPanel = true;
            GridViewList.SettingsContextMenu.Enabled = true;
            GridViewList.SettingsContextMenu.Enabled = true;
            GridViewList.SettingsContextMenu.RowMenuItemVisibility.ExportMenu.Visible = true;
            GridViewList.SettingsContextMenu.RowMenuItemVisibility.NewRow = false;
            GridViewList.SettingsContextMenu.RowMenuItemVisibility.EditRow = false;
            GridViewList.SettingsContextMenu.RowMenuItemVisibility.DeleteRow = false;

            GridViewList.SettingsLoadingPanel.Mode = GridViewLoadingPanelMode.ShowOnStatusBar;
            GridViewList.SettingsBehavior.EnableCustomizationWindow = true;

            GridViewList.SettingsExport.EnableClientSideExportAPI = true;


            GridViewList.SettingsPager.PageSize = 100;
            GridViewList.SettingsPager.Mode = GridViewPagerMode.EndlessPaging;
            GridViewList.SettingsPager.EnableAdaptivity = true;

            //         GridViewList.Height = new Unit("100%");
            GridViewList.SettingsAdaptivity.AdaptivityMode = GridViewAdaptivityMode.Off;
            EntityServerModeSource data = CreateData();
            GridViewList.DataSource = data;
            GridViewList.DataBind();

            #region Grid Toolbar

            GridViewToolbar toolbar = CustomControl.CreateToolbar();

            toolbar.Items.ForEach(item =>
            {
                if (item.Name == "btnNew" || item.Name == "btnEdit" || item.Index == 3)
                {
                    item.Visible = false;
                }
            });
            ASPxComboBox comboBox = new ASPxComboBox();
            GridViewDataItemTemplateContainer container = comboBox.NamingContainer as GridViewDataItemTemplateContainer;

            GridViewList.Toolbars.Add(toolbar);


            #endregion

            GridViewList.ToolbarItemClick += GridView_ToolbarItemClick;
            pageForm.Controls.Add(GridViewList);
            pageForm.Controls.Add(Exporter);
        }

        private void GridView_ToolbarItemClick(object source,
            DevExpress.Web.Data.ASPxGridViewToolbarItemClickEventArgs e)
        {
            if (e.Item.Name.StartsWith("Export"))
            {
                switch (e.Item.Name)
                {
                    case "ExportToPDF":
                        Exporter.WritePdfToResponse();
                        break;
                    case "ExportToXLS":
                        Exporter.WriteXlsToResponse();
                        break;
                    case "ExportToXLSX":
                        Exporter.WriteXlsxToResponse();
                        break;
                    default:
                        break;
                }
            }
            else if (e.Item.Name == "btnNew")
            {
                string[] fileds = { "LOGICALREF", "LOGICALREF" };
                object[] list = GridViewList.GetRowValues(GridViewList.FocusedRowIndex, fileds) as object[];

                foreach (object value in list)
                {
                    // OpenEditForm(value.ToString());
                    break;
                }
            }
        }

        protected EntityServerModeSource CreateData()
        {
            string sql = @"SELECT L.LOGICALREF,
CASE L.TRCODE
    WHEN 8 THEN 'TOPTAN SATIŞ FATURASI'
    WHEN 7 THEN 'PERAKENDE SATIŞ FATURASI'
    WHEN 3 THEN 'TOPTAN SATIŞ İADE FATURASI'
    WHEN 1 THEN 'ALIM FATURASI'
    WHEN 6 THEN 'ALIM İADE FATURASI'
    WHEN 4 THEN 'ALINAN HİZMET FATURASI'
    WHEN 9 THEN 'VERİLEN HİZMET FATURASI'
    WHEN 13 THEN 'SATINALMA FİYAT FARKI'
    WHEN 14 THEN 'SATIŞ FİYAT FARKI'
    END AS FATTURU,
        CASE L.LINETYPE WHEN 0 THEN M.STGRPCODE ELSE (SELECT DEFINITION_ FROM SMPLOGO.DBO.LG_002_SRVCARD WHERE LOGICALREF=(SELECT PARENTSRVREF FROM SMPLOGO.DBO.LG_002_SRVCARD WHERE LOGICALREF=S.LOGICALREF)) END  AS GRUPKODU,
L.DATE_ AS TARIH,MONTH(L.DATE_) AS AY,DATEPART( wk, L.DATE_)  AS HAFTA, I.FICHENO AS FISNO,C.CODE AS KOD,C.DEFINITION_ AS CARIHESAP,CASE L.LINETYPE WHEN 0 THEN '(M)' WHEN 4 THEN '(H)' ELSE 'TANIMSIZ' END AS LINETYPE,
CASE L.LINETYPE WHEN 0 THEN M.CODE ELSE S.CODE END AS SHKODU,
CASE L.LINETYPE WHEN 0 THEN M.NAME ELSE S.DEFINITION_ END AS SHADI ,


CASE L.TRCODE
WHEN 3 THEN (CASE L.UINFO2 WHEN 0 THEN L.AMOUNT ELSE L.AMOUNT END)*-1
WHEN 6 THEN (CASE L.UINFO2 WHEN 0 THEN L.AMOUNT ELSE L.AMOUNT END)*-1
ELSE (CASE L.UINFO2 WHEN 0 THEN L.AMOUNT ELSE L.AMOUNT END)
END AS MIKTAR,



U.CODE AS BRMKODU,
CASE L.TRCODE
WHEN 3 THEN L.PRICE*-1
WHEN 6 THEN L.PRICE*-1
ELSE L.PRICE END AS FIYAT,
ISNULL(D.DISCPER,0) AS INDIRIM,
CASE L.TRCODE
WHEN 3 THEN L.VATMATRAH*-1
WHEN 6 THEN L.VATMATRAH*-1
ELSE L.VATMATRAH END AS KDVMATRAH,

 L.VAT  AS KDV,
CASE L.TRCODE
WHEN 3 THEN L.VATAMNT*-1
WHEN 6 THEN L.VATAMNT*-1
ELSE L.VATAMNT END AS KDVTUTARI,
CASE L.TRCODE
WHEN 3 THEN ISNULL(I.TOTALDISCOUNTED,0)*-1
WHEN 6 THEN ISNULL(I.TOTALDISCOUNTED,0)*-1
ELSE ISNULL(I.TOTALDISCOUNTED,0) END AS TOTALDISCOUNTED ,
CASE L.TRCODE
WHEN 3 THEN ISNULL(I.NETTOTAL,0)*-1
WHEN 6 THEN ISNULL(I.NETTOTAL,0)*-1
ELSE ISNULL(I.NETTOTAL,0) END AS NETTOPLAM,
CASE L.TRCURR WHEN 0 THEN 'TL' WHEN 1 THEN 'USD' WHEN 20 THEN 'EUR' ELSE 'TANIMSIZ' END AS PB, CASE L.TRRATE WHEN 0 THEN 1 ELSE L.TRRATE END AS KUR,

CASE I.TRCODE
WHEN 14 THEN (CASE
WHEN I.DECPRDIFF=1 THEN 'AZALTILACAK'
WHEN I.DECPRDIFF=0 THEN 'ARTIRILACAK' END) else '' END 'STOKTUTARI',
CASE I.TRCODE
WHEN 14 THEN (CASE
WHEN I.INEFFECTIVECOST=1 THEN 'ETKİLENMEYECEK'
WHEN I.INEFFECTIVECOST=0 THEN 'ETKİLENECEK' END)
else '' END 'STOKMALIYETI'
 FROM SMPLOGO.DBO.LG_002_01_STLINE L
 LEFT JOIN SMPLOGO.DBO.LG_002_01_STLINE D ON D.PARENTLNREF = L.LOGICALREF
LEFT JOIN SMPLOGO.DBO.LG_002_01_INVOICE I ON I.LOGICALREF = L.INVOICEREF
LEFT JOIN SMPLOGO.DBO.LG_002_CLCARD C ON C.LOGICALREF = L.CLIENTREF
LEFT JOIN SMPLOGO.DBO.LG_002_ITEMS M ON M.LOGICALREF = L.STOCKREF
LEFT JOIN SMPLOGO.DBO.LG_002_UNITSETL U ON U.LOGICALREF = L.UOMREF
LEFT JOIN SMPLOGO.DBO.LG_002_SRVCARD S ON S.LOGICALREF = L.STOCKREF
WHERE I.CANCELLED=0 AND L.LINETYPE IN (0,4) AND L.TRCODE NOT IN (12)
ORDER BY L.DATE_
";

            RaporcuDbContext cnt = new RaporcuDbContext();
            EntityServerModeSource source = new EntityServerModeSource()
            {
                QueryableSource = cnt.Database.SqlQuery<Model.Fatura.FaturaDetay>(sql).AsQueryable(),
                KeyExpression = KeyExpression
            };
            return source;
        }
    }
}