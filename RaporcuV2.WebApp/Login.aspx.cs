﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;

namespace RaporcuV2.WebApp
{
    public partial class Login : System.Web.UI.Page
    {
        #region Declaretions

        protected ASPxFormLayout Layout;

        #region Submit Button

        protected ASPxButton SubmitButton = new ASPxButton()
        {
            ID = "submitButton",
            Width = 100,
            Text = "Giriş",
            Image = { IconID = DevExpress.Web.ASPxThemes.IconID.ToolboxitemsCheckbox216x16 },
            ImagePosition = ImagePosition.Left,
            ImageSpacing = 10,
            AutoPostBack = true
        };


        #endregion

        #region TextBox Username

        protected ASPxTextBox AsPxTextBoxUserName = new ASPxTextBox()
        {
            ID = "userName",
            Width = 200,
        };

        #endregion

        #region TextBox Password

        protected ASPxTextBox AsPxTextBoxPassword = new ASPxTextBox()
        {
            ID = "password",
            Width = 200,
            Password = true,

        };

        #endregion

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            CreateLogin();
        }

        #region Methots

        #region Create Login

        protected void CreateLogin()
        {
            SubmitButton.Click += SubmitButton_Click;

            ScriptManager scriptManager = new ScriptManager();
            theBody.Style["background-image"] = GetRandomFile("");
            theBody.Style["background-size"] = "100%";

            #region User Name

            LayoutItem layoutItemUserName = new LayoutItem()
            {
                Caption = RaporcuV2.Util.Labels.userUserName, // Twoth.Bridal.Utils.Labels.userUserName,
                FieldName = "UserName",
                LayoutItemNestedControlCollection =
                {
                    new LayoutItemNestedControlContainer()
                    {
                        Controls =
                        {
                            AsPxTextBoxUserName
                        },
                    }
                }
            };

            #endregion

            #region Password

            LayoutItem layoutItemPassword = new LayoutItem()
            {
                Caption = RaporcuV2.Util.Labels.userPassword,
                FieldName = "Password",
                LayoutItemNestedControlCollection =
                {
                    new LayoutItemNestedControlContainer()
                    {
                        Controls =
                        {
                            AsPxTextBoxPassword
                        }
                    }
                }
            };

            #endregion

            #region Submit Button

            LayoutItem layoutItemSubmitButton = new LayoutItem()
            {
                ShowCaption = DefaultBoolean.False,
                HorizontalAlign = FormLayoutHorizontalAlign.Right,
                Width = 100,
                LayoutItemNestedControlCollection =
                {
                    new LayoutItemNestedControlContainer()
                    {
                        Controls =
                        {
                            SubmitButton,
                        }
                    }
                }
            };

            #endregion

            #region Layout Group

            LayoutGroup layoutGroup = new LayoutGroup()
            {
                ShowCaption = DefaultBoolean.False,
                Items =
                {

                    layoutItemUserName,
                    layoutItemPassword,
                    layoutItemSubmitButton,
                }
            };

            #endregion

            #region Layout

            Layout = new ASPxFormLayout()
            {
                ID = "layout",
                Items =
                {
                    layoutGroup
                }
            };
            #endregion

            #region Panel

            ASPxPanel panel = new ASPxPanel()
            {
                ID = "loginPanel",
                Controls =
                {
                    Layout
                }
            };

            #endregion

            #region Popup Control


            ASPxPopupControl popupControl = new ASPxPopupControl()
            {

                ID = "pcLogin",
                ShowCloseButton = false,
                CloseAction = CloseAction.None,
                ShowFooter = false,
                CloseOnEscape = false,
                Modal = false,
                PopupHorizontalAlign = PopupHorizontalAlign.WindowCenter,
                PopupVerticalAlign = PopupVerticalAlign.WindowCenter,
                ClientInstanceName = "pcLogin",
                HeaderText = "Login",
                AllowDragging = true,
                PopupAnimationType = AnimationType.None,
                EnableViewState = false,
                ShowOnPageLoad = true,
                Controls =
                {
                    panel
                }


            };

            #endregion

            form1.Controls.Add(popupControl);
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            UserLogin(AsPxTextBoxUserName.Text, AsPxTextBoxPassword.Text);
        }

        #endregion

        #region Login

        public void UserLogin(string userName, string passWord)
        {
            bool login = AuthController.Login(userName, passWord);
            if (login)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {


            }
        }

        #endregion

        #region Random Image

        private string GetRandomFile(string path)
        {
            path = Server.MapPath("~/resources/wallpapers");
            string file = null;
            if (!string.IsNullOrEmpty(path))
            {
                var extensions = new string[] { ".png", ".jpg", ".gif" };
                try
                {
                    var di = new DirectoryInfo(path);
                    var rgFiles = di.GetFiles("*.*").Where(f => extensions.Contains(f.Extension.ToLower()));
                    Random R = new Random();
                    file = rgFiles.ElementAt(R.Next(0, rgFiles.Count())).Name;
                }
                // probably should only catch specific exceptions
                // throwable by the above methods.
                catch (Exception ex)
                {
                    string msg = ex.Message;
                }
            }
            return "resources/wallpapers/" + file;
        }

        #endregion


        #endregion
    }
}