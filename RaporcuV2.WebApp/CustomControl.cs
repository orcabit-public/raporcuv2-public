﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using DevExpress.Web.ASPxPivotGrid;
using DevExpress.Web.ASPxSpreadsheet;
using DevExpress.Web.ASPxThemes;
using DevExpress.XtraCharts.Web;

namespace RaporcuV2.WebApp
{
    internal class CustomControl
    {

        public static ASPxSpreadsheet CreateSpreadsheet()
        {
            ASPxSpreadsheet spreadsheet = new ASPxSpreadsheet
            {
                //SettingsView = {Mode = SpreadsheetViewMode.Reading},
                RibbonMode = SpreadsheetRibbonMode.Ribbon,
                ShowFormulaBar = true,
                WorkDirectory = "~/",
            };
            
            spreadsheet.CreateDefaultRibbonTabs(true);
            //spreadsheet.FullscreenMode = true;
            spreadsheet.Style.Add(HtmlTextWriterStyle.Height, "100%");
            spreadsheet.Style.Add(HtmlTextWriterStyle.Width, "100%");
            
            RibbonTab readingViewTab = spreadsheet.RibbonTabs.Find(tab => tab is SRReadingViewTab);
            if (readingViewTab != null)
                readingViewTab.Groups[0].Items.Add(new SRFullScreenCommand());

            return spreadsheet;


        }

        public static ASPxGridView CreateGridView(string keyField)
        {


            ASPxGridView grid = new ASPxGridView()
            {
                ID = "GridViewList",
                ClientInstanceName = "GridViewList",
                ClientIDMode = ClientIDMode.AutoID,
                KeyFieldName = keyField,

                Width = Unit.Percentage(100),
                Height = Unit.Percentage(100),
                AutoGenerateColumns = true,
                SettingsBehavior =
                {
                    AllowFocusedRow = true
                },

            };

            grid.ClientSideEvents.ToolbarItemClick = @" function OnToolbarItemClick(s,e) {
                                                                    if(e.item.name.startsWith('ExportTo')){
                                                                        e.processOnServer=true;
                                                                        e.usePostBack=true;
                                                                    }
                                                                    else if(e.item.name.startsWith('btn')){
                                                                        e.processOnServer=true;
                                                                        e.usePostBack=true;
                                                                    }
                                                                }";
            return grid;
        }

        public static ASPxGridViewExporter CreateExporter()
        {
            return new ASPxGridViewExporter()
            {
                ID = "Exporter",
                GridViewID = "GridViewList"
            };
        }

        public static ASPxButton CreateASPxButton()
        {
            return new ASPxButton();
        }

        public static GridViewToolbar CreateToolbar()
        {
            GridViewToolbar gridViewToolbar = new GridViewToolbar()
            {
                Index = 1,
                Items =
                {
                    new GridViewToolbarItem()
                    {

                        //Command = GridViewToolbarCommand.New
                        Text = "Yeni",
                        Name = "btnNew",
                        Image =
                        {
                            IconID = IconID.ActionsNew16x16
                        }
                    },
                    new GridViewToolbarItem()
                    {
                        //Command = GridViewToolbarCommand.New
                        Text = "Değiştir",
                        Name = "btnEdit",
                        Image =
                        {
                            IconID = IconID.ActionsEdit16x16devav
                        }
                    },
                    new GridViewToolbarItem()
                    {
                        Text = "Arama",
                        Command = GridViewToolbarCommand.ShowSearchPanel
                    },
                    new GridViewToolbarItem()
                    {
                        Command = GridViewToolbarCommand.Delete
                    },
                    new GridViewToolbarItem()
                    {
                        Command = GridViewToolbarCommand.Refresh,
                        BeginGroup = true
                    },
                    new GridViewToolbarItem()
                    {
                        Text = "Export",
                        Image =
                        {
                            IconID = DevExpress.Web.ASPxThemes.IconID.ActionsDownload16x16office2013
                        },
                        Command = GridViewToolbarCommand.Refresh,
                        BeginGroup = true,
                        Items =
                        {
                            new GridViewToolbarItem
                            {
                                Name = "ExportToPDF",
                                Text = "PDF" ,
                                Image =
                                {
                                    IconID = IconID.ExportExporttopdf16x16office2013
                                },

                            },
                            new GridViewToolbarItem
                            {
                                Name = "ExportToXLSX",
                                Text = "XLSX" ,
                                Image =
                                {
                                    IconID = IconID.ExportExporttoxlsx16x16office2013
                                }
                            },
                            new GridViewToolbarItem
                            {
                                Name = "ExportToXLS",
                                Text = "XLS" ,
                                Image =
                                {
                                    IconID = IconID.ExportExporttoxls16x16office2013
                                }
                            },
                        }
                    },
                }
            };
            return gridViewToolbar;
        }


        public static ASPxPivotGrid CreatePivotGrid(string keyField)
        {


            ASPxPivotGrid grid = new ASPxPivotGrid()
            {
                ID = "GridViewList",
                ClientInstanceName = "GridViewList",
                ClientIDMode = ClientIDMode.AutoID,
                //KeyFieldName = keyField,

                Width = Unit.Percentage(100),
                Height = Unit.Percentage(100),
                //AutoGenerateColumns = true,
               
            };

            //grid.ClientSideEvents.ToolbarItemClick = @" function OnToolbarItemClick(s,e) {
            //                                                        if(e.item.name.startsWith('ExportTo')){
            //                                                            e.processOnServer=true;
            //                                                            e.usePostBack=true;
            //                                                        }
            //                                                        else if(e.item.name.startsWith('btn')){
            //                                                            e.processOnServer=true;
            //                                                            e.usePostBack=true;
            //                                                        }
            //                                                    }";
            return grid;
        }


        public static WebChartControl CreateWebChartControl()
        {


            WebChartControl chart = new WebChartControl()
            {
                ID = "pivotChartId",
                ClientInstanceName = "pivotChart",
                ClientIDMode = ClientIDMode.AutoID,
                //KeyFieldName = keyField,

                Width = Unit.Pixel(500),
                Height = Unit.Pixel(500),
                //AutoGenerateColumns = true,

            };

            //grid.ClientSideEvents.ToolbarItemClick = @" function OnToolbarItemClick(s,e) {
            //                                                        if(e.item.name.startsWith('ExportTo')){
            //                                                            e.processOnServer=true;
            //                                                            e.usePostBack=true;
            //                                                        }
            //                                                        else if(e.item.name.startsWith('btn')){
            //                                                            e.processOnServer=true;
            //                                                            e.usePostBack=true;
            //                                                        }
            //                                                    }";
            return chart;
        }

        public static UpdatePanel CreateUpdatePanel()
        {
            UpdatePanel panel = new UpdatePanel()
            {
                ID = "updatePanel1",
                ClientIDMode = ClientIDMode.AutoID,
                
            };
            return panel;
        }

        public static ScriptManager CreateScriptManager()
        {
            ScriptManager scriptManager = new ScriptManager()
            {
                ID = "scriptManager1",
                ClientIDMode = ClientIDMode.AutoID,

            };
            return scriptManager;
        }


    }
}