﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing.Printing;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DevExpress.Data.Linq;
using DevExpress.Data.Linq.Helpers;
using DevExpress.Spreadsheet;
using DevExpress.Web;
using DevExpress.Web.ASPxSpreadsheet;
using RaporcuV2.DataAccess;
using RaporcuV2.Model;

namespace RaporcuV2.WebApp
{
    public partial class Test : System.Web.UI.Page
    {
        private static string KeyExpression = "LOGICALREF";
        protected ASPxGridView GridViewList = CustomControl.CreateGridView(KeyExpression);
        protected ASPxGridViewExporter Exporter = CustomControl.CreateExporter();
        protected ASPxSpreadsheet Spreadsheet = CustomControl.CreateSpreadsheet();
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsCallback)
            {
                Thread.Sleep(500);
            }
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                CreatePageExcelContent();
                //CreatePageContent();
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "UpdateGridHeight()", true);

        }

        #region Create Excel 

        protected void CreatePageExcelContent()
        {
            HtmlForm pageForm = this.Page.Form;
            ExcelPrepare();
            pageForm.Controls.Add(Spreadsheet);
        }
        protected List<Model.Fatura.FaturaDetay> GetData()
        {
            string sql = @"SELECT
CASE L.TRCODE
    WHEN 8 THEN 'TOPTAN SATIŞ FATURASI'
    WHEN 7 THEN 'PERAKENDE SATIŞ FATURASI'
    WHEN 3 THEN 'TOPTAN SATIŞ İADE FATURASI'
    WHEN 1 THEN 'ALIM FATURASI'
    WHEN 6 THEN 'ALIM İADE FATURASI'
    WHEN 4 THEN 'ALINAN HİZMET FATURASI'
    WHEN 9 THEN 'VERİLEN HİZMET FATURASI'
    WHEN 13 THEN 'SATINALMA FİYAT FARKI'
    WHEN 14 THEN 'SATIŞ FİYAT FARKI'
    END AS FATTURU,
        CASE L.LINETYPE WHEN 0 THEN M.STGRPCODE ELSE (SELECT DEFINITION_ FROM SMPLOGO.DBO.LG_002_SRVCARD WHERE LOGICALREF=(SELECT PARENTSRVREF FROM SMPLOGO.DBO.LG_002_SRVCARD WHERE LOGICALREF=S.LOGICALREF)) END  AS GRUPKODU,
L.DATE_ AS TARIH,MONTH(L.DATE_) AS AY,DATEPART( wk, L.DATE_)  AS HAFTA, I.FICHENO AS FISNO,C.CODE AS KOD,C.DEFINITION_ AS CARIHESAP,CASE L.LINETYPE WHEN 0 THEN '(M)' WHEN 4 THEN '(H)' ELSE 'TANIMSIZ' END AS LINETYPE,
CASE L.LINETYPE WHEN 0 THEN M.CODE ELSE S.CODE END AS SHKODU,
CASE L.LINETYPE WHEN 0 THEN M.NAME ELSE S.DEFINITION_ END AS SHADI ,


CASE L.TRCODE
WHEN 3 THEN (CASE L.UINFO2 WHEN 0 THEN L.AMOUNT ELSE L.AMOUNT END)*-1
WHEN 6 THEN (CASE L.UINFO2 WHEN 0 THEN L.AMOUNT ELSE L.AMOUNT END)*-1
ELSE (CASE L.UINFO2 WHEN 0 THEN L.AMOUNT ELSE L.AMOUNT END)
END AS MIKTAR,



U.CODE AS BRMKODU,
CASE L.TRCODE
WHEN 3 THEN L.PRICE*-1
WHEN 6 THEN L.PRICE*-1
ELSE L.PRICE END AS FIYAT,
ISNULL(D.DISCPER,0) AS INDIRIM,
CASE L.TRCODE
WHEN 3 THEN L.VATMATRAH*-1
WHEN 6 THEN L.VATMATRAH*-1
ELSE L.VATMATRAH END AS KDVMATRAH,

 L.VAT  AS KDV,
CASE L.TRCODE
WHEN 3 THEN L.VATAMNT*-1
WHEN 6 THEN L.VATAMNT*-1
ELSE L.VATAMNT END AS KDVTUTARI,
CASE L.TRCODE
WHEN 3 THEN ISNULL(I.TOTALDISCOUNTED,0)*-1
WHEN 6 THEN ISNULL(I.TOTALDISCOUNTED,0)*-1
ELSE ISNULL(I.TOTALDISCOUNTED,0) END AS TOTALDISCOUNTED ,
CASE L.TRCODE
WHEN 3 THEN ISNULL(I.NETTOTAL,0)*-1
WHEN 6 THEN ISNULL(I.NETTOTAL,0)*-1
ELSE ISNULL(I.NETTOTAL,0) END AS NETTOPLAM,
CASE L.TRCURR WHEN 0 THEN 'TL' WHEN 1 THEN 'USD' WHEN 20 THEN 'EUR' ELSE 'TANIMSIZ' END AS PB, CASE L.TRRATE WHEN 0 THEN 1 ELSE L.TRRATE END AS KUR,

CASE I.TRCODE
WHEN 14 THEN (CASE
WHEN I.DECPRDIFF=1 THEN 'AZALTILACAK'
WHEN I.DECPRDIFF=0 THEN 'ARTIRILACAK' END) else '' END 'STOKTUTARI',
CASE I.TRCODE
WHEN 14 THEN (CASE
WHEN I.INEFFECTIVECOST=1 THEN 'ETKİLENMEYECEK'
WHEN I.INEFFECTIVECOST=0 THEN 'ETKİLENECEK' END)
else '' END 'STOKMALIYETI'
 FROM SMPLOGO.DBO.LG_002_01_STLINE L
 LEFT JOIN SMPLOGO.DBO.LG_002_01_STLINE D ON D.PARENTLNREF = L.LOGICALREF
LEFT JOIN SMPLOGO.DBO.LG_002_01_INVOICE I ON I.LOGICALREF = L.INVOICEREF
LEFT JOIN SMPLOGO.DBO.LG_002_CLCARD C ON C.LOGICALREF = L.CLIENTREF
LEFT JOIN SMPLOGO.DBO.LG_002_ITEMS M ON M.LOGICALREF = L.STOCKREF
LEFT JOIN SMPLOGO.DBO.LG_002_UNITSETL U ON U.LOGICALREF = L.UOMREF
LEFT JOIN SMPLOGO.DBO.LG_002_SRVCARD S ON S.LOGICALREF = L.STOCKREF
WHERE I.CANCELLED=0 AND L.LINETYPE IN (0,4) AND L.TRCODE NOT IN (12)
ORDER BY L.DATE_
";

            RaporcuDbContext cnt = new RaporcuDbContext();
            return cnt.Database.SqlQuery<Model.Fatura.FaturaDetay>(sql).ToList();
        }
        #region Excel Prepare

        protected void ExcelPrepare()
        {
            BuiltInStyleId headerStyle = BuiltInStyleId.Accent1;
            BuiltInTableStyleId tableStyle = BuiltInTableStyleId.TableStyleMedium2;

            var data = GetData();

            int i = 0;
            IWorkbook document = Spreadsheet.Document;
            Worksheet ws = document.Worksheets[0];
            ws.ActiveView.PaperKind = PaperKind.A4;
            ws.ActiveView.Orientation = PageOrientation.Landscape;
            ws.ActiveView.Margins.Left = 0;
            ws.ActiveView.Margins.Top = 0;
            ws.ActiveView.Margins.Right = 0;
            ws.ActiveView.Margins.Bottom = 0;

            ws.Clear(ws.GetUsedRange());

            ws.PrintOptions.FitToPage = true;
            ws.PrintOptions.FitToWidth = 1;
            ws.PrintOptions.FitToHeight = 0;

            var range = ws.Range.FromLTRB(0, i, 2, i);

            ws.MergeCells(range);
            range.Value = string.Format("{0} Web Excel Raporu", DateTime.Now.ToString("dd MMMM yyyy , dddd"));
            range.Style = document.Styles[BuiltInStyleId.Heading1];

            #region Kasa Toplamları

            #region Grup Başlığı Renklendir

            i = 2;
            range = ws.Range.FromLTRB(0, i, 2, i);
            ws.MergeCells(range);
            range.Value = "WEB EXCEL FATURA DETAY RAPORLAMA";
            range.Style = document.Styles[headerStyle];
            i++;

            #endregion


            ////SqlText = SqlReports.AtaKasa;
            //_reportTag = RaporcuV2.Helper.ReportTags.AtaKasa;

            i = 3;
            range = ws.Range.FromLTRB(0, i, 2, i);
            ExternalDataSourceOptions dsOptions = new ExternalDataSourceOptions
            {
                ImportHeaders = true, SkipHiddenRows = true
            };

            document.MailMergeDataSource = data; 
            // (data, range, dsOptions);
            //string mode = rblMailMergeMode.Value.ToString();
            //document.DefinedNames.GetDefinedName("MAILMERGEMODE").RefersTo = string.Format("\"{0}\"","osman");
            //var source = ws.DataBindings.BindTableToDataSource(data, range,dsOptions);
            //source.Table.Style = document.TableStyles[tableStyle];
            //source.Table.AutoFilter.Disable();
            //source.Table.ShowTotals = true;

            //source.Table.Name = "table1";

            //TableColumnCollection columns = source.Table.Columns;

            //foreach (TableColumn column in columns)
            //{
            //    if (column.Index != 0)
            //    {
            //        column.TotalRowFunction = TotalRowFunction.Sum;
            //    }
            //}

            //Range oRng = source.Table.Range;
            //oRng.AutoFitColumns();

            #endregion
        }

        #endregion
        #endregion
    }
}