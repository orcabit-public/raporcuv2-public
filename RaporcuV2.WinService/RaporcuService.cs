﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Hangfire;
using Hangfire.SqlServer;
using NLog;

namespace RaporcuV2.WinService
{
    public partial class RaporcuService : ServiceBase
    {
        /**
 *     installutil %path%\Twoth.Bridal.WinService.exe
 * */
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Timer timer = null;
        private BackgroundJobServer _server;

        public RaporcuService()
        {
            InitializeComponent();
            InitDbConfig();
        }

        protected override void OnStart(string[] args)
        {
            _server = new BackgroundJobServer();
            RunService();
        }

        
        protected override void OnStop()
        {
            _server.Dispose();
            logger.Info("Servis Durduruldu.");
        }

        private void RunService()
        {
            

            timer = new Timer();
            this.timer.Interval = 10 * 1 * 1000;
            this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timerTick);
            timer.Enabled = true;
            logger.Info("Servis başlatıldı.");
            //CreateReport<TopSales>();
        }

        private void timerTick(Object sender, ElapsedEventArgs e)
        {
            this.timer.Elapsed -= new System.Timers.ElapsedEventHandler(this.timerTick);
            //CreateItemSlipPdf("bridaldb");
            //CreateItemSlipPdf("novasposadb");
            this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timerTick);
        }

        private void InitDbConfig()
        {
            SqlConnectionStringBuilder sqlString = new SqlConnectionStringBuilder()
            {

                DataSource = "app.orcabit.io", // Server name
                InitialCatalog = "OrcaReport", //DbName,  //Database
                UserID = "sa",         //Username
                Password = "Or5038ca",  //Password

            };
            string conString = sqlString.ConnectionString;
            logger.Info(conString);
            GlobalConfiguration.Configuration
                .UseSqlServerStorage(conString, new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                });
        }
    }
}
